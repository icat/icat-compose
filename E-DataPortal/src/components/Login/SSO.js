import React from 'react';
import UI from '../../config/ui';
import Loader from '../Loader';
import { ErrorUserMessage } from '../UserMessages/ErrorUserMessage';
import AnonymousSignIn from './AnonymousSignIn';
import keycloak from '../../keycloak';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Button, Alert } from 'react-bootstrap';
import { trackKeycloakSignIn } from '../../helpers/trackData';

function SSO() {
  const user = useSelector((state) => state.user);
  const location = useLocation();

  function handleClick() {
    const fromLocation = location.state?.from;
    trackKeycloakSignIn();
    if (fromLocation) {
      const { pathname, search, hash } = fromLocation;
      const redirectPath = `${pathname}${search}${hash}`;
      const redirectUri = `${window.location.origin}/login${
        redirectPath === '/' ? '' : `?to=${encodeURIComponent(redirectPath)}`
      }`;

      keycloak.login({ redirectUri });
    } else {
      keycloak.login();
    }
  }

  if (user.isAuthenticating) {
    return (
      <div style={{ margin: '1.5rem' }}>
        <Loader message="Authenticating..." inPanel />
      </div>
    );
  }

  return (
    <div style={{ padding: '30px 15px', textAlign: 'center' }}>
      {user.error && <ErrorUserMessage text={user.error} />}
      {user.isSessionExpired && (
        <Alert variant="warning">Session expired</Alert>
      )}

      <Button type="submit" variant="primary" size="lg" onClick={handleClick}>
        {UI.loginForm.ssoBtnLabel}
      </Button>

      <AnonymousSignIn />
    </div>
  );
}

export default SSO;
