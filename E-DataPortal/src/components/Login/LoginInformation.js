import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Alert, Stack } from 'react-bootstrap';
import UI from '../../config/ui';

function LoginInformation() {
  const getFeedbackURL = () => {
    return `mailto:${UI.feedback.email}`;
  };

  return (
    <Stack gap={3}>
      <div>
        Don't have an account yet?
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://smis.esrf.fr/misapps/SMISWebClient/accountManager/searchExistingAccount.do?action=search"
        >
          {' '}
          Register now
        </a>
      </div>
      <div>
        <h5>
          <FontAwesomeIcon icon={faEnvelope} style={{ marginRight: 10 }} />
          <a href={getFeedbackURL()}>I need further assistance</a>
        </h5>
      </div>
      <Alert variant="secondary">
        <Alert.Heading>Important note</Alert.Heading>
        <p>
          During 2019 and according to the General Data Protection Regulation,
          all portal users who did not consent to the{' '}
          <a
            href="http://www.esrf.fr/GDPR"
            rel="noopener noreferrer"
            target="_blank"
          >
            User Portal Privacy Statement
          </a>{' '}
          have had their account deactivated. Please contact the{' '}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="http://www.esrf.eu/UsersAndScience/UserGuide/Contacts"
          >
            User Office
          </a>{' '}
          if you wish to reactivate it.
        </p>
      </Alert>
    </Stack>
  );
}

export default LoginInformation;
