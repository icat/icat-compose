import {
  faBook,
  faCog,
  faCommentAlt,
  faList,
  faPlane,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef } from 'react';
import { Badge, Nav } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import UI from '../../config/ui';
import { setInvestigationBreadCrumbs } from '../../containers/investigation-breadcrumbs';
import { useQueryParams } from '../../helpers/hooks';

function TabContainerMenu(props) {
  const { doi, investigation } = props;
  const { isAnonymous, isAdministrator, isInstrumentScientist } = useSelector(
    (state) => state.user
  );

  const { experimental } = useQueryParams();

  const datasetCount = useSelector((state) => state.datasets.datasetsCount);
  const { investigationId } = useParams();

  const breadcrumbsList = useSelector((state) => state.breadcrumbsList);
  const currentBreadcrumbsList = useRef(); // work around stale breadcrumbsList reference
  currentBreadcrumbsList.current = breadcrumbsList;

  const dispatch = useDispatch();

  useEffect(() => {
    if (investigation) {
      dispatch(
        setInvestigationBreadCrumbs(
          investigation,
          currentBreadcrumbsList.current
        )
      );
    }
  }, [dispatch, investigation, currentBreadcrumbsList]); // eslint-disable-line react-hooks/exhaustive-deps

  const routePrefix = `/investigation/${investigationId}`;

  return (
    <>
      <Nav variant="tabs">
        {UI.investigationContainer.isDatasetListVisible && (
          <Nav.Item>
            <LinkContainer
              to={doi ? `/public/${doi}` : `${routePrefix}/datasets`}
            >
              <Nav.Link>
                <FontAwesomeIcon icon={faList} />
                <span style={{ marginLeft: 2 }}> Dataset List &nbsp;</span>
                <Badge bsPrefix="ourBadges-m"> {datasetCount} </Badge>
              </Nav.Link>
            </LinkContainer>
          </Nav.Item>
        )}

        {!doi && (
          <>
            <Nav.Item>
              <LinkContainer to={`${routePrefix}/events`}>
                <Nav.Link>
                  <FontAwesomeIcon icon={faCommentAlt} />
                  <span style={{ marginLeft: 2 }}> Logbook </span>
                </Nav.Link>
              </LinkContainer>
            </Nav.Item>
            {UI.etherpad.enabled &&
              experimental &&
              (((isAdministrator || isInstrumentScientist) &&
                UI.etherpad.onlyAdministrators) ||
                !UI.etherpad.onlyAdministrators) && (
                <>
                  <Nav.Item>
                    <LinkContainer to={`${routePrefix}/report`}>
                      <Nav.Link>
                        <FontAwesomeIcon icon={faBook} />
                        <span style={{ marginLeft: 2 }}>
                          {' '}
                          Notebook <sub className="text-danger">NEW</sub>{' '}
                        </span>
                      </Nav.Link>
                    </LinkContainer>
                  </Nav.Item>
                </>
              )}

            {UI.sampleTracking.enabled && !isAnonymous && (
              <>
                <Nav.Item>
                  <LinkContainer to={`${routePrefix}/shipping`}>
                    <Nav.Link>
                      <FontAwesomeIcon icon={faPlane} />
                      <span style={{ marginLeft: 2 }}> Shipping </span>
                    </Nav.Link>
                  </LinkContainer>
                </Nav.Item>
              </>
            )}
            {!isAnonymous && (
              <>
                <Nav.Item>
                  <LinkContainer to={`${routePrefix}/proposal`}>
                    <Nav.Link>
                      <FontAwesomeIcon icon={faCog} />
                      <span style={{ marginLeft: 2 }}> Proposal </span>
                    </Nav.Link>
                  </LinkContainer>
                </Nav.Item>
              </>
            )}
          </>
        )}
      </Nav>
    </>
  );
}

export default TabContainerMenu;
