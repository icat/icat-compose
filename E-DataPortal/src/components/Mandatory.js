import React from 'react';

export default function Mandatory() {
  return <span style={{ color: 'red' }}>*</span>;
}
