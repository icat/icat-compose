import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import moment from 'moment';
import { getDatasetParameterValueByName } from '../../../../helpers';
import ParameterTableWidget from '../../../Instrument/ParameterTableWidget';
import GalleryDatasetThumbnail from '../../GalleryDatasetThumbnail';

function EMDatasetSummary(props) {
  const { dataset } = props;

  const getParameters = () => {
    return [
      { name: 'Dataset', value: dataset.name },
      { name: 'Sample', value: dataset.sampleName },
      {
        name: 'Protein',
        value: getDatasetParameterValueByName(dataset, 'EM_protein_acronym'),
      },
      {
        name: 'Definition',
        value: getDatasetParameterValueByName(dataset, 'definition'),
      },
      {
        name: 'Start',
        value: moment(dataset.startDate).format('LTS'),
      },
      {
        name: 'Description',
        value: getDatasetParameterValueByName(dataset, 'Sample_description'),
      },
    ];
  };

  const getTechniqueParameters = () => {
    return [
      {
        name: 'Amplitude',
        value: `${getDatasetParameterValueByName(
          dataset,
          'EM_amplitude_contrast'
        )} %`,
      },
      {
        name: 'Initial Dose',
        value: getDatasetParameterValueByName(dataset, 'EM_dose_initial'),
      },
      {
        name: 'Dose/frame',
        value: getDatasetParameterValueByName(dataset, 'EM_dose_per_frame'),
      },
      {
        name: 'Images',
        value: getDatasetParameterValueByName(dataset, 'EM_images_count'),
      },
      {
        name: 'Magnification',
        value: getDatasetParameterValueByName(dataset, 'EM_magnification'),
      },
      {
        name: 'Sampling Rate',
        value: `${getDatasetParameterValueByName(
          dataset,
          'EM_sampling_rate'
        )} Å/pixel`,
      },
      {
        name: 'Spherical Ab.',
        value: `${getDatasetParameterValueByName(
          dataset,
          'EM_spherical_aberration'
        )} mm`,
      },
      {
        name: 'Voltage',
        value: getDatasetParameterValueByName(dataset, 'EM_voltage'),
      },
    ];
  };

  return (
    <Container fluid style={{ margin: 20 }}>
      <Row>
        <Col xs={12} md={2}>
          <ParameterTableWidget striped={false} parameters={getParameters()} />
        </Col>
        <Col xs={12} sm={12} md={2}>
          <ParameterTableWidget
            striped={false}
            parameters={getTechniqueParameters()}
          />
        </Col>
        <Col xs={12} sm={12} md={2} />
        <Col xs={12} sm={12} md={2} />

        <Col xs={12} sm={12} md={2}>
          <GalleryDatasetThumbnail dataset={dataset} index={0} />
        </Col>
      </Row>
    </Container>
  );
}

export default EMDatasetSummary;
