import { faThList } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Card } from 'react-bootstrap';
import LoadingBoundary from '../LoadingBoundary';
import SamplesTable from './SamplesTable';

function SamplesPanel(props) {
  const { investigationId } = props;
  return (
    <Card variant="info">
      <Card.Header>
        <Card.Title>
          {' '}
          <FontAwesomeIcon icon={faThList} style={{ marginRight: 10 }} />
          Samples
        </Card.Title>
      </Card.Header>
      <Card.Body>
        <LoadingBoundary message="Loading samples">
          <SamplesTable investigationId={investigationId} />
        </LoadingBoundary>
      </Card.Body>
    </Card>
  );
}

export default SamplesPanel;
