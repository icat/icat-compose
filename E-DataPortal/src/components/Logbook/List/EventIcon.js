import React from 'react';
import CategoryIcon from './CategoryIcon';

/**
 * A basic event of the logbook
 */
function EventIcon(props) {
  const { event } = props;
  return <CategoryIcon category={event.category} />;
}

export default EventIcon;
