import { cloneDeep } from 'lodash-es';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import {
  Button,
  Col,
  Container,
  OverlayTrigger,
  Card,
  Popover,
  Row,
  Form,
} from 'react-bootstrap';
import { GithubPicker } from 'react-color';
import UI from '../../../config/ui';
import Mandatory from '../../Mandatory';

function renderPopoverColorPickerBottom(callback) {
  return (
    <Popover id="popover-positioned-bottom" title="Select a color">
      <GithubPicker triangle="hide" width="230px" onChangeComplete={callback} />
    </Popover>
  );
}

/**
 * React component used to edit or create a tag
 */
function NewOrEditTagPanel(props) {
  const {
    tag,
    investigationId,
    panelHeaderText,
    onCancelButtonClicked,
    onSaveButtonClicked,
  } = props;

  const [updatedTag, setUpdatedTag] = useState(
    cloneDeep(tag) || {
      name: '',
      description: '',
      color: UI.logbook.DEFAULT_TAG_COLOR,
      investigationId,
    }
  );

  /**
   * CallBack function triggered when the user changes the tag field
   */
  const onInputChange = (changedTagProperty, event) => {
    const value = event.target.value;

    const tagCopy = updatedTag;
    tagCopy[changedTagProperty] = value;

    setUpdatedTag(tagCopy);
  };

  /* CallBack function triggered when the user selects a new color */
  const onChangeComplete = (color) => {
    const fakeEvent = {};
    fakeEvent.target = {};
    fakeEvent.target.value = color.hex;
    onInputChange('color', fakeEvent);
  };

  const onSave = () => {
    onSaveButtonClicked(updatedTag);
  };

  return (
    <Container fluid style={{ marginTop: 20, marginBottom: 70 }}>
      <Row className="show-grid">
        <Col xs={0} md={1}>
          {' '}
        </Col>
        <Col xs={12} md={10}>
          <Card>
            <Card.Header>
              {' '}
              <strong> {panelHeaderText} </strong>{' '}
            </Card.Header>
            <Card.Body>
              <Form.Group controlId="formControlTagLabel">
                <Form.Label>
                  Label <Mandatory />
                </Form.Label>
                <Form.Control
                  as="input"
                  type="text"
                  placeholder="Tag name"
                  value={updatedTag.name}
                  onChange={(e) => onInputChange('name', e)}
                />
              </Form.Group>

              <Form.Group controlId="formControlTagDescription">
                <Form.Label>Description </Form.Label>
                <Form.Control
                  as="input"
                  type="text"
                  placeholder="Tag description"
                  value={updatedTag.description}
                  onChange={(e) => onInputChange('description', e)}
                />
              </Form.Group>

              <Form.Group controlId="formControlTagScope">
                <Form.Label>
                  Scope <Mandatory />{' '}
                </Form.Label>
                <Form.Control
                  as="select"
                  type="text"
                  placeholder="Tag scope"
                  value={updatedTag.description}
                  onChange={(e) => this.onInputChange('scope', e)}
                >
                  <option value={investigationId}>investigation</option>
                  {/* <option value='beamline'>beamline</option> */}
                </Form.Control>
              </Form.Group>

              <Form.Group controlId="formControlTagColor">
                <Form.Label> Color </Form.Label>
                <Row>
                  <Col xs={12}>
                    <OverlayTrigger
                      trigger="click"
                      placement="top"
                      rootClose
                      overlay={renderPopoverColorPickerBottom(onChangeComplete)}
                    >
                      <div
                        style={{
                          backgroundColor:
                            updatedTag.color || UI.logbook.DEFAULT_TAG_COLOR,
                          width: 30,
                          height: 30,
                          marginLeft: '50%',
                          marginRight: '50%',
                        }}
                      />
                    </OverlayTrigger>
                  </Col>
                </Row>
              </Form.Group>
              <br />

              <div style={{ float: 'right' }}>
                <Button
                  variant="primary"
                  style={{ marginRight: 10 }}
                  onClick={() => onSave()}
                >
                  {' '}
                  Save{' '}
                </Button>
                <Button
                  variant="primary"
                  style={{ marginRight: 10 }}
                  onClick={() => onCancelButtonClicked()}
                >
                  {' '}
                  Cancel{' '}
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={0} md={1}>
          {' '}
        </Col>
      </Row>
    </Container>
  );
}

NewOrEditTagPanel.propTypes = {
  /** investigation identifier */
  investigationId: PropTypes.string,
  /** Function triggered when the user clicks on the save button during tag creation or edition*/
  onSaveButtonClicked: PropTypes.func,
  /** Text display in the panel header */
  panelHeaderText: PropTypes.string,
  /** Function triggered when the user clicks the cancel button */
  onCancelButtonClicked: PropTypes.func,
  /* if present, it corresponds to the tag to edit. If null, it indcates that a tag is being created*/
  tag: PropTypes.object,
};

export default NewOrEditTagPanel;
