import PropTypes from 'prop-types';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';

/**
 * This class represents the event footer of an event panel.
 */
function EventFooter(props) {
  const {
    cancelButtonLabel,
    isSaveButtonVisible,
    isSaveButtonEnabled,
    loading = false,
    onSaveButtonClicked,
    onCancelButtonClicked,
  } = props;

  return (
    <div style={{ position: 'relative', height: 37 }}>
      &nbsp;
      <div
        className="noUnderlinedLink"
        style={{ position: 'absolute', right: 10, top: 5 }}
      >
        <ButtonToolbar>
          <ButtonGroup style={{ height: 30 }}>
            {isSaveButtonVisible &&
              (isSaveButtonEnabled ? (
                <OverlayTrigger
                  placement="left"
                  overlay={
                    <Tooltip id="tooltip">
                      <p> Save the event. </p>
                    </Tooltip>
                  }
                >
                  <Button
                    className="btn btn-primary"
                    size="sm"
                    style={{ marginRight: 5 }}
                    disabled={loading}
                    onClick={() => onSaveButtonClicked()}
                  >
                    {' '}
                    Save{' '}
                  </Button>
                </OverlayTrigger>
              ) : (
                <OverlayTrigger
                  placement="left"
                  overlay={
                    <Tooltip id="tooltip2">
                      <p>
                        {' '}
                        First add a comment before you can save the event.{' '}
                      </p>
                    </Tooltip>
                  }
                >
                  <div>
                    {/* the following div is a workaround to the abscence of tooltip on a disabled button */}
                    <div
                      style={{
                        position: 'relative',
                        height: 30,
                        zIndex: '2',
                      }}
                    >
                      {' '}
                      &nbsp;{' '}
                    </div>
                    <Button
                      style={{
                        position: 'relative',
                        top: '-30px',
                        zIndex: '1',
                      }}
                      className="btn btn-primary"
                      size="sm"
                      onClick={() => onSaveButtonClicked()}
                      disabled
                    >
                      Save
                    </Button>
                  </div>
                </OverlayTrigger>
              ))}
          </ButtonGroup>
          <ButtonGroup>
            <Button
              className="btn btn-primary"
              size="sm"
              onClick={(e) => onCancelButtonClicked(e)}
            >
              {' '}
              {cancelButtonLabel ? cancelButtonLabel : 'Cancel'}{' '}
            </Button>
          </ButtonGroup>
        </ButtonToolbar>
      </div>
    </div>
  );
}

EventFooter.PropType = {
  /** a custom text displayed in the button. 'Cancel' when not provided */
  cancelButtonLabel: PropTypes.string,
  /** the callback function executed when the cancel button is clicked */
  onCancelButtonClicked: PropTypes.func.isRequired,
  /** the callback function executed when the save button is clicked */
  onSaveButtonClicked: PropTypes.func.isRequired,
  /** whether the save button must be enabled or disabled */
  isSaveButtonEnabled: PropTypes.bool,
  /** whether the save button is visible or not */
  isSaveButtonVisible: PropTypes.bool,
};

EventFooter.defaultProps = {
  /** Default text displayed in the button */
  cancelButtonLabel: 'Cancel',
  isSaveButtonVisible: true,
};

export default EventFooter;
