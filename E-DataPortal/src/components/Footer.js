import React from 'react';
import UI from '../config/ui';
import logo from '../images/ebs.gif';
import styles from './Footer.module.css';

function Footer() {
  return (
    <footer className={styles.footer}>
      <img className={styles.logo} src={logo} alt="ESRF" />
      <a href={UI.footer.link} target="_blank" rel="noopener noreferrer">
        {UI.footer.text}
      </a>
    </footer>
  );
}

export default Footer;
