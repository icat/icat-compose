import React from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import { orcidFormatter } from '../Investigation/utils';

export function UsersWidget(props) {
  const { title, users } = props;

  function getOrcidIdentifier(nameIdentifiers) {
    if (nameIdentifiers) {
      const orcidObject = nameIdentifiers.find((item) => {
        return item.nameIdentifierScheme.toLowerCase() === 'orcid';
      });
      if (orcidObject) {
        return orcidFormatter(orcidObject.nameIdentifier, false);
      }
    }
    return '';
  }

  return (
    <>
      <Card border="secondary" style={{ marginBottom: 12 }}>
        <Card.Header>{title}</Card.Header>
        <Card.Body>
          <Container fluid style={{ marginBottom: '1rem', padding: 0 }}>
            <Row>
              {users.map(
                ({ name, givenName, familyName, nameIdentifiers }, idU) => {
                  const orcidIdentifier = getOrcidIdentifier(
                    nameIdentifiers,
                    false
                  );
                  const userName = givenName
                    ? `${givenName} ${familyName}`
                    : name;
                  return (
                    <Col md={3} xs={6} sm={4} lg={2} key={`user-${idU}`}>
                      {userName} {orcidIdentifier}
                    </Col>
                  );
                }
              )}
            </Row>
          </Container>
        </Card.Body>
      </Card>
    </>
  );
}
