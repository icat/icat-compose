import React from 'react';
import { Card } from 'react-bootstrap';

export function TitleWidget(props) {
  const { titles } = props;
  const title = titles && titles.length > 0 ? titles[0].title : '';
  return (
    <>
      <Card bg="info" text="white" style={{ marginBottom: 12 }}>
        <Card.Body>
          <Card.Title>{title}</Card.Title>
        </Card.Body>
      </Card>
    </>
  );
}
