/**
 * Checks whether the logbook is properly configured
 * @param {object} config current logbook ui specific configuration to be checked
 * @returns {boolean} true when properly configured; flase other wise
 */
export function isConfigValid(config) {
  if (!config) {
    console.log(
      '[ERROR] [CONFIG] logbook section is missing from config/ui.js'
    );
    return false;
  }
  if (!config.SORT_EVENTS_BY) {
    console.log('[ERROR] [CONFIG] SORT_EVENTS_BY object is missing');
    return false;
  }

  if (
    config.SORT_EVENTS_BY !== 'creationDate' &&
    config.SORT_EVENTS_BY !== '_id' &&
    config.SORT_EVENTS_BY !== 'createdAt' &&
    config.SORT_EVENTS_BY !== 'updatedAt'
  ) {
    console.log(
      "[ERROR] [CONFIG] SORT_EVENTS_BY value is not supported. Possible values are 'creationDate', '_id', 'createdAt' and 'updatedAt' "
    );
    return false;
  }
  if (!config.SORTING_ORDER) {
    console.log('[ERROR] [CONFIG] SORTING_ORDER object is missing');
    return false;
  }
  if (!config.EVENTS_PER_PAGE) {
    console.log('[ERROR] [CONFIG] EVENTS_PER_PAGE value is missing');
    return false;
  }
  return true;
}
