import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import TabContainerMenu from '../components/TabContainerMenu/TabContainerMenu';
import PageNotFound from './PageNotFound';
import { useResource } from 'rest-hooks';
import InvestigationResource from '../resources/investigation';
import UI from '../config/ui';

function LogbookReportPage() {
  const { investigationId } = useParams();
  const user = useSelector((state) => state.user);
  const investigation = useResource(InvestigationResource.detailShape(), {
    id: investigationId,
  });

  if (!investigation) {
    return <PageNotFound />;
  }

  return (
    <Container fluid>
      <Row>
        <Col>
          <TabContainerMenu investigation={investigation} />
          <div style={{ margin: 5 }}>
            <iframe
              title="Report Auth"
              src={`${UI.etherpad.url}?sessionID=${user.sessionId}&padName=${investigationId}&userName=${user.fullName}`}
              width="100%"
              height={window.innerHeight}
              frameBorder="0"
            ></iframe>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default LogbookReportPage;
