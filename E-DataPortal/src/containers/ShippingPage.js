import React, { useEffect, useRef } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import ShippingPanel from '../components/Shipping/ShippingPanel';
import TabContainerMenu from '../components/TabContainerMenu/TabContainerMenu';
import PageNotFound from './PageNotFound';
import LoadingBoundary from '../components/LoadingBoundary';
import { useResource } from 'rest-hooks';
import InvestigationResource from '../resources/investigation';
import { setInvestigationBreadCrumbs } from './investigation-breadcrumbs';
import { usePageTracking } from '../helpers/hooks';

function ShippingPage() {
  usePageTracking();
  const { investigationId } = useParams();
  const investigation = useResource(InvestigationResource.detailShape(), {
    id: investigationId,
  });
  const dispatch = useDispatch();
  const breadcrumbsList = useSelector((state) => state.breadcrumbsList);
  const currentBreadcrumbsList = useRef(); // work around stale breadcrumbsList reference
  currentBreadcrumbsList.current = breadcrumbsList;

  useEffect(() => {
    if (investigation) {
      dispatch(
        setInvestigationBreadCrumbs(
          investigation,
          currentBreadcrumbsList.current
        )
      );
    }
  }, [dispatch, investigation, currentBreadcrumbsList]); // eslint-disable-line react-hooks/exhaustive-deps

  if (!investigation) {
    return <PageNotFound />;
  }

  return (
    <Container fluid>
      <Row>
        <Col sm={12}>
          <TabContainerMenu investigation={investigation} />
          <LoadingBoundary message="Loading shipping data..." spacedOut>
            <ShippingPanel investigation={investigation} />
          </LoadingBoundary>
        </Col>
      </Row>
    </Container>
  );
}

export default ShippingPage;
