import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Card } from 'react-bootstrap';
import InvestigationTable from '../../components/Investigation/InvestigationTable';

function MyIndustryProposalsPanel() {
  return (
    <Card variant="info">
      <Card.Header>
        <Card.Title>
          <FontAwesomeIcon icon={faUser} style={{ marginRight: 10 }} />
          My Industry Proposals
        </Card.Title>
      </Card.Header>
      <Card.Body>
        <p>
          You may create parcels for the proposals below if no sessions have
          been scheduled for them.
        </p>
        <InvestigationTable
          filter="industry"
          withInvestigationStats
          withProposalLinks
        />
      </Card.Body>
    </Card>
  );
}

export default MyIndustryProposalsPanel;
