import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import TabContainerMenu from '../components/TabContainerMenu/TabContainerMenu';
import PageNotFound from './PageNotFound';
import { useResource } from 'rest-hooks';
import InvestigationResource from '../resources/investigation';
import ParticipantsPanel from '../components/Investigation/ParticipantsPanel';
import SamplesPanel from '../components/Investigation/SamplesPanel';
import ProposalPanel from '../components/Investigation/ProposalPanel';
import { usePageTracking } from '../helpers/hooks';

function InvestigationProposalPage() {
  usePageTracking();
  const { investigationId } = useParams();
  const user = useSelector((state) => state.user);

  const investigation = useResource(InvestigationResource.detailShape(), {
    id: investigationId,
  });

  if (!investigation) {
    return <PageNotFound />;
  }

  return (
    <Container fluid>
      <Row>
        <Col sm={12}>
          <TabContainerMenu investigation={investigation} />
          <div style={{ margin: 20 }}>
            <ProposalPanel investigation={investigation} />
          </div>
          <div style={{ margin: 20 }}>
            <ParticipantsPanel
              investigationId={investigationId}
              name={user.name}
            />
          </div>
          <div style={{ margin: 20 }}>
            <SamplesPanel investigationId={investigationId} />
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default InvestigationProposalPage;
