import { Resource } from 'rest-hooks';
import { store } from '../store';
import { getFilesByDatasetId } from '../api/icat-plus/catalogue';

export default class Datafile extends Resource {
  Datafile = undefined;

  pk() {
    return this.Datafile.id?.toString();
  }

  static get key() {
    return 'DataFileResource';
  }

  static listUrl(params) {
    const { sessionId } = store.getState().user;
    const { datasetId } = params;
    return getFilesByDatasetId(sessionId, datasetId);
  }

  static url(params) {
    return this.listUrl(params);
  }
}
