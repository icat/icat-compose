import '@testing-library/jest-dom/extend-expect';

// Monkey-patch `console.warn` to silence React methods deprecation warning
// => remove once Bootstrap and `react-bootstrap` have been updated
const warn = console.warn;
console.warn = (param, ...rest) => {
  if (
    typeof param !== 'string' ||
    !/(componentWillMount|componentWillReceiveProps)/.test(param)
  ) {
    warn(param, ...rest);
  }
};
// eslint-disable-next-line no-unused-vars
const _paq = (window._paq = window._paq || []);
