import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { Button } from 'react-bootstrap';
import EventFooter from '../components/Logbook/EventFooter';

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe('EventFooter Tests', () => {
  it('displays enabled button when prop isSaveButtonEnabled is true', () => {
    const myEventFooter = Enzyme.shallow(
      <EventFooter
        isSaveButtonEnabled
        onSaveButtonClicked={() => null}
        onCancelButtonClicked={() => null}
      />
    );

    expect(myEventFooter.find(Button).first().prop('disabled')).toBe(false);
  });

  it('displays disabled button when prop isSaveButtonEnabled is false', () => {
    const myEventFooter = Enzyme.shallow(
      <EventFooter
        isSaveButtonEnabled={false}
        onSaveButtonClicked={() => null}
        onCancelButtonClicked={() => null}
      />
    );

    expect(myEventFooter.find(Button).first().prop('disabled')).toBe(true);
  });
});
