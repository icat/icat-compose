/**
 * This files hosts all events used in the tests. The 'good' prefix indicates that this event is error free.
 * Content:
 * goodComment1 : user comment, content contains less than 100 letters, with tags, previousEventVersiion is null
 * goodComment2 : user comment, content contains more than 100 letters, no tags
 * goodComment3: user comment that has no title
 * goodComment4: user comment, 2 versions, title changed
 * goodComment4_OriginalVersion : the original version of goodComment4
 * goodComment5: user comment, 2 versions, title changed, common word in title with goodComment4
 * goodComment6: user comment, title is not set, username is not set
 * goodComment7: user comment, 3 versions, title changed, content changed
 * goodComment7_OriginalVersion : the original version of goodComment7
 *
 * goodError1: an error event , previousVersionEvent undefined
 * goodError2: an error event which has been commented (in plain text)
 * goodError3: an error event which has been commented (in html text)
 * goodError3_OriginalVersion: original version of goodError3
 * goodError4: an error event which has been commented 2 times
 * goodError4_OriginalVersion: original version of goodError4
 * goodInfo1: an info event
 * goodCommandLine1 : a command line event
 * goodDebug1 : a debug event
 */

export const badComment3 = {
  title: 'event title 1',
  category: 'comment',
  content: [],
  createdAt: '2018-09-10T01:00:00.727Z',
  creationDate: '2018-09-10T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: [],
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment1 = {
  title: 'event title 1',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'plain text content 1',
    },
    {
      format: 'html',
      text: '<p> html text content 1 </p>',
    },
  ],
  createdAt: '2018-09-10T01:00:00.727Z',
  creationDate: '2018-09-10T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment2 = {
  title: 'event title 2',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: `${singleLetterStringGenerator(95)}123456789`,
    },
    {
      format: 'html',
      text: `<p>${singleLetterStringGenerator(95)}123456789 </p>`,
    },
  ],
  createdAt: '2018-09-11T01:00:00.727Z',
  creationDate: '2018-09-11T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: [],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'agoetz',
};

export const goodComment3 = {
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'plain text content 1',
    },
    {
      format: 'html',
      text: '<p> html text content 1 </p>',
    },
  ],
  createdAt: '2018-09-12T01:00:00.727Z',
  creationDate: '2018-09-12T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment4 = {
  title: 'this is the title second version',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'new version of plain text',
    },
    {
      format: 'html',
      text: '<p> new version of plain text </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-13T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: {
    title: 'this is the title initial version',
    category: 'comment',
    content: [
      {
        format: 'plainText',
        text: 'plain text content 1',
      },
      {
        format: 'html',
        text: '<p> html text content 1 </p>',
      },
    ],
    createdAt: '2018-09-10T13:23:08.727Z',
    creationDate: '2018-09-12T01:00:00.000Z',
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: ['firstTag', 'secondTag'],
    type: 'annotation',
    updatedAt: '2018-09-10T13:23:08.727Z',
    username: 'mchaille',
  },
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment4_OriginalVersion = {
  title: 'this is the title initial version',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'plain text content 1',
    },
    {
      format: 'html',
      text: '<p> html text content 1 </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-12T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment5 = {
  title: "this title shares 'second' in commun with goodComment4",
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'new version of plain text',
    },
    {
      format: 'html',
      text: '<p> new version of plain text </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-14T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: {
    title: 'this is the title initial version',
    category: 'comment',
    content: [
      {
        format: 'plainText',
        text: 'plain text content 1',
      },
      {
        format: 'html',
        text: '<p> html text content 1 </p>',
      },
    ],
    createdAt: '2018-09-10T13:23:08.727Z',
    creationDate: '2018-09-10T01:00:00.000Z',
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: ['firstTag', 'secondTag'],
    type: 'annotation',
    updatedAt: '2018-09-10T13:23:08.727Z',
    username: 'agoetz',
  },
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'asole',
};

export const goodComment6 = {
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'plain text content 1',
    },
    {
      format: 'html',
      text: '<p> html text content 1 </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-15T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
};

export const goodComment7 = {
  title: 'this is the title third version',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'version 3 of plain text',
    },
    {
      format: 'html',
      text: '<p> version 3 of html text </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-13T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: {
    title: 'this is the second version',
    category: 'comment',
    content: [
      {
        format: 'plainText',
        text: 'version 2 of plain text',
      },
      {
        format: 'html',
        text: '<p> version 3 of html text </p>',
      },
    ],
    createdAt: '2018-09-10T13:23:08.727Z',
    creationDate: '2018-09-12T01:00:00.000Z',
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: {
      title: 'this is the title initial version',
      category: 'comment',
      content: [
        {
          format: 'plainText',
          text: 'version 1 of plain text',
        },
        {
          format: 'html',
          text: '<p> version 1 of html text </p>',
        },
      ],
      createdAt: '2018-09-10T13:23:08.727Z',
      creationDate: '2018-09-11T01:00:00.000Z',
      datasetId: null,
      file: [],
      fileSize: null,
      filename: null,
      investigationId: 77570462,
      machine: null,
      previousVersionEvent: null,
      software: null,
      tag: ['firstTag', 'secondTag'],
      type: 'annotation',
      updatedAt: '2018-09-10T13:23:08.727Z',
      username: 'mchaille',
    },
    software: null,
    tag: ['firstTag', 'secondTag'],
    type: 'annotation',
    updatedAt: '2018-09-10T13:23:08.727Z',
    username: 'mchaille',
  },
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodComment7_OriginalVersion = {
  title: 'this is the title initial version',
  category: 'comment',
  content: [
    {
      format: 'plainText',
      text: 'version 1 of plain text',
    },
    {
      format: 'html',
      text: '<p> version 1 of html text </p>',
    },
  ],
  createdAt: '2018-09-10T13:23:08.727Z',
  creationDate: '2018-09-11T01:00:00.000Z',
  datasetId: null,
  file: [],
  fileSize: null,
  filename: null,
  investigationId: 77570462,
  machine: null,
  previousVersionEvent: null,
  software: null,
  tag: ['firstTag', 'secondTag'],
  type: 'annotation',
  updatedAt: '2018-09-10T13:23:08.727Z',
  username: 'mchaille',
};

export const goodError1 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'mchaille',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'beamline lost',
    },
  ],
};

export const goodError2 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'mchaille',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'this is a user comment on the error',
    },
  ],
  previousVersionEvent: {
    investigationId: 77570462,
    creationDate: '2018-09-10T13:23:08.000Z',
    type: 'notification',
    category: 'error',
    username: 'mchaille',
    software: 'E-Dataportal',
    content: [
      {
        format: 'plainText',
        text: 'beamline lost',
      },
    ],
  },
};

export const goodError3 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'asole',
  software: 'E-Dataportal',
  content: [
    {
      format: 'html',
      text: '<p> this is a user comment on the error </p>',
    },
  ],
  previousVersionEvent: {
    investigationId: 77570462,
    creationDate: '2018-09-10T13:23:08.000Z',
    type: 'notification',
    category: 'error',
    username: 'agoetz',
    software: 'E-Dataportal',
    content: [
      {
        format: 'plainText',
        text: 'beamline lost',
      },
    ],
  },
};
export const goodError3_OriginalVersion = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'agoetz',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'beamline lost',
    },
  ],
};

export const goodError4 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'asole',
  software: 'E-Dataportal',
  content: [
    {
      format: 'html',
      text: '<p> this is the second version of a comment </p>',
    },
  ],
  previousVersionEvent: {
    investigationId: 77570462,
    creationDate: '2018-09-10T13:23:08.000Z',
    type: 'notification',
    category: 'error',
    username: 'agoetz',
    software: 'E-Dataportal',
    content: [
      {
        format: 'html',
        text: '<p> this is the first version of a comment </p>',
      },
    ],
    previousVersionEvent: {
      investigationId: 77570462,
      creationDate: '2018-09-10T13:23:08.000Z',
      type: 'notification',
      category: 'error',
      username: 'agoetz',
      software: 'E-Dataportal',
      content: [
        {
          format: 'plainText',
          text: 'beamline lost',
        },
      ],
    },
  },
};

export const goodError4_OriginalVersion = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'error',
  username: 'agoetz',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'beamline lost',
    },
  ],
};

export const goodInfo1 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'info',
  username: 'mchaille',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'dataset collection started',
    },
  ],
};

export const goodCommandLine1 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'commandLine',
  username: 'mchaille',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'dataset collection started',
    },
  ],
};

export const goodDebug1 = {
  investigationId: 77570462,
  creationDate: '2018-09-10T13:23:08.000Z',
  type: 'notification',
  category: 'debug',
  username: 'mchaille',
  software: 'E-Dataportal',
  content: [
    {
      format: 'plainText',
      text: 'dataset collection started',
    },
  ],
};

/**
 * Helper function which generates a string composed of the same letter
 */
export function singleLetterStringGenerator(numberofLetters) {
  const letter = 'a';
  let result = '';
  let counter = 0;

  while (counter < numberofLetters) {
    result += letter;
    counter += 1;
  }
  return result;
}
