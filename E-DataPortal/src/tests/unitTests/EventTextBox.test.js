import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import {
  ANNOTATION,
  EVENT_CATEGORY_COMMANDLINE,
  NOTIFICATION,
} from '../../constants/eventTypes';
import EventTextBox from '../../components/Logbook/List/EventTextBox';

const resources = require('./resources/eventTextBox.resource.js');

require('it-each')({ testPerIteration: true });

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe.skip('EventTextBoxUnitTests', () => {
  describe('Rendering', () => {
    it.each(resources.rendering, '%s', ['description'], (element, next) => {
      const wrapper = getWrapper({ event: element.event });

      if (element.event.type === NOTIFICATION) {
        if (element.event.previousVersionEvent) {
          // it renders 2 lazyLoadedText : the first for the original event and the second for the latest comment
          expect(wrapper.find('LazyLoadedText').length).toEqual(2);

          // Check the content
          expect(wrapper.find('LazyLoadedText').at(0).prop('text')).toEqual(
            element.expected.texts[0]
          );
          expect(wrapper.find('LazyLoadedText').at(1).prop('text')).toEqual(
            element.expected.texts[1]
          );

          // Test the <pre> which wraps notification original version only.
          expect(wrapper.find('LazyLoadedText').at(0).parent().is('pre')).toBe(
            true
          );
          expect(wrapper.find('LazyLoadedText').at(1).parent().is('pre')).toBe(
            false
          );
        } else {
          // Test notifications which have not been commented yet
          expect(wrapper.find('LazyLoadedText').length).toEqual(1);
          // Check the content
          expect(wrapper.find('LazyLoadedText').prop('text')).toEqual(
            element.expected.texts[0]
          );
          // Test the <pre> which must wrap the text.
          expect(wrapper.find('pre').exists()).toBe(true);
        }

        //The pre on the original version must have the proper className
        if (
          element.event.category.toLowerCase() === EVENT_CATEGORY_COMMANDLINE
        ) {
          expect(wrapper.find('pre').prop('className')).toBeUndefined();
        } else {
          expect(wrapper.find('pre').prop('className')).toBe('whitePre');
        }
      }

      if (element.event.type === ANNOTATION) {
        expect(wrapper.find('LazyLoadedText').length).toEqual(1);

        // Test the <pre>
        expect(wrapper.find('pre').exists()).toBe(false);

        // Check the content
        expect(wrapper.find('LazyLoadedText').prop('text')).toEqual(
          element.expected.texts[0]
        );
      }

      next();
    });
  });
});

function getWrapper(params) {
  return Enzyme.shallow(<EventTextBox event={params.event} />);
}
