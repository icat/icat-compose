import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import EditorWrapper from '../../components/Logbook/Editor/EditorWrapper';

require('it-each')({ testPerIteration: true });

const resources = require('./resources/EditorWrapper.resource.js');

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe('EditorWrapperUnitTests', () => {
  describe('rendering', () => {
    it.each(
      resources.rendering.editor,
      '%s',
      ['description'],
      (element, next) => {
        const wrapper = getShallowWrapper({ event: element.event });
        expect(wrapper.find('Editor').prop('value')).toEqual(element.expected);

        next();
      }
    );
  });
});

function getShallowWrapper(params) {
  if (params) {
    params.event = params.event ? params.event : null;
    params.onContentChanged = params.onContentChanged
      ? params.onContentChanged
      : () => null;
    params.user = params.user ? params.user : {};

    return Enzyme.shallow(
      <EditorWrapper
        event={params.event}
        investigationId={params.investigationId}
        onContentChanged={params.onContentChanged}
        user={params.user}
      />
    );
  }
}
