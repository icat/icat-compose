import { getFirstIndexToDisplay } from '../../helpers/eventsProviderHelper';

require('it-each')({ testPerIteration: true });
const resources = require('./resources/eventsProviderHelper.resource.js');

describe('EventsProviderHelper tests', () => {
  describe('getFirstEventToShowIndex', () => {
    it.each(
      resources.getFirstEventToShowIndex,
      'It returns the proper index: %s',
      ['description'],
      (element, next) => {
        const { page, eventsPerPage, expected } = element;

        expect(getFirstIndexToDisplay(page, eventsPerPage)).toEqual(expected);
        next();
      }
    );
  });
});
