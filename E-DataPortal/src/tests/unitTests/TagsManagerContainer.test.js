import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import JestMockPromise from 'jest-mock-promise';
import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import { LOGGED_IN } from '../../constants/actionTypes';
import reducer from '../../reducers';

const resources = require('./resources/TagsManagerContainer.resource.js');

require('it-each')({ testPerIteration: true });

describe.skip('TagsManagerContainerUnitTests', () => {
  beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
  });

  describe('rendering', () => {
    it.each(resources.rendering, '%s', ['description'], (element, next) => {
      const TagsManagerContainerClass = require('../../containers/Logbook/Tags/TagsManagerContainer')
        .TagsManagerContainer;
      const getTagsPromise = new JestMockPromise((resolve) =>
        resolve(element.tags)
      );
      TagsManagerContainerClass.prototype.getTags = () => getTagsPromise;
      const wrapper = getShallowWrapper({
        logbookContext: element.logbookContext,
      });
      getTagsPromise.resolve();

      //wrapper.update();

      expect(wrapper.find('UserMessage').prop('message')).toEqual(
        'There is no tag in this proposal yet.'
      );
      expect(wrapper.find('TagListPanel').length).toBe(0);

      // if (element.tags) {
      //     expect(wrapper.find('TagListPanelLine').length).toBe(2);
      //     expect(wrapper.find('TagListPanelLine').at(0).prop("tag")).toEqual(element.tags[0]);
      //     expect(wrapper.find('TagListPanelLine').at(1).prop("tag")).toEqual(element.tags[1]);
      // } else {
      //     expect(wrapper.find('TagListPanelLine').length).toBe(0);
      //     expect(wrapper.find('UserMessage').prop('message')).toEqual("There is no tag in this proposal yet.");
      // }
      next();
    });
  });

  describe('testFunctionalProps', () => {});
});

/**
 * Get a mounted wrapper
 */
function getShallowWrapper(propsObject) {
  const middleware = [thunk];
  const persistConfig = { key: 'root', storage };
  const persistedReducer = persistReducer(persistConfig, reducer);
  const store = createStore(
    persistedReducer,
    {
      user: {
        type: LOGGED_IN,
        username: 'username',
        sessionId: 'testSessionId',
      },
    },
    applyMiddleware(...middleware, promise(), thunk)
  );
  const TagsManagerContainer = require('../../containers/Logbook/Tags/TagsManagerContainer')
    .default;

  return Enzyme.mount(
    <Provider store={store}>
      <TagsManagerContainer
        investigationId={
          propsObject.investigationId
            ? propsObject.investigationId
            : 'testInvstigationId'
        }
        logbookContext={propsObject.logbookContext}
      />
    </Provider>
  );
}
