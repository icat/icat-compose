const { createEventByAttributes } = require('../../helpers');

module.exports = {
  rendering: [
    {
      description: 'Annotation, User comment, no previous version',
      event: createEventByAttributes({
        type: 'annotation',
        category: 'comment',
        content: [
          { format: 'plainText', text: 'a comment' },
          { format: 'html', text: '<p> a comment </p>' },
        ],
      }),
      expected: {
        texts: ['<p> a comment </p>'],
      },
    },
    {
      description:
        'Annotation, User comment, no previous version, with an image',
      event: createEventByAttributes({
        type: 'annotation',
        category: 'comment',
        content: [
          { format: 'plainText', text: 'a comment' },
          {
            format: 'html',
            text:
              '<p> a comment  <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
          },
        ],
      }),
      expected: {
        texts: [
          '<p> a comment  <img onClick=\'makeFullScreen(this)\'   style="height:200px;width:auto;cursor:zoom-in;" src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
        ],
      },
    },
    {
      description:
        'Annotation, User comment, with previous version, with an image',
      event: createEventByAttributes({
        type: 'annotation',
        category: 'comment',
        content: [
          { format: 'plainText', text: 'an updated comment' },
          {
            format: 'html',
            text:
              '<p> an updated comment  <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
          },
        ],
        previousVersionEvent: createEventByAttributes({
          type: 'annotation',
          category: 'comment',
          content: [
            { format: 'plainText', text: 'a comment' },
            {
              format: 'html',
              text:
                '<p> a comment  <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
            },
          ],
        }),
      }),
      expected: {
        texts: [
          '<p> an updated comment  <img onClick=\'makeFullScreen(this)\'   style="height:200px;width:auto;cursor:zoom-in;" src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
        ],
      },
    },
    {
      description:
        'Annotation, User comment, no previous version, no html format',
      event: createEventByAttributes({
        type: 'annotation',
        category: 'comment',
        content: [{ format: 'plainText', text: 'a comment' }],
      }),
      expected: {
        texts: ['a comment'],
      },
    },
  ]
    .concat(getNotificationResourcesByCategory('info'))
    .concat(getNotificationResourcesByCategory('error'))
    .concat(getNotificationResourcesByCategory('debug'))
    .concat(getNotificationResourcesByCategory('commandline'))
    .concat(getNotificationResourcesByCategory('commandLine'))
    .concat(getNotificationResourcesByCategory('comment')),
};

/** Helper function for the renderingEvent test which returns resources for this test */
function getNotificationResourcesByCategory(category) {
  return [
    {
      description: `Notification, ${category}, no comment, 2 formats`,
      event: createEventByAttributes({
        type: 'notification',
        category,
        content: [
          { format: 'plainText', text: 'originalText' },
          { format: 'html', text: '<p> originalText </p>' },
        ],
      }),
      expected: {
        texts: ['<p> originalText </p>'],
      },
    },
    {
      description: `Notification, ${category}, no comment, plaintext format only`,
      event: createEventByAttributes({
        type: 'notification',
        category,
        content: [{ format: 'plainText', text: 'originalText' }],
      }),
      expected: {
        texts: ['originalText'],
      },
    },
    {
      description: `Notification, ${category}, commented, 2 formats`,
      event: createEventByAttributes({
        type: 'notification',
        category,
        content: [
          { format: 'plainText', text: 'a comment' },
          { format: 'html', text: '<p> a comment</p>' },
        ],
        previousVersionEvent: createEventByAttributes({
          type: 'notification',
          category,
          content: [
            { format: 'plainText', text: 'originalText' },
            { format: 'html', text: '<p> originalText </p>' },
          ],
        }),
      }),
      expected: {
        texts: ['<p> originalText </p>', '<p> a comment</p>'],
      },
    },
    {
      description: `Notification, ${category}, commented, plaintext format only`,
      event: createEventByAttributes({
        type: 'notification',
        category,
        content: [
          { format: 'plainText', text: 'a comment' },
          { format: 'html', text: '<p> a comment</p>' },
        ],
        previousVersionEvent: createEventByAttributes({
          type: 'notification',
          category,
          content: [{ format: 'plainText', text: 'originalText' }],
        }),
      }),
      expected: {
        texts: ['originalText', '<p> a comment</p>'],
      },
    },
    {
      description: `Notification, ${category}, commented, with an image in the comment`,
      event: createEventByAttributes({
        type: 'notification',
        category,
        content: [
          { format: 'plainText', text: 'a comment' },
          {
            format: 'html',
            text:
              '<p> a comment <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
          },
        ],
        previousVersionEvent: createEventByAttributes({
          type: 'notification',
          category,
          content: [{ format: 'plainText', text: 'originalText' }],
        }),
      }),
      expected: {
        texts: [
          'originalText',
          '<p> a comment <img onClick=\'makeFullScreen(this)\'   style="height:200px;width:auto;cursor:zoom-in;" src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
        ],
      },
    },
  ];
}
