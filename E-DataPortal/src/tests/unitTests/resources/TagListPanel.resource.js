module.exports = {
  rendering: [
    {
      description: 'renders 2 investigation tags',
      isTagEditionEnabled: true,
      tags: [
        {
          _id: 1,
          color: '#000001',
          description: 'description1',
          name: 'tag1',
          investigation: '0123456879',
        },
        {
          _id: 2,
          color: '#000002',
          description: 'description2',
          name: 'tag2',
          investigation: '0123456879',
        },
      ],
    },
    {
      description: 'returns null when no tags are provided',
      isTagEditionEnabled: true,
      tags: [],
    },
  ],
  testFunctionalProps: {
    isTagEditionEnabled: true,
    tags: [
      {
        _id: 1,
        color: '#000001',
        description: 'description1',
        name: 'tag1',
        investigation: '0123456879',
      },
    ],
  },
};
