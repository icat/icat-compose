import { LOGBOOK_CONTEXT_NAME_PROPOSAL } from '../../../constants/eventTypes';

module.exports = {
  renderingButtons: [
    {
      description: "[newButton] logbook is not public, no 'New Event' Panel",
      isNewButtonEnabled: true,
      text: 'New',
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      expected: { isEnabled: true, isVisible: true },
    },
    {
      description: '[newButton] logbook is not public, new button not enabled',
      isNewButtonEnabled: false,
      text: 'New',
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      expected: { isEnabled: false, isVisible: true },
    },
    {
      description: '[newButton] logbook is public, new button not visible',
      isNewButtonEnabled: false,
      text: 'New',
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      expected: { isEnabled: false, isVisible: false },
    },
    {
      description: '[photoButton] logbook is not public',
      text: 'Take a photo',
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      expected: { isEnabled: true, isVisible: true },
    },
    {
      description: '[photoButton] logbook is public',
      text: 'Take a photo',
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      expected: { isEnabled: true, isVisible: false },
    },
    {
      description:
        '[pdfButton] logbook is not public, logbook is empty, no new Event Panel opened',
      text: 'PDF',
      isNewButtonEnabled: true,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      eventCountBySelectionFilter: 0,
      expected: { isEnabled: false },
    },
    {
      description:
        '[pdfButton] logbook is not public, logbook is not empty, no new Event Panel opened',
      text: 'PDF',
      isNewButtonEnabled: true,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      eventCountBySelectionFilter: 2,
      expected: { isEnabled: true },
    },
    {
      description:
        '[pdfButton] logbook is not public, logbook is not empty, a new Event Panel is opened',
      text: 'PDF',
      isNewButtonEnabled: false,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      eventCountBySelectionFilter: 2,
      expected: { isEnabled: false },
    },
    {
      description:
        '[pdfButton] logbook is not public, logbook is empty, a new Event Panel is opened',
      text: 'PDF',
      isNewButtonEnabled: false,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: false,
      },
      eventCountBySelectionFilter: 0,
      expected: { isEnabled: false },
    },
    {
      description:
        '[pdfButton] logbook is public, logbook is empty, no new Event Panel opened',
      text: 'PDF',
      isNewButtonEnabled: true,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      eventCountBySelectionFilter: 0,
      expected: { isEnabled: false },
    },
    {
      description:
        '[pdfButton] logbook is public, logbook is not empty, no new Event Panel opened',
      text: 'PDF',
      isNewButtonEnabled: true,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      eventCountBySelectionFilter: 2,
      expected: { isEnabled: true },
    },
    {
      description:
        '[pdfButton] logbook is public, logbook is not empty, a new Event Panel is opened',
      text: 'PDF',
      isNewButtonEnabled: false,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      eventCountBySelectionFilter: 2,
      expected: { isEnabled: false },
    },
    {
      description:
        '[pdfButton] logbook is public, logbook is empty, a new Event Panel is opened',
      text: 'PDF',
      isNewButtonEnabled: false,
      logbookContext: {
        name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
        isReleased: true,
      },
      eventCountBySelectionFilter: 0,
      expected: { isEnabled: false },
    },
  ],
};
