import moment from 'moment';
import {
  EVENT_CATEGORY_COMMANDLINE,
  EVENT_CATEGORY_COMMENT,
  EVENT_CATEGORY_ERROR,
  ANNOTATION,
  NOTIFICATION,
  EVENT_CATEGORY_INFO,
} from '../../../constants/eventTypes';

const ANNOTATION_FILTER = {
  $or: [
    { type: 'annotation' },
    {
      $and: [{ type: 'notification' }, { previousVersionEvent: { $ne: null } }],
    },
  ],
};

const SINGLE_ANNOTATION_FILTER = {
  $or: [ANNOTATION_FILTER],
};

module.exports = {
  getUserSpecificSelectionFilters: [
    {
      description: 'returns null when criteria is empty',
      criteria: [],
      expected: null,
    },
    {
      description: 'returns null when criterion.criteria is not set',
      criteria: [
        {
          // criteria: "Title",
          search: 'title 1',
          selectText: 'Title',
        },
      ],
      expected: null,
    },
    {
      description: 'returns null when criterion.search is not set',
      criteria: [
        {
          criteria: 'Title',
          //search: "title 1",
          selectText: 'Title',
        },
      ],
      expected: null,
    },
    {
      description: 'returns selection filter for a search based on title',
      criteria: [
        {
          criteria: 'Title',
          search: 'title 1',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            title: { $regex: '.*title 1.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on title in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Title',
          search: 'TITLE 1',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            title: { $regex: '.*TITLE 1.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on title in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'TITLE',
          search: 'title 1',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            title: { $regex: '.*title 1.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description: 'returns selection filter for a search based on author',
      criteria: [
        {
          criteria: 'Author',
          search: 'testName',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            username: { $regex: '.*testName.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on author in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Author',
          search: 'TESTNAME',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            username: { $regex: '.*TESTNAME.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on author in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'AUTHOR',
          search: 'testName',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            username: { $regex: '.*testName.*', $options: 'i' },
          },
        ],
      },
    },

    {
      description: 'returns selection filter for a search based on type',
      criteria: [
        {
          criteria: 'Type',
          search: 'testType',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            type: { $regex: '.*testType.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on type in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Type',
          search: 'TESTTYPE',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            type: { $regex: '.*TESTTYPE.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on type in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'TYPE',
          search: 'testType',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            type: { $regex: '.*testType.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description: 'returns null when criteria for date are not provided',
      criteria: [
        {
          criteria: 'Creation date',
          //search: "before",
          date: '12 Sep 2018',
          selectText: 'Creation date',
          momentDate: moment('2018-09-12'),
        },
      ],
      expected: null,
    },
    {
      description: 'returns null when criteria for date are not provided',
      criteria: [
        {
          criteria: 'Creation date',
          search: 'before',
          date: '12 Sep 2018',
          selectText: 'Creation date',
          //momentDate: Moment('2018-09-12')
        },
      ],
      expected: null,
    },
    {
      description: 'Search based on starting date only',
      criteria: [
        {
          criteria: 'Creation date',
          search: 'before',
          date: '12 Sep 2018',
          selectText: 'Creation date',
          momentDate: moment.utc('2018-09-12'),
        },
      ],
      expected: {
        $and: [
          {
            creationDate: {
              $lt: '2018-09-12T00:00:00.000Z',
            },
          },
        ],
      },
    },
    {
      description: 'Search based on ending date only',
      criteria: [
        {
          criteria: 'Creation date',
          search: 'after',
          date: '12 Sep 2018',
          selectText: 'Creation date',
          momentDate: moment.utc('2018-09-12'),
        },
      ],
      expected: {
        $and: [
          {
            creationDate: {
              $gte: '2018-09-12T00:00:00.000Z',
            },
          },
        ],
      },
    },
    {
      description: 'returns selection filter for a search based on content',
      criteria: [
        {
          criteria: 'Content',
          search: 'hello',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            'content.text': { $regex: '.*hello.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on content in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Content',
          search: 'HELLO',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            'content.text': { $regex: '.*HELLO.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on content in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'CONTENT',
          search: 'hello',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            'content.text': { $regex: '.*hello.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description: 'returns selection filter for a search based on category',
      criteria: [
        {
          criteria: 'Category',
          search: 'error',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            category: { $regex: '.*error.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on category in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Category',
          search: 'ERROR',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            category: { $regex: '.*ERROR.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on category in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'CATEGORY',
          search: 'error',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            category: { $regex: '.*error.*', $options: 'i' },
          },
        ],
      },
    },
    {
      description: 'returns selection filter for a search based on everywhere',
      criteria: [
        {
          criteria: 'Everywhere',
          search: 'hello',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            $or: [
              { title: { $regex: '.*hello.*', $options: 'i' } },
              { type: { $regex: '.*hello.*', $options: 'i' } },
              { category: { $regex: '.*hello.*', $options: 'i' } },
              { username: { $regex: '.*hello.*', $options: 'i' } },
              { 'content.text': { $regex: '.*hello.*', $options: 'i' } },
            ],
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on everywhere in a CASE INSENSITIVE way on the searched term',
      criteria: [
        {
          criteria: 'Everywhere',
          search: 'HELLO',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            $or: [
              { title: { $regex: '.*HELLO.*', $options: 'i' } },
              { type: { $regex: '.*HELLO.*', $options: 'i' } },
              { category: { $regex: '.*HELLO.*', $options: 'i' } },
              { username: { $regex: '.*HELLO.*', $options: 'i' } },
              { 'content.text': { $regex: '.*HELLO.*', $options: 'i' } },
            ],
          },
        ],
      },
    },
    {
      description:
        'returns selection filter for a search based on everywhere in a CASE INSENSITIVE way on the criteria term',
      criteria: [
        {
          criteria: 'EVERYWHERE',
          search: 'hello',
          selectText: 'Title',
        },
      ],
      expected: {
        $and: [
          {
            $or: [
              { title: { $regex: '.*hello.*', $options: 'i' } },
              { type: { $regex: '.*hello.*', $options: 'i' } },
              { category: { $regex: '.*hello.*', $options: 'i' } },
              { username: { $regex: '.*hello.*', $options: 'i' } },
              { 'content.text': { $regex: '.*hello.*', $options: 'i' } },
            ],
          },
        ],
      },
    },
    {
      description: 'returns the filter for multi criteria search',
      criteria: [
        {
          criteria: 'Content',
          search: 'beamline',
        },
        {
          criteria: 'Category',
          search: 'error',
        },
      ],
      expected: {
        $and: [
          { 'content.text': { $regex: '.*beamline.*', $options: 'i' } },
          { category: { $regex: '.*error.*', $options: 'i' } },
        ],
      },
    },
  ],

  getSelectionFiltersForMongoQuery: [
    {
      description: 'no criteria; sort by creationDate',
      criteria: [],
      SORT_EVENTS_BY: 'creationDate',
      SORTING_ORDER: -1,
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: {
        find: {
          $and: [SINGLE_ANNOTATION_FILTER],
        },
        sort: { creationDate: -1 },
      },
    },
    {
      description: 'no criteria; sort by _id',
      criteria: [],
      SORT_EVENTS_BY: '_id',
      SORTING_ORDER: 1,
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: {
        find: {
          $and: [SINGLE_ANNOTATION_FILTER],
        },
        sort: { _id: 1 },
      },
    },
    {
      description: 'one criteria; sort by creationDate',
      criteria: [{ criteria: 'Title', search: 'title 1', selectText: 'Title' }],
      SORT_EVENTS_BY: 'creationDate',
      SORTING_ORDER: -1,
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: {
        find: {
          $and: [
            SINGLE_ANNOTATION_FILTER,
            { $and: [{ title: { $regex: '.*title 1.*', $options: 'i' } }] },
          ],
        },
        sort: { creationDate: -1 },
      },
    },
  ],

  buildEventFilterForUserComment: [
    {
      description: 'returns the event filter for annotation type',
      expected: ANNOTATION_FILTER,
    },
  ],

  buildSingleEventFilter: [
    {
      description:
        'returns the single event filter for annotation without category',
      filter: { type: ANNOTATION },
      expected: ANNOTATION_FILTER,
    },
    {
      description:
        'returns the single event filter for annotation with category',
      filter: { type: ANNOTATION, category: EVENT_CATEGORY_COMMENT },
      expected: {
        $and: [{ type: 'annotation' }, { category: 'comment' }],
      },
    },
    {
      description:
        'returns the single event filter for notification with category',
      filter: { type: NOTIFICATION, category: EVENT_CATEGORY_INFO },
      expected: {
        $and: [{ type: 'notification' }, { category: 'info' }],
      },
    },
    {
      description:
        'returns the single event filter for notification without category',
      filter: { type: NOTIFICATION },
      expected: {
        $and: [{ type: 'notification' }],
      },
    },
  ],

  buildEventFilter: [
    {
      description: 'returns the event filter for only users comment',
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: SINGLE_ANNOTATION_FILTER,
    },
    {
      description: 'returns the event filter for only notifications',
      filterTypeCategory: [
        { type: NOTIFICATION, category: EVENT_CATEGORY_COMMANDLINE },
      ],
      expected: {
        $or: [
          {
            $and: [{ type: 'notification' }, { category: 'commandline' }],
          },
        ],
      },
    },
    {
      description:
        'returns the event filter for both notifications and annotation',
      filterTypeCategory: [
        { type: ANNOTATION },
        { type: NOTIFICATION, category: EVENT_CATEGORY_COMMENT },
        { type: NOTIFICATION, category: EVENT_CATEGORY_ERROR },
        { type: NOTIFICATION, category: EVENT_CATEGORY_COMMANDLINE },
      ],
      expected: {
        $or: [
          {
            $or: [
              { type: 'annotation' },
              {
                $and: [
                  { type: 'notification' },
                  { previousVersionEvent: { $ne: null } },
                ],
              },
            ],
          },
          {
            $and: [{ type: 'notification' }, { category: 'comment' }],
          },
          {
            $and: [{ type: 'notification' }, { category: 'error' }],
          },
          {
            $and: [{ type: 'notification' }, { category: 'commandline' }],
          },
        ],
      },
    },
    {
      description: 'returns the event filter for empty array',
      filterTypeCategory: [],
      expected: {
        $and: [{ type: 'undefined' }],
      },
    },
    {
      description:
        'returns the event filter for only notification without category',
      filterTypeCategory: [{ type: NOTIFICATION }],
      expected: {
        $or: [
          {
            $and: [{ type: 'notification' }],
          },
        ],
      },
    },
  ],

  buildSearchFilter: [
    {
      description:
        'returns the events specific filter when no user criteria are provided',
      criteria: [],
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: { $and: [SINGLE_ANNOTATION_FILTER] },
    },
    {
      description:
        'returns the events specific filter when criterion.criteria is not set',
      criteria: [
        {
          // criteria: "Title",
          search: 'title 1',
          selectText: 'Title',
        },
      ],
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: { $and: [SINGLE_ANNOTATION_FILTER] },
    },
    {
      description:
        'returns the events specific filter when criterion.search is not set',
      criteria: [
        {
          criteria: 'Title',
          //search: "title 1",
          selectText: 'Title',
        },
      ],
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: { $and: [SINGLE_ANNOTATION_FILTER] },
    },
    {
      description: 'returns the proper selection filters',
      criteria: [
        {
          criteria: 'Title',
          search: 'title 1',
          selectText: 'Title',
        },
      ],
      filterTypeCategory: [{ type: ANNOTATION }],
      expected: {
        $and: [
          SINGLE_ANNOTATION_FILTER,
          {
            $and: [
              {
                title: { $regex: '.*title 1.*', $options: 'i' },
              },
            ],
          },
        ],
      },
    },
  ],
};
