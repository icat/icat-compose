import {
  LATEST_EVENT_VERSION,
  ORIGINAL_EVENT_VERSION,
} from '../../../constants/eventTypes';
import {
  goodComment1,
  goodComment4,
  goodComment7,
  goodError1,
  goodError3,
  goodError3_OriginalVersion,
  goodError4,
  goodError4_OriginalVersion,
} from '../../GoodEventLibrary';

module.exports = {
  renderingParagraphWhenEventHasASingleVersion: [
    {
      description:
        'paragraph is displayed when the event has only a single version',
      event: goodComment1,

      expected: {
        paragraphCount: 1,
        paragraphText: 'This event has never been commented.',
      },
    },
    {
      description:
        'paragraph is not displayed when the event has more that one version (here 2 versions)',
      event: goodComment4,

      expected: {
        paragraphCount: 0,
      },
    },
    {
      description:
        'paragraph is not displayed when the event has more that one version (here 3 versions)',
      event: goodComment7,

      expected: {
        paragraphCount: 0,
      },
    },
  ],
  renderingEventVersionPanel: [
    {
      description: 'Annotation which has never been commented',
      event: goodComment1,

      expected: {
        eventVersionPanelCount: 1,
        first: {
          event: goodComment1,
          version: LATEST_EVENT_VERSION,
        },
      },
    },
    {
      description: 'Annotation which has been commented',
      event: goodComment4,

      expected: {
        eventVersionPanelCount: 1,
        first: {
          event: goodComment4,
          version: LATEST_EVENT_VERSION,
        },
      },
    },
    {
      description: 'Notification which has never been commented',
      event: goodError1,

      expected: {
        eventVersionPanelCount: 1,
        first: {
          event: goodError1,
          version: LATEST_EVENT_VERSION,
        },
      },
    },
    {
      description: 'Notification which has been commented',
      event: goodError3,

      expected: {
        eventVersionPanelCount: 2,
        first: {
          event: goodError3,
          version: LATEST_EVENT_VERSION,
        },
        second: {
          event: goodError3_OriginalVersion,
          version: ORIGINAL_EVENT_VERSION,
        },
      },
    },
    {
      description: 'Notification which has been commented 2 times',
      event: goodError4,

      expected: {
        eventVersionPanelCount: 2,
        first: {
          event: goodError4,
          version: LATEST_EVENT_VERSION,
        },
        second: {
          event: goodError4_OriginalVersion,
          version: ORIGINAL_EVENT_VERSION,
        },
      },
    },
  ],
  renderingEventFooter: [
    {
      description: 'check eventFooter is rendered',
      event: goodComment1,
      expected: {
        isSaveButtonEnabled: false,
        isSaveButtonVisible: false,
        buttonLabel: 'Close',
      },
    },
  ],
};
