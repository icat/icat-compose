module.exports = {
  rendering: [
    {
      description: 'Tests on info messages',
      message: 'This is an message',
      type: 'info',
      expected: 'info',
    },
    {
      description: 'Tests on success messages',
      message: 'This is an message',
      type: 'success',
      expected: 'success',
    },
    {
      description: 'Tests on error messages',
      message: 'This is an message',
      type: 'error',
      expected: 'danger',
    },
  ],
};
