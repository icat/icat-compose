import { createEventsWithContentEqualsId } from '../../helpers';

module.exports = {
  renderingLoadingAnimation: [
    {
      description:
        'shows animation when events to display are not downloaded yet',
      config: { EVENTS_PER_PAGE: 2, EVENTS_PER_DOWNLOAD: 2 },
      getEventResponse: createEventsWithContentEqualsId(1, 2),
      clickedPage: 2,
      isAnimationVisible: true,
    },
    {
      description:
        'shows animation even when events to display are already downloaded',
      config: { EVENTS_PER_PAGE: 2, EVENTS_PER_DOWNLOAD: 4 },
      getEventResponse: createEventsWithContentEqualsId(1, 4),
      clickedPage: 2,
      isAnimationVisible: false,
    },
  ],
  renderingEvents: [
    {
      description: 'On page 1, User clicks page 2 (events already downloaded)',
      config: {
        AUTOREFRESH_EVENTLIST: false,
        EVENTS_PER_PAGE: 2,
        EVENTS_PER_DOWNLOAD: 4,
      },
      startingPage: 1,
      clickedPage: 2,
      getEventsResponseOnStartingPage: createEventsWithContentEqualsId(1, 4),
      expected: {
        eventsOnStartingPage: createEventsWithContentEqualsId(1, 2),
        eventsOnClickedPage: createEventsWithContentEqualsId(3, 4),
        getEventsCallsCount: 0,
      },
    },
    {
      description: 'On page 3, User clicks page 4 (events already downloaded)',
      config: {
        AUTOREFRESH_EVENTLIST: false,
        EVENTS_PER_PAGE: 2,
        EVENTS_PER_DOWNLOAD: 4,
      },
      startingPage: 3,
      clickedPage: 4,
      getEventsResponseOnStartingPage: createEventsWithContentEqualsId(5, 8),
      expected: {
        eventsOnStartingPage: createEventsWithContentEqualsId(5, 6),
        eventsOnClickedPage: createEventsWithContentEqualsId(7, 8),
        getEventsCallsCount: 0,
      },
    },
    {
      description:
        'On page 5, user clicks last page (events already downloaded)',
      config: {
        AUTOREFRESH_EVENTLIST: false,
        EVENTS_PER_PAGE: 2,
        EVENTS_PER_DOWNLOAD: 4,
      },
      startingPage: 5,
      clickedPage: 6,
      getEventsResponseOnStartingPage: createEventsWithContentEqualsId(9, 11),
      expected: {
        eventsOnStartingPage: createEventsWithContentEqualsId(9, 10),
        eventsOnClickedPage: createEventsWithContentEqualsId(11, 11),
        getEventsCallsCount: 0,
      },
    },
    {
      description: 'On page 1, User clicks page 2 (events not downloaded)',
      config: {
        AUTOREFRESH_EVENTLIST: false,
        EVENTS_PER_PAGE: 2,
        EVENTS_PER_DOWNLOAD: 2,
      },
      startingPage: 1,
      clickedPage: 2,
      getEventsResponseOnStartingPage: createEventsWithContentEqualsId(1, 2),
      getEventsResponseForClickedPage: createEventsWithContentEqualsId(3, 4),
      expected: {
        eventsOnStartingPage: createEventsWithContentEqualsId(1, 2),
        eventsOnClickedPage: createEventsWithContentEqualsId(3, 4),
        getEventsCallsCount: 1,
      },
    },
    {
      description: 'When autorefresh occurs, the displayed events are the same',
      config: {
        AUTOREFRESH_EVENTLIST: true,
        AUTOREFRESH_DELAY: 100,
        EVENTS_PER_PAGE: 2,
        EVENTS_PER_DOWNLOAD: 2,
      },
      startingPage: 1,
      clickedPage: 2,
      getEventsResponseOnStartingPage: createEventsWithContentEqualsId(1, 2),
      getEventsResponseForClickedPage: createEventsWithContentEqualsId(3, 4),
      expected: {
        eventsOnStartingPage: createEventsWithContentEqualsId(1, 2),
        eventsOnClickedPage: createEventsWithContentEqualsId(3, 4),
        getEventsCallsCount: 1,
      },
    },
  ],
};
