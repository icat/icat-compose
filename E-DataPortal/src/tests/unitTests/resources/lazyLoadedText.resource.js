module.exports = {
  rendering: [
    {
      description: 'text with no <img>',
      text: '<p> I am happy </p>',
    },
    {
      description: 'text with a <img>',
      text:
        '<p> a comment  <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/> </p>',
    },
  ],
};
