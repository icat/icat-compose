const { createEventByAttributes } = require('../../helpers');

module.exports = {
  rendering: [
    {
      description: 'null when null "events" props provided',
      events: null,
      eventCountBySelectionFilter: 0,
    },
    {
      description: 'information message when empty "events" props provided',
      events: [],
      eventCountBySelectionFilter: 0,
    },
    {
      description: '1 event',
      events: [createEventByAttributes({})],
      eventCountBySelectionFilter: 1,
    },
  ],
};
