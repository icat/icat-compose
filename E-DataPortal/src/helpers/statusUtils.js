import { STATUS_DEFS } from '../constants/parcelStatuses';

export function isSendable(parcel) {
  return STATUS_DEFS[parcel.status].sendable;
}

export function isEditable(parcel) {
  return STATUS_DEFS[parcel.status].editable;
}
