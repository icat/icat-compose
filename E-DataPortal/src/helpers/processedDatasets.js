const INPUT_DATASETS_PARAMETER_NAME = 'input_datasets';

/**
 * This method returns the list of processed datasets that are attached to dataset
 * @param {*} dataset the dataset that is the input
 * @param {*} processedDatasets a list of datasets which will be searched by
 */
export function getProcessedDatasets(dataset, processedDatasets) {
  if (!processedDatasets) {
    return [];
  }
  return processedDatasets.filter((processedDataset) =>
    processedDataset.parameters.find(
      (p) =>
        p.name === INPUT_DATASETS_PARAMETER_NAME &&
        (p.value.indexOf(dataset.id) !== -1 || p.value === dataset.location)
    )
  );
}
