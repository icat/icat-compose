/** Get the index of the first event on the given page.
 * @param {number} page page of interest
 * @param {number} eventsPerPage number of events per page
 * @returns {number} index of the first event on the page
 */
export function getFirstIndexToDisplay(page, eventsPerPage) {
  if (!page || !eventsPerPage) {
    return 0;
  }

  return eventsPerPage * (page - 1);
}
