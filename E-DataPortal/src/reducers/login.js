import moment from 'moment';
import {
  LOGGED_IN,
  LOGIN_ERROR,
  LOG_IN,
  LOG_OUT,
} from '../constants/actionTypes';
import ICAT from '../config/icat';

const initialState = {
  username: null,
  isSSO: false,
  sessionId: null,
  expirationTime: null,
  fullName: null,
  name: null,
  isAnonymous: false,
  isAdministrator: false,
  isInstrumentScientist: false,
  isAuthenticating: false,
  isSessionExpired: false,
  usersByPrefix: [],
  error: null,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN: {
      state = {
        ...initialState,
        username: action.username,
        isSSO: action.username === null,
        isAuthenticating: true,
      };
      break;
    }
    case LOGGED_IN: {
      state = {
        ...initialState,
        username: state.username,
        isSSO: state.isSSO,
        sessionId: action.sessionId,
        expirationTime: moment()
          .add(action.lifeTimeMinutes, 'minutes')
          .format(),
        name: action.name,
        fullName: action.fullName,
        isAnonymous:
          ICAT.authentication.anonymous.enabled &&
          state.username === ICAT.authentication.anonymous.username,
        isAdministrator: action.isAdministrator,
        isInstrumentScientist: action.isInstrumentScientist,
        usersByPrefix: action.usersByPrefix,
      };
      break;
    }
    case LOG_OUT: {
      state = { ...initialState, isSessionExpired: action.expired };
      break;
    }
    case LOGIN_ERROR: {
      state = { ...initialState, error: action.error };
      break;
    }
    default:
      break;
  }
  return state;
};

export default user;
