import axios from 'axios';
import ICATPLUS from '../config/icatPlus';
import {
  LOGGED_IN,
  LOGIN_ERROR,
  LOG_IN,
  LOG_OUT,
} from '../constants/actionTypes';
import keycloak from '../keycloak';
import ICAT from '../config/icat';
import { trackSignIn } from '../helpers/trackData';

export function doLogOut(sessionId, params = { expired: false }) {
  if (sessionId) {
    axios.delete(`${ICATPLUS.server}/session/${sessionId}`);
  }
  return { type: LOG_OUT, ...params };
}

export function doSignIn(plugin, username, password) {
  return (dispatch) => {
    dispatch({ type: LOG_IN, username });

    axios
      .post(`${ICATPLUS.server}/session`, {
        plugin,
        username,
        password,
      })
      .then(({ data }) => {
        if (!data || !data.sessionId) {
          throw new Error('Invalid authentication response');
        }

        trackSignIn(plugin);
        const {
          sessionId,
          name,
          fullName,
          isAdministrator,
          isInstrumentScientist,
          lifeTimeMinutes,
          usersByPrefix,
        } = data;
        dispatch({
          type: LOGGED_IN,
          sessionId,
          name,
          fullName,
          isAdministrator,
          isInstrumentScientist,
          lifeTimeMinutes,
          usersByPrefix,
        });
      })
      .catch((error) => {
        console.error(error);
        dispatch({ type: LOGIN_ERROR, error: 'Authentication failed' });
      });
  };
}

export function doSilentRefreshFromSSO(usernameSuffix) {
  return (dispatch) => {
    axios
      .post(`${ICATPLUS.server}/session`, {
        plugin: ICAT.authentication.sso.plugin,
        username: null,
        password: keycloak.idToken,
        usernameSuffix,
      })
      .then(({ data }) => {
        if (!data || !data.sessionId) {
          throw new Error('Invalid re-authentication response');
        }

        const {
          sessionId,
          name,
          fullName,
          isAdministrator,
          isInstrumentScientist,
          lifeTimeMinutes,
          usersByPrefix,
        } = data;
        dispatch({
          type: LOGGED_IN,
          sessionId,
          name,
          fullName,
          isAdministrator,
          isInstrumentScientist,
          lifeTimeMinutes,
          usersByPrefix,
        });
      })
      .catch((error) => {
        console.error(error);
        dispatch({ type: LOGIN_ERROR, error: 'Re-authentication failed' });
      });
  };
}
