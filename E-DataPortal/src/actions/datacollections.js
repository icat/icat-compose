import { getDataCollections } from '../api/icat-plus/catalogue';
import { FETCH_DATACOLLECTIONS } from '../constants/actionTypes';

export function fetchDataCollections(sessionId) {
  return {
    type: FETCH_DATACOLLECTIONS,
    payload: getDataCollections(sessionId),
  };
}
