/**
 * icat.example.js configuration file
 */
const ICAT = {
  authentication: {
    /** Single sign-on configuration */
    sso: {
      enabled: true,
      /** ICAT authentication plugin for json web tokens */
      plugin: 'esrf',
      /** Configuration to be passed to keycloak.js (https://github.com/keycloak/keycloak-documentation/blob/master/securing_apps/topics/oidc/javascript-adapter.adoc) */
      configuration: {
        realm: 'ESRF',
        url: 'https://websso.esrf.fr/auth/',
        clientId: 'icat',
      },
    },
    /** Anonymous user's credentials*/
    anonymous: {
      enabled: true,
      /** ICAT plugin used for authentication. */
      plugin: 'db',
      /** Username for the anonymous user */
      username: 'reader',
      /** Password for the anonymous user */
      password: 'reader',
    },
    authenticators: [
      {
        plugin: 'esrf',
        /** title will be displayed as the title of the Tab at the login Form */
        title: 'ESRF',
        /** If will be hidden in the login form */
        hidden: false,
      },
      {
        plugin: 'db',
        title: 'Database',
        hidden: false,
      },
    ],
  },
};

export default ICAT;
