const ICATPLUS = {
  /** URL of ICAT+ server */
  server: process.env.REACT_APP_ICATPLUS_URL,
};

export default ICATPLUS;
