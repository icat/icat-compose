const ICAT = {
  authentication: {
    // Single sign-on configuration
    sso: {
      enabled: process.env.REACT_APP_SSO_AUTH_ENABLED,
      plugin: 'esrf',
      // Keycloak configuration - https://www.keycloak.org/docs/latest/securing_apps/#_javascript_adapter
      configuration: {
        realm: 'ESRF',
        url: 'https://websso.esrf.fr/auth/',
        clientId: 'icat',
      },
    },
    // Anonymous user's credentials
    anonymous: {
      enabled: process.env.REACT_APP_ANONYMOUS_AUTH_ENABLED,
      plugin: 'db',
      username: 'reader',
      password: 'reader',
    },
    authenticators: [
      // Database authenticator
      {
        plugin: 'db',
        title: 'Database',
        hidden: !process.env.REACT_APP_DB_AUTH_ENABLED,
        message:
          'ESRF, CRG staff or long term visitors, please sign in with ESRF SSO',
      },
    ],
  },
};

export default ICAT;
