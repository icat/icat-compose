import { parse } from 'date-fns';

const DATASET_TABLE_WIDGET_FACTORY = [
  {
    instrumentName: 'BM29',
    startDate: parse('25-09-2022', 'dd-MM-yyyy', new Date()),
    endDate: '',
    widget: 'BioSaxsInlineDatasetTable',
  },
];

export default DATASET_TABLE_WIDGET_FACTORY;
