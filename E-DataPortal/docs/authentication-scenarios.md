# Authentication scenarios

## Login with form or as anonymous

### 1. Login/logout

The user arrives on the portal for the first time (or after having previously logged out).

- The user is redirected to the login page where they can log in with a username/password form.
- Upon successful log-in, they are redirected to the homepage.
- If they log out manually, they are redirected back to the login page.

### 2. Session restoration

The user arrives on the portal with a persisted session that is still valid, or the user is logged in to the portal and refreshes the page.

- The persisted session is restored.
- The user lands directly on the logged-in homepage.

### 3. Auto-logout

The user arrives on the portal with a persisted session that has expired, or is about to expire (in less than 5 minutes).

- The persisted session is cleared.
- The user is redirected to the login page.

#### 4. Expiry while browsing

The user is on the portal and their session is about to expire (in 1 minute).

- The user is logged out automatically and redirected to the login page.

## Login via SSO

### 1. Login/logout

The user is logged out of SSO and arrives on the portal for the first time (or after having previously logged out).

- The user is redirected to the login page where they can log in with SSO.
- When logging in, they are redirected to the SSO login page and then back to the portal's logged-in hompage.
- If they log out manually from the portal, they are redirected to the SSO logout page and then back to the portal; since they are now logged out, they are redirected to the login page.

### 2. Session restoration

The user is logged in to SSO and arrives on the portal with a persisted session that is still valid (e.g. the user refreshes the page, or leaves and comes back soon after).

- The persisted session is restored.
- The user lands directly on the logged-in homepage.

### 3. Auto-refresh

The user is logged in to SSO and arrives on the portal with a persisted session that has expired, or is about to expire (in less than 5 minutes).

- The persisted session is cleared.
- The user lands on the login page and sees a spinner as they are being logged back in to the portal automatically.
- They are then redirected to the logged-in homepage.

### 4. Auto-login

The user has logged in to SSO externally and arrives on the portal with no persisted session.

- The user lands on the login page and sees a spinner as they are being logged in to the portal automatically.
- They are then redirected to the logged-in homepage.

> If the user was planning on logging in without SSO (via a username/password form or as anonymous), they can just log out and log back in however they want.

### 5. Auto-logout

The user has logged out of SSO externally, or their SSO session has expired, and they arrive on the portal with a persisted session that was created via SSO.

- The persisted session is cleared.
- The user is redirected to the login page.

#### 6. Expiry while browsing

The user is on the portal and their session is about to expire (but their SSO session is still active).

- The user's session is refreshed silently.

### 7. SSO expiry while browsing

The user is on the portal and their SSO session expires (i.e. their SSO access token expires and cannot be refreshed).

- The user is logged out automatically and redirected to the login page.
