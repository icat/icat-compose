# datahub

datahub is a frontend available as a [Node.js](https://nodejs.org/en/) package. It is based on the [React](https://reactjs.org/) framework. Through a web interface, this application provides:

- a user authentication portal
- a GUI for scientific metadata management
- a GUI for the electronic logbook which can complement data
- experimental data downloading feature
- manual DOI minting feature

Currently, datahub depends on:

- ICAT+ package. ICAT+ is the backend which stores logbook events
- [ICAT](https://icatproject.org) metadata catalogue. ICAT stores metadata and handles the user authentication mechanism

# Menu

- [Installation](#installation)
- [Configuration](#configuration)
- [Development](#development)

# Installation

1. Make sure the dependencies mentioned above are installed and properly configured.
1. Clone or download the project from [GitLab](https://gitlab.esrf.fr/icat/E-DataPortal)
1. Install the dependencies

   ```bash
   npm install
   ```

1. Start the app

   ```bash
   npm start
   ```

# Configuration

Datahub configuration is spread among different files located in <kbd>src/config</kbd>. These files are:

- `icat.js` - configure access to metadata catalogue
- `icatPlus.js` - configure access to ICAT+ server
- `ids.js` - configure IDs server for downloading datasets
- [`techniques.js`](#techniquesjs) - configure known techniques
- [`ui.js`](#uijs) - general UI configuration

Some of the configuration options in these files are stored in [environment variables](#environment-variables).

## Environment variables

Create React App loads environment variables from [environment files](https://create-react-app.dev/docs/adding-custom-environment-variables/#what-other-env-files-can-be-used) (<kbd>.env</kbd>, <kbd>.env.test</kbd>, etc.) according to the Node environment (`NODE_ENV`).
File <kbd>.env</kbd> is loaded in every environment with the lowest priority and is therefore used to declare default values and development fallbacks.

The following portal configuration variables are declared in <kbd>.env</kbd>:

- `REACT_APP_ICATPLUS_URL` - the URL of the ICAT+ server (used in <kbd>src/config/icatPlus.js</kbd>)
- `REACT_APP_SSO_AUTH_ENABLED` - whether to allow users to sign-in to ICAT with SSO (used in <kbd>src/config/icat.js</kbd>)
- `REACT_APP_ANONYMOUS_AUTH_ENABLED` - whether to allow users to sign-in to ICAT as anonymous (used in <kbd>src/config/icat.js</kbd>)
- `REACT_APP_DB_AUTH_ENABLED` - whether to allow users to sign-in to ICAT with database credentials (used in <kbd>src/config/icat.js</kbd>)

In development, you can override any of these variables by creating a file called `.env.local`. This file is ignored from version control.

## `techniques.js`

The file [techniques.js](src/config/techniques/techniques.js) complements short named techniques as stored in ICAT metadata catalogue. It maps a short name to a description and display settings.

To add a new technique, use the following object to fully define the technique.

```js
/** Object defining the new technique */
{
/** Full name of the technique */
name: "Ptychography",
/** Short name of the technique as stored in ICAT */
shortname: "PTYCHO",
/** Full description of the technique */
description: "Ptychography is a technique invented by Walter Hoppe that aims to solve the diffraction-pattern phase problem by interfering adjacent Bragg reflections coherently and thereby determine their relative phase.",
/** Font color */
color: "#97E0FE"
},
```

## `ui.js`

See [ui.js](https://gitlab.esrf.fr/icat/E-DataPortal/-/blob/master/src/config/ui.js)

# Development

- [Tests](#tests)
- [Code quality](#code-quality)
  - [Automatic fixing and formatting](#automatic-fixing-and-formatting)
  - [Editor integration](#editor-integration)
- [Widgets](#Widgets)
  - [ResponsiveTable](#responsivetable)
  - [Loader](#loader)

## Tests

Run:

```
npm test
```

## Code quality 🔎

- `npm run lint` - run linting and code formatting commands
- `npm run lint:eslint` - lint all JS files with ESLint (this command will report warnings as errors)
- `npm run lint:prettier` - check that all files have been formatted with Prettier

### Automatic fixing and formatting

- `npm run lint:eslint -- --fix` - auto-fix linting issues
- `npm run lint:prettier -- --write` - format all files with Prettier

### Editor integration

Most editors support fixing and formatting files automatically on save. The configuration for VSCode is provided out of
the box, so all you need to do is install the recommended extensions.

## Widgets

### ResponsiveTable

This widget is based on react-bootstrap-table2 (https://react-bootstrap-table.github.io/react-bootstrap-table2/) but adds responsiveness.

Columns can now be configured based on the size of the device as bootstrap does (xs, sm, md, lg)

Configuration:

```js
const columns = [
  {
    text: 'Proposal',
    dataField: 'name',
    headerStyle: (column, colIndex) => {
      return { width: '50%', textAlign: 'center' };
    },
    responsiveHeaderStyle: {
      xs: { width: '20%', textAlign: 'center' },
      sm: { width: '50%', textAlign: 'center' },
      md: { width: '70%', textAlign: 'center' },
      lg: { width: '90%', hidden: true, textAlign: 'center' },
    },
  },
];

var pageOptions = {
  showTotal: true,
  hidePageListOnlyOnePage: true,
  sizePerPageList: [
    {
      text: '5',
      value: 5,
    },
    {
      text: '10',
      value: 10,
    },
    {
      text: 'All',
      value: data.length,
    },
  ],
};

<ResponsiveTable data={data} columns={columns} pageOptions={pageOptions} />;
```

If there is no responsiveHeaderStyle then headerStyle will be used as default. If there is no the used screen size (xs, sm, md, lg) configured in the responsiveHeaderStyle attribute then headerStyle will be used by default.

You may specify which field of the table is unique by passing a `keyField` prop. If you omit it, the default is `"id"`.

### Loader

It informs users that data are still been loaded

```js
// With default loading message
<Loader />
```

```js
// With custom loading message and space above/below
<Loader message="Loading authors..." spacedOut />
```

```js
// When used inside a Bootstrap `Panel`
<Loader message="Loading datasets..." inPanel />
```
