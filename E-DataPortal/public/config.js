/* eslint-disable no-unused-vars */
// eslint-disable-next-line import/unambiguous
const MATOMO = {
  isActive: true,
  server: 'https://www.esrf.fr/piwik/',
  siteId: '12',
  tracker: 'matomo.php',
  src: 'matomo.js',
};
