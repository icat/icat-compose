
#!/bin/bash

names=`jq '.users' $1  | jq '.[].name'`
passwords=`jq '.users' $1  | jq '.[].password'`

array_names=($names)
array_passwords=($passwords)

for index in "${!array_names[@]}";
do    
    mysql -u $2 -p$3 -h database -P $4 -D icat -e "insert into PASSWD (USERNAME, ENCODEDPASSWORD) VALUES (${array_names[$index]},${array_names[$index]})"  
done
