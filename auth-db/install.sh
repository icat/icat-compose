

asadmin start-domain icat && cd $1 \
&& python $1/setup configure \
&& python $1/setup install \
&& asadmin stop-domain icat 