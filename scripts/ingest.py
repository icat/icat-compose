#! /usr/bin/python
from __future__ import print_function
import icat
import icat.config
import json


config = icat.config.Config(ids="optional")
client, conf = config.getconfig()
client.login(conf.auth, conf.credentials)


# QuerIES
FACILITY_BY_NAME = "SELECT facility FROM Facility facility where facility.name='{}'"
USER_BY_NAME = "SELECT user FROM User user where user.name='{}'"
INVESTIGATION_BY_NAME_AND_VISITID = "SELECT investigation FROM Investigation investigation where investigation.name='{}' and investigation.visitId='{}'"
INSTRUMENT_BY_NAME = (
    "SELECT instrument FROM Instrument instrument where instrument.name='{}'"
)
GROUPING_BY_NAME = "SELECT grouping FROM Grouping grouping where grouping.name='{}'"

# Opening JSON file
f = open("data.json")
data = json.load(f)

# Adding facilities
if "facilities" in data:
    for facility in data["facilities"]:
        facitilyRecord = client.new("facility")
        facitilyRecord.name = facility["name"]
        facitilyRecord.fullName = facility["fullName"]
        client.create(facitilyRecord)

# Adding grouping
if "groupings" in data:
    for name in data["groupings"]:
        print("Creating grouping:  %s" % (name))
        groupingRecord = client.new("grouping")
        groupingRecord.name = name
        client.create(groupingRecord)

if "publicSteps" in data:
    for publicStep in data["publicSteps"]:
        publicStepRecord = client.new("publicStep")
        publicStepRecord.origin = publicStep["origin"]
        publicStepRecord.field = publicStep["field"]
        client.create(publicStepRecord)

if "instruments" in data:
    for instrument in data["instruments"]:
        instrumentRecord = client.new("instrument")
        instrumentRecord.name = instrument["name"]
        instrumentRecord.fullName = instrument["fullName"]
        instrumentRecord.facility = client.search(
            FACILITY_BY_NAME.format(instrument["facility"])
        )[0]
        client.create(instrumentRecord)

if "parameterTypes" in data:
    for parameterType in data["parameterTypes"]:
        parameterTypeRecord = client.new("parameterType")
        parameterTypeRecord.name = parameterType["name"]
        parameterTypeRecord.units = parameterType["units"]
        if "applicableToDataset" in parameterType:
            parameterTypeRecord.applicableToDataset = parameterType[
                "applicableToDataset"
            ]
        if "applicableToDataCollection" in parameterType:
            parameterTypeRecord.applicableToDataCollection = parameterType[
                "applicableToDataCollection"
            ]
        parameterTypeRecord.valueType = "STRING"  # TODO: check python icat, it's not mandatory https://repo.icatproject.org/site/icat/server/4.9.0/schema.html#ParameterType
        parameterTypeRecord.facility = client.search(
            FACILITY_BY_NAME.format(parameterType["facility"])
        )[0]
        client.create(parameterTypeRecord)


if "investigationType" in data:
    for investigationType in data["investigationType"]:
        investigationTypeRecord = client.new("investigationType")
        investigationTypeRecord.name = investigationType["name"]
        investigationTypeRecord.facility = client.search(
            FACILITY_BY_NAME.format(instrument["facility"])
        )[0]
        client.create(investigationTypeRecord)

if "users" in data:
    # Adding Users and userGroup
    for user in data["users"]:
        root = client.new("user")
        root.name = user["name"]
        root.fullName = user["fullName"]
        root.email = user["email"] if "email" in user else ""
        root.orcidId = user["orcidId"] if "orcidId" in user else ""
        client.create(root)

        if "grouping" in user:
            user_group = client.new("userGroup")
            users = client.search(USER_BY_NAME.format(user["name"]))
            user_group.user = users[0]
            groupings = client.search(GROUPING_BY_NAME.format(user["grouping"]))
            user_group.grouping = groupings[0]
            client.create(user_group)

if "instrumentScientists" in data:
    for instrumentScientist in data["instrumentScientists"]:
        instrumentScientistRecord = client.new("instrumentScientist")
        instrumentScientistRecord.user = client.search(
            USER_BY_NAME.format(instrumentScientist["userName"])
        )[0]

        instrumentScientistRecord.instrument = client.search(
            "SELECT instrument FROM Instrument instrument where instrument.name='{}'".format(
                instrumentScientist["instrumentName"]
            )
        )[0]
        client.create(instrumentScientistRecord)

if "datasetTypes" in data:
    for datasetType in data["datasetTypes"]:
        datasetTypeRecord = client.new("datasetType")
        datasetTypeRecord.name = datasetType["name"]
        datasetTypeRecord.facility = client.search(
            FACILITY_BY_NAME.format(datasetType["facility"])
        )[0]
        client.create(datasetTypeRecord)

if "investigations" in data:
    for investigation in data["investigations"]:
        investigationRecord = client.new("investigation")
        investigationRecord.name = investigation["name"]
        investigationRecord.title = investigation["title"]
        investigationRecord.visitId = investigation["visitId"]
        investigationRecord.summary = investigation["summary"]
        investigationRecord.summary = investigation["summary"]
        investigationRecord.startDate = investigation["startDate"]
        investigationRecord.endDate = investigation["endDate"]
        if "releaseDate" in investigation:
            investigationRecord.releaseDate = investigation["releaseDate"]
        if "doi" in investigation:
            investigationRecord.doi = investigation["doi"]
        investigationRecord.facility = client.search(
            FACILITY_BY_NAME.format(instrument["facility"])
        )[0]
        investigationRecord.type = client.search(
            "SELECT investigationType FROM InvestigationType investigationType where investigationType.name='{}'".format(
                investigation["investigationType"]
            )
        )[0]
        client.create(investigationRecord)

        investigationRecord = client.search(
            INVESTIGATION_BY_NAME_AND_VISITID.format(
                investigation["name"], investigation["visitId"]
            )
        )[0]

        if "datasets" in investigation:
            for dataset in investigation["datasets"]:
                if "sample" in dataset:
                    sampleRecord = client.new("sample")
                    sampleRecord.name = dataset["sample"]["name"]
                    sampleRecord.investigation = investigationRecord
                    sampleId = client.create(sampleRecord)
                else:
                    sampleId = None

                datasetRecord = client.new("dataset")
                datasetRecord.name = dataset["name"]
                if sampleId is not None:
                    datasetRecord.sample = client.search(
                        "SELECT sample from Sample sample where sample.name='{}'".format(
                            dataset["sample"]["name"]
                        )
                    )[0]
                datasetRecord.investigation = investigationRecord
                datasetRecord.type = client.search(
                    "SELECT datasetType from DatasetType datasetType where datasetType.name='{}'".format(
                        dataset["type"]
                    )
                )[
                    0
                ]  # TODO: check python icat, it's not mandatory https://repo.icatproject.org/site/icat/server/4.9.0/schema.html#Dataset

                datasetId = client.create(datasetRecord)

                datasetRecord = client.search(
                    "SELECT dataset from Dataset dataset where dataset.id={}".format(
                        datasetId
                    )
                )[0]
                if "datafiles" in dataset:
                    for datafile in dataset["datafiles"]:
                        datafileRecord = client.new("datafile")
                        datafileRecord.name = datafile["name"]
                        datafileRecord.location = datafile["location"]
                        datafileRecord.dataset = datasetRecord
                        client.create(datafileRecord)

                if "parameters" in dataset:
                    for parameter in dataset["parameters"]:
                        datasetParameterRecord = client.new("datasetParameter")
                        datasetParameterRecord.dataset = datasetRecord
                        datasetParameterRecord.type = client.search(
                            "SELECT parameter from ParameterType parameter where parameter.name='{}'".format(
                                parameter["name"]
                            )
                        )[0]
                        datasetParameterRecord.stringValue = parameter["stringValue"]
                        client.create(datasetParameterRecord)

        if "investigationInstruments" in investigation:
            for investigationInstruments in investigation["investigationInstruments"]:
                investigationInstrumentsRecord = client.new("investigationInstrument")
                investigationInstrumentsRecord.investigation = investigationRecord
                investigationInstrumentsRecord.instrument = client.search(
                    INSTRUMENT_BY_NAME.format(investigationInstruments["name"])
                )[0]
                client.create(investigationInstrumentsRecord)

        if "investigationUsers" in investigation:
            for investigationUser in investigation["investigationUsers"]:
                investigationUserRecord = client.new("investigationUser")
                investigationUserRecord.role = investigationUser["role"]
                investigationUserRecord.user = client.search(
                    USER_BY_NAME.format(investigationUser["name"])
                )[0]
                investigationUserRecord.investigation = investigationRecord
                client.create(investigationUserRecord)


client.logout()
print("Ingest finished and logged out")
