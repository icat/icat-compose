# ICAT COMPOSE

Docker compose file to run the whole software stack. This is not conceived to be run in production environments but as a tool for developing/testing.

It uses docker profiles:

- Core: the services that can be considered the core of the metadata catalogue
- ICAT+: the extension of the classic metadata catalogue

It contains:

- [CORE] **MariaDB**: used by ICAT
- [CORE] **MongoDB**: used by ICAT+ (logbook, gallery and sample tracking)
- [CORE] **Mongo Express**: web app to explore mongo database
- [CORE] **ICAT**: The ICAT metadata catalogue
- [CORE] **ICAT DB Authenticator**: Official plugin for authentication based on DB
- [ICAT+] **ICAT+**: NodeJS application that exposes the metadata catalogue, the logbook endpoints, sample tracking, panosc APIs, etc...
- [ICAT+] **E-Dataportal**: React front-end that integrates all the needs from the user in terms of data browsing, tracking and logbook
- Ingestion of:
  - [CORE] Rules
  - Parameters
  - [CORE] Synthetic data

## Installation

### Requirements

It requires docker compose >= 2.5

### Configuration

Environment variables can be configured in the [.env](./.env)

## Getting started

### Build

```
docker compose --profile core --profile icat+  build

```

### Run

```
docker compose --profile core --profile icat+ up
```

By doing this you will find:

- Mongo express on http://localhost:8888
- Mongo database on port 27777
- Maria database on port 3333
- ICAT running on http://localhost:8080
- Portal running on http://localhost:3000

# Populating the database

WIP

Python-icat is used to ingest the data provided as a json file in the folder data. [Example](./data/data.json)
