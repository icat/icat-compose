/**
 * restore email configuration file
 */
module.exports = {
  enabled: true,
  fromAddress: "restorerequests@test.fr",
  subject: "[ESRF Data Restoration] Dataset restoration request",
  smtp: {
    host: "smtp.test.fr",
    port: 25,
  },
};
