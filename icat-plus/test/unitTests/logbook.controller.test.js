require("it-each")({ testPerIteration: true });

const resources = require("./resources/logbook.controller.resource");
const eventControllerHelper = require("../../app/controllers/helpers/helper.logbook.controller");

describe("Units tests on logbook.controller.js", () => {
  describe("replaceImageSrc()", () => {
    it.each(resources.replaceImageSrc, "[replaceImageSrc] %s", ["description"], (element, next) => {
      const updatedEvents = eventControllerHelper.replaceImageSrc(element.events, element.sessionId, element.serverURI);
      expect(updatedEvents).to.deep.equal(element.expected);
      next();
    });
  });

  describe("translateEventContentToHtml()", () => {
    it.each(resources.translateEventContentToHtml, "[translateEventContentToHtml]", [], (element, next) => {
      const translatedEventContent = eventControllerHelper.translateEventContentToHtml(element.content);
      expect(translatedEventContent).to.deep.equal(element.expected);
      next();
    });
  });
});
