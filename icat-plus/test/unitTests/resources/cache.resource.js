module.exports = {
  getUserByNamePattern: [
    {
      name: "/265147",
      expected: {
        name: "bodin/265147",
      },
    },
    {
      name: "/123",
      expected: {},
    },
  ],
};
