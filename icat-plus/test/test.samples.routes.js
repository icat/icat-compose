const sampleResource = require("./resources/sample.resource.js");
const sampleHelper = require("./helper/sample.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("/catalogue/:sessionId/investigation/id/:investigationId/sample", () => {
  it.each(sampleResource.getSamplesByInvestigationId, "%s", ["description"], async (element, next) => {
    const response = await sampleHelper.getSamplesByInvestigationId(element.user, element.investigationId);
    expect(response.status).to.equal(element.expected.status);
    expect(response.body).to.be.an("array");
    return next();
  });
});
