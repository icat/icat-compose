const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
module.exports = {
  request: [
    {
      endPoint: "/h5grove/meta",
      description: "Anoynmous can not get a file that is not public",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/attr",
      description: "Anoynmous can not get a file that is not public",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/data",
      description: "Anoynmous can not get a file that is not public",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/stats",
      description: "Anoynmous can not get a file that is not public",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
  ],
};
