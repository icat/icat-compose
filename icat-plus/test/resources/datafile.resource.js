const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const investigationUser = globaResource.users.investigationUser;
const administrator = globaResource.users.administrator;

module.exports = {
  byDatasets: [
    {
      description: "Investigation user gets files by dataset id",
      datasetId: 123755487,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 1,
      },
    },
    {
      description: "Anonymous gets files by dataset id, there is not middleware but it should retrieve an empty list from ICAT",
      datasetId: 123755487,
      user: anonymous.credential,
      expected: {
        status: 200,
        fileCount: 0,
      },
    },
    {
      description: "Administrator gets files by dataset id",
      datasetId: 123755487,
      user: administrator.credential,
      expected: {
        status: 200,
        fileCount: 1,
      },
    },
    {
      description: "Investigation user gets files by dataset id with limit",
      datasetId: 117991666,
      limit: 3,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 3,
      },
    },
    {
      description: "Investigation user gets files by dataset id with limit and skip",
      datasetId: 117991666,
      limit: 3,
      skip: 2,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 3,
      },
    },
    {
      description: "Investigation user gets files by dataset id with search",
      datasetId: 117991666,
      limit: 20,
      search: "E10_overview_xia00_0001_0000_0182",
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 1,
      },
    },
    {
      description: "Investigation user gets files by multiple dataset id",
      datasetId: "117991666,123755487",
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 534,
      },
    },
  ],
};
