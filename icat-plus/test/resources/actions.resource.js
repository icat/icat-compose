const globaResource = require("./global.resource.js");
const CONSTANTS = require("../../app/constants");
const anonymous = globaResource.users.anonymous;
const administrator = globaResource.users.administrator;
const investigationUser = globaResource.users.investigationUser;

module.exports = {
  fill: [
    {
      description: "Administrators triggers the action to fill the instrumentName",
      user: administrator.credential,
      logbook: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [{ format: "plainText", text: "this is my second" }],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [{ format: "plainText", text: "this is my second" }],
        },
      ],
      expected: {
        status: 200,
        missing: 0,
        count: 2,
        updated: 2,
      },
    },
    {
      description: "Anonymous triggers the action to fill the instrumentName",
      user: anonymous.credential,
      logbook: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [{ format: "plainText", text: "this is my second" }],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [{ format: "plainText", text: "this is my second" }],
        },
      ],
      expected: {
        status: 403,
      },
    },
  ],
};
