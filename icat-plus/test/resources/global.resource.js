module.exports = {
  investigationTypes: {
    industrials: ["FX", "IX", "IN", "IM"],
  },
  instruments: [
    {
      name: "ID01",
    },
    {
      name: "ID19",
    },
    {
      name: "ID21",
    },
  ],
  doi: {
    public: [
      { prefix: "10.15151", suffix: "ESRF-ES-34497655" },
      { prefix: "10.5072", suffix: "ESRF-DC-123753799" },
    ],
  },
  addresses: {
    valid: {
      complete: {
        name: "Jonh",
        surname: "Doe",
        companyName: "ESRF",
        address: "RUE DE LA POSTE",
        city: "Grenoble",
        region: "Rhone-Alpes/Isere",
        postalCode: "38000",
        email: "esrf@esrf.fr",
        phoneNumber: "123123123",
        country: "France",
      },
    },
  },
  users: {
    principalInvestigator: {
      credential: {
        plugin: "db",
        username: "principalInvestigator",
        password: "principalInvestigator",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 63335100,
        },
      },
    },
    localContact: {
      credential: {
        plugin: "db",
        username: "localContact",
        password: "localContact",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 63335100,
        },
      },
    },
    administrator: {
      name: "admin",
      fullName: "adminFullName",
      credential: {
        plugin: "db",
        username: "admin",
        password: "test_admin",
      },
      investigations: {
        administrator: {
          investigationId: 123398449, //any investigationId would fit
        },
      },
    },
    anonymous: {
      name: "reader",
      credential: {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
      investigations: {
        underEmbargo: {
          investigationId: 24211970,
        },
        released: {
          investigationId: 123845948,
        },
      },
    },
    minter: {
      name: "minter",
      credential: {
        plugin: "db",
        username: "minter",
        password: "givemeadoi",
      },
    },

    instrumentScientist: {
      name: "instrumentScientist",
      fullName: "instrumentScientist",
      credential: {
        plugin: "db",
        username: "instrumentScientist",
        password: "instrumentScientist",
      },
      instrument: { name: "id01" },
      investigations: {
        instrumentScientist: {
          investigationId: 63335100,
          instrumentName: "id01",
        },
      },
    },
    investigationUser: {
      name: "investigationUser",
      fullName: "investigationUserFullName",
      email: "investigationUser@test.fr",
      credential: {
        plugin: "db",
        username: "investigationUser",
        password: "investigationUser",
      },
      investigations: {
        participates: [
          {
            investigationId: 63335100,
            investigationName: "ID010100",
            instrumentName: "id01",
            datasets: {
              datasetWithNoSample: {
                id: 123755487,
              },
            },
            samples: [
              {
                id: 63335950,
              },
            ],
          },
          {
            investigationId: 116934142,
            investigationName: "EV-358",
            instrumentName: "id21",
          },
        ],
        noAccess: {
          investigationId: 2483226,
        },
      },
    },

    allowed: [
      {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
    ],
    denied: [
      {
        plugin: "db",
        username: "reader",
        password: "badpassword",
      },
    ],
  },
};
