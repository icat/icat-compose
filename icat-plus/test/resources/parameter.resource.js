const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const investigationUser = globaResource.users.investigationUser;
const administrator = globaResource.users.administrator;
const instrumentScientist = globaResource.users.instrumentScientist;

module.exports = {
  getParameters: [
    {
      description: "Get all parameterTypes",
      expected: { status: 200 },
    },
    {
      description: "Get parameterType by id",
      parameterTypeId: 167658,
      expected: { status: 200 },
    },
    {
      description: "Get parameterType by name",
      name: "attenuators_labels",
      expected: { status: 200 },
    },
    {
      description: "Get parameterType with invalid id",
      parameterTypeId: 12312312131,
      expected: { status: 400 },
    },
  ],
  updateParameter: [
    {
      description: "InvestigationUser cannot update dataset parameter",
      parameterId: 100932482,
      value: "motors ",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous cannot update dataset parameter",
      parameterId: 100932482,
      value: "motors ",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot update dataset parameter",
      parameterId: 100932482,
      value: "motors ",
      user: instrumentScientist.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can update dataset parameter",
      parameterId: 100932482,
      value: "motors ",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator cannot update dataset parameter without parameterId",
      parameterId: "",
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot update dataset parameter with undefined value",
      parameterId: 100932482,
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot update dataset parameter if parameter does not exist",
      parameterId: 12,
      value: "motors ",
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
  ],
};
