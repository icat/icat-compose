const globalResource = require("./global.resource");

const investigationTypes = globalResource.investigationTypes;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const anonymous = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;

module.exports = {
  allocateInvestigations: [
    {
      user: administrator.credential,
      description: "Allocate proposal with a single session when time in between the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T10:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal with a single session when time is 6 hours before the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T02:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal with a single session when time is 8 hours before the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T00:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal with a single session when time is 8 hours and 1 second before the time slot but it has a single session",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-25T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time in between the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T10:00:00.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 6 hours before the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T02:00:00.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 8 hours before the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T00:00:01.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 48 hours and 1 second before the time slot but it has multiple sessions",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-25T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },

    {
      user: instrumentScientist.credential,
      description: "InstrumentScientist can allocate proposal when time is in the range 48 hours",
      investigationName: "ID010100",
      instrumentName: "ID01",
      time: "2017-10-26T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "id01",
      },
    },
    {
      user: investigationUser.credential,
      description: "investigationUser can allocate proposal when time is in the range 48 hours",
      investigationName: "ID010100",
      instrumentName: "ID01",
      time: "2017-10-26T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "id01",
      },
    },
    {
      user: administrator.credential,
      description: "Administrator can allocate proposal when time is in the range 48 hours",
      investigationName: "ID010100",
      instrumentName: "ID01",
      time: "2017-10-26T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "id01",
      },
    },
    {
      user: anonymous.credential,
      description: "anonymous can not allocate proposal when time is in the range 48 hours",
      investigationName: "ID010100",
      instrumentName: "ID01",
      time: "2017-10-26T23:59:59.000Z",
      expected: {
        status: 404,
      },
    },
  ],
  normalize: [
    {
      input: "20-011219",
      expected: "20-01-1219",
    },
    {
      input: "MX0000",
      expected: "MX-0000",
    },
    {
      input: "mx2260",
      expected: "MX-2260",
    },
    {
      input: "ihls3203",
      expected: "IH-LS-3203",
    },
    {
      input: "ihls3203",
      expected: "IH-LS-3203",
    },
    {
      input: "hc4170",
      expected: "HC-4170",
    },
    {
      input: "ih-ls-3203",
      expected: "IH-LS-3203",
    },
    {
      input: "ev280",
      expected: "EV-280",
    },
    {
      input: "ip0001",
      expected: "IP-0001",
    },
    {
      input: "id000000",
      expected: "ID000000",
    },
    {
      input: "bm230002",
      expected: "BM230002",
    },
    {
      input: "bM23-0002",
      expected: "BM230002",
    },
    {
      input: "BM230002",
      expected: "BM230002",
    },
  ],
  investigation: [
    {
      description: "Get investigation by participant",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  investigations: [
    {
      description: "Investigation User  investigations by sessionId",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets investigations by sessionId",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets investigations by sessionId",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getInvestigationByReleasedStatus: [
    {
      description: "Anonymous gets all released investigations",
      user: anonymous.credential,
      status: "released",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "InvestigationUser gets all released investigations",
      user: investigationUser.credential,
      status: "released",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all released investigations",
      user: administrator.credential,
      status: "released",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all released investigations limit to 1",
      user: administrator.credential,
      status: "released",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "InvestigationUser gets all released investigations limit to 1",
      user: investigationUser.credential,
      status: "released",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations after a date",
      user: investigationUser.credential,
      status: "released",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations before a date",
      user: investigationUser.credential,
      status: "released",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations sorted by name",
      user: investigationUser.credential,
      status: "released",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations filtered by search",
      user: investigationUser.credential,
      status: "released",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationByEmbargoedStatus: [
    {
      description: "Anonymous gets all embargoed investigations",
      user: anonymous.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations",
      user: investigationUser.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all embargoed investigations",
      user: administrator.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations limit to 1",
      user: investigationUser.credential,
      status: "embargoed",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all embargoed investigations limit to 1",
      user: administrator.credential,
      status: "embargoed",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations after a date",
      user: investigationUser.credential,
      status: "embargoed",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations before a date",
      user: investigationUser.credential,
      status: "embargoed",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations sorted by name",
      user: investigationUser.credential,
      status: "embargoed",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations filtered by search",
      user: investigationUser.credential,
      status: "embargoed",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationByParticipantStatus: [
    {
      description: "Anonymous gets all participant investigations",
      user: anonymous.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations",
      user: investigationUser.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all participant investigations",
      user: administrator.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations limit to 1",
      user: investigationUser.credential,
      status: "participant",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations after a date",
      user: investigationUser.credential,
      status: "participant",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations before a date",
      user: investigationUser.credential,
      status: "participant",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations sorted by name",
      user: investigationUser.credential,
      status: "participant",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations filtered by search",
      user: investigationUser.credential,
      status: "participant",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getIndustryInvestigationByParticipant: [
    {
      description: "Anonymous gets all industry investigations",
      user: anonymous.credential,
      status: "industry",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations",
      user: investigationUser.credential,
      status: "industry",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all industry investigations",
      user: administrator.credential,
      status: "industry",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations limit to 1",
      user: investigationUser.credential,
      status: "industry",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations after a date",
      user: investigationUser.credential,
      status: "industry",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations before a date",
      user: investigationUser.credential,
      status: "industry",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations sorted by name",
      user: investigationUser.credential,
      status: "industry",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all industry investigations filtered by search",
      user: investigationUser.credential,
      status: "industry",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationByInstrument: [
    {
      description: "Investigation user gets all investigation by instrument",
      user: instrumentScientist.credential,
      instrument: "Id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation by instrument",
      user: administrator.credential,
      instrument: "Id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigation by instrument limit to 1",
      user: instrumentScientist.credential,
      instrument: "Id01",
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation by instrument limit to 1",
      user: administrator.credential,
      instrument: "Id01",
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  after a date",
      user: investigationUser.credential,
      status: "participant",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  before a date",
      user: investigationUser.credential,
      status: "participant",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  sorted by name",
      user: investigationUser.credential,
      status: "participant",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument filtered by search",
      user: investigationUser.credential,
      status: "participant",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigations: [
    {
      description: "Investigation user gets investigation",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigation",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument",
      user: investigationUser.credential,
      instrument: "Id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument in upper case",
      user: investigationUser.credential,
      instrument: "ID01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument in lower case",
      user: investigationUser.credential,
      instrument: "id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigations by instrument",
      user: administrator.credential,
      instrument: "Id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigations by instrument",
      user: anonymous.credential,
      instrument: "Id01",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigations by instrument limit to 1",
      user: administrator.credential,
      instrument: "Id01",
      limit: 1,
      expected: {
        status: 200,
      },
    },
  ],
  getInvestigationsFilteredOnInstrument: [],
  getMultipleInvestigations: [
    {
      description: "Administrator gets multiple investigations",
      user: administrator.credential,
      ids: investigationUser.investigations.participates.map((inv) => inv.investigationId),
      expected: {
        status: 200,
        numberInvestigations: investigationUser.investigations.participates.length,
      },
    },
    {
      description: "User gets multiple investigations he has access to",
      user: investigationUser.credential,
      ids: investigationUser.investigations.participates.map((inv) => inv.investigationId),
      expected: {
        status: 200,
        numberInvestigations: investigationUser.investigations.participates.length,
      },
    },
    {
      description: "When asking for multiple investigations, the user gets only the ones he has access to",
      user: investigationUser.credential,
      ids: [investigationUser.investigations.participates[0].investigationId, investigationUser.investigations.noAccess.investigationId],
      expected: {
        status: 200,
        numberInvestigations: 1,
      },
    },
  ],
  getInvestigationsByPeriod: [
    {
      description: "Investigation user gets investigation on a period",
      startDate: "2020-07-01",
      endDate: "2020-07-31",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation on a period",
      user: administrator.credential,
      startDate: "2020-07-01",
      endDate: "2020-07-31",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigation on a period",
      user: anonymous.credential,
      startDate: "2017-07-01",
      endDate: "2017-07-31",
      expected: {
        status: 200,
      },
    },
  ],
};
