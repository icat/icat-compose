const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const instrumentScientist = globaResource.users.instrumentScientist;
const administrator = globaResource.users.administrator;
const investigationUser = globaResource.users.investigationUser;
const addresses = globaResource.addresses;

module.exports = {
  getShipmentById: [
    {
      description: "A user retrieves a shipment by shipmentId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipmentCreator: investigationUser.credential,
      shipmentReader: investigationUser.credential,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "A user can not access to shipments from other investigations",
      investigationId: investigationUser.investigations.noAccess.investigationId,
      shipmentCreator: administrator.credential,
      shipmentReader: investigationUser.credential,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.noAccess.investigationId,
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator retrieves a shipment by shipmentId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipmentCreator: investigationUser.credential,
      shipmentReader: administrator.credential,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist retrieves a shipment by shipmentId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipmentCreator: investigationUser.credential,
      shipmentReader: instrumentScientist.credential,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous is not allowed to retrieve a shipment",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipmentCreator: investigationUser.credential,
      shipmentReader: anonymous.credential,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 403,
      },
    },
  ],
  getShipmentsByInvestigationId: [
    {
      description: "User retrieves a shipment from an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous is not allowed to retrieve a shipment",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator retrieves a shipment from an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument scientist is not allowed to retrieve a shipment from other's investigation",
      investigationId: investigationUser.investigations.participates[1].investigationId,
      user: instrumentScientist.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Instrument scientist retrieves a shipment from an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
  ],
  deleteShipment: [
    {
      description: "Investigation user deletes a shipment",
      shipmentCreator: investigationUser.credential,
      shipmentRemover: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator deletes a shipment",
      shipmentCreator: investigationUser.credential,
      shipmentRemover: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist deletes a shipment",
      shipmentCreator: investigationUser.credential,
      shipmentRemover: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can not delete a shipment",
      shipmentCreator: investigationUser.credential,
      shipmentRemover: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 403,
      },
    },
  ],

  createShipment: [
    {
      description: "Investigation user creates a shipment with shipping and return addresses",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shippingAddress: addresses.valid.complete,
      returnAddress: addresses.valid.complete,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user creates a shipment with shipping address",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shippingAddress: addresses.valid.complete,
      shipment: {
        name: "Shipment createShipmentWithExistingAddress",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator creates a shipment",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user is not allowed to create a shipment for an investigation which he is not participant",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.noAccess.investigationId,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous is not allow to create a shipment for an investigation",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        name: "test",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user is not allowed to create a shipment with no name",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      shipment: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 500,
      },
    },
  ],
};
