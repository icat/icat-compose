module.exports = {
  listIdentifiers: [
    {
      description: "Check that non-public investigations are exposed by ListIdentifiers",
      requestListIdentifiers: "request?verb=ListIdentifiers&metadataPrefix=oai_dc",
      requestByIdentifier: "request?verb=GetRecord&metadataPrefix=oai_dc&identifier=",
      expected: {
        /** Propietary research **/
        forbiddenProposalTypes: ["fx", "in", "im", "ix", "blc"],
      },
    },
  ],
  listRecords: [
    {
      description: "Check that non-public investigations are exposed by ListRecords",
      requestListRecord: "request?verb=ListRecords&metadataPrefix=oai_dc",
      expected: {
        /** Propietary research **/
        forbiddenProposalTypes: ["fx", "in", "im", "ix", "blc"],
      },
    },
  ],
};
