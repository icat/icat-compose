const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const investigationUser = globaResource.users.investigationUser;
const administrator = globaResource.users.administrator;

module.exports = {
  getSamplesByInvestigationId: [
    {
      description: "InvestigationUser retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
};
