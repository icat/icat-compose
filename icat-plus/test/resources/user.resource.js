const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const investigationUser = globaResource.users.investigationUser;
const instrumentScientist = globaResource.users.instrumentScientist;
const administrator = globaResource.users.administrator;

const logOut = {
  message: "Error when retrieving the session information. Your session might be expired or it is invalid.",
};

module.exports = {
  doLogout: [
    {
      description: "InstrumentScientist logs out",
      user: instrumentScientist.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "administrator logs out",
      user: administrator.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "anonymous logs out",
      user: anonymous.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "investigationUser logs out",
      user: investigationUser.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
  ],
  doLogin: [
    {
      description: "InstrumentScientist logs in",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
        isInstrumentScientist: true,
        isAdministrator: false,
        usersByPrefix: [],
      },
    },
    {
      description: "Administrator logs in",
      user: administrator.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: true,
        usersByPrefix: [],
      },
    },
    {
      description: "Anonymous logs in",
      user: anonymous.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: false,
        usersByPrefix: [],
      },
    },
    {
      description: "InvestigationUser logs in",
      user: investigationUser.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: false,
        usersByPrefix: [],
      },
    },
  ],

  getUsers: [
    {
      description: "InstrumentScientist gets the list of users",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets the list of users",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous doesn't get the list of users",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser doesn't get the list of users",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
  ],
};
