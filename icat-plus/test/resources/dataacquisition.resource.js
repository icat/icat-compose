const globalResource = require("./global.resource.js");
const { createEventByAttributes } = require("./../helper/logbook.js");
const investigationUser = globalResource.users.investigationUser;
const anonymousUser = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;
const CONSTANTS = require("../../app/constants");

module.exports = {
  mint: [
    {
      description: "A DOI is minted per dataset with the API KEY",
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: "Daniyal J. Jafry, Maire Bellier, Christopher Werlan",
      expected: {
        status: 200,
      },
    },
  ],
  allocateInvestigations: [
    {
      description: "Allocate proposal with a single session when time in between the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T10:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      description: "Allocate proposal with a single session when time is 6 hours before the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T02:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      description: "Allocate proposal with a single session when time is 8 hours before the time slot",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-26T00:00:00.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      description: "Allocate proposal with a single session when time is 8 hours and 1 second before the time slot but it has a single session",
      investigationName: "HC4234",
      instrumentName: "ID15B",
      time: "2020-08-25T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
      },
    },
    {
      description: "Allocate proposal when time in between the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T10:00:00.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      description: "Allocate proposal when time is 6 hours before the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T02:00:00.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      description: "Allocate proposal when time is 8 hours before the time slot",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-28T00:00:01.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },
    {
      description: "Allocate proposal when time is 48 hours and 1 second before the time slot but it has multiple sessions",
      investigationName: "IN1111",
      instrumentName: "ID16B-NA",
      time: "2020-09-25T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "IN/1111 ID16B-NA 28-09-2020/28-09-2020",
      },
    },

    {
      description: "Allocate proposal when time is in the range 48 hours",
      investigationName: "ID010100",
      instrumentName: "ID01",
      time: "2017-10-26T23:59:59.000Z",
      expected: {
        status: 200,
        visitId: "id01",
      },
    },
  ],
  getDatasetByInvestigationId: [
    {
      description: "Data acquisition can retrieve datasets by investigationId",
      investigationId: 63335100,
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId with a limit to 1",
      investigationId: 63335100,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId with a search query",
      investigationId: 63335100,
      search: "test",
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId sorted by name",
      investigationId: 63335100,
      sortBy: "name",
      sortOrder: 1,
      expected: {
        status: 200,
      },
    },
  ],

  createBeamlineNotification: [
    {
      description: "Creation of a beamline notification",
      tagsToFillDB: [],
      event: {
        instrumentName: "ID32",
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: "ID32",
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Creation of a beamline notification with new tag",
      tagsToFillDB: [],
      adminUser: administrator.credential,
      event: {
        instrumentName: "ID32",
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "newtag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: "ID32",
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "newtag", instrumentName: "ID32" }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "newtag", instrumentName: "ID32" }],
      },
    },
    {
      description: "Creation of a beamline notification with existing global tag",
      tagsToFillDB: [{ name: "tag" }],
      adminUser: administrator.credential,
      event: {
        instrumentName: "ID32",
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: "ID32",
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag" }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "tag" }],
      },
    },
    {
      description: "Creation of a beamline notification with existing beamline tag",
      tagsToFillDB: [{ name: "tag", instrumentName: "ID32" }],
      adminUser: administrator.credential,
      event: {
        instrumentName: "ID32",
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: "ID32",
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag", instrumentName: "ID32" }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "tag", instrumentName: "ID32" }],
      },
    },
    {
      description: "Creation of a beamline notification with existing another beamline tag",
      tagsToFillDB: [{ name: "tag", instrumentName: "ID00" }],
      adminUser: administrator.credential,
      event: {
        instrumentName: "ID32",
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: "ID32",
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag", instrumentName: "ID32" }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [
          { name: "tag", instrumentName: "ID00" },
          { name: "tag", instrumentName: "ID32" },
        ],
      },
    },
  ],
  createInvestigationNotification: [
    {
      description: "Event investigation name in capital letters",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "hc4170",
        instrumentName: "ID32",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with no creationDate",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, not in summer time",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, a date in summer time",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "2020-04-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-04-09T10:18:22.749+02:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, with offset",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749+01:00",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, UTC",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749Z",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749Z",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format)",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
        }),
      },
    },
    {
      description: "Event with creationDate: unknown format)",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
        }),
      },
    },
    {
      description: "Event with a tag with name",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        tag: [{ name: "myTestTag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          tag: [
            {
              name: "mytesttag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag with ID",
      adminUser: administrator.credential,
      tagsToFillDB: [
        {
          name: "My TAG",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        tag: [{ name: "My TAG" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "my tag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag that comes as a string with ID",
      adminUser: administrator.credential,
      tagsToFillDB: [
        {
          name: "mytag",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        category: "comment",
        tag: '[{"name":"global"}]',
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "global",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with investigation name that can not be normalized",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "XX-III-ProposalMathilde",
        instrumentName: "ID32",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with wrong instrumentName",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "id010100",
        instrumentName: "not instrument",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with no instrumentName",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        type: "notification",
        investigationName: "id010100",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with no type",
      adminUser: administrator.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: "id010100",
        instrumentName: "id01",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
    {
      description: "Event with no category",
      adminUser: administrator.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: "id010100",
        instrumentName: "id01",
        type: "notification",
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
    {
      description: "Creation of an investigation notification with new tag",
      tagsToFillDB: [],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "ID01",
        type: "notification",
        tag: [{ name: "newtag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: "id010100",
          instrumentName: "ID01",
          type: "notification",
          tag: [{ name: "newtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
        }),
        tags: [{ name: "newtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      },
    },
    {
      description: "Creation of an investigation notification with existing global tag",
      tagsToFillDB: [{ name: "globaltag" }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "ID01",
        type: "notification",
        tag: [{ name: "globaltag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: "id010100",
          instrumentName: "ID01",
          type: "notification",
          tag: [{ name: "globaltag" }],
        }),
        tags: [{ name: "globaltag" }],
      },
    },
    {
      description: "Creation of an investigation notification with existing beamline tag",
      tagsToFillDB: [{ name: "beamlinetag", instrumentName: "ID01" }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "ID01",
        type: "notification",
        tag: [{ name: "beamlinetag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: "id010100",
          instrumentName: "ID01",
          type: "notification",
          tag: [{ name: "beamlinetag", instrumentName: "ID01" }],
        }),
        tags: [{ name: "beamlinetag", instrumentName: "ID01" }],
      },
    },
    {
      description: "Creation of an investigation notification with existing investigation tag",
      tagsToFillDB: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: "id010100",
        instrumentName: "ID01",
        type: "notification",
        tag: [{ name: "investigationtag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: "id010100",
          instrumentName: "ID01",
          type: "notification",
          tag: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
        }),
        tags: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      },
    },
  ],
  createBroadcastEvent: [
    {
      description: "Creation of a broadcast notification",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_INFO,
        title: "machine title",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 200,
        event: {
          type: "broadcast",
          title: "machine title",
          category: "info",
          machine: "ebs",
          content: [{ format: "plainText", text: "machine comment" }],
        },
      },
    },
    {
      description: "Creation of a broadcast notification with tags",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "machine title",
        machine: "ebs",
        tag: [{ name: "machine" }, { name: "machine tag2" }],
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 200,
        event: {
          type: "broadcast",
          title: "machine title",
          category: "error",
          content: [{ format: "plainText", text: "machine comment" }],
          tag: [
            {
              name: "machine",
            },
            {
              name: "machine tag2",
            },
          ],
        },
      },
    },
    {
      description: "Creation of a broadcast notification fails if wrong sessionId",
      tagsToFillDB: [],
      sessionId: "wrong sessionId",
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "machine title",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Creation of a broadcast notification fails if type is not broadcast",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_ANNOTATION,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "wrong type",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Creation of a broadcast notification fails if type is undefined",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "no type",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 400,
      },
    },
  ],
  createFromBase64byBeamlineAndInsturmentName: [
    {
      description: "InvestigationUser creates a event with base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid instrument",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: "Invalid instrument",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid investigation name",
      user: investigationUser.credential,
      investigationName: "Invalid investigation name",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with wrong apiKey",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      apiKey: "WrongAPI",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser creates a event with no base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no investigationName",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no instrumentName",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no apiKey",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
  ],
  restore: [
    {
      description: "Restore data should trigger an error if no datasetId",
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [],
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data should return empty array if no entry to restore",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [],
      expected: {
        status: 200,
        nbUpdated: 0,
      },
    },
    {
      description: "Restore data should return expected values to restore",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [
        {
          datasetId: 123,
          user: investigationUser.name,
        },
      ],
      expected: {
        status: 200,
        nbUpdated: 1,
      },
    },
    {
      description: "Restore data should send email to several users for the same datasetId",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [
        {
          datasetId: 123,
          user: investigationUser.name,
        },
        {
          datasetId: 123,
          user: anonymousUser.name,
          email: "test@test.fr",
        },
        {
          datasetId: 456,
          user: anonymousUser.name,
          email: "test2@test.fr",
        },
      ],
      expected: {
        status: 200,
        nbUpdated: 2,
      },
    },
  ],
};
