const globalResource = require("./global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

const doi = {
  investigation: {
    prefix: globalResource.doi.public[0].prefix,
    suffix: globalResource.doi.public[0].suffix,
  },
  dc: {
    prefix: globalResource.doi.public[1].prefix,
    suffix: globalResource.doi.public[1].suffix,
  },
};

module.exports = {
  getDataCollectionByDataSetId: [
    {
      description: "Retrieve a datacollection by datasetId",
      datasetId: "63335951",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetByDOI: [
    {
      description: "Administrator get datasets by DOI attached to an investigation",
      user: administrator.credential,
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous get datasets by DOI attached to an investigation",
      user: anonymous.credential,
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation",
      user: investigationUser.credential,
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator get datasets by DOI attached to a data collection",
      user: administrator.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous get datasets by DOI attached to a data collection",
      user: anonymous.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection",
      user: investigationUser.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection with a limit to 1",
      user: investigationUser.credential,
      limit: 1,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection sorted by sampleName ",
      user: investigationUser.credential,
      sortBy: "SAMPLENAME",
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection with a search query ",
      user: investigationUser.credential,
      search: "test",
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation with a limit to 1",
      user: investigationUser.credential,
      limit: 1,
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation sorted by sampleName ",
      user: investigationUser.credential,
      sortBy: "SAMPLENAME",
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation with a search query ",
      user: investigationUser.credential,
      search: "MX",
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetDocumentByDateRange: [
    {
      description: "Administrator retrieves a list of datasets, from beamlines which beamline name contains a dash,  by date range",
      startDate: "2018-08-20",
      endDate: "2018-08-22",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user can retrieve a list of datasets by date range",
      startDate: "2017-01-23",
      endDate: "2017-01-24",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous user can not retrieve a list of datasets by date range",
      startDate: "2017-01-23",
      endDate: "2017-01-24",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetByDatasetIds: [
    {
      description: "Retrieve a single dataset with NO Sample",
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieve a single dataset",
      datasetIds: "63335951",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieve a comma separated list of datasets",
      datasetIds: "63335951,52245527",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetByInvestigationId: [
    {
      description: "InvestigationUser can retrieve datasets by investigationId",
      user: investigationUser.credential,
      investigationId: 63335100,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId with a limit to 1",
      user: investigationUser.credential,
      investigationId: 63335100,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId with a search query",
      user: investigationUser.credential,
      investigationId: 63335100,
      search: "test",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId sorted by name",
      user: investigationUser.credential,
      investigationId: 63335100,
      sortBy: "name",
      sortOrder: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and datasetType",
      user: investigationUser.credential,
      investigationId: 63335100,
      datasetType: "acquisition",
      expected: {
        status: 200,
      },
    },

    {
      description: "Anonymous cannot retrieve datasets by investigationId",
      user: anonymous.credential,
      investigationId: 63335100,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve datasets by investigationId",
      user: administrator.credential,
      investigationId: 63335100,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can retrieve datasets by investigationId",
      user: instrumentScientist.credential,
      investigationId: 63335100,
      expected: {
        status: 200,
      },
    },
  ],
  getDataDownload: [
    {
      description: "Data download failed if no datasetIds and no datafileIds",
      user: investigationUser.credential,
      expected: {
        status: 404,
      },
    },
  ],
  getDataRestore: [
    {
      description: "Restore data failed if no datasetId ",
      user: investigationUser.credential,
      name: investigationUser.name,
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data failed if no name ",
      user: investigationUser.credential,
      datasetId: "63335951",
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data should record a dataset query",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: "investigationUser@test.fr",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a dataset query with email",
      user: investigationUser.credential,
      name: anonymous.name,
      email: "test@test.com",
      datasetId: "63335951",
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: anonymous.name,
          email: "test@test.com",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should not record a query if a query already exists for the same user, dataset, ongoing",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      prepopulate: {
        datasetId: 63335951,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: "investigationUser@test.fr",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists but done",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      prepopulate: {
        datasetId: 63335951,
        user: investigationUser.name,
        type: "RESTORE",
        status: "DONE",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: "investigationUser@test.fr",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another user",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      prepopulate: {
        datasetId: 63335951,
        user: instrumentScientist.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: "investigationUser@test.fr",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another dataset",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      prepopulate: {
        datasetId: 123,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: "investigationUser@test.fr",
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
  ],
};
