const globalResource = require("./global.resource");

const investigationUser = globalResource.users.investigationUser;
const principalInvestigator = globalResource.users.principalInvestigator;
const localContact = globalResource.users.localContact;
const anonymous = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;

module.exports = {
  // Use to revoke permissions one the test failed so DB is cleaned
  revoker: administrator.credential,
  investigationusers: [
    {
      description: "Get investigation Users from a investigation by participant",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  addInvestigationUser: [
    {
      description: "Anyonymous can not grant permissions",
      // User that grants the permissions to the investigation
      granter: anonymous.credential,
      // User that revokes access to the investigation
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 403,
        },
      },
    },
    {
      description: "InvestigationUser can not grant permissions",
      granter: investigationUser.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 403,
        },
      },
    },
    {
      description: "Principal investigator grants and Principal investigator revokes access to investigation",
      granter: principalInvestigator.credential,
      revoker: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 200,
        },
      },
    },
    {
      description: "Administrator grants and revokes access to investigation",
      granter: administrator.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 200,
        },
      },
    },
    {
      description: "Local contact grants and revokes access to investigation",
      granter: localContact.credential,
      revoker: localContact.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 200,
        },
      },
    },
    {
      description: "Local contact grants and PI revokes access to investigation",
      granter: localContact.credential,
      revoker: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 200,
        },
      },
    },
    {
      description: "Administrator grants and PI revokes access to investigation",
      granter: administrator.credential,
      revoker: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 200,
        },
      },
    },
    {
      description: "Principal investigator can not grant access to a non existing user",
      granter: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "Non-existing-user" },
      expected: {
        grant: {
          status: 400,
        },
      },
    },
    {
      description: "Principal investigator can not grant access to a non existing investigation",
      granter: principalInvestigator.credential,
      investigationId: 1111000999888990,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 403,
        },
      },
    },
    {
      description: "Anonymous can not revoke permissions",
      granter: administrator.credential,
      revoker: anonymous.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 403,
        },
      },
    },
    {
      description: "InvestigationUser can not revoke permissions",
      granter: administrator.credential,
      revoker: investigationUser.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        grant: {
          status: 200,
        },
        revoke: {
          status: 403,
        },
      },
    },
  ],
};
