module.exports = {
  getItemsByDateRange: [
    {
      description: "Get items by date",
      startDate: "2019-05-05",
      endDate: "2019-06-05",
      expected: {
        status: 200,
      },
    },
    {
      description: "Get items by date with no startDate",
      endDate: "2019-06-05",
      expected: {
        status: 500,
      },
    },
    {
      description: "Get items by date with no startDate, endDate",
      expected: {
        status: 500,
      },
    },
  ],
};
