const globalResource = require("./global.resource.js");
const globaResource = require("./global.resource.js");

const anonymous = globaResource.users.anonymous;
const investigationUser = globaResource.users.investigationUser;
const instrumentScientist = globaResource.users.instrumentScientist;
const administrator = globaResource.users.administrator;
const principalInvestigator = globalResource.users.principalInvestigator;
module.exports = {
  mint: [
    {
      description: "Principal investigator mints a DOI",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator mints a DOI with creatorName",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ creatorName: "principal, Investigator", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous is not allowed to mint a DOI",
      user: anonymous.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "investigationUser mints a DOI",
      user: investigationUser.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser is not allowed to mint a DOI on open dataset if he is not a participant ",
      user: investigationUser.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [95316824],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "investigationUser is not allowed to mint a DOI on dataset if he is not a participant of the investigation",
      user: investigationUser.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [118040244],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "instrumentScientist is not allowed to mint a DOI",
      user: instrumentScientist.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "administrator is not allowed to mint a DOI",
      user: administrator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [63335951],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
  ],
  datacite: [
    {
      description: "JSON datacite returns doi data ",
      prefix: "10.15151",
      suffix: "ESRF-ES-117583838",
      expected: {
        status: 200,
        instrument: "BM29",
        creators: [
          {
            name: "Ajay Kumar SAXENA",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "0000-0001-6967-030X",
              },
            ],
          },
          { name: "Garima VERMA" },
          { name: "Komal CHOUKATE" },
          { name: "Martha BRENNICH" },
          { name: "Nishant Kumar VARSHNEY" },
          { name: "RUCHIR CHANDRAKANT BOBDE" },
          { name: "Yogita SHARMA" },
        ],
        contributors: [
          { name: "Alexander POPOV" },
          {
            name: "Antoine ROYANT",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "0000-0002-1919-8649",
              },
            ],
          },
          { name: "Christoph MUELLER DIECKMANN" },
          {
            name: "Matthew BOWLER",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "0000-0003-0465-3351",
              },
            ],
          },
        ],
        proposalTypeDescription: "Macromolecular Crystallography",
        name: "MX-2077",
      },
    },
    {
      description: "JSON datacite returns doi data with related identifiers",
      prefix: "10.15151",
      suffix: "ESRF-ES-117605227",
      expected: {
        status: 200,
        instrument: "ID11",
        creators: [{ name: "André CAVALEIRO" }, { name: "Edgar CAMACHO" }, { name: "Younes EL HACHI" }],
        contributors: [{ name: "Pavel SEDMAK" }],
        proposalTypeDescription: "Applied Material Science",
        name: "MA-4196",
        relatedIdentifiers: [
          { relatedIdentifier: "10.5072/ESRF-DC-123753799" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398411" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398420" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398429" },
          { relatedIdentifier: "10.5072/test-esrf-123398540" },
          { relatedIdentifier: "10.5072/test-esrf-123398549" },
          { relatedIdentifier: "10.5072/test-esrf-123398585" },
          { relatedIdentifier: "10.5072/test-esrf-123398594" },
          { relatedIdentifier: "10.5072/test-esrf-123398597" },
          { relatedIdentifier: "10.5072/test-esrf-123399173" },
          { relatedIdentifier: "10.5072/test-esrf-123399182" },
          { relatedIdentifier: "10.5072/test-esrf-123399191" },
          { relatedIdentifier: "10.5072/test-esrf-123399218" },
          { relatedIdentifier: "10.5072/test-esrf-123399227" },
          { relatedIdentifier: "10.5072/test-esrf-123399236" },
          { relatedIdentifier: "10.5072/test-esrf-123399245" },
          { relatedIdentifier: "10.5072/test-esrf-123399254" },
          { relatedIdentifier: "10.5072/test-esrf-123399263" },
          { relatedIdentifier: "10.5072/test-esrf-123399272" },
          { relatedIdentifier: "10.5072/test-esrf-123399299" },
          { relatedIdentifier: "10.5072/test-esrf-123399308" },
          { relatedIdentifier: "10.5072/test-esrf-123399326" },
          { relatedIdentifier: "10.5072/test-esrf-123399335" },
          { relatedIdentifier: "10.5072/test-esrf-123753807" },
          { relatedIdentifier: "10.5072/test-esrf-123753815" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816708" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816716" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816732" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816740" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816748" },
          { relatedIdentifier: "123399290" },
        ],
      },
    },
  ],
};
