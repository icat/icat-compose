require("it-each")({ testPerIteration: true });
process.env.NODE_ENV = "test";
global.gServerConfig = require("./config/server.test.config.js");
const { expect } = require("chai");
const cache = require("../app/cache/cache.js");
const sessionHelper = require("./helper/session.js");
const cacheResource = require("./resources/cache.resource.js");

describe("Cache", () => {
  describe("extractUserNamePrefix", () => {
    it.each(cacheResource.extractUserNamePrefix, "%s", ["description"], (element) => {
      expect(cache.extractUserNamePrefix(element.username)).equal(element.expected.username);
    });
  });

  describe("getUserByUsernamePrefix", () => {
    it.each(cacheResource.getUserByUsernamePrefix, "%s", ["description"], async (element, next) => {
      try {
        const getSessionResponse = await sessionHelper.doGetSession(element.user);
        const { sessionId } = getSessionResponse.body;
        const users = await cache.getUserByUsernamePrefix(sessionId, element.username);
        expect(users.length).equal(element.expected.users.length);
        users.forEach((user, i) => {
          expect(user.name).equal(element.expected.users[i].name);
          expect(user.createTime).to.not.be.null;
        });
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
