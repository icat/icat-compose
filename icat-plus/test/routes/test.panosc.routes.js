const resource = require("../resources/panosc.resource.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("Panosc", () => {
  describe("/api/scoring/items", () => {
    it.each(resource.getItemsByDateRange, "%s", ["description"], async (element, next) => {
      const { startDate, endDate } = element;
      const params = new URLSearchParams();
      if (startDate) params.set("startDate", startDate);
      if (endDate) params.set("endDate", endDate);

      const response = await global.gRequester.get(`/api/scoring/items?${params.toString()}`).set("Content-Type", "application/json").send();
      expect(response.status).to.equal(element.expected.status);
      return next();
    });
  });
});
