require("it-each")({ testPerIteration: true });

const instrumentScientistResource = require("../resources/instrumentscientist.resource.js");
const sessionHelper = require("../helper/session.js");

describe("Instrumentscientist", () => {
  describe("GET /catalogue/:sessionId/instrumentscientist", () => {
    it.each(instrumentScientistResource.getInstrumentScientist, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        expect(getSessionResponse.status).equal(200);
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/instrumentscientist`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            next();
          });
      });
    });
  });

  describe("PUT and DELETE /catalogue/:sessionId/instrumentscientist", () => {
    it.each(instrumentScientistResource.createInstrumentScientist, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        expect(getSessionResponse.status).equal(200);
        global.gRequester
          .put(`/catalogue/${getSessionResponse.body.sessionId}/instrumentscientist`)
          .set("Content-Type", "application/json")
          .send(element.instrumentScientist)
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            if (element.expected.status === 200) {
              // Remove from database the new create instrumentScientist
              const instrumentScientistIds = JSON.parse(response.text);
              global.gRequester
                .delete(`/catalogue/${getSessionResponse.body.sessionId}/instrumentscientist`)
                .set("Content-Type", "application/json")
                .send({
                  instrumentscientistsid: instrumentScientistIds,
                })
                .end((_err, deleteInstrumentScientistsResponse) => {
                  expect(deleteInstrumentScientistsResponse.status).to.equal(element.expected.status);
                  next();
                });
            } else {
              return next();
            }
          });
      });
    });
  });
});
