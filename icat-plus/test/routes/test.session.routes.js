require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const userResource = require("../resources/user.resource.js");
const { expect } = require("chai");

describe("Session", () => {
  describe("/session", () => {
    it.each(userResource.doLogin, "%s", ["description"], async (element, next) => {
      const { user, expected } = element;
      const res = await sessionHelper.doGetSession(user);
      try {
        const { body, status } = res;
        const { fullName, name, isInstrumentScientist, isAdministrator, usersByPrefix } = body;
        expect(status).to.equal(expected.status);
        expect(body).to.have.property("sessionId").to.be.a("string").to.have.length(36);
        expect(fullName).is.not.null;
        expect(name).is.not.null;
        expect(isInstrumentScientist).equal(expected.isInstrumentScientist);
        expect(isAdministrator).equal(expected.isAdministrator);
        expect(usersByPrefix.length).equal(element.expected.usersByPrefix.length);
        return next();
      } catch (e) {
        return next(e);
      }
    });

    it.each(userResource.doLogout, "%s", ["description"], async (element, next) => {
      const { user, expected } = element;
      const res = await sessionHelper.doGetSession(user);
      try {
        /* do logout */
        const doLogoutResponse = await sessionHelper.doLogout(res.body.sessionId);
        expect(doLogoutResponse.statusCode).to.equal(200);

        /* Test doLogout has been performed */
        const getSessionInformationResponse = await sessionHelper.getSessionInformation(res.body.sessionId);
        const { status, body } = getSessionInformationResponse;
        expect(status).to.equal(expected.status);
        expect(body.message).to.equal(expected.message);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
