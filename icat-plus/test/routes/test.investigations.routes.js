require("it-each")({ testPerIteration: true });

const investigationsResource = require("../resources/investigations.resource.js");
const sessionHelper = require("../helper/session.js");
const investigationHelper = require("../helper/investigation.js");
const { expect } = require("chai");

describe("Investigations", () => {
  function checkLimitParameterInResponse(limit, response) {
    if (limit) {
      expect(response.body.length).to.be.at.most(limit);
    }
  }

  function checkInvestigationDates(startDate, endDate, response) {
    if (startDate) {
      response.body.forEach((inv) => {
        expect(inv.endDate === undefined || new Date(inv.endDate) >= new Date(startDate)).to.be.true;
      });
    }
    if (endDate) {
      response.body.forEach((inv) => {
        expect(inv.startDate === undefined || new Date(inv.startDate) <= new Date(endDate)).to.be.true;
      });
    }
  }

  function checkNoIndustrials(investigation, industrials) {
    industrials.forEach((industrialType) => {
      expect(investigation.name.toLowerCase()).to.not.have.string(industrialType.toLowerCase());
    });
  }

  describe("/catalogue/:sessionId/investigation", () => {
    it.each(
      [...investigationsResource.getInvestigations, ...investigationsResource.getInvestigationByInstrument, ...investigationsResource.getInvestigationsByPeriod],
      "%s",
      ["description"],
      async (element, next) => {
        try {
          const { user, instrumentName, limit, startDate, endDate } = element;
          const response = await investigationHelper.getInvestigationBy(user, instrumentName, null, limit, startDate, endDate);

          expect(response.status).to.equal(element.expected.status);
          if (element.expected.status < 300) {
            expect(response.body).to.be.an("array");
            expect(response.body.length).to.be.above(0);
          }
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
          if (instrumentName) {
            const instrumentNameUpperCase = instrumentName.toUpperCase();
            response.body.forEach((inv) => {
              expect(inv.instrument).not.to.be.null;
              expect(inv.instrument.name).to.be.equal(instrumentNameUpperCase);
            });
          }
          return next();
        } catch (e) {
          return next(e);
        }
      }
    );
  });

  describe("/catalogue/:sessionId/investigation", () => {
    it.each(investigationsResource.allocateInvestigations, "%s", ["description"], async (element, next) => {
      try {
        const { instrumentName, investigationName, time, user, expected } = element;
        const response = await investigationHelper.getInvestigationBy(user, instrumentName, null, null, null, null, null, null, null, null, time, investigationName);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body[0].visitId).to.equal(expected.visitId);
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?ids=[...]", () => {
    it.each(investigationsResource.getMultipleInvestigations, "%s", ["description"], async (element, next) => {
      try {
        const response = await investigationHelper.getInvestigationBy(element.user, "ids", null, null, null, null, null, null, null, element.ids);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          expect(response.body.length).to.equal(element.expected.numberInvestigations);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?filter=participant", () => {
    it.each(investigationsResource.getInvestigationByParticipantStatus, "%s", ["description"], async (element, next) => {
      try {
        const { user, status, limit, startDate, endDate, sortBy, sortOrder, search } = element;
        const response = await investigationHelper.getInvestigationByFilter(user, status, limit, startDate, endDate, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          response.body.forEach((inv) => {
            checkNoIndustrials(inv, element.expected.industrials);
          });
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?filter=embargoed", () => {
    it.each(investigationsResource.getInvestigationByEmbargoedStatus, "%s", ["description"], async (element, next) => {
      try {
        const { user, status, limit, startDate, endDate, sortBy, sortOrder, search } = element;
        const response = await investigationHelper.getInvestigationByFilter(user, status, limit, startDate, endDate, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          response.body.forEach((inv) => {
            expect(inv.releaseDate).not.to.be.null;
            expect(new Date(inv.releaseDate) > new Date()).to.be.true;
            checkNoIndustrials(inv, element.expected.industrials);
          });
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?filter=released", () => {
    it.each(investigationsResource.getInvestigationByReleasedStatus, "%s", ["description"], async (element, next) => {
      try {
        const { user, status, limit, startDate, endDate, sortBy, sortOrder, search } = element;
        const response = await investigationHelper.getInvestigationByFilter(user, status, limit, startDate, endDate, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");

          response.body.forEach((inv) => {
            expect(inv.releaseDate).not.to.be.null;
            expect(new Date(inv.releaseDate) <= new Date()).to.be.true;
            expect(inv.doi).not.to.be.null;
            checkNoIndustrials(inv, element.expected.industrials);
          });
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?filter=industry", () => {
    it.each(investigationsResource.getIndustryInvestigationByParticipant, "%s", ["description"], async (element, next) => {
      try {
        const { user, status, limit, startDate, endDate, sortBy, sortOrder, search } = element;
        const response = await investigationHelper.getInvestigationByFilter(user, status, limit, startDate, endDate, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          response.body.forEach((inv) => {
            const industrialTypes = element.expected.industrials.map((ind) => ind.toLowerCase()).join("|");
            const regex = new RegExp(`^[${industrialTypes}]`);
            expect(inv.name.toLowerCase()).to.match(regex);
          });
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/investigation/name/:investigationName/normalize", () => {
    it.each(investigationsResource.normalize, "[%s -> %s]", ["input", "expected"], (element, next) => {
      global.gRequester
        .get(`/catalogue/investigation/name/${element.input}/normalize`)
        .set("Content-Type", "application/json")
        .send()
        .end((_err, response) => {
          expect(response.text).to.equal(element.expected);
          next();
        });
    });
  });

  describe("/catalogue/:sessionId/investigation/id/:investigationId", () => {
    it.each(investigationsResource.investigation, "[%s]", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            expect(response.body).to.have.property("id");
            expect(response.body.id).to.equal(element.investigationId);
            next();
          });
      });
    });
  });

  describe("/catalogue/:sessionId/investigation/id/:investigationId/investigationX", () => {
    it.each(investigationsResource.investigation, "[%s]", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/investigation`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            expect(response.body).to.be.an("array");
            next();
          });
      });
    });
  });
});
