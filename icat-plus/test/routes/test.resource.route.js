require("it-each")({ testPerIteration: true });

const resource = require("../resources/resource.resource");
const sessionHelper = require("../helper/session.js");
const resourceHelper = require("../helper/resource.js");
const { expect } = require("chai");
const { testEvent } = require("../helper/logbook");

describe("Resource", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (!global.gServerConfig.database.isMongoUnitDisabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("/resource/:sessionId/file/upload", () => {
    it.each(resource.upload, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, apiKey, investigationId, instrumentName, file, expected } = element;
        const response = await resourceHelper.uploadFile(user, apiKey, file, investigationId, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          testEvent(response.body, expected.event);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/resource/:sessionId/file/download", () => {
    it.each(resource.download, "[%s]", ["description"], async (element, next) => {
      try {
        const { uploadUser, user, investigationId, instrumentName, file, eventId, expected } = element;
        // Upload a file in an event
        const uploadResponse = await resourceHelper.uploadFile(uploadUser, undefined, file, investigationId, instrumentName);

        // Download this file
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;
        const resourceId = eventId ? eventId : uploadResponse.body._id;
        const response = await global.gRequester.get(`/resource/${sessionId}/file/download?resourceId=${resourceId}`);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          expect(response.text).to.be.a("string");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("[DEPRECATED] /resource/:sessionId/file/id/:resourceId/investigation/id/:investigationId/download", () => {
    it.each(resource.download, "[%s]", ["description"], async (element, next) => {
      try {
        const { uploadUser, user, investigationId, instrumentName, file, eventId, expected } = element;
        // Upload a file in an event
        const uploadResponse = await resourceHelper.uploadFile(uploadUser, null, file, investigationId, instrumentName);

        // Download this file
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;
        const resourceId = eventId ? eventId : uploadResponse.body._id;
        const response = await global.gRequester.get(`/resource/${sessionId}/file/id/${resourceId}/investigation/id/${investigationId}/download`);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          expect(response.text).to.be.a("string");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
