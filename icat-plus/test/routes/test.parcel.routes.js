require("it-each")({ testPerIteration: true });

const resource = require("../resources/parcel.resource.js");
const shipmentHelper = require("../helper/shipment.js");
const parcelHelper = require("../helper/parcel.js");
const itemHelper = require("../helper/item.js");
const { expect } = require("chai");

const testParcel = (parcel) => {
  expect(parcel._id).not.to.be.null;
  expect(parcel.shipmentId).not.to.be.null;
  expect(parcel.investigatonId).not.to.be.null;
  expect(parcel.investigation).not.to.be.null;
  // expect(parcel.investigation.id).to.equal(parcel.investigatonId);
  expect(parcel.investigation).not.to.be.null;
  //expect(parcel.localContactNames).to.be.an("array").to.not.be.empty;
};

const getParcelById = async (user, investigationId, parcelId) => {
  const getParcelByIdResponse = await parcelHelper.getParcelById(user, investigationId, parcelId);
  if (getParcelByIdResponse.status < 300) {
    testParcel(getParcelByIdResponse.body);
  }
  return getParcelByIdResponse;
};

const createParcel = async (user, parcel, investigationId, shipmentId, status) => {
  const createParcelResponse = await parcelHelper.createParcel(user, parcel, investigationId, shipmentId);
  expect(createParcelResponse.status).to.equal(status);
  if (status < 300) {
    /** createParcel returns the shipment */
    expect(createParcelResponse.body._id).to.not.be.null;
    expect(createParcelResponse.body.parcels).to.be.an("array").to.not.be.empty;
    getParcelById(user, investigationId, createParcelResponse.body.parcels[0]);
  }
  return createParcelResponse;
};

describe("Tracking Parcel", () => {
  describe("E2E Shipment and parcel creation and set status", async () => {
    it.each(resource.setStatus, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);

        for (let i = 0; i < element.statuses.length; i++) {
          const { statusName, containsDangerousGoods, user, expected } = element.statuses[i];
          const setParcelStatusResponse = await parcelHelper.setParcelStatus(user, element.investigationId, createParcelResponse.body.parcels[0], statusName, {
            containsDangerousGoods,
          });
          expect(setParcelStatusResponse.status).to.equal(expected.status);

          const getParcelByIdResponse = await getParcelById(element.user, createParcelResponse.body.investigationId, createParcelResponse.body.parcels[0]);
          expect(getParcelByIdResponse.body.status).to.equal(statusName);
          testParcel(getParcelByIdResponse.body);
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /tracking/{sessionId}/investigation/id/{investigationId}/parcel", () => {
    it.each(resource.createParcel, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, element.expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel/status/{status}", () => {
    it.each(resource.getParcelByStatus, "%s %s", ["description", "status"], async (element, next) => {
      try {
        const getParcelByStatusResponse = await parcelHelper.getParcelByStatus(element.user, element.status);
        expect(getParcelByStatusResponse.status).to.be.equal(element.expected.status);
        const parcels = getParcelByStatusResponse.body;
        if (element.expected.status < 300) {
          expect(parcels).to.be.an("array");
          parcels.forEach((parcel) => {
            expect(parcel.status).to.equal(element.status);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel", () => {
    it.each(resource.getParcelsByInvestigationId, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);

        const getParcelByShipmentIdResponse = await parcelHelper.getParcelsByShipmentId(element.user, element.investigationId, createShipmentResponse.body._id);
        expect(getParcelByShipmentIdResponse.status).to.be.equal(element.expected.status);

        if (element.expected.status < 300) {
          expect(getParcelByShipmentIdResponse.body).to.be.an("array").to.have.lengthOf(1);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelBySessionId, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, element.expected.status);
        const _id = createParcelResponse.body.parcels[createParcelResponse.body.parcels.length - 1];

        /** Check that the parcel is visible */
        const getParcelBySessionIdResponse = await parcelHelper.getParcelBySessionId(element.user);

        expect(getParcelBySessionIdResponse.body).to.be.an("array");
        expect(getParcelBySessionIdResponse.body).to.have.lengthOf.above(0);

        const parcelFound = getParcelBySessionIdResponse.body.find((parcel) => {
          return parcel._id === _id;
        });
        expect(parcelFound._id).to.equal(_id);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/investigation/id/investigationId/parcel/id/{parcelId}", () => {
    it.each(resource.getParcelById, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);
        const parcelId = createParcelResponse.body.parcels[0];

        const getParcelByShipmentIdResponse = await getParcelById(element.user, element.investigationId, parcelId);
        expect(getParcelByShipmentIdResponse.status).to.be.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(getParcelByShipmentIdResponse.body).not.to.be.an("array");
          expect(parcelId).to.be.equal(getParcelByShipmentIdResponse.body._id);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Creates a shipment, then a parcel, then removes the parcel
   */
  describe("DELETE /tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel", () => {
    it.each(resource.deleteParcel, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);

        const deleteParcelResponse = await parcelHelper.deleteParcel(
          element.parcelCreator,
          { _id: createParcelResponse.body.parcels[0] },
          element.investigationId,
          element.parcel.shipmentId
        );

        expect(deleteParcelResponse.status).to.equal(element.expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/investigation/id/:toInvestigationId/parcel", () => {
    it.each(resource.transferItems, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id);
        expect(createParcelResponse.status).to.equal(200);

        const addItemResponse = await itemHelper.addItem(element.itemCreator, element.investigationId, createParcelResponse.body.parcels[0], element.item);
        expect(addItemResponse.status).to.be.equal(200);
        if (element.expected.status < 300) {
          expect(addItemResponse.body.items).to.be.an("array").to.have.lengthOf(1);
        }
        const transferItemResponse = await parcelHelper.transferParcels(
          element.user,
          element.investigationId,
          element.toInvestigationId,
          createShipmentResponse.body._id,
          createParcelResponse.body.parcels
        );

        expect(transferItemResponse.status).to.be.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
