require("it-each")({ testPerIteration: true });

const tagResource = require("../resources/tag.logbook.resource");
const { getSessionId } = require("../helper/session.js");
const { expect } = require("chai");
const { testTag } = require("../helper/logbook");
const { createTag, getTags } = require("../helper/tag");

const prepopulateTags = async (tags) => {
  /** Prepopulating the tags */
  for (let i = 0; i < tags.length; i++) {
    const tag = tags[i];
    const sessionId = await getSessionId(tag.user);
    const createdTag = await createTag(sessionId, tag.investigationId, tag.instrumentName, tag);
    expect(createdTag.status).equal(200);
  }
};

describe("Logbook Tags", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (!global.gServerConfig.database.isMongoUnitDisabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("GET /logbook/:sessionId/tag", () => {
    it.each(tagResource.getTags, "[%s]", ["description"], async (element, next) => {
      try {
        const { tags, instrumentName, investigationId, user, expected } = element;
        /** Prepopulating the tags */
        await prepopulateTags(tags);

        const sessionId = await getSessionId(user);
        const response = await getTags(sessionId, investigationId, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.length).equal(expected.occurrences);
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /logbook/:sessionId/tag", () => {
    it.each(tagResource.createTag, "[%s]", ["description"], async (element, next) => {
      try {
        const sessionId = await getSessionId(element.user);

        const { tags, tag, expected } = element;
        /** Prepopulating the tags */
        await prepopulateTags(tags);
        const createTagResponse = await createTag(sessionId, tag.investigationId, tag.instrumentName, tag);
        expect(createTagResponse.status).to.equal(expected.status);
        if (expected.status < 400) {
          testTag(createTagResponse.body, element.tag);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /logbook/:sessionId/tag", () => {
    it.each(tagResource.updateTag, "[%s]", ["description"], async (element, next) => {
      try {
        const { populate, update, expected } = element;
        const sessionId = await getSessionId(populate.user);
        const { investigationId, instrumentName } = populate.tag;
        /* Creating the tag **/
        const tagResponse = await createTag(sessionId, investigationId, instrumentName, populate.tag);
        const params = new URLSearchParams();
        if (investigationId) params.set("investigationId", investigationId);
        if (instrumentName) params.set("instrumentName", instrumentName);
        params.set("id", tagResponse.body._id);
        const updaterSessionId = await getSessionId(update.user);
        /* Updating the tag **/
        const response = await global.gRequester.put(`/logbook/${updaterSessionId}/tag?${params.toString()}`).send(update.tag);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          testTag(response.body, update.tag);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("E2E /logbook/:sessionId/tag", () => {
    it.each(tagResource.e2e, "[%s]", ["description"], async (element, next) => {
      try {
        const { tags, investigationUser, instrumentScientist, investigationId, instrumentName } = element;

        /** Test starts with some prepopulated tags */
        await prepopulateTags(tags);

        /** InvestigationUser gets the tags of the investigation */
        const investigationUserSessionId = await getSessionId(investigationUser);
        let investigationUserGetTagsResponse = await getTags(investigationUserSessionId, investigationId, instrumentName);
        const investigationTags = tags.filter((tag) => {
          return tag.investigationId === investigationId || tag.instrumentName === instrumentName;
        });
        expect(investigationTags.length).equal(investigationUserGetTagsResponse.body.length);

        /** InstrumentScient gets the tags from the beamline */
        const instrumentScientistSessionId = await getSessionId(instrumentScientist);
        const instrumentScientistGetTagsReponse = await getTags(instrumentScientistSessionId, null, instrumentName);

        const instrumentTags = tags.filter((tag) => {
          return tag.instrumentName === instrumentName;
        });

        expect(instrumentTags.length).equal(instrumentScientistGetTagsReponse.body.length);

        /** Investigation user creates a tag */
        let createTagResponse = await createTag(investigationUserSessionId, investigationId, instrumentName, { name: "test" });
        expect(createTagResponse.status).equal(200);

        investigationUserGetTagsResponse = await getTags(investigationUserSessionId, investigationId, instrumentName);
        /** Checking that the number of investigation tags is increased by one */
        expect(investigationTags.length + 1).equal(investigationUserGetTagsResponse.body.length);

        /** Attempt to create a tag with the same name should produce an error */
        createTagResponse = await createTag(investigationUserSessionId, investigationId, instrumentName, { name: "test" });
        expect(createTagResponse.status).equal(400);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
