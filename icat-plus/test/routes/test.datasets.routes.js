const datasetResource = require("../resources/dataset.resource.js");
const sessionHelper = require("../helper/session.js");
const datasetHelper = require("../helper/dataset.js");
const { expect } = require("chai");
const nock = require("nock");

const datasetAccessController = require("../../app/controllers/datasetaccess.controller");
const STATUS = require("../../app/models/datasetaccess.status.js");
const TYPE = require("../../app/models/datasetaccess.type.js");

require("it-each")({
  testPerIteration: true,
});

describe("Datasets", () => {
  beforeEach(() => {
    try {
      if (!global.gServerConfig.database.isMongoUnitDisabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  function checkLimitParameterInResponse(limit, response) {
    if (limit) {
      expect(response.body.length).to.be.at.most(limit);
    }
  }

  describe("/catalogue/:sessionId/dataset/id/:datasetId/datacollection", () => {
    it.each(datasetResource.getDataCollectionByDataSetId, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/dataset/id/${element.datasetId}/datacollection`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            expect(response.body).to.be.an("array");
            next();
          });
      });
    });
  });

  describe("[DEPRECATED] /catalogue/:sessionId/investigation/id/:investigationId/dataset", () => {
    it.each(datasetResource.getDatasetByInvestigationId, "%s", ["description"], async (element, next) => {
      const { user, investigationId, limit, sortBy, sortOrder, search } = element;
      const response = await datasetHelper.getDeprecatedDatasetsByInvestigationId(user, investigationId, limit, sortBy, sortOrder, search);
      expect(response.status).to.equal(element.expected.status);
      if (element.expected.status < 300) {
        expect(response.body).to.be.an("array").to.have.length.above(0);
        checkLimitParameterInResponse(limit, response);
      }
      return next();
    });
  });

  describe("/catalogue/:sessionId/dataset?investigationId=", () => {
    it.each(datasetResource.getDatasetByInvestigationId, "%s", ["description"], async (element, next) => {
      const { user, investigationId, limit, sortBy, sortOrder, search, datasetType } = element;
      const response = await datasetHelper.getDatasetsByInvestigationId(user, investigationId, limit, sortBy, sortOrder, search, datasetType);
      expect(response.status).to.equal(element.expected.status);
      if (element.expected.status < 300) {
        expect(response.body).to.be.an("array").to.have.length.above(0);
        checkLimitParameterInResponse(limit, response);
        if (datasetType) {
          response.body.forEach((dataset) => {
            expect(dataset.type).equals(datasetType);
          });
        }
      }
      return next();
    });
  });

  describe("[DEPRECATED] /catalogue/:sessionId/dataset/id/:datasetIds/dataset", () => {
    it.each(datasetResource.getDatasetByDatasetIds, "%s", ["description"], (element, next) => {
      global.gRequester
        .post("/session")
        .set("Content-Type", "application/json")
        .send(element.user)
        .end((_err, res) => {
          global.gRequester
            .get(`/catalogue/${res.body.sessionId}/dataset/id/${element.datasetIds}/dataset`)
            .set("Content-Type", "application/json")
            .send()
            .end((_err, response) => {
              expect(response.status).to.equal(element.expected.status);
              expect(response.body).to.be.an("array");
              expect(response.body).to.be.an("array").to.have.lengthOf.above(0);
              next();
            });
        });
    });
  });

  describe("/catalogue/:sessionId/dataset?datasetIds=", () => {
    it.each(datasetResource.getDatasetByDatasetIds, "%s", ["description"], (element, next) => {
      global.gRequester
        .post("/session")
        .set("Content-Type", "application/json")
        .send(element.user)
        .end((_err, res) => {
          global.gRequester
            .get(`/catalogue/${res.body.sessionId}/dataset?datasetIds=${element.datasetIds}`)
            .set("Content-Type", "application/json")
            .send()
            .end((_err, response) => {
              expect(response.status).to.equal(element.expected.status);
              expect(response.body).to.be.an("array");
              expect(response.body).to.be.an("array").to.have.lengthOf.above(0);
              next();
            });
        });
    });
  });

  describe("/catalogue/:sessionId/dataset/id/:datasetIds/dataset_document", () => {
    it.each(datasetResource.getDatasetByDatasetIds, "%s", ["description"], async (element, next) => {
      const response = await datasetHelper.getDatasetDocumentByDatasetIds(element.user, element.datasetIds);
      expect(response.status).to.equal(element.expected.status);
      return next();
    });
  });

  describe("/catalogue/:sessionId/dataset/startdate/:startDate/enddate/:endDate/dataset_document", () => {
    it.each(datasetResource.getDatasetDocumentByDateRange, "%s", ["description"], async (element, next) => {
      const response = await datasetHelper.getDatasetDocumentByDates(element.user, element.startDate, element.endDate);
      try {
        for (const index in response.body) {
          expect(response.body[index].instrumentName).to.be.a("string").to.not.contain("-");
        }
      } catch (e) {
        return next(e);
      }
      expect(response.status).to.equal(element.expected.status);
      return next();
    });
  });

  describe("/doi/prefix/suffix/datasets?sessionId", () => {
    it.each(datasetResource.getDatasetByDOI, "%s", ["description"], async (element, next) => {
      const { user, doi, limit, sortBy, sortOrder, search } = element;
      const response = await datasetHelper.getDatasetsByDOI(user, doi.prefix, doi.suffix, limit, sortBy, sortOrder, search);
      expect(response.status).to.equal(element.expected.status);
      if (element.expected.status < 300) {
        expect(response.status).to.equal(element.expected.status);
        expect(response.body).to.be.an("array").to.have.length.above(0);
        checkLimitParameterInResponse(limit, response);
      }
      return next();
    });
  });

  describe("/catalogue/:sessionId/data/download", () => {
    it.each(datasetResource.getDataDownload, "%s", ["description"], async (element, next) => {
      const { user, datasetIds, datafilesIds } = element;
      const response = await datasetHelper.downloadData(user, datasetIds, datafilesIds);
      expect(response.status).to.equal(element.expected.status);
      return next();
    });
  });

  function checkDatasetAccess(datasetAccess, expected) {
    expect(datasetAccess.datasetId).equal(expected.datasetId);
    expect(datasetAccess.user).equal(expected.user);
    expect(datasetAccess.email).equal(expected.email);
    expect(datasetAccess.type).equal(expected.type);
    expect(datasetAccess.status).equal(expected.status);
  }

  describe("/catalogue/:sessionId/dataset/restore", () => {
    it.each(datasetResource.getDataRestore, "%s", ["description"], async (element, next) => {
      const { user, datasetId, name, email, prepopulate } = element;
      const sessionId = await sessionHelper.getSessionId(user);
      nock(global.gServerConfig.ids.server).get(`/ids/getData?sessionId=${sessionId}&datasetIds=${datasetId}`).reply(200, {});
      await this.prepopulateDataAccesses(prepopulate);
      const response = await datasetHelper.restoreData(sessionId, datasetId, name, email);
      expect(response.status).to.equal(element.expected.status);
      if (element.expected.status < 300) {
        const datasetAccesses = await datasetAccessController.findDatasetAccesses(datasetId, TYPE.RESTORE, STATUS.ONGOING, name);
        expect(datasetAccesses).to.not.be.null;
        expect(datasetAccesses.length).to.equal(1);
        checkDatasetAccess(datasetAccesses[0], element.expected.datasetAccess);
      }
      return next();
    });
  });
});

exports.prepopulateDataAccesses = async (dataAccesses) => {
  if (!dataAccesses) {
    return;
  }
  for (let i = 0; i < dataAccesses.length; i++) {
    await dataAccesses[i].save();
  }
};
