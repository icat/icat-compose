require("it-each")({ testPerIteration: true });

const resource = require("../resources/investigationUsers.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");

describe("InvestigationsUsers", () => {
  describe("/catalogue/:sessionId/investigation/id/:investigationId/investigationusers", () => {
    it.each(resource.investigationusers, "[ %s]", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/investigationusers`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            expect(response.body).to.be.an("array");
            expect(response.body).to.be.an("array").that.is.not.empty;
            next();
          });
      });
    });
  });

  describe("/catalogue/:sessionId/investigation/id/:investigationId/investigationusers", () => {
    it.each(resource.addInvestigationUser, "%s", ["description"], async (element, next) => {
      try {
        const getSessionResponse = await sessionHelper.doGetSession(element.granter);
        const response = await global.gRequester
          .post(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/investigationusers`)
          .set("Content-Type", "application/json")
          .send(element.collaborator);

        if (element.expected.grant.status < 400) {
          expect(response.status).to.equal(element.expected.grant.status);
          /** add was successfully then it is revoked now */
          const getRevokerSessionResponse = await sessionHelper.doGetSession(element.revoker);
          const revokeResponse = await global.gRequester
            .delete(`/catalogue/${getRevokerSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/investigationusers`)
            .set("Content-Type", "application/json")
            .send(element.collaborator);

          expect(revokeResponse.status).to.equal(element.expected.revoke.status);
          if (revokeResponse.status < 400) {
            expect(revokeResponse.body).to.be.an("array");
          } else {
            // Revoke to leave the DB as it was
            const getRevokerSession = await sessionHelper.doGetSession(resource.revoker);
            await global.gRequester
              .delete(`/catalogue/${getRevokerSession.body.sessionId}/investigation/id/${element.investigationId}/investigationusers`)
              .set("Content-Type", "application/json")
              .send(element.collaborator);
          }
        } else {
          expect(response.status).to.equal(element.expected.grant.status);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
