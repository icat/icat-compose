require("it-each")({ testPerIteration: true });

const trackingResource = require("../resources/shipment.resource.js");
const sessionHelper = require("../helper/session.js");
const shipmentHelper = require("../helper/shipment.js");
const addressHelper = require("../helper/address.js");
const { expect } = require("chai");

const testShipment = (shipment) => {
  expect(shipment._id).to.not.equal(null);
  expect(shipment.investigationId).to.not.equal(null);
  expect(shipment.investigationName).to.not.equal(null);
};

describe("Tracking Shipment", () => {
  describe("GET /tracking/{sessionId}/investigation/id/{investigationId}/shipment", () => {
    it.each(trackingResource.getShipmentsByInvestigationId, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/shipment`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            try {
              expect(response.status).to.equal(element.expected.status);
              return next();
            } catch (e) {
              return next(e);
            }
          });
      });
    });
  });

  describe("GET /tracking/{sessionId}/shipment/id/{shipmentId}", () => {
    it.each(trackingResource.getShipmentById, "%s", ["description"], async (element, next) => {
      const responseShipmentCreation = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
      const responseGetShiomentById = await shipmentHelper.getShipmentById(element.shipmentReader, responseShipmentCreation.body._id);
      try {
        expect(responseGetShiomentById.status).to.equal(element.expected.status);
        if (element.shipment.status < 300) {
          testShipment(responseGetShiomentById.body);
          expect(responseGetShiomentById.body._id).to.equal(responseShipmentCreation.body._id);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("DELETE /tracking/{sessionId}/investigation/id/{investigationId}/shipment", () => {
    it.each(trackingResource.deleteShipment, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        const deleteShipmentResponse = await shipmentHelper.deleteShipment(element.shipmentRemover, createShipmentResponse.body, element.investigationId);
        expect(deleteShipmentResponse.status).to.equal(element.expected.status);
        if (deleteShipmentResponse.status < 300) {
          const getShipmentByIdResponse = await shipmentHelper.getShipmentById(element.shipmentCreator, createShipmentResponse.body._id);
          expect(getShipmentByIdResponse.body.status).to.be.equal("REMOVED");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /tracking/{sessionId}/investigation/id/{investigationId}/shipment", () => {
    it.each(trackingResource.createShipment, "%s", ["description"], async (element, next) => {
      try {
        if (element.shippingAddress) {
          const shippingAddressCreationResponse = await addressHelper.createAddress(element.user, element.shippingAddress, element.investigationId);
          element.shipment.defaultShippingAddress = shippingAddressCreationResponse.body._id;
        }

        if (element.returnAddress) {
          const addressCreationResponse = await addressHelper.createAddress(element.user, element.returnAddress, element.investigationId);
          element.shipment.defaultReturnAddress = addressCreationResponse.body;
        }

        const shipmentCreationResponse = await shipmentHelper.createShipment(element.user, element.shipment, element.investigationId);
        expect(shipmentCreationResponse.status).to.equal(element.expected.status);
        testShipment(shipmentCreationResponse.body);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
