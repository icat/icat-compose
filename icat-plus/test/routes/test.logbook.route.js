require("it-each")({ testPerIteration: true });

const moment = require("moment");
const _ = require("lodash");
const eventResource = require("../resources/logbook.resource");
const sessionHelper = require("../helper/session.js");
const logbookHelper = require("../helper/logbook.js");
const { expect } = require("chai");
const { testEvent, doFindEventsDates } = require("../helper/logbook");
const logbookControllerHelper = require("../../app/controllers/helpers/helper.logbook.controller");
const { prepopulateTags } = require("../helper/tag");

describe("Logbook", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (!global.gServerConfig.database.isMongoUnitDisabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("/logbook/:sessionId/stats/investigation", () => {
    it.each(eventResource.stats, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, startDate, endDate, expected, logbook } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;

        /** Populating events */
        await this.prepopulateEvents(logbook, sessionId);

        const params = new URLSearchParams();
        if (startDate) params.set("startDate", startDate);
        if (endDate) params.set("endDate", endDate);

        const response = await global.gRequester.get(`/logbook/${sessionId}/stats/investigation?${params.toString()}`).send();
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.length).equal(new Set(logbook.map((log) => log.investigationId)).size);
          /** This test could be completed by checking the number of annotations and notifications retrieved for each investigation */
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/logbook/:sessionId/stats/count", () => {
    it.each(eventResource.count, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, startDate, endDate, expected, logbook } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;

        /** Populating events */
        await this.prepopulateEvents(logbook, sessionId);

        const params = new URLSearchParams();
        if (startDate) params.set("startDate", startDate);
        if (endDate) params.set("endDate", endDate);

        const response = await global.gRequester.get(`/logbook/${sessionId}/stats/count?${params.toString()}`).send();
        expect(response.status).to.equal(expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/logbook/:sessionId/event/createfrombase64", () => {
    it.each(eventResource.createFromBase64, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, investigationId, instrumentName, expected } = element;
        const response = await logbookHelper.createEventFrombase64(user, element.body, investigationId, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          testEvent(response.body, expected.event);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("[DEPRECATED] /logbook/:sessionId/investigation/name/:investigationName/instrument/name/:instrumentName/event/createfrombase64", () => {
    it.each(eventResource.createFromBase64byBeamlineAndInsturmentName, "[DEPRECATED] %s", ["description"], async (element, next) => {
      try {
        const response = await global.gRequester
          .post(`/logbook/${element.API_KEY}/investigation/name/${element.investigationName}/instrument/name/${element.instrumentName}/event/createfrombase64`)
          .set("Content-Type", "application/json")
          .send(element.body);

        expect(response.status).to.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/logbook/:sessionId/event/page", () => {
    it.each(eventResource.page, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, event, types, limit, sortOrder, instrumentName, testEventIndices, creator } = element;
        const getCreatorSessionResponse = await sessionHelper.doGetSession(creator.credential);
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;
        /** Populating events by the creator */
        const events = await this.prepopulateEvents(
          [...new Array(element.eventCount)].map(() => event),
          getCreatorSessionResponse.body.sessionId
        );

        const responses = await Promise.all(testEventIndices.map((index) => doPageEvent(sessionId, events[index]._id, investigationId, types, limit, sortOrder, instrumentName)));
        const pages = responses.map((response) => response.body.page);
        const status = new Set(responses.map((response) => response.status));

        expect(status.has(element.expected.status)).equal(true);

        if (element.expected.status < 400) {
          for (let i = 0; i < pages.length; i++) {
            expect(pages[i].index).equal(element.expected.index[i]);
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event/count", () => {
    it.each(eventResource.countEvents, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, populateUser, events, types, instrumentName, search, useDate, filterInvestigation, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;
        /** Populating events */
        await this.userPrepopulateEvents(events, populateUser);
        /** Do query */
        const date = useDate ? moment().format("YYYY-MM-DD") : undefined;
        const queryResponse = await doCountEvent(sessionId, investigationId, types, search, instrumentName, date, filterInvestigation);
        const { body } = queryResponse;
        expect(queryResponse.status).equal(expected.status);
        if (queryResponse.status < 300) {
          expect(body[0].totalNumber).equal(expected.occurrences);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("SEARCH GET /logbook/:sessionId/event", () => {
    it.each(eventResource.search, "[%s]", ["description"], async (element, next) => {
      try {
        const { expected } = element;
        let { events } = element;
        const { matches, status, searchs } = expected;
        const { investigationId, user, search, limit, skip, types, sortBy, sortOrder, tags, doExtraSearch, instrumentName, useDate } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;

        let tagList = [];
        if (tags) {
          /** Populating tags **/
          tagList = await prepopulateTags(tags, sessionId);
          /** This is needed to work with the tags otherwise it is constant and its value remains unchanged */
          events = JSON.parse(JSON.stringify(events));
          /** Event.tag is an array of ObjectId then we need to replace to simulate that the event has the right reference to the tag **/
          events.forEach((event) => {
            const tagIds = [];
            if (event.tag) {
              event.tag.forEach((evenTag) => {
                const foundTag = tagList.find((tag) => evenTag.toLowerCase() === tag.name.toLowerCase());
                if (foundTag) {
                  tagIds.push(foundTag._id);
                }
              });
            }

            event.tag = tagIds;
          });
        }
        /** Populating events */
        await this.prepopulateEvents(events, sessionId);

        /** Do query */
        const date = useDate ? moment().format("YYYY-MM-DD") : undefined;
        const queryResponse = await doFindEvent(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName, date);
        const { body } = queryResponse;
        expect(queryResponse.status).equal(status);

        if (status < 300) {
          expect(body.length).equal(matches);
          if (matches > 0) {
            if (searchs) {
              for (let i = 0; i < body.length; i++) {
                const event = body[i];
                expect(event.meta.search.page).equal(expected.occurrences[i].page);
                expect(event.meta.search.occurrences).equal(expected.occurrences[i].occurrences);
              }
            }
            checkEventDate(date, body);
          }
        }

        if (doExtraSearch) {
          const doExtraSearchResponse = await doFindEvent(sessionId, doExtraSearch.investigationId, types, limit, skip, sortBy, sortOrder, search);
          expect(doExtraSearchResponse.status).equal(doExtraSearch.expected.status);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /logbook/:sessionId/event", () => {
    it.each(eventResource.createEvent, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, event, expected, instrumentName } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;

        const url = getEventEndPointURL(sessionId, investigationId, null, null, null, null, null, null, instrumentName);
        const response = await global.gRequester.post(url).send(event);
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          event.content = expected.content;
          testEvent(response.body, event, ["title", "investigationId", "type", "category", "creationDate"]);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /logbook/:sessionId/event", () => {
    it.each(eventResource.updateEvent, "%s", ["description"], async (element, next) => {
      try {
        const { user, createEventTags, updateEventTags, updateEventStep, investigationId, expected, instrumentName } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);

        let { sessionId } = getSessionResponse.body;
        /** Tag Creation */
        let tags = [];
        let tagsCreation = [];
        if (createEventTags) {
          tagsCreation = [...tagsCreation, ...createEventTags];
        }
        if (updateEventTags) {
          tagsCreation = [...tagsCreation, ...updateEventTags];
        }

        if (tagsCreation.length > 0) {
          tags = await prepopulateTags(tagsCreation, sessionId);
        }

        // Assing the recently created tag to the event
        if (createEventTags) {
          createEventTags.forEach((createEventTag) => {
            element.event.tag.push(_.find(tags, (createdTag) => createdTag.name === createEventTag.name)._id);
          });
        }

        // Creating event
        const events = await this.prepopulateEvents([element.event], sessionId);

        // Updating the event
        if (updateEventStep) {
          updateEventStep.event._id = events[0]._id;

          if (updateEventStep.user) {
            const updaterSessionResponse = await sessionHelper.doGetSession(updateEventStep.user.credential);
            sessionId = updaterSessionResponse.body.sessionId;
          }
          if (updateEventTags) {
            updateEventTags.forEach((updateEventTag) => {
              updateEventStep.event.tag.push(_.find(tags, (createdTag) => createdTag.name === updateEventTag.name)._id);
            });
          }

          const url = getEventEndPointURL(sessionId, investigationId, null, null, null, null, null, null, instrumentName);

          const eventUpdatedResponse = await global.gRequester.put(url).send(updateEventStep.event);
          expect(eventUpdatedResponse.status).to.equal(expected.status);
          if (element.expected.status === 200) {
            testEvent(eventUpdatedResponse.body, expected.event); // test the latest version of the event
            if (expected.event.previousVersionEvent) {
              testEvent(eventUpdatedResponse.body.previousVersionEvent, expected.event.previousVersionEvent);
            }
            if (instrumentName) {
              /**Instrument are upper case in ICAT */
              expect(eventUpdatedResponse.body.instrumentName).equal(instrumentName.toUpperCase());
            }
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("[DEPRECATED] /logbook/:sessionId/investigation/name/:investigationName/instrument/name/:instrumentName/event", () => {
    describe("[DEPRECATED]  Create investigation notification", () => {
      it.each(eventResource.createInvestigationNotification, "[DEPRECATED]  %s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, user, expected, event } = element;
          const { investigationName, instrumentName } = event;
          const getSessionResponse = await sessionHelper.doGetSession(user);
          const sessionId = getSessionResponse.body;
          await prepopulateTags(tagsToFillDB, sessionId);
          const response = await global.gRequester
            .post(`/logbook/${global.gServerConfig.server.API_KEY}/investigation/name/${investigationName}/instrument/name/${instrumentName}/event`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            testEvent(response.body, expected.event);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("[DEPRECATED] /logbook/:sessionId/notification", () => {
    describe("[DEPRECATED] Create investigation notification", () => {
      it.each(eventResource.createInvestigationNotification, "[DEPRECATED] %s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, user, expected, event } = element;
          const { investigationName, instrumentName } = event;
          const getSessionResponse = await sessionHelper.doGetSession(user);
          const sessionId = getSessionResponse.body;
          await prepopulateTags(tagsToFillDB, sessionId);
          const response = await global.gRequester
            .post(`/logbook/${global.gServerConfig.server.API_KEY}/notification?investigationName=${investigationName}&instrumentName=${instrumentName}`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            testEvent(response.body, expected.event);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("[DEPRECATED]  /logbook/:sessionId/instrument/name/:instrumentName/event", () => {
    describe("[DEPRECATED] Create beamline notification", () => {
      it.each(eventResource.createBeamlineNotification, "[DEPRECATED]  %s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, sessionId, event, expected } = element;
          const { instrumentName } = event;
          await prepopulateTags(tagsToFillDB, sessionId);
          const response = await global.gRequester
            .post(`/logbook/${global.gServerConfig.server.API_KEY}/instrument/name/${instrumentName}/event`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            testEvent(response.body, event);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("/logbook/:sessionId/notification", () => {
    describe("Create beamline notification", () => {
      it.each(eventResource.createBeamlineNotification, "%s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, sessionId, event, expected } = element;
          const { instrumentName } = event;
          await prepopulateTags(tagsToFillDB, sessionId);
          const response = await global.gRequester
            .post(`/logbook/${global.gServerConfig.server.API_KEY}/notification?instrumentName=${instrumentName}`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            testEvent(response.body, event);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe.skip("/logbook/:sessionId/investigation/id/:investigationId/event/pdf", () => {
    it("Get a PDF", (done) => {
      const input = eventResource.getPDF.input;

      sessionHelper.doGetSession(input.user).then((getSessionResponse) => {
        global.gRequester
          .post(`/logbook/${getSessionResponse.body.sessionId}/investigation/id/${input.investigationId}/event/create`)
          .set("Content-Type", "application/json")
          .send(input.event)
          .end((err, response1) => {
            expect(err).to.be.null;
            expect(response1.status).to.equal(200);
            global.gRequester
              .get(`/logbook/${getSessionResponse.body.sessionId}/investigation/id/${input.investigationId}/event/pdf`)
              .query({
                find: JSON.stringify(input.query.find),
                sort: JSON.stringify(input.query.sort),
                skip: JSON.stringify(input.query.skip),
                limit: JSON.stringify(input.query.limit),
              })
              .end((err, response2) => {
                expect(err).to.be.null;
                expect(response2.status).to.equal(200);
                expect(response2.text).to.be.not.empty; //that's where the file is.
                expect(response2.body).to.be.empty;
                done();
              });
          });
      });
    });
  });

  describe("-GET /logbook/:sessionId/event", () => {
    it.each(eventResource.getEvents, "%s", ["description"], async (element, next) => {
      try {
        const { user, events, investigationId, instrumentName, expected, types, limit, skip, sortBy, sortOrder, search, useDate, filterInvestigation } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        if (element.events) {
          await this.prepopulateEvents(events, sessionId);
        }
        /** Doing the find request */
        const date = useDate ? moment().format("YYYY-MM-DD") : undefined;
        const response = await doFindEvent(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName, date, filterInvestigation);
        const eventsResponse = response.body;
        expect(response.status).to.equal(expected.status);

        if (expected.status < 300) {
          /** if investigationId/instrumentName then it checks that events belong to that investigation/beamline */
          eventsResponse.forEach((event) => {
            if (investigationId) {
              expect(event.investigationId).to.equal(investigationId);
            }
            if (instrumentName) {
              expect(event.instrumentName.toUpperCase()).to.equal(instrumentName.toUpperCase());
            }
          });

          if (types) {
            /** if types then it checks that events are filtered corerctly by type and category */
            const categories = logbookControllerHelper.parseTypesToTypeCategory(types);
            eventsResponse.forEach((event) => {
              const found = categories.find((e) => e.type === event.type && e.category === event.category) !== null;
              expect(found).to.equal(true);
            });
          }
          checkEventDate(date, eventsResponse);

          /** Check number of occurences */
          if (expected.occurrences) {
            expect(eventsResponse.length).to.equal(expected.occurrences);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event for broadcast", () => {
    it.each(eventResource.getBroadcastEvents, "%s", ["description"], async (element, next) => {
      try {
        const { user, events, investigationId, instrumentName, types, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        if (events) {
          await this.prepopulatesBroadcastEvents(events);
        }
        /** Doing the find request */
        const response = await doFindEvent(sessionId, investigationId, types, null, null, null, null, null, instrumentName, null);
        const eventsResponse = response.body;
        expect(response.status).to.equal(expected.status);

        if (expected.status < 300) {
          /** Check number of occurences */
          if (expected.occurrences) {
            expect(eventsResponse.length).to.equal(expected.occurrences);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event/dates", () => {
    it.each(eventResource.eventsDates, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, populateUser, search, events, types, instrumentName, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        await this.userPrepopulateEvents(events, populateUser);
        /** Do query */
        const response = await doFindEventsDates(sessionId, investigationId, types, search, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const { body } = response;
          expect(body.length).equal(expected.result.length);
          body.forEach((date, i) => {
            expect(date._id).equal(expected.result[i]._id);
            expect(date.event_count).equal(expected.result[i].event_count);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /logbook/:sessionId/event/move", () => {
    it.each(eventResource.moveEvents, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, events, sourceInvestigationId, destinationInvestigationId, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        await this.userPrepopulateEvents(events, user);
        /** Do query */
        const response = await logbookHelper.doMoveEvents(sessionId, sourceInvestigationId, destinationInvestigationId);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const { body } = response;
          expect(body.nModified).equal(expected.modified);
          if (expected.events) {
            const getResponse = await doFindEvent(sessionId, destinationInvestigationId);
            const eventsResponse = getResponse.body;
            expect(eventsResponse.length).to.equal(expected.events.length);
            expected.events.forEach((expectedEvent, index) => {
              testEvent(eventsResponse[index], expectedEvent);
            });
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

exports.prepopulatesBroadcastEvents = async (events) => {
  const responses = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationName, instrumentName } = event;
    const params = new URLSearchParams();
    if (investigationName) params.set("investigationName", investigationName);
    if (instrumentName) params.set("instrumentName", instrumentName);
    const url = `/dataacquisition/${global.gServerConfig.server.API_KEY}/notification?${params.toString()}`;
    const response = await global.gRequester.post(url).send(event);
    responses.push(response.body);
  }
  return responses;
};

/** We can not use Promise.all because of the order of the events could not match the order of the creation */
exports.prepopulateEvents = async (events, sessionId) => {
  const responses = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationId, instrumentName } = event;
    const url = getEventEndPointURL(sessionId, investigationId, null, null, null, null, null, null, instrumentName);
    const response = await global.gRequester.post(url).send(event);
    responses.push(response.body);
  }
  return responses;
};

exports.userPrepopulateEvents = async (events, user) => {
  const sessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = sessionResponse.body;
  await this.prepopulateEvents(events, sessionId);
};

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event endpoint
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} types
 * @param {*} limit
 * @param {*} skip
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} search
 * @param {*} instrumentName
 * @returns
 */
function getEventEndPointURL(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName, date, filterInvestigation) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", skip);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (date) params.set("date", date);
  if (filterInvestigation) params.set("filterInvestigation", filterInvestigation);

  return `/logbook/${sessionId}/event?${params.toString()}`;
}

async function doFindEvent(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName, date, filterInvestigation) {
  const url = getEventEndPointURL(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName, date, filterInvestigation);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
}

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event/meta endpoint
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} types
 * @param {*} limit
 * @param {*} sortOrder
 * @param {*} instrumentName
 * @returns
 */
function getEventPageEndPointURL(sessionId, _id, investigationId, types, sortOrder, instrumentName, limit) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  if (_id) params.set("_id", _id);
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (limit) params.set("limit", limit);
  return `/logbook/${sessionId}/event/page?${params.toString()}`;
}

async function doPageEvent(sessionId, _id, investigationId, types, limit, sortOrder, instrumentName) {
  const url = getEventPageEndPointURL(sessionId, _id, investigationId, types, sortOrder, instrumentName, limit);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
}

function checkEventDate(date, events) {
  if (date) {
    events.forEach((event) => {
      expect(event.creationDate).is.not.null;
      expect(event.creationDate.substr(0, 10)).equal(date);
    });
  }
}

async function doCountEvent(sessionId, investigationId, types, search, instrumentName, date, filterInvestigation) {
  const url = getCountEventEndPointURL(sessionId, investigationId, types, search, instrumentName, date, filterInvestigation);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
}

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event/count endpoint
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} types
 * @param {*} search
 * @param {*} instrumentName
 * @param {*} date
 * @param {*} filterInvestigation
 * @returns
 */
function getCountEventEndPointURL(sessionId, investigationId, types, search, instrumentName, date, filterInvestigation) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (search) params.set("search", search);
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (date) params.set("date", date);
  if (filterInvestigation) params.set("filterInvestigation", filterInvestigation);

  return `/logbook/${sessionId}/event/count?${params.toString()}`;
}
