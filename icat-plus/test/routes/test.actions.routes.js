require("it-each")({ testPerIteration: true });

const resource = require("../resources/actions.resource.js");
const sessionHelper = require("../helper/session");
const { expect } = require("chai");
const { prepopulateEvents } = require("./test.logbook.route.js");

const Event = require("../../app/models/event.model.js");

describe("Actions", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (!global.gServerConfig.database.isMongoUnitDisabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("GET /actions/:sessionId/logbook/instrumentname?action=fill", () => {
    it.each(resource.fill, "%s", ["description"], async (element, next) => {
      try {
        const { expected, logbook, user } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;

        await prepopulateEvents(logbook, sessionId);

        /** Removing instrumentName */
        const events = await Event.find({});
        events.forEach(async (event) => {
          event.instrumentName = null;
          await event.save();
        });

        const response = await global.gRequester.put(`/actions/${sessionId}/logbook/instrumentname?action=fill`).send();

        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.missing).to.equal(expected.missing);
          expect(response.body.count).to.equal(expected.count);
          expect(response.body.updated).to.equal(expected.updated);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
