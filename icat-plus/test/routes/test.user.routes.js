require("it-each")({ testPerIteration: true });

const userResource = require("../resources/user.resource.js");
const sessionHelper = require("../helper/session.js");

describe("Users", () => {
  describe("GET /catalogue/:sessionId/user", () => {
    it.each(userResource.getUsers, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        expect(getSessionResponse.status).equal(200);
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/user`)
          .set("Content-Type", "application/json")
          .send()
          .end((err, response) => {
            if (!err) {
              expect(response.status).to.equal(element.expected.status);
              return next();
            }
            return next(err);
          });
      });
    });
  });
});
