require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const doiResource = require("../resources/doi.resource.js");
const { expect } = require("chai");

describe("DOI", () => {
  describe("/doi/:sessionId/mint", () => {
    it.each(doiResource.mint, "%s", ["description"], async (element, next) => {
      const { user, datasetIdList, title, abstract, authors, expected } = element;
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      try {
        // TODO SKIP until we use the test datacite plateform #242
        const response = await global.gRequester.post(`/doi/${sessionId}/mint?skip=true`).send({ datasetIdList, title, abstract, authors });
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  function checkDOIUser(expected, user) {
    expect(user.name).equal(expected.name);
    if (expected.nameIdentifiers) {
      expect(user.nameIdentifiers).not.to.be.undefined;
      expect(user.nameIdentifiers.length).to.equal(1);
      expect(user.nameIdentifiers[0].nameIdentifier).equal(expected.nameIdentifiers[0].nameIdentifier);
    } else {
      expect(user.nameIdentifiers).to.be.undefined;
    }
  }

  describe("GET /doi/:prefix/:suffix/json-datacite", async () => {
    it.each(doiResource.datacite, "[%s]", ["description"], async (element, next) => {
      try {
        const { prefix, suffix, expected } = element;
        /** Do query */
        const queryResponse = await global.gRequester.get(`/doi/${prefix}/${suffix}/json-datacite`).send();
        expect(queryResponse.status).to.equal(queryResponse.status);
        const { subjects, creators, contributors, relatedIdentifiers } = queryResponse.body;
        expect(subjects.length).equal(3);
        expect(subjects[0].subjectScheme).equal("Proposal Type Description");
        expect(subjects[0].subject).equal(expected.proposalTypeDescription);
        expect(subjects[1].subjectScheme).equal("Proposal");
        expect(subjects[1].subject).equal(expected.name);
        expect(subjects[2].subjectScheme).equal("Instrument");
        expect(subjects[2].subject).equal(expected.instrument);
        expect(creators.length).equal(expected.creators.length);
        creators.forEach((creator, id) => {
          checkDOIUser(creator, expected.creators[id]);
        });
        expect(contributors.length).equal(expected.contributors.length);
        contributors.forEach((contributor, id) => {
          checkDOIUser(contributor, expected.contributors[id]);
        });
        if (expected.relatedIdentifiers) {
          expect(relatedIdentifiers).not.to.be.null;
          expect(relatedIdentifiers.length).equal(expected.relatedIdentifiers.length);
          relatedIdentifiers.forEach((relatedIdentifier, id) => {
            expect(relatedIdentifier.relatedIdentifier).equal(expected.relatedIdentifiers[id].relatedIdentifier);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
