const parameterResource = require("../resources/parameter.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("Parameters", () => {
  describe("/catalogue/:sessionId/datasetParameter", () => {
    it.each(parameterResource.updateParameter, "%s", ["description"], async (element, next) => {
      try {
        const { user, parameterId, value } = element;
        const newValue = value ? value + new Date().toLocaleString() : undefined;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const sessionId = sessionResponse.body.sessionId;
        const parameter = { value: newValue };
        const response = await global.gRequester.put(`/catalogue/${sessionId}/datasetParameter?parameterId=${parameterId}`).set("Content-Type", "application/json").send(parameter);
        expect(response.status).to.be.equal(element.expected.status);
        if (element.expected.status === 200) {
          expect(response.body.value).to.be.equal(newValue);
          expect(response.body.parameterId).to.be.equal(parameterId);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/parameters", () => {
    it.each(parameterResource.getParameters, "%s", ["description"], async (element, next) => {
      try {
        const { expected, parameterTypeId, name } = element;
        let response = null;

        const params = new URLSearchParams();
        if (parameterTypeId) params.set("parameterTypeId", parameterTypeId);
        if (name) params.set("name", name);

        if (parameterTypeId || name) {
          response = await global.gRequester.get(`/catalogue/parameters?${params.toString()}`).set("Content-Type", "application/json").send();
        } else {
          response = await global.gRequester.get(`/catalogue/parameters`).set("Content-Type", "application/json").send();
        }
        expect(response.status).to.be.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.a("array");
          if (parameterTypeId || name) {
            expect(response.body.length).to.be.equal(1);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
