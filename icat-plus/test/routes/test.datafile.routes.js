require("it-each")({ testPerIteration: true });

const datafileResource = require("../resources/datafile.resource.js");
const datafileHelper = require("../helper/datafile.js");
const { expect } = require("chai");

describe("Datafile", () => {
  describe("/catalogue/:sessionId/dataset/id/:datasetId/datafile", () => {
    it.each(datafileResource.byDatasets, "[%s]", ["description"], async (element, next) => {
      const { user, datasetId, limit, skip, search, expected } = element;
      const response = await datafileHelper.getDatafilesByDatasetId(user, datasetId, limit, skip, search);
      expect(response.status).to.equal(expected.status);
      if (expected.status < 300) {
        expect(response.body).to.be.an("array").of.length(expected.fileCount);
        if (limit) {
          expect(response.body.length).to.be.at.most(limit);
        }
      }
      return next();
    });
  });
});
