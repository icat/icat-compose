const sessionHelper = require("./session.js");

/**
 * This method will login and will create a shipment
 * @param {*} user
 * @param {*} shipment
 */
exports.createShipment = async (user, shipment, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .post(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment`)
    .set("Content-Type", "application/json")
    .send(shipment);
};

/**
 * This method will login and will create a shipment
 * @param {*} user
 * @param {*} shipment
 */
exports.deleteShipment = async (user, shipment, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .delete(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment`)
    .set("Content-Type", "application/json")
    .send(shipment);
};

/**
 * This method will login and will get a Shipment by Id
 * @param {*} user
 * @param {*} shipment
 */
exports.getShipmentById = async (user, shipmentId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/shipment/id/${shipmentId}`).set("Content-Type", "application/json").send();
};

module.exports.createShipment = this.createShipment;
module.exports.getShipmentById = this.getShipmentById;
