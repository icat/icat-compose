const sessionHelper = require("./session.js");
/*
exports.getInvestigationBy = async (user, key, value) => {
  const params = key ? `${key}=${value}` : undefined;
  return this.sendGetInvestigation(user, params);
};
*/

exports.getInvestigationBy = async (user, instrumentName, filter, limit, startDate, endDate, sortBy, sortOrder, search, ids, time, investigationName, skip) => {
  const params = new URLSearchParams();
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (filter) params.set("filter", filter);
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", 0);
  if (startDate) params.set("startDate", startDate);
  if (endDate) params.set("endDate", endDate);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  if (ids) params.set("ids", ids);
  if (time) params.set("time", time);
  if (investigationName) params.set("investigationName", investigationName);

  const getSessionResponse = await sessionHelper.doGetSession(user);
  const request = `/catalogue/${getSessionResponse.body.sessionId}/investigation?${params.toString()}`;
  return global.gRequester.get(request).set("Content-Type", "application/json").send();
};

exports.sendGetInvestigation = async (user, params) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const withParams = params ? `?${params}` : "";
  const request = `/catalogue/${getSessionResponse.body.sessionId}/investigation${withParams}`;
  return global.gRequester.get(request).set("Content-Type", "application/json").send();
};

exports.getInvestigationByFilter = async (user, filter, limit, startDate, endDate, sortBy, sortOrder, search) => {
  const keyFilter = filter ? `&filter=${filter}` : "";
  const keyLimit = limit ? `&limit=${limit}&skip=0` : "";
  const keyStartDate = startDate ? `&startDate=${startDate}` : "";
  const keyEndDate = endDate ? `&endDate=${endDate}` : "";
  const keySortBy = sortBy ? `&sortBy=${sortBy}` : "";
  const keySortOrder = sortOrder ? `&sortOrder=${sortOrder}` : "";
  const keySearch = search ? `&search=${search}` : "";
  return this.sendGetInvestigation(user, `${keyFilter}${keyLimit}${keyStartDate}${keyEndDate}${keySortBy}${keySortOrder}${keySearch}`);
};

exports.getInvestigationByInstrument = async (user, instrument, limit) => {
  const keyLimit = limit ? `&limit=${limit}&skip=0` : "";
  const keyInstrument = instrument ? `&instrument=${instrument}` : "";
  return this.sendGetInvestigation(user, `${keyLimit}${keyInstrument}`);
};

exports.getAllocationBy = async (user, investigationName, instrumentName, time) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);

  return global.gRequester
    .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/name/${investigationName}/instrument/name/${instrumentName}?time=${time}`)
    .set("Content-Type", "application/json")
    .send();
};
