exports.doGetSession = async (user) => {
  const response = await global.gRequester.post(`/session`).set("Content-Type", "application/json").send(user);
  return response;
};

exports.getSessionId = async (user) => {
  const getInvestigationUserSessionResponse = await this.doGetSession(user);
  const { sessionId } = getInvestigationUserSessionResponse.body;
  return sessionId;
};

exports.doLogout = async (sessionId) => {
  const response = await global.gRequester.delete(`/session/${sessionId}`).set("Content-Type", "application/json").send();
  return response;
};

exports.getSessionInformation = async (sessionId) => {
  const response = await global.gRequester.get(`/session/${sessionId}`).set("Content-Type", "application/json").send();
  return response;
};
