const sessionHelper = require("./session.js");

/**
 * This method will login and will create a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function createParcel(user, parcel, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .post(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/parcel`)
    .set("Content-Type", "application/json")
    .send(parcel);
}

/**
 * This method will login and will create a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function deleteParcel(user, parcelId, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .delete(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/parcel`)
    .set("Content-Type", "application/json")
    .send(parcelId);
}

/**
 * This method will login and will get the parcels by a shipmentId
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelsByShipmentId(user, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/parcel`)
    .set("Content-Type", "application/json")
    .send();
}

/**
 * This method will login and will get the parcels by a shipmentId
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelBySessionId(user) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/parcel`).set("Content-Type", "application/json").send();
}

/**
 * This method will login and will get a parcel by id
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelById(user, investigationId, parcelId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}`)
    .set("Content-Type", "application/json")
    .send();
}

/**
 * This method will login and will get a parcel by status
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelByStatus(user, status) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/parcel/status/${status}`).set("Content-Type", "application/json").send();
}

/**
 * This method will login and will set the status of a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function setParcelStatus(user, investigationId, parcelId, status, parcel) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .put(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}/status/${status}`)
    .set("Content-Type", "application/json")
    .send(parcel);
}

/**
 * This method will login and will set the status of a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function transferParcels(user, investigationId, toInvestigationId, shipmentId, parcels) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .put(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/investigation/id/${toInvestigationId}/parcel`)
    .set("Content-Type", "application/json")
    .send(parcels);
}

module.exports = {
  createParcel,
  deleteParcel,
  getParcelsByShipmentId,
  getParcelById,
  getParcelByStatus,
  setParcelStatus,
  transferParcels,
  getParcelBySessionId,
};
