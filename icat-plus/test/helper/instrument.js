const sessionHelper = require("./session.js");

/**
 * This method will get the list of instruments
 * @param {*} user
 * @param {*} shipment
 */
exports.getInstruments = async () => {
  return new Promise((resolve, reject) => {
    global.gRequester
      .get("/catalogue/instruments")
      .set("Content-Type", "application/json")
      .send()
      .end((err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
  });
};

exports.getInstrumentsBySessionId = async (user, filter) => {
  const param = filter ? `?filter=${filter}` : "";
  return new Promise((resolve, reject) => {
    sessionHelper.doGetSession(user).then((getSessionResponse) => {
      global.gRequester
        .get(`/catalogue/${getSessionResponse.body.sessionId}/instruments${param}`)
        .set("Content-Type", "application/json")
        .send()
        .end((err, response) => {
          if (err) {
            reject(err);
          } else {
            resolve(response);
          }
        });
    });
  });
};
