const sessionHelper = require("./session.js");

exports.getSamplesByInvestigationId = async (user, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/sample`).set("Content-Type", "application/json").send();
};
