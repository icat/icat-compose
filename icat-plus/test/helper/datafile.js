const sessionHelper = require("./session.js");

exports.getDatafilesByDatasetId = async (user, datasetId, limit, skip, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", skip);
  if (search) params.set("search", search);
  return global.gRequester
    .get(`/catalogue/${getSessionResponse.body.sessionId}/dataset/id/${datasetId}/datafile?${params.toString()}`)
    .set("Content-Type", "application/json")
    .send();
};
