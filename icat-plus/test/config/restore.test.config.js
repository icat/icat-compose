/**
 * restore email configuration file
 */
module.exports = {
  enabled: false,
  fromAddress: "restorerequests@esrf.fr",
  smtp: {
    host: "smtp.esrf.fr",
    port: 25,
  },
};
