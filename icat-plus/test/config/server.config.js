const path = require("path");
require("dotenv").config({ path: path.resolve(process.cwd(), ".env.test.local") });

module.exports = {
  server: {
    port: 8000,
    url: "https://test22.esrf.fr",
    API_KEY: process.env.LOGBOOK_API_KEY,
  },
  database: {
    uri: process.env.MONGO_DB_URL,
    isMongoUnitDisabled: !!process.env.MONGO_DB_URL, // Disable Mongo Unit if URL to a DB is supplied
  },
  oaipmh: {
    enabled: true,

    server: {
      /** Server that actually implements OAI-PMH and requests will be transfered to */
      url: process.env.ICAT_SERVER_URL || "https://ovm-icat-test.esrf.fr",
      port: process.env.ICAT_SERVER_PORT || "8181",
    },
  },
  elasticsearch: {
    enabled: true,
    apiVersion: 7.2,
    server: process.env.ELASTIC_SEARCH_SERVER,
  },
  datacite: {
    prefix: "10.81044",
    suffix: "TEST-ESRF-",
    username: process.env.DATACITE_USERNAME,
    password: process.env.DATACITE_PASSWORD,
    mds: "https://mds.test.datacite.org",
    landingPage: "https://doi.esrf.fr/",
    publisher: "European Synchrotron Radiation Facility",
    proxy: {
      host: "proxy.esrf.fr",
      port: "3128",
    },
  },
  logging: {
    graylog: {
      enabled: false,
    },
    console: {
      level: process.env.LOGGING_LEVEL || "error",
    },
    file: {
      enabled: false,
    },
  },
  ids: {
    server: "http://test.ids:8080",
  },
  icat: {
    server: `${process.env.ICAT_SERVER_URL || "https://ovm-icat-test.esrf.fr"}:${process.env.ICAT_SERVER_PORT || "8181"}`,
    maxQueryLimit: 10000,
    authorizations: {
      /**
       * adminUsersReader is capable to get the list of users that are administrators
       * adminUsersReader needs access to tables user and userGroup in order to know who are administrators
       * User can read electronic logbook for all proposals
       */
      adminUsersReader: {
        administrationGroups: ["admin"],
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_ADMIN_USERNAME }, { password: process.env.ICAT_ADMIN_PASSWORD }],
        },
      },
      /**
       * This user is allowed to mint DOI's and needs some permission on ICAT tables
       */
      minting: {
        group: ["minting"],
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_LISTREADER_USERNAME }, { password: process.env.ICAT_LISTREADER_PASSWORD }],
        },
      },
      parameterListReader: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_LISTREADER_USERNAME }, { password: process.env.ICAT_LISTREADER_PASSWORD }],
        },
      },
      /**
       * notifier needs to get access to all investigations as it will convert investigationName and beamline into investigationId
       */
      notifier: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_NOTIFIER_USERNAME }, { password: process.env.ICAT_NOTIFIER_PASSWORD }],
        },
      },
      dataacquisition: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_DATAACQUISITION_USERNAME }, { password: process.env.ICAT_DATAACQUISITION_PASSWORD }],
        },
      },
    },
    anonymous: {
      plugin: "db",
      credentials: [{ username: process.env.ICAT_ANONYMOUS_USERNAME }, { password: process.env.ICAT_ANONYMOUS_PASSWORD }],
    },
  },
  dmp: {
    enabled: true,
    server: "http://dswizard-test01.esrf.fr:3000",
    credentials: { email: "dmp.ws@esrf.fr", password: "password" },
  },
};
