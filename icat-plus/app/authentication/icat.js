const _ = require("lodash");
const icat = require("../api/icat.js");

const { ERROR } = require("../errors.js");
const {
  hasPermissionOnInvestigation,
  getInvestigationIdFromRequest,
  getSessionIdFromRequest,
  getApiKeyFromRequest,
  isInvestigationUnderEmbargo,
  getSession,
  getAuthenticator,
  setAuthenticator,
  allowsPrincipalInvestigatorOrByRoles,
} = require("./helper.authentication.js");

const CONSTANTS = require("../constants.js");

/**
 * It allows if  elastic search is enabled
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.isElasticSearchEnabled = (req, res, next) => {
  if (global.gServerConfig.elasticsearch.enabled) {
    return next();
  }
  return res.status(ERROR.NO_ELASTIC_SEARCH.code).send(ERROR.NO_ELASTIC_SEARCH.message);
};

/**
 * It allows only to administrators
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowAdministrators = async (req, res, next) => {
  const session = await getSession(req);
  if (session.isAdministrator) {
    return next();
  }
  return res.status(ERROR.NO_ADMINISTRATOR_PRIVILEGES.code).send(ERROR.NO_ADMINISTRATOR_PRIVILEGES.message);
};

exports.allowsPrincipalInvestigatorOrAdministrator = async (req, res, next) => {
  return allowsPrincipalInvestigatorOrByRoles(req, res, next, [CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR]);
};

exports.allowsPrincipalInvestigatorOrLocalContactOrAdministrator = async (req, res, next) => {
  return allowsPrincipalInvestigatorOrByRoles(req, res, next, [CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR, CONSTANTS.ROLE_LOCAL_CONTACT]);
};

/**
 * allows if there is an investigation which user is participant or investigation is not under embargo anymore
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser = async (req, res, next) => {
  const authenticator = "allowsInvestigationAfterEmbargoOrAllowsInvestigationUser";
  const sessionId = getSessionIdFromRequest(req);
  const investigationId = getInvestigationIdFromRequest(req);

  const session = await getSession(req);

  if (getAuthenticator(sessionId, authenticator, investigationId)) {
    return next();
  }
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  if (session.isAdministrator) {
    setAuthenticator(sessionId, authenticator, investigationId);
    return next();
  }

  try {
    const isEmbargoed = await isInvestigationUnderEmbargo(sessionId, investigationId);
    global.gLogger.debug("Is under embargo", { isEmbargoed, investigationId });
    if (isEmbargoed) {
      /** This happens when investigation is under embargo then it allows if and only if user is a participant (InvestigationUser)   */
      return this.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
    }
    setAuthenticator(sessionId, authenticator, investigationId);
    return next();
  } catch (error) {
    res.status(error.code).send(error.message);
  }
};

/**
 * Permission validator by investigationId: it allows the following 'person' (identified by its sessionId) to access the resource associated to a given investigationId:
 * - administrators
 * - investigationUsers , ie participants of the investigation indentified by investigationId
 * - instrumentScientist
 * @param {Request} req http request. It should contain sessionId and investigationId
 * @param {Response} res http response
 * @param {callback} next the next function
 */
exports.allowsInvestigationUserOrAdministratorsOrInstrumentScientists = async (req, res, next) => {
  try {
    global.gLogger.debug("allowsInvestigationUserOrAdministratorsOrInstrumentScientists");
    const authenticator = "allowsInvestigationUserOrAdministratorsOrInstrumentScientists";
    const sessionId = getSessionIdFromRequest(req);
    const investigationId = getInvestigationIdFromRequest(req);

    if (!sessionId) {
      return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
    }

    if (!investigationId) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
    }

    const { isAdministrator, username } = await getSession(req);

    if (getAuthenticator(sessionId, authenticator, investigationId)) {
      return next();
    }

    if (isAdministrator) {
      setAuthenticator(sessionId, authenticator, investigationId);
      return next();
    }

    /** Check if user is participant of the investigations **/
    const investigationUsers = await icat.asyncGetInvestigationUserBySessionId(sessionId);
    const isParticipant = _.find(investigationUsers, (inv) => {
      return inv.investigationId === Number(investigationId);
    });
    if (isParticipant) {
      global.gLogger.debug("User is a investigationUser of the investigation", {
        investigationId,
        username,
      });
      setAuthenticator(sessionId, authenticator, investigationId);
      return next();
    }

    /** If not participant it could be instrumentScientist */
    const investigations = await icat.getInvestigationByInstrumentScientistAndInvestigationId(sessionId, username, investigationId);
    if (investigations && investigations.length && investigations.length > 0) {
      global.gLogger.debug("User is a instrumentScientist of the investigation", {
        investigations,
        investigationId,
        username,
      });
      setAuthenticator(sessionId, authenticator, investigationId);
      return next();
    }
  } catch (e) {
    global.gLogger.error(e);
  }
  return res.status(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.code).send(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.message);
};

/**
 * It allows to instrumentScientist and administrator
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowsInstrumentScientistsOrAdministrators = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  const { isAdministrator, username } = await getSession(req);

  if (isAdministrator) {
    return next();
  }

  const instrumentScientists = await icat.getInstrumentScientistsByUserName(sessionId, username);
  if (instrumentScientists && instrumentScientists.length && instrumentScientists.length > 0) {
    return next();
  }
  return res.status(ERROR.NO_ADMINISTRATOR_OR_INSTRUMENTSCIENTIST.code).send(ERROR.NO_ADMINISTRATOR_OR_INSTRUMENTSCIENTIST.message);
};

/**
 *  It checks if the sessionId matches with the API key
 *  In the future it would be recommended to filter by network's IP
 **/
exports.requiresApiKey = (req, res, next) => {
  const apiKey = getApiKeyFromRequest(req);
  if (!apiKey) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }
  if (apiKey === global.gServerConfig.server.API_KEY) {
    return next();
  }
  return res.status(ERROR.INCORRECT_API_KEY.code).send(ERROR.INCORRECT_API_KEY.message);
};

/**
 * It requires to be signed in and a non-expired session found
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.requiresSession = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  const session = await getSession(req);
  if (!session) {
    global.gLogger.error(ERROR.NO_SESSION);
    return res.status(ERROR.NO_SESSION.code).send(ERROR.NO_SESSION.message);
  }
  return next();
};

/**
 * It allows to the participants of the investigation and the administrator
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.requiresInvestigation = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  const investigationId = getInvestigationIdFromRequest(req);

  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  const { isAdministrator } = await getSession(req);

  if (isAdministrator) {
    return next();
  }

  if (hasPermissionOnInvestigation(sessionId, investigationId)) {
    return next();
  }
  return res.status(ERROR.NO_PERMISSIONS_INVESTIGATION.code).send(ERROR.NO_PERMISSIONS_INVESTIGATION.message);
};

/**
 * Allows access to instrumentScientist and Administrator if the instrumentName is filled, or it allows access to user/InstrumentScientist/Admin for a given investigationId
 * This allows to implement allowing the access to beamline scientist to the logbook of the beamline
 * @param {*} req query contains either investigationId or instrumentName
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowAccessToInstrumentScientistOrUsers = async (req, res, next) => {
  if (!req.query.investigationId && req.query.instrumentName) {
    return this.allowsInstrumentScientistsOrAdministrators(req, res, next);
  }
  return this.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
};

/**
 *  It checks if the sessionId matches with the API key
 **/
exports.isValidApiKey = (req) => {
  const sessionId = getSessionIdFromRequest(req);
  return sessionId && sessionId === global.gServerConfig.server.API_KEY;
};
