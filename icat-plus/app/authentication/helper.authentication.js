const { ERROR, CustomError } = require("../errors.js");
const icat = require("../api/icat.js");
const _ = require("lodash");
const { initSession } = require("../cache/cache.js");

/** This property should be optional */
const IN_MEMORY_AUTHENTICATION = true;

/**
 * Gets the basic information needed from the user
 * @param {*} req
 */
exports.getSession = async (req) => {
  req.session = await initSession(this.getSessionIdFromRequest(req));
  if (!req.session) {
    global.gLogger.warn("No session retrieved");
  }
  return req.session;
};

/** Checks in the memory if the method is authorized (true) */
exports.getAuthenticator = (sessionId, method, entity) => {
  if (global.authenticators && global.authenticators[sessionId] && global.authenticators[sessionId][`${method}_${entity}`] === true) {
    global.gLogger.debug("Authorized by using fast authenticator", { sessionId, method, entity });
    return true;
  }
  global.gLogger.debug("No authorized by using fast authenticator", { sessionId, method, entity });
  return false;
};

/** Set to true the authorization for this method and session */
exports.setAuthenticator = (sessionId, method, entity) => {
  global.gLogger.debug("setAuthenticator", { sessionId, method, entity });
  if (IN_MEMORY_AUTHENTICATION) {
    if (!global.authenticators[sessionId]) {
      global.authenticators[sessionId] = {};
      global.authenticators[sessionId][method] = false;
      global.authenticators[sessionId][`${method}_${entity}`] = false;
    }
    global.authenticators[sessionId][`${method}_${entity}`] = true;
  }
};

/**
 * It allows to principal investigator or if by roles
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @param {*} roles [CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR, CONSTANTS.ROLE_LOCAL_CONTACT]
 * @returns
 */
exports.allowsPrincipalInvestigatorOrByRoles = async (req, res, next, roles) => {
  const investigationId = this.getInvestigationIdFromRequest(req);
  const sessionId = this.getSessionIdFromRequest(req);
  global.gLogger.debug("allowsPrincipalInvestigatorOrByRoles", { sessionId, investigationId });
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  const session = await this.getSession(req);

  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  /** Check if user is participant of the investigations **/
  const investigationUsers = await icat.asyncGetInvestigationUserBySessionId(sessionId);
  const isPrincipalInvestigator = _.find(investigationUsers, (inv) => {
    return inv.investigationId === Number(investigationId) && roles.find((role) => role === inv.role);
  });
  if (isPrincipalInvestigator) {
    global.gLogger.debug("User is principal investigatior");
    return next();
  }

  /** Next is user is administrator */
  const isAdministrator = session.isAdministrator;
  if (isAdministrator) {
    global.gLogger.debug("User is administrator");
    return next();
  }

  return res.status(ERROR.NO_PRINCIPAL_INVESTIGATOR_OR_ADMINISTRATOR.code).send(ERROR.NO_PRINCIPAL_INVESTIGATOR_OR_ADMINISTRATOR.message);
};

const getParameterByName = (req, parameterName) => {
  if (req.params) {
    if (req.params[parameterName]) {
      return req.params[parameterName];
    }
  }
  if (req.header) {
    if (req.header[parameterName]) {
      return req.header[parameterName];
    }
  }
  if (req.body) {
    if (req.body[parameterName]) {
      return req.body[parameterName];
    }
  }
  if (req.query) {
    if (req.query[parameterName]) {
      return req.query[parameterName];
    }
  }
  global.gLogger.error(`No ${parameterName} @ header, body or query`);
  return null;
};

exports.getInvestigationIdFromRequest = (req) => {
  const param = getParameterByName(req, "investigationId");
  return param ? Number(param) : param;
};

exports.getInstrumentName = (req) => {
  return getParameterByName(req, "instrumentName");
};

exports.getSessionIdFromRequest = (req) => {
  return getParameterByName(req, "sessionId");
};

exports.getApiKeyFromRequest = (req) => {
  return getParameterByName(req, "apiKey");
};

/**
 * Check that the user identified by sessionID has permission to read investigationId
 * @param {string} sessionId sessionId identifying the user
 * @param {string} investigationId investigation identifier
 * @param {function} next express callback function
 */
exports.hasPermissionOnInvestigation = async (sessionId, investigationId) => {
  const investigations = await icat.asyncGetInvestigationsBySessionId(sessionId);
  const found = investigations.find((element) => {
    return Number(element.Investigation.id) === Number(investigationId);
  });
  if (found) {
    return true;
  }
  return false;
};

/** It affects to tags, to be checked later */
exports.allowsInstrumentScientistsOrAdministratorsByInstrumentName = async (req, res, next, username) => {
  const sessionId = this.getSessionIdFromRequest(req);
  const instrumentName = this.getInstrumentName(req).toUpperCase();

  if (sessionId) {
    if (instrumentName) {
      const users = await icat.getInstrumentScientistsByInstrumentName(sessionId, instrumentName);
      global.gLogger.debug("users count who are instrumentUsers or administrator : ", { userCount: users.length, users: JSON.stringify(users) });

      // check whether the current user identified by its sessionId is listed in the response
      const foundUser = _.find(users, (user) => {
        return user.User.name === username;
      });

      if (foundUser) {
        return next();
      }
      return res.status(403).send("You are not allowed to create a bemline tag.");
    }
    return res.status(400).send("No Instrument");
  }
  return res.status(403).send("No sessionId");
};

/**
 *  This method checks if the release date is < now
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.isInvestigationUnderEmbargo = async (sessionId, investigationId) => {
  try {
    const investigations = await icat.asyncGetInvestigationById(sessionId, investigationId);

    if (investigations && investigations.length > 0 && investigations[0].Investigation) {
      const investigation = investigations[0].Investigation;

      if (!investigation.releaseDate) {
        return true;
      }
      if (new Date(investigation.releaseDate) < new Date()) {
        return false;
      }
    } else {
      throw ERROR.NO_INVESTIGATION_FOUND;
    }
  } catch (e) {
    global.gLogger.error(e);
    throw new CustomError(e);
  }
  return true;
};
