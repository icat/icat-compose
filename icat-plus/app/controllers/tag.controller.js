const { ERROR } = require("../errors.js");
const Tag = require("../models/tag.model.js");
const { getInstrumentNameByInvestigationId } = require("./helpers/helper.logbook.controller.js");
const { asyncGetInvestigationById } = require("../api/icat");
const { findTagByName } = require("./helpers/helper.tag.controller.js");
/**
 * Get all available tags (Global, instrumentName specific and investigation specific) by Investigation and Beamline
 * @returns {Array} tags
 * - When investigationId is provided, returns investigation specific tags + all beamline tags + all global tags
 *
 */
exports.findTagsBy = async (sessionId, investigationId) => {
  let instrumentName = "";
  if (investigationId) {
    // request which aims at getting available tags from an investigationId (ie investigation + beamline + global tags). Needs to find out the beamline first.
    const investigations = await asyncGetInvestigationById(sessionId, investigationId);
    if (investigations && investigations.length > 0) {
      try {
        instrumentName = investigations[0].Investigation.investigationInstruments[0].instrument.name.toLowerCase();
      } catch {
        global.gLogger.error("findTagsBy(). instrumentName not found");
      }
    }
  }

  const tag = await Tag.find({
    $or: [{ investigationId }, { instrumentName }, { investigationId: undefined, instrumentName: undefined }],
  });
  return tag;
};

/**
 * Get all tags for a given investigation
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier
 * @param {*} res the http response
 */
exports.getTags = async (req, res) => {
  try {
    const { sessionId } = req.params;
    const { investigationId } = req.query;
    const tags = await this.findTagsBy(sessionId, investigationId);
    res.send(tags);
  } catch (err) {
    res.status(500).send(err);
  }
};

/**
 * Creates a tag. It checks that the tag name does not exist yet within any scope (investigation, instrument or global)
 * @param {string} req.params.sessionId session identifier
 * @param {object} req.body tag to be created
 * @param {*} res http response
 */
exports.createTag = async (req, res) => {
  const { sessionId } = req.params;
  const { investigationId } = req.query;
  let { instrumentName } = req.query;
  const { name, description, color } = req.body;
  global.gLogger.debug("createTag", { investigationId, instrumentName, name, description, color });
  try {
    /** Check that tag does not exist within the investigation and instrument */
    if (investigationId) {
      /** setting the instrument name of the investigation */
      instrumentName = await getInstrumentNameByInvestigationId(sessionId, investigationId);
    }

    const tags = await findTagByName(investigationId, instrumentName, name.toLowerCase());
    if (tags.length > 0) {
      global.gLogger.error(ERROR.DUPLICATED_TAG_NAME.description);
      return res.status(ERROR.DUPLICATED_TAG_NAME.code).send(ERROR.DUPLICATED_TAG_NAME.message);
    }

    /** Saving the tag into the DB */
    let tag = new Tag({ name: name.toLowerCase(), description, color, instrumentName, investigationId });
    tag = await tag.save();
    res.send(tag);
  } catch (e) {
    res.status(500).send({ message: e.message || "Some error occurred while creating the tag" });
  }
};

/**
 * Update an existing investigation tag
 * @param {string} req.params.sessionId session indentifier
 * @param {string} req.params.tagId investigation tag identifier which needs to be updated
 * @param {object} req.body.tag investigation tag used for the update.
 */
exports.updateTag = async (req, res) => {
  const { id } = req.query;
  if (!id || id.length === 0) {
    return res.status(ERROR.INVALID_TAG_ID.code).send({ message: ERROR.INVALID_TAG_ID.message });
  }
  if (!req.body) {
    return res.status(400).send({ message: "Could not get the tag object from the body" });
  }
  const { name, description, color } = req.body;
  if (!name || name === "") {
    return res.status(400).send({
      message: "name field was not found or empty. Tag name is unknown",
    });
  }
  try {
    const foundTag = await Tag.findById(id);
    foundTag.name = name.toLowerCase();
    foundTag.description = description || null;
    foundTag.color = color || null;
    //foundTag.investigationId = investigationId;
    const data = await foundTag.save();
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while modifying the tag.",
    });
  }
};
