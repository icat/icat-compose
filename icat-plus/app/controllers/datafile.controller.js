const { getDatafilesByDatasetId, countDatafileByDatasetId } = require("../api/icat.js");
const { sendError } = require("../errors.js");
const { getParamAsEscapedLowerString } = require("./helpers/helper.controller.js");

exports.getDatafilesByDatasetId = async (req, res) => {
  const { limit, skip, search } = req.query;
  const { sessionId, datasetId } = req.params;
  const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
  const searchSkip = parseInt(skip) || 0;
  const searchText = getParamAsEscapedLowerString(search);
  global.gLogger.debug("getDatafilesByDatasetId. ", {
    datasetId,
    searchLimit,
    searchSkip,
    searchText,
  });
  try {
    const datafiles = await findDatafilesByDatasetId(sessionId, datasetId, searchLimit, searchSkip, searchText);
    res.send(datafiles);
  } catch (error) {
    sendError(500, "Failed to getDatafilesByDatasetId", error, res);
  }
};

async function findDatafilesByDatasetId(sessionId, datasetId, limit, skip, search) {
  global.gLogger.debug("findDatafilesByDatasetId. ", { limit, skip, search });
  const hrstart = process.hrtime();
  const [totalWithoutFilters, total, datafiles] = await Promise.all([
    countDatafileByDatasetId(sessionId, datasetId, undefined),
    countDatafileByDatasetId(sessionId, datasetId, search),
    getDatafilesByDatasetId(sessionId, datasetId, limit, skip, search),
  ]);
  setMetaDataToDatafile(datafiles, totalWithoutFilters, total, limit, skip);

  global.gLogger.debug("Final time to findDatafilesByDatasetId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return datafiles;
}

function setMetaDataToDatafile(datafiles, totalWithoutFilters, total, limit, skip) {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;
  datafiles.forEach((d) => {
    d.meta = {
      page: {
        totalWithoutFilters,
        total,
        totalPages,
        currentPage,
      },
    };
  });
}
