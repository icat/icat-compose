const icat = require("../api/icat.js");
const { ERROR } = require("../errors.js");
/**
 * Get all instruments
 */
exports.getInstruments = async (_req, res) => {
  try {
    const data = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.parameterListReader.user);
    const instruments = await icat.getInstrumentsBySessionId(data.sessionId);
    res.send(instruments);
  } catch (error) {
    global.gLogger.error(error);
    res.status(ERROR.FAILED_TO_RETRIEVE_INSTRUMENTS.code).send(ERROR.FAILED_TO_RETRIEVE_INSTRUMENTS.message);
  }
};
