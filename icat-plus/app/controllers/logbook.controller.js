const wkhtmltopdf = require("wkhtmltopdf");
const { getInstrumentByName } = require("../cache/cache.js");
const Event = require("../models/event.model.js");

const icat = require("../api/icat.js");
/** This is required otherwise TypeError: moment.tz is not a function */
const moment = require("moment-timezone");
//const moment = require("moment");
//const momentTimeZone = require("moment-timezone");
const investigationController = require("./investigations.controller.js");
/** PDF generation response timeout */
const TIMEOUT = 1000 * 60 * 10;
const {
  getInstrumentNameByInvestigationId,
  createBase64Event,
  translateEventContentToHtml,
  saveEvent,
  saveTags,
  replaceImageSrc,
  searchEvents,
  findEvents,
  getServerURI,
  convertEventsToHTML,
  getDates,
  buildEventFindQuery,
  getEventPage,
  getMinAndMaxIdForBroadcastEvents,
  isSearchingBroadcastEvents,
  count,
} = require("./helpers/helper.logbook.controller.js");
const { ERROR } = require("../errors.js");
const { EVENT_TYPE_ANNOTATION, EVENT_TYPE_NOTIFICATION, EVENT_TYPE_BROADCAST } = require("../constants");
const { getParamAsDateString } = require("./helpers/helper.controller.js");

/** Query to search event statistics based on start and end dates and type (annotation or notification) */
const STATS_QUERY = (start, end, type) => [
  { $match: { type, creationDate: { $gte: start, $lt: end } } },
  { $group: { _id: { instrumentName: "$instrumentName", investigationId: "$investigationId", investigationName: "$investigationName" }, count: { $sum: 1 } } },
];

/** Query counts the number of event per day */
const STATS__COUNT_QUERY = (start, end, type) => [
  { $match: { creationDate: { $gte: start, $lt: end }, type } },
  {
    $group: {
      _id: { $dateToString: { format: "%Y-%m-%d", date: "$creationDate" } },
      count: { $sum: 1 },
    },
  },
  {
    $sort: {
      _id: -1,
    },
  },
];

async function getEventStatistics(start, end) {
  const [all, annotations, notifications] = await Promise.all([
    await Event.aggregate([
      { $match: { creationDate: { $gte: start, $lt: end } } },
      {
        $group: {
          _id: { investigationId: "$investigationId" },
          count: { $sum: 1 },
        },
      },
    ]),
    await Event.aggregate(STATS_QUERY(start, end, EVENT_TYPE_ANNOTATION)),
    await Event.aggregate(STATS_QUERY(start, end, EVENT_TYPE_NOTIFICATION)),
  ]);

  const getCount = (annotation, annotations) => {
    const found = annotations.find((r) => {
      return r._id.investigationId === annotation._id.investigationId;
    });
    if (found) return found.count;
  };
  all.forEach((stats) => {
    stats.investigationId = stats._id.investigationId;
    const investigation = global.investigationsKeys[stats._id.investigationId];
    if (investigation) {
      const { title, parameters, startDate, endDate, name } = investigation;
      stats.title = title;
      stats.name = name;
      stats = Object.assign(stats, parameters);
      stats.startDate = startDate;
      stats.endDate = endDate;
      stats.instrument = investigation.instrument.name;
      stats.annotations = getCount(stats, annotations);
      stats.notifications = getCount(stats, notifications);
    }
  });

  all.sort((a, b) => a.investigationId - b.investigationId);
  return all;
}

exports.getEventsCount = async (req, res) => {
  const { startDate, endDate, type } = req.query;
  global.gLogger.debug("getEventsCount", { startDate, endDate, type, query: JSON.stringify(STATS__COUNT_QUERY(new Date(startDate), new Date(endDate), type)) });
  if (!startDate || !endDate) {
    return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
  }
  try {
    const statistics = await Event.aggregate(STATS__COUNT_QUERY(new Date(startDate), new Date(endDate), type));
    res.send(statistics);
  } catch (e) {
    return res.send(e);
  }
};
/**
 * Endpoint to retrieve statistics of the events + the parameters of the investigation
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.getEventsStatistics = async (req, res) => {
  const { startDate, endDate } = req.query;
  if (!startDate || !endDate) {
    return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
  }

  try {
    const statistics = await getEventStatistics(new Date(startDate), new Date(endDate));
    res.send(statistics);
  } catch (e) {
    return res.send(e);
  }
};
/**
 * This method moves the events from one investigation to an other. It changes the investigationId, the investigationName and the instrumentName.
 * It returns the number of modified events
 * Example of returned information: { n: 2, nModified: 2, ok: 1 }

 * @param {*} req
 * @param {*} res
 */
exports.moveEvents = async (req, res) => {
  const { sessionId } = req.params;
  const { sourceInvestigationId, destinationInvestigationId } = req.query;

  global.gLogger.info("moveEvents ", { sourceInvestigationId, destinationInvestigationId });

  if (!sourceInvestigationId || !destinationInvestigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  try {
    const investigations = await icat.asyncGetInvestigationById(sessionId, destinationInvestigationId);
    if (!investigations || investigations.length === 0) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send({ message: ERROR.NO_INVESTIGATION_FOUND.message });
    }
    let instrumentName = await getInstrumentNameByInvestigationId(sessionId, destinationInvestigationId);
    if (instrumentName) {
      instrumentName = instrumentName.toUpperCase();
    }

    const conditions = { investigationId: sourceInvestigationId };
    const update = {
      $set: {
        investigationId: destinationInvestigationId,
        investigationName: investigations[0].Investigation.name,
        instrumentName,
      },
    };
    const options = { multi: true, upsert: false };
    const updatedEventsInformation = await Event.updateMany(conditions, update, options);
    res.send(updatedEventsInformation);
  } catch (e) {
    global.gLogger.error(e);
    res.status(500).send({
      message: e.message || "Some error occurred while moving events investigation",
    });
  }
};

exports.createfrombase64ByInvestigationName = async (req, res) => {
  const { sessionId, investigationName, instrumentName } = req.params;
  const { base64, datasetId, software, machine } = req.body;
  global.gLogger.info("createfrombase64ByInvestigationName ", { investigationName, instrumentName });

  if (!investigationName) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  if (!instrumentName) {
    return res.status(ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.message });
  }

  if (!base64) {
    return res.status(ERROR.NO_BASE64_BAD_PARAMS.code).send({ message: ERROR.NO_BASE64_BAD_PARAMS.message });
  }

  const investigation = await investigationController.getInvestigationByNameAndInstrument(investigationName, instrumentName, new Date());
  if (investigation) {
    try {
      const eventCreated = await createBase64Event(sessionId, investigation.id, base64, datasetId, software, machine, null, null, investigationName, instrumentName);
      res.send(eventCreated);
    } catch (e) {
      global.gLogger.error("createfrombase64", { error: e.message });
      res.status(500).send({
        message: e.message || "Some error occurred while creating the Attachment",
      });
    }
  } else {
    return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
  }
};

/**
 * This method will create an attachment and a annotation from a base64 encoded image
 * @param {string} sessionId session identifier
 * @param {string} investigationId investigation identifier if in an investigation logbook
 * @param {string} instrumentName instrumentName if in a beamline logbook
 * @param {string} session userName and fullName information
 * @param {string} body.base64 base64 image png
 **/
exports.createfrombase64 = async (sessionId, investigationId, instrumentName, username, fullName, base64, datasetId, software, machine, res) => {
  global.gLogger.info("createfrombase64", { investigationId, username, fullName, instrumentName });

  if (isNaN(investigationId) && !instrumentName) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }
  if (!base64) {
    global.gLogger.error("createfrombase64 No base64 parameter", { base64 });
    return res.status(ERROR.NO_BASE64_BAD_PARAMS.code).send({ message: ERROR.NO_BASE64_BAD_PARAMS.message });
  }
  /** Check if instrumentName exists */
  if (instrumentName) {
    instrumentName = instrumentName.toUpperCase();
    if (!getInstrumentByName(instrumentName)) {
      return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
    }
  }
  let investigationName = null;
  if (investigationId) {
    const investigation = await icat.asyncGetInvestigationById(sessionId, investigationId);
    if (investigation.length > 0) {
      investigationName = investigation[0].Investigation.name;
    }
  }
  try {
    const eventCreated = await createBase64Event(sessionId, investigationId, base64, datasetId, software, machine, username, fullName, investigationName, instrumentName);
    res.send(eventCreated);
  } catch (e) {
    global.gLogger.error("createfrombase64", { error: e.message });
    res.status(500).send({ message: e.message || "Some error occurred while creating the Attachment" });
  }
};

/**
 * Creates an event in the logbook linked to an investigation or an instrument
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.query.investigationId investigation identifier
 * @param {string} req.query.instrumentName investigation identifier
 * @param {object} req.body event to be created in the logbook
 **/
exports.createEvent = async (req, res) => {
  try {
    const { name } = req.session;
    const { sessionId } = req.params;
    const { investigationId } = req.query;
    let { instrumentName } = req.query;
    const { datasetId, datasetName, tag, title, type, category, filename, fileSize, software, machine, creationDate, content } = req.body;

    global.gLogger.info("createEvent", {
      investigationId,
      name,
      sessionId,
      instrumentName,
    });

    if (isNaN(investigationId) && !instrumentName) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
    }

    let investigationName = null;
    if (investigationId) {
      const investigation = await icat.asyncGetInvestigationById(sessionId, investigationId);
      if (investigation.length > 0) {
        investigationName = investigation[0].Investigation.name;
      }
    }

    /** Check instrumentName exists */
    if (instrumentName) {
      if (!getInstrumentByName(instrumentName.toUpperCase())) {
        return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
      }
      instrumentName = instrumentName.toUpperCase();
    }

    if (!isNaN(investigationId)) {
      instrumentName = await getInstrumentNameByInvestigationId(sessionId, investigationId);
      if (instrumentName) {
        instrumentName = instrumentName.toUpperCase();
      }
    }

    const event = new Event({
      investigationId,
      investigationName,
      datasetId,
      datasetName,
      tag,
      title,
      type,
      category,
      filename,
      fileSize,
      username: name,
      software,
      machine,
      instrumentName,
      creationDate,
      content: translateEventContentToHtml(content),
      previousVersionEvent: null,
    });

    const eventCreated = await saveEvent(event);
    res.send(eventCreated);
  } catch (e) {
    global.gLogger.error(e);
    res.status(500).send({
      message: e.message || "Some error occurred while creating the event",
    });
  }
};

function parseCreationDate(date) {
  if (date) {
    if (moment(date, moment.ISO_8601).isValid() || moment(date, moment.RFC_2822).isValid()) {
      return moment.tz(date, "Europe/Paris").toDate();
    }
    // the date is neither ISO_8601 nor RFC_2822 format. There is such a case in the tests 'Mon Mar 09 2020 15:24:38 GMT+0400 (CET)'
    return date;
  }
  return new Date();
}
/**
 * This method receives the json tag and stores in the database
 * @param {*} tag json object
 * @param {*} investigationId optional
 * @param {*} instrumentName optional
 * @returns an array with the tag saved
 */
const createNotificationTag = async (tag, investigationId, instrumentName) => {
  let tags = [];
  if (tag) {
    try {
      tag = JSON.parse(tag);
    } catch {
      global.gLogger.debug("createNotificationTag. Could not convert list of tags to JSON", { tag });
    }
    tag.forEach((tag) => {
      tag.name = tag.name.toLowerCase();
      if (investigationId) tag.investigationId = investigationId;
      if (instrumentName) tag.instrumentName = instrumentName;
    });
    tags = await saveTags(tag);
  }
  return tags;
};
/**
 * Create a new logbook event.
 * @param {string} req.params.instrumentName instrument name
 * @param {string} req.params.sessionId the magic token
 */
exports.createBeamlineNotification = async (req, res) => {
  const { instrumentName } = req.params;
  const { tag, creationDate, datasetName, title, type, category, datasetId, filename, fileSize, software, machine, username, content } = req.body;
  global.gLogger.info("createBeamlineNotification ", { instrumentName });

  if (!instrumentName || instrumentName === undefined || instrumentName === "undefined") {
    return res.status(ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.message });
  }
  const localcreationDate = parseCreationDate(creationDate);
  try {
    const tags = await createNotificationTag(tag, null, instrumentName);
    try {
      const event = new Event({
        datasetId,
        datasetName,
        tag: tags,
        title,
        type,
        category,
        filename,
        fileSize,
        username,
        instrumentName: instrumentName.toUpperCase(),
        software,
        machine,
        creationDate: localcreationDate,
        content: translateEventContentToHtml(content),
        previousVersionEvent: null,
      });

      try {
        const eventCreated = await saveEvent(event);
        res.send(eventCreated);
      } catch (error) {
        global.gLogger.error("createBeamlineNotification. There was an error when saving", {
          error: error.message,
        });
        res.status(422).send({
          message: error.message || "Some error occurred while saving the event.",
        });
      }
    } catch (e) {
      global.gLogger.error("createBeamlineNotification saving event", { error: e.message });
      return res.status(400).send(`createBeamlineNotification: ${e.message}`);
    }
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

/**
 * Create a new event without investigationId. Instead it uses investigationName and instrumentName.
 * @param {string} req.params.investigationName investigation name
 * @param {string} req.params.instrumentName instrument name
 * @param {string} req.params.sessionId the magic token
 */
exports.createInvestigationNotification = async (req, res) => {
  const { investigationName, instrumentName } = req.params;

  const { tag, creationDate, datasetName, title, type, category, datasetId, filename, fileSize, software, machine } = req.body;
  global.gLogger.info("createInvestigationNotification ", { investigationName, instrumentName });

  if (!investigationName) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  if (!instrumentName || instrumentName === undefined || instrumentName === "undefined") {
    return res.status(ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.message });
  }

  const localcreationDate = parseCreationDate(creationDate);

  try {
    const investigation = await investigationController.getInvestigationByNameAndInstrument(investigationName, instrumentName, localcreationDate);

    if (!investigation) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
    }
    const { name, id } = investigation;
    const tags = await createNotificationTag(tag, investigation.id, instrumentName);
    // Create a Event
    const event = new Event({
      investigationName: name,
      investigationId: id,
      datasetId,
      datasetName,
      tag: tags,
      title,
      type,
      category,
      filename,
      fileSize,
      username: req.body.username,
      instrumentName: instrumentName.toUpperCase(),
      software,
      machine,
      creationDate: localcreationDate,
      content: translateEventContentToHtml(req.body.content),
      previousVersionEvent: null,
    });

    try {
      const eventCreated = await saveEvent(event);
      res.send(eventCreated);
    } catch (error) {
      global.gLogger.error("createInvestigationNotification. There was an error when saving", {
        error: error.message,
      });
      res.status(422).send({
        message: error.message || "Some error occurred while saving the event.",
      });
    }
  } catch (e) {
    global.gLogger.error("createInvestigationNotification", { message: e.message });
    return res.status(400).send(`createInvestigationNotification ${e.message}`);
  }
};

/**
 * Update an event
 * @param {string} req.params.investigationId investigation identifier
 * @param {string} req.params.sessionId sessin identifier
 * @param {string} req.body.event the updated event
 */
exports.updateEvent = async (req, res) => {
  const { investigationId, instrumentName } = req.query;
  const { name } = req.session;
  const event = req.body;

  global.gLogger.debug("updateEvent", { investigationId, name, instrumentName });
  if (!investigationId && !instrumentName) {
    return res.status(400).send({ message: "investigationId and instrumentName can not be empty" });
  }

  if (!event) {
    return res.status(400).send({ message: "Event can not be empty" });
  }

  if (!event._id) {
    return res.status(400).send({ message: "_id can not be empty" });
  }

  // We are sure that the event has been allowed by the middleware
  event.investigationId = investigationId;

  try {
    const currentEventVersion = await Event.findById(event._id).populate("tag");

    global.gLogger.info("Found event to modify.", {
      _id: currentEventVersion._id,
    });
    // create a copy of the current version as it is curretnyl stored in the DB
    const currentEventVersionCopy = JSON.parse(JSON.stringify(currentEventVersion));

    // update event fields
    currentEventVersion.investigationId = event.investigationId || null;
    currentEventVersion.datasetId = event.datasetId || null;
    currentEventVersion.tag = event.tag || [];
    currentEventVersion.title = event.title || "";
    currentEventVersion.type = event.type || null;
    currentEventVersion.category = event.category || null;
    currentEventVersion.filename = event.filename || null;
    currentEventVersion.fileSize = event.fileSize || null;
    currentEventVersion.username = name;
    currentEventVersion.software = event.software;
    currentEventVersion.machine = event.machine;
    currentEventVersion.creationDate = event.creationDate;
    currentEventVersion.content = event.content;
    /** instrument can not be modifed since the creation and the original is kept*/
    currentEventVersion.instrumentName = currentEventVersionCopy.instrumentName;

    //keep event history
    currentEventVersion.previousVersionEvent = currentEventVersionCopy;

    const savedEvent = await saveEvent(currentEventVersion);
    res.send(savedEvent);
  } catch (error) {
    global.gLogger.error("Update event error", { error: JSON.stringify(error) });
    res.status(500).send({ message: error || "Error was produced when finding the event" });
  }
};

const sendPDF = (html, req, sessionId, investigationId, response) => {
  function onInvestigationsFound(investigation) {
    let summary = "";
    let proposal = "";
    let instrument = "";
    try {
      if (investigation[0]) {
        summary = investigation[0].Investigation.summary;
        proposal = investigation[0].Investigation.title.toUpperCase();
        instrument = investigation[0].Investigation.investigationInstruments[0].instrument.name;
      }
    } catch (error) {
      global.gLogger.error(error);
    }

    /** This is need because we want to access to the ESRF logo that is in the public folder as a img so we need to build the http URL */
    const serverURL = `http://localhost:${global.gServerConfig.server.port}`;
    const searchParams = `?title=${summary}&instrument=${instrument}&proposal=${proposal}&logo=${serverURL}/static/pdf/esrf_logo.jpg`;

    //const header = `${serverURL}/static/pdf/header.html${searchParams}`;
    const footer = `${serverURL}/static/pdf/footer.html${searchParams}`;
    global.gLogger.debug("PDF Process has started");
    const pdf = wkhtmltopdf(html, {
      pageSize: "A4",
      orientation: "Portrait",
      marginTop: 10,
      marginBottom: 10,
      marginLeft: 10,
      marginRight: 10,
      //"header-html": header,
      "footer-html": footer,
      "header-spacing": 5,
    });
    global.gLogger.debug("PDF Process has finished");
    try {
      pdf.pipe(response);
    } catch (e) {
      global.gLogger.error("count failed", {
        error: e.message,
      });
      response.status(500).send({
        message: e.message || "Some error occurred while retrieving events.",
      });
    }
  }

  function onInvestigationsError(error) {
    global.gLogger.error(error);
  }
  icat.getInvestigationById(sessionId, investigationId, onInvestigationsFound, onInvestigationsError);
};

/**
 * Returns the meta field of an entry in the logbook
 * @param {*} req
 * @param {*} res
 */
exports.getPageByEventId = async (req, res) => {
  const { _id, investigationId, instrumentName, sortOrder = "-1", limit, types, filterInvestigation = false, date } = req.query;
  global.gLogger.debug("getMetaByEventId", { investigationId, instrumentName, sortOrder, limit, types, filterInvestigation });
  const page = await getEventPage(_id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder);
  return res.send(page);
};

exports.countEvents = async (req, res) => {
  const { investigationId, instrumentName, types, search, format = "json", filterInvestigation = false, date } = req.query;
  const { username, fullName, isAdministrator, isInstrumentScientist } = req.session;

  const formattedDate = getParamAsDateString(date);
  global.gLogger.debug("countEvents", {
    investigationId,
    instrumentName,
    search,
    types,
    format,
    username,
    fullName,
    isAdministrator,
    isInstrumentScientist,
    filterInvestigation,
    formattedDate,
  });

  // determine min and max event id to load broadcast events
  let minEventId;
  let maxEventId;
  if (isSearchingBroadcastEvents(types)) {
    [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
  }

  try {
    const find = await buildEventFindQuery(investigationId, filterInvestigation, instrumentName, types, formattedDate, search, minEventId, maxEventId);
    const nbEvents = await count(find);
    return res.send([{ totalNumber: nbEvents }]);
  } catch (err) {
    global.gLogger.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while counting events.",
    });
  }
};

/**
 * Returns a list of events based in the url query params
 * @param {*} req
 * @param {*} res
 */
exports.getEvents = async (req, res) => {
  const { investigationId, instrumentName, sortBy = "_id", sortOrder = "-1", skip = 0, limit, types, search, format = "json", filterInvestigation = false, date } = req.query;
  const { sessionId } = req.params;
  const { username, fullName, isAdministrator, isInstrumentScientist } = req.session;

  const formattedDate = getParamAsDateString(date);
  global.gLogger.debug("getEvents", {
    investigationId,
    instrumentName,
    search,
    sortBy,
    sortOrder,
    skip,
    limit,
    types,
    format,
    username,
    fullName,
    isAdministrator,
    isInstrumentScientist,
    filterInvestigation,
    formattedDate,
  });

  const sort = {};
  sort[sortBy] = sortOrder === "undefined" ? -1 : sortOrder;

  // determine min and max event id to load broadcast events
  let minEventId;
  let maxEventId;
  if (isSearchingBroadcastEvents(types)) {
    [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
  }

  if (search || formattedDate) {
    const occurrences = await searchEvents(search, sort, investigationId, limit, skip, filterInvestigation, instrumentName, types, date, sortOrder, minEventId, maxEventId);
    const parsed = replaceImageSrc(occurrences, sessionId, getServerURI(req.headers.host, req.secure));
    return res.send(parsed);
  }

  try {
    const find = await buildEventFindQuery(investigationId, filterInvestigation, instrumentName, types, formattedDate, undefined, minEventId, maxEventId);
    const events = await findEvents(find, sort, parseFloat(skip) || 0, parseFloat(limit) || global.gServerConfig.icat.maxQueryLimit);
    if (format.toLowerCase() !== "pdf") {
      return res.send(replaceImageSrc(events, req.params.sessionId, getServerURI(req.headers.host, req.secure)));
    }

    req.setTimeout(TIMEOUT);
    return sendPDF(convertEventsToHTML(events, sessionId, investigationId, getServerURI(req.headers.host, req.secure)), req, sessionId, investigationId, res);
  } catch (err) {
    global.gLogger.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving events.",
    });
  }
};

/**
 * Returns a list of dates of all events based on the url query params. For each date, the number of events is given.
 * List of {"_id": "YYYY-DD-MM", event_count: ""}
 * @param {*} req
 * @param {*} res
 */
exports.findAllDatesByEvents = async (req, res) => {
  const { investigationId, instrumentName, types, search, filterInvestigation = false } = req.query;

  global.gLogger.debug("findAllDatesByEvents", {
    investigationId,
    instrumentName,
    search,
    types,
    filterInvestigation,
  });

  try {
    // determine min and max event id to load broadcast events
    let minEventId;
    let maxEventId;
    if (isSearchingBroadcastEvents(types)) {
      [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
    }
    const dates = await getDates(investigationId, filterInvestigation, instrumentName, types, search, minEventId, maxEventId);
    return res.send(dates);
  } catch (err) {
    global.gLogger.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving events.",
    });
  }
};

/**
 * Create a new logbook broadcast event.
 * @param {string} req.params.sessionId the magic token
 */
exports.createBroadcastNotification = async (req, res) => {
  const { tag, creationDate, title, type, category, datasetId, software, machine, username, content } = req.body;
  global.gLogger.info("createBroadcastNotification ");

  const localcreationDate = parseCreationDate(creationDate);
  if (!type || type.toLowerCase() !== EVENT_TYPE_BROADCAST) {
    global.gLogger.error(`createBroadcastNotification error, the type is not ${EVENT_TYPE_BROADCAST}, but ${type}`);
    return res.status(ERROR.INVALID_EVENT_TYPE.code).send(ERROR.INVALID_EVENT_TYPE.message);
  }
  try {
    const tags = await createNotificationTag(tag);
    try {
      const event = new Event({
        datasetId,
        tag: tags,
        title,
        type: type.toLowerCase(),
        category,
        username,
        software,
        machine,
        creationDate: localcreationDate,
        content: translateEventContentToHtml(content),
        previousVersionEvent: null,
      });

      try {
        const eventCreated = await saveEvent(event);
        res.send(eventCreated);
      } catch (error) {
        global.gLogger.error("createBroadcastNotification. There was an error when saving", {
          error: error.message,
        });
        res.status(422).send({
          message: error.message || "Some error occurred while saving the event.",
        });
      }
    } catch (e) {
      global.gLogger.error("createBroadcastNotification saving event", { error: e.message });
      return res.status(400).send(`createBroadcastNotification: ${e.message}`);
    }
  } catch (e) {
    return res.status(400).send(e.message);
  }
};
