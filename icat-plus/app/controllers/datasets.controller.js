const {
  getDataColletionByDatasetId,
  getStatusByDatasetIds,
  getDatasetsByInvestigationId,
  countDatasetByInvestigationId,
  getDatasetsById,
  getDatasetsByDateRange,
  asyncGetSession,
  getDatasetsByDOI,
  countDatasetsByDOI,
} = require("../api/icat.js");
const { parse } = require("../api/parsers/datasetdocumentparser.js");
const { downloadData } = require("../api/query.js");
const { ERROR, sendError } = require("../errors.js");
const { getParamAsEscapedLowerString, getSortOrder } = require("./helpers/helper.controller.js");
const DatasetAccessSchema = require("../models/datasetaccess.model.js");
const { findDatasetAccesses } = require("./datasetaccess.controller.js");
const STATUS = require("../models/datasetaccess.status.js");
const TYPE = require("../models/datasetaccess.type.js");

exports.getStatusByDatasetIds = async (req, res) => {
  try {
    global.gLogger.debug("getStatusByDatasetIds", { datasetIds: req.params.datasetIds });
    const status = await getStatusByDatasetIds(req.params.sessionId, req.params.datasetIds);
    res.send(status);
  } catch (error) {
    sendError(500, "Failed to get status of datasets", error, res);
  }
};

/**
 * This method will retrieve the datasets that are referred by the dataset parameter output_datasetIds
 * @param {*} sessionId
 * @param {*} datasets
 * @returns it returns the list of output datasets
 */
async function findOutputDatasets(sessionId, datasets) {
  let outputDatasetIdsList = datasets.map((dataset) => dataset.parameters.filter((p) => p.name === "output_datasetIds").map((p) => p.value)).flat();
  /* this can give results like : [ "123957469", "123957450 123957485", "123957434" ] */
  outputDatasetIdsList = outputDatasetIdsList.map((e) => e.split(" ")).flat();

  const outputDatasets = await getDatasetsById(sessionId, outputDatasetIdsList.toString());
  return outputDatasets;
}

async function populateOutputDatasets(sessionId, datasets) {
  /** Getting output datasets and mergin them*/
  const outputDatasets = await findOutputDatasets(sessionId, datasets);
  if (outputDatasets.length > 0) {
    /** Adding datasets to the list of datasets */
    datasets.forEach((ds) => {
      if (ds.parameters && ds.parameters.length > 0) {
        const output_datasetIds = ds.parameters.find((p) => p.name === "output_datasetIds");
        if (output_datasetIds) {
          ds.outputDatasets = outputDatasets.filter((outputDataset) => output_datasetIds.value.indexOf(outputDataset.id) !== -1);
        }
      }
    });
  }
  return datasets;
}

/**
 * Get all datasets for a given investigationId.
 */
exports.getDatasetsByInvestigationId = async (req, res) => {
  try {
    const { limit, skip, search, sortBy, sortOrder, investigationId, datasetType } = req.query;
    const { sessionId } = req.params;
    const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
    const searchSkip = parseInt(skip) || 0;
    const searchText = getParamAsEscapedLowerString(search);

    global.gLogger.debug("getDatasetsByInvestigationId. ", {
      sessionId,
      investigationId,
      searchLimit,
      searchSkip,
      searchText,
      sortBy,
      sortOrder,
      datasetType,
    });
    const formattedSortBy = getDatasetParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);

    let datasets = await findDatasetsByInvestigationId(sessionId, investigationId, searchLimit, searchSkip, searchText, formattedSortBy, formattedSortOrder, datasetType);
    /** This populates the output Datasets */
    datasets = await populateOutputDatasets(sessionId, datasets);

    res.send(datasets);
  } catch (error) {
    sendError(500, `Failed to get getDatasetsByInvestigationId. ${error.message}`, error, res);
  }
};

async function findDatasetsByInvestigationId(sessionId, investigationId, limit, skip, search, sortBy, sortOrder, datasetType) {
  global.gLogger.debug("findDatasetsByInvestigationId. ", { limit, skip, search, sortBy, sortOrder, datasetType });
  const hrstart = process.hrtime();

  const [totalWithoutFilters, total, datasets] = await Promise.all([
    countDatasetByInvestigationId(sessionId, investigationId, undefined, datasetType),
    countDatasetByInvestigationId(sessionId, investigationId, search, datasetType),
    getDatasetsByInvestigationId(sessionId, investigationId, limit, skip, search, sortBy, sortOrder, datasetType),
  ]);
  setMetaDataToDataset(datasets, totalWithoutFilters, total, limit, skip);

  global.gLogger.debug("Final time to findDatasetsByInvestigationId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return datasets;
}

function setMetaDataToDataset(datasets, totalWithoutFilters, total, limit, skip) {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;
  datasets.forEach((d) => {
    d.meta = {
      page: {
        totalWithoutFilters,
        total,
        totalPages,
        currentPage,
      },
    };
  });
}

function getDatasetParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "NAME":
        sortFilter = "dataset.name";
        break;
      case "STARTDATE":
        sortFilter = "dataset.startDate";
        break;
      case "SAMPLENAME":
        sortFilter = "sample.name";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[STARTDATE|SAMPLENAME|NAME]`);
    }
  }
  return sortFilter;
}

/**
 * Get all datasets for a given investigationId.
 */
exports.getDatasetDocumentsByInvestigationId = async (req, res) => {
  try {
    global.gLogger.debug("getDatasetDocumentsByInvestigationId", { investigationId: req.params.investigationId });
    const datasets = await getDatasetsByInvestigationId(req.params.sessionId, req.params.investigationId);
    res.send(parse(datasets));
  } catch (error) {
    sendError(500, "Failed to getDatasetDocumentsByInvestigationId", error, res);
  }
};

/**
 * Get dataset from a list of datasetId
 */
exports.getDatasetsById = async (req, res) => {
  try {
    const { sessionId } = req.params;
    let datasets = await getDatasetsById(sessionId, req.query.datasetIds);

    /** This populates the output Datasets */
    datasets = await populateOutputDatasets(sessionId, datasets);

    res.send(datasets);
  } catch (error) {
    sendError(500, "Failed to getDatasetsById", error, res);
  }
};

/**
 * Get dataset documents from a list of datasetId
 */
exports.getDatasetsDocumentsById = async (req, res) => {
  try {
    const datasets = await getDatasetsById(req.params.sessionId, req.params.datasetIds);
    res.send(parse(datasets));
  } catch (error) {
    sendError(500, "Failed to getDatasetsById", error, res);
  }
};

/** Get a list of datasetDocuments filtered by start and end date.  Format of dates are YYYY-MM-DD */
exports.getDatasetsDocumentsByDateRange = async (req, res) => {
  const { sessionId, startDate, endDate } = req.params;
  global.gLogger.debug("getDatasetsDocumentsByDateRange", { startDate, endDate });
  try {
    const datasets = await getDatasetsByDateRange(sessionId, startDate, endDate);
    res.send(parse(datasets));
  } catch (error) {
    sendError(500, "Failed to getDatasetsDocumentsByDateRange", error, res);
  }
};

/**
 * Get the datacollection associated to a given dataset id
 */
exports.getDataColletionByDatasetId = async (req, res) => {
  try {
    const dataCollection = await getDataColletionByDatasetId(req.params.sessionId, req.params.datasetId);
    res.send(dataCollection);
  } catch (error) {
    sendError(500, "Failed to getDataColletionByDatasetId", error, res);
  }
};

exports.getDatasetsByDOI = async (req, res) => {
  try {
    const { prefix, suffix } = req.params;
    const { limit, skip, search, sortBy, sortOrder } = req.query;
    const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
    const searchSkip = parseInt(skip) || 0;
    const searchText = getParamAsEscapedLowerString(search);

    global.gLogger.debug("getDatasetsByDOI. ", {
      prefix,
      suffix,
      searchLimit,
      searchSkip,
      searchText,
      sortBy,
      sortOrder,
    });
    const formattedSortBy = getDatasetParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);

    const data = await asyncGetSession(global.gServerConfig.icat.authorizations.notifier.user);
    const datasets = await findDatasetsByDOI(data.sessionId, `${prefix}/${suffix}`, searchLimit, searchSkip, searchText, formattedSortBy, formattedSortOrder);
    res.send(datasets);
  } catch (error) {
    sendError(500, "Failed to getDatasetsByDOI", error, res);
  }
};

async function findDatasetsByDOI(sessionId, doi, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findDatasetsByDOI. ", { limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [totalWithoutFilters, total, datasets] = await Promise.all([
    countDatasetsByDOI(sessionId, doi, undefined),
    countDatasetsByDOI(sessionId, doi, search),
    getDatasetsByDOI(sessionId, doi, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToDataset(datasets, totalWithoutFilters, total, limit, skip);

  global.gLogger.debug("Final time to findDatasetsByDOI: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return datasets;
}

exports.downloadData = async (sessionId, datasetIds, datafileIds, res) => {
  try {
    global.gLogger.debug("downloadData", { datasetIds, datafileIds });
    if (!datasetIds && !datafileIds) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    res.redirect(downloadData(sessionId, datasetIds, datafileIds));
  } catch (error) {
    sendError(500, "Failed to download data", error, res);
  }
};

/**
 * records the information that a user asked the restoration of a datasetId and redirects to the download data method to trigger the restoration
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} name
 * @param {*} email empty if not anonymous user
 * @param {*} res
 * @returns
 */
exports.restoreData = async (sessionId, datasetId, name, email, res) => {
  try {
    global.gLogger.debug("restoreData", { datasetId, name, email });
    if (!datasetId || !name) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const datasetAccesses = await findDatasetAccesses(datasetId, TYPE.RESTORE, STATUS.ONGOING, name);
    if (datasetAccesses.length === 0) {
      const datasetAccess = new DatasetAccessSchema({
        datasetId,
        user: name,
        email,
      });

      await datasetAccess.save();
    }
    res.redirect(downloadData(sessionId, [datasetId], undefined));
  } catch (error) {
    sendError(500, "Failed to restore data", error, res);
  }
};
