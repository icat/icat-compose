const icat = require("../api/icat.js");
const sampleDocumentParser = require("../api/parsers/sampledocumentparser.js");
const { sendError } = require("./helpers/helper.tracking.controller.js");

/**
 * Get all samples for a given investigationId.
 */
exports.getSamplesByInvestigationId = async (req, res) => {
  try {
    const data = await icat.getSamplesByInvestigationId(req.params.sessionId, req.params.investigationId);
    res.send(sampleDocumentParser.parse(data));
  } catch (error) {
    sendError(500, "Failed to getSamplesByInvestigationId", error, res);
  }
};
