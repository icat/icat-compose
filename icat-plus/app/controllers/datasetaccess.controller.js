const { ERROR, sendError } = require("../errors");
const DatasetAccess = require("../models/datasetaccess.model.js");
const STATUS = require("../models/datasetaccess.status.js");
const TYPE = require("../models/datasetaccess.type.js");
const nodemailer = require("nodemailer");
const moment = require("moment");
const path = require("path");
const hbs = require("nodemailer-express-handlebars");

const config = global.gRestoreConfig;

/**
 * find dataset acceses by datasetId, type and status, user name
 * For each item, set the email (if not filled) to the user's email
 * @param {*} datasetId
 * @param {*} type eg. RESTORE
 * @param {*} status eg. ONGOING, DONE
 * @param {*} username optional username
 * @returns an array of dataset acess
 */
exports.findDatasetAccesses = async (datasetId, type, status, username) => {
  try {
    global.gLogger.debug("findDatasetAccesses", { datasetId, type, status, username });
    if ((type && ![TYPE.RESTORE].includes(type.toUpperCase())) || (status && ![STATUS.ONGOING, STATUS.DONE].includes(status.toUpperCase()))) {
      throw ERROR.BAD_PARAMS;
    }
    const $and = [{ datasetId }];
    if (type) {
      $and.push({ type: type.toUpperCase() });
    }
    if (status) {
      $and.push({ status: status.toUpperCase() });
    }
    if (username) {
      $and.push({ user: username });
    }
    const datasetAccesses = await DatasetAccess.find({ $and }).lean();
    global.gLogger.debug(`findDatasetAccesses find ${datasetAccesses.length} dataAccess`);
    datasetAccesses.forEach((datasetAccess, index) => {
      const user = global.users[datasetAccess.user];
      if (user && (!datasetAccess.email || datasetAccess.email === "")) {
        datasetAccesses[index] = { ...datasetAccess, email: user.email };
      }
    });
    return datasetAccesses;
  } catch (error) {
    throw new Error(`Failed to retrieve dataset accesses ${error}`);
  }
};

function getContext(datasetAccess, level, message) {
  const isAvailable = level && "ERROR" !== level.toUpperCase();
  return {
    restorationDate: moment(datasetAccess.creationDate).format("DD-MM-YYYY HH:mm"),
    datasetId: datasetAccess.datasetId,
    message,
    isAvailable,
  };
}

exports.sendRestoreEmail = async (datasetAccess, level, message) => {
  try {
    global.gLogger.debug("sendRestoreEmail:", { datasetAccess: JSON.stringify(datasetAccess), level, message });
    if (!config) {
      throw new Error("No configuration for sending email restoration");
    }

    // point to the template folder
    const handlebarOptions = {
      viewEngine: {
        partialsDir: path.resolve("./app/views/"),
        defaultLayout: false,
      },
      viewPath: path.resolve("./app/views/"),
    };

    const email = {
      from: config.fromAddress,
      to: datasetAccess.email,
      subject: config.subject,
      template: "email.restore",
      context: getContext(datasetAccess, level, message),
    };
    global.gLogger.debug(`Email=${JSON.stringify(email)}`);

    const transport = nodemailer.createTransport({
      host: config.smtp.host,
      port: config.smtp.port,
    });

    transport.use("compile", hbs(handlebarOptions));

    global.gLogger.debug("Email is about to be sent", JSON.stringify(email));

    if (config.enabled) {
      await transport.sendMail(email);
    } else {
      global.gLogger.info("Email notifications for restoration are disabled and no email will be sent");
    }
  } catch (error) {
    throw new Error(`Failed to send restore email ${error}`);
  }
};

/**
 * retrieve all data accesses for the given datasetId, with a RESTORE type and status ONGOING. For each item, send an email to the user and set the status to DONE
 * @param {*} datasetId
 * @param {*} level
 * @param {*} message
 * @param {*} res
 * @returns an array of dataset access updated
 */
exports.updateRestoreStatus = async (datasetId, level, message, res) => {
  try {
    global.gLogger.debug("updateRestoreStatus", { datasetId, level, message });
    if (!datasetId || datasetId === "undefined") {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const datasetAccesses = await this.findDatasetAccesses(datasetId, TYPE.RESTORE, STATUS.ONGOING);
    global.gLogger.debug(`updateRestoreStatus find ${datasetAccesses.length} dataset accesses to be updated`);
    if (datasetAccesses.length === 0) {
      global.gLogger.error(`Update restore status: no user has been found to send email for datasetId ${datasetId}`);
      return res.send([]);
    }
    for (let i = 0; i < datasetAccesses.length; i++) {
      const datasetAccess = datasetAccesses[i];
      await this.sendRestoreEmail(datasetAccess, level, message);
      const datasetAccessDone = { ...datasetAccess, status: STATUS.DONE };
      await DatasetAccess.findOneAndUpdate(
        { _id: datasetAccess._id },
        {
          status: STATUS.DONE,
        },
        { new: false }
      );

      datasetAccesses[i] = datasetAccessDone;
    }
    return res.send(datasetAccesses);
  } catch (e) {
    sendError(500, "Failed to updateRestoreStatus", e, res);
  }
};
