const icat = require("../api/icat.js");
const { getInstrumentByName, getUserByName } = require("../cache/cache.js");
const { ERROR, sendError } = require("../errors.js");

exports.getUsers = async (req, res) => {
  const { sessionId } = req.params;
  global.gLogger.debug("getUsers", { sessionId });
  try {
    const users = await icat.getUsersBySessionId(sessionId);
    return res.send(users);
  } catch (error) {
    return sendError(500, "Failed to getUsers", error, res);
  }
};

exports.getInstrumentScientistsByUserName = async (req, res) => {
  const { sessionId, name } = req.params;
  global.gLogger.debug("getInstrumentScientistsByUserName", { sessionId, name });
  try {
    const users = await icat.getInstrumentScientistsByUserName(sessionId, name);
    res.send(users);
  } catch (error) {
    return sendError(ERROR.FAILED_RETRIEVE_INSTRUMENT_SCIENTISTS_BY_USERNAME.code, ERROR.FAILED_RETRIEVE_INSTRUMENT_SCIENTISTS_BY_USERNAME.message, error, res);
  }
};

exports.getInstrumentScientists = async (req, res) => {
  const { sessionId } = req.params;
  global.gLogger.debug("getInstrumentScientists", { sessionId });
  try {
    const users = await icat.getInstrumentScientists(sessionId);
    return res.send(users);
  } catch (error) {
    return sendError(500, "Failed to getInstrumentScientists", error, res);
  }
};

exports.createInstrumentScientists = async (req, res) => {
  const { usernames, instrumentnames } = req.body;
  const { sessionId } = req.params;
  global.gLogger.info("createInstrumentScientists", { usernames, instrumentnames, sessionId });

  try {
    const instrumentScientists = [];
    try {
      usernames.forEach((username) => {
        instrumentnames.forEach((instrumentname) => {
          const user = getUserByName(username);
          const instrument = getInstrumentByName(instrumentname.toUpperCase());

          if (!user) throw new Error("User not found");
          if (!instrument) throw new Error("Instrument not found");

          instrumentScientists.push({
            InstrumentScientist: {
              user: { id: user.id },
              instrument: { id: instrument.id },
            },
          });
        });
      });
    } catch (error) {
      return sendError(400, "Error creating an instrument scientist", error, res);
    }

    const data = await icat.createInstrumentScientists(sessionId, instrumentScientists);
    res.send(data);
  } catch (error) {
    return sendError(500, "Failed to createInstrumentScientists", error, res);
  }
};

exports.deleteInstrumentScientists = async (req, res) => {
  global.gLogger.info("deleteInstrumentScientists", {
    body: JSON.stringify(req.body),
    instrumentscientistsid: req.body.instrumentscientistsid,
    sessionId: req.params.sessionId,
  });
  const instrumentscientistsids = [JSON.parse(req.body.instrumentscientistsid)];
  const instrumentScientists = instrumentscientistsids.map((id) => {
    return {
      InstrumentScientist: {
        id,
      },
    };
  });

  try {
    const data = await icat.deleteInstrumentScientists(req.params.sessionId, instrumentScientists);
    res.send(data);
  } catch (error) {
    return sendError(500, "Failed to deleteInstrumentScientists", error, res);
  }
};
