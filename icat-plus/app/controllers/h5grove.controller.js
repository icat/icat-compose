const request = require("request");

exports.request = (req, location, res) => {
  const server = "http://bcu-mq-05.esrf.fr:8890"; // This should be changed once Antoine is back by global.gServerConfig.h5grove.server.url
  const url = `${`${server}${req.url.replace("/h5grove/", "/")}` + "&file="}${location}`;
  global.gLogger.info(`[H5Grove] Req URL: ${req.url}`);
  global.gLogger.info(`[H5Grove] Redirection: ${url}`);

  request(url)
    .on("error", (e) => {
      global.gLogger.error(`[H5Grove]${e.message}`);
      res.status(500).send({
        message: e.message,
      });
    })
    .pipe(res);
};
