const Event = require("../models/event.model.js");
const constants = require("../constants.js");
const { createModel } = require("mongoose-gridfs");
const { ERROR } = require("../errors.js");

// Create a storage object with a given configuration
const storage = require("multer-gridfs-storage")({
  url: global.gServerConfig.database.uri,
  file: (req, file) => {
    return {
      filename: file.originalname,
    };
  },
});

const multer = require("multer");
const { getInstrumentByName } = require("../cache/cache.js");
// Set multer storage engine to the newly created object
const upload = multer({ storage }).single("file");

// Create and Save a new event
exports.upload = (req, res) => {
  upload(req, res, async (err) => {
    let { instrumentName } = req.query;
    const { investigationId } = req.query;
    const { datasetId, machine, software, creationDate, text } = req.body;
    const { name, fullName } = req.session;

    if (err) {
      return res.json({ error_code: 1, err_desc: err });
    }

    if (isNaN(investigationId) && !instrumentName) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
    }

    if (instrumentName) {
      instrumentName = instrumentName.toUpperCase();
      if (!getInstrumentByName(instrumentName)) {
        return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
      }
    }

    global.gLogger.info("Upload file", { investigationId, instrumentName });

    /**
     * Creating the event
     */
    const event = new Event({
      investigationId,
      datasetId,
      type: constants.EVENT_TYPE_ATTACHMENT,
      category: constants.EVENT_CATEGORY_FILE,
      text,
      filename: req.file.filename,
      username: name,
      software,
      machine,
      instrumentName,
      creationDate,
      file: req.file.id,
      contentType: req.file.contentType,
    });
    event
      .save()
      .then(async (event) => {
        global.gLogger.info("Upload file done successfully");
        event.username = fullName;
        res.send(event);
      })
      .catch((e) => {
        global.gLogger.error("Error uploading file as resource", { e: e.message });
        res.status(500).send({
          message: e.message || "Some error occurred while creating the Note.",
        });
      });
  });
};

// Create and Save a new event
exports.test = (req, res) => {
  res.writeHead(200, { "Content-Type": "text/html" });
  res.write('<form action="/resource/:sessionId/file/investigation/id/:investigationId/upload" method="post" enctype="multipart/form-data">');
  res.write('<input type="file" name="file"><br>');
  res.write('<input type="text" name="investigationId" value="0">');
  res.write('<input type="submit">');
  res.write("</form>");
  res.send();
};

/**
 * Download a file given a eventId.
 *
 **/
exports.download = async (eventId, res) => {
  const Attachment = createModel({
    modelName: "fs",
  });

  try {
    const event = await Event.findById(eventId);
    global.gLogger.debug("Event found", { eventId });
    const readStream = Attachment.read({ _id: event.file[0] });
    readStream.pipe(res);
    readStream.on("error", (error) => {
      global.gLogger.error(error);
    });
  } catch (err) {
    global.gLogger.error("Download", { error: err.message });
    res.status(500).send({
      message: "Event not found",
    });
  }
};
