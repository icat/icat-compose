//const { getMyInvestigationsId } = require("../cache/cache.js");
const Shipment = require("../models/shipment.model.js");
const helper = require("./helpers/helper.tracking.controller.js");
const { ERROR, sendError } = require("../errors.js");

/**
 * This method is called before the authorization middleware and will find the shipment Id and will add it to the request then the authorization layer can authoriza the sessionId with investigation
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function addInvestigationIdToRequest(req, res, next) {
  global.gLogger.info("addInvestigationIdToRequest", {
    sessionId: req.params.sessionId,
    shipmentId: req.params.shipmentId,
  });
  try {
    const shipment = await Shipment.findById(req.params.shipmentId);
    if (shipment) {
      global.gLogger.info("addInvestigationIdToRequest", { investigationId: shipment.investigationId });
      req.params.investigationId = shipment.investigationId;
      return next();
    }
    throw ERROR.NO_SHIPMENT_FOUND;
  } catch (e) {
    helper.sendError(500, "addInvestigationIdToRequest", e, res);
  }
}

/**
 * Returns a shipment by shipmentId. It ensures that shipment belongs to the user, instrumentScientist or administrator because of the middleware
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getShipmentById(req, res) {
  const { sessionId, shipmentId } = req.params;
  global.gLogger.info("getShipmentById", { sessionId, shipmentId });

  try {
    const shipment = await helper.findShipmentById(req.params.shipmentId);
    if (!shipment) {
      throw new Error("Shipment not found");
    }
    res.send(shipment);
  } catch (e) {
    helper.sendError(500, "getShipmentById", e, res);
  }
}

async function getShipmentsBySessionId(req, res) {
  global.gLogger.error("Deprecated");
  res.status(500).send("Deprecated");
}

/**
 * Gets shipments by investigationId
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getShipmentsByInvestigationId(req, res) {
  const { sessionId, investigationId } = req.params;
  global.gLogger.info("getShipmentsByInvestigationId", { sessionId, investigationId });
  try {
    const shipments = await helper.findNotRemovedShipmentsBy({ investigationId });
    res.send(shipments);
  } catch (e) {
    helper.sendError(500, "Failed to getShipmentsByInvestigationId", e, res);
  }
}

/**
 * It creates a shipment. It receives the shipment document in the body.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function createShipment(req, res) {
  const { investigationId, sessionId } = req.params;
  global.gLogger.info("createShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    let shipment = new Shipment(req.body);
    shipment = await helper.createShipment(shipment, investigationId, sessionId);
    res.send(shipment);
  } catch (error) {
    helper.sendError(500, "Failted to createShipment", error, res);
  }
}

/**
 * It updates the updatable parameters of a shipment
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function updateShipment(req, res) {
  const { investigationId, sessionId } = req.params;
  global.gLogger.debug("updateShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    const shipment = await Shipment.findOneAndUpdate(
      { _id: req.body._id },
      {
        name: req.body.name,
        comments: req.body.comments,
        defaultReturnAddress: req.body.defaultReturnAddress,
        defaultShippingAddress: req.body.defaultShippingAddress,
        modifiedBy: req.session.name,
      },
      { new: true }
    )
      .populate("defaultShippingAddress")
      .populate("defaultReturnAddress");
    res.send(shipment);
  } catch (e) {
    helper.sendError(500, "updateShipment", e, res);
  }
}

async function deleteShipment(req, res) {
  const { investigationId, sessionId } = req.params;
  global.gLogger.debug("deleteShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(req.params.investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    const shipment = await Shipment.findOneAndUpdate(
      { _id: req.body._id },
      {
        status: "REMOVED",
        deletedBy: req.session.name,
      },
      { new: true }
    );
    res.send(shipment);
  } catch (e) {
    helper.sendError(500, "delete", e, res);
  }
}

module.exports = {
  addInvestigationIdToRequest,
  getShipmentById,
  getShipmentsBySessionId,
  getShipmentsByInvestigationId,
  createShipment,
  updateShipment,
  deleteShipment,
};
