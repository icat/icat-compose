const _ = require("lodash");
const axios = require("axios-https-proxy-fix");
const template = require("../doi/datacite.template.js");
const icat = require("../api/icat.js");
const cache = require("../cache/cache.js");
const moment = require("moment");
const { sendError } = require("../errors.js");

function getDataCiteSubjects(investigation, investigationDescription) {
  const subjects = [
    {
      subject: investigationDescription,
      subjectScheme: "Proposal Type Description",
    },
    {
      subject: investigation.name,
      subjectScheme: "Proposal",
    },
  ];
  if (investigation.investigationInstruments && investigation.investigationInstruments.length > 0) {
    subjects.push({
      subject: investigation.investigationInstruments[0].instrument.name,
      subjectScheme: "Instrument",
    });
  }
  return subjects;
}

function getDataCiteCreators(investigationUsers) {
  const creators = [];
  investigationUsers
    .filter((investigationUser) => investigationUser.role === "Participant" || investigationUser.role === "Scientist")
    .map((investigationUser) => investigationUser.user)
    .sort((u1, u2) => (u1.fullName > u2.fullName ? 1 : -1))
    .forEach((user) => {
      if (!isUserInCreatorsList(user.fullName, creators)) {
        creators.push({
          name: user.fullName,
          nameIdentifiers: getOrcidNameIdentifier(user.orcidId),
        });
      }
    });
  return creators;
}

function getOrcidNameIdentifier(orcid) {
  return orcid
    ? [
        {
          schemeUri: "https://orcid.org",
          nameIdentifierScheme: "ORCID",
          nameIdentifier: orcid,
        },
      ]
    : undefined;
}

function getDataCiteContributors(investigationUsers, creators) {
  const contributors = [];
  investigationUsers
    .filter((investigationUser) => investigationUser.role === "Local contact")
    .map((investigationUser) => investigationUser.user)
    .filter((user) => !isUserInCreatorsList(user.fullName, creators))
    .sort((u1, u2) => (u1.fullName > u2.fullName ? 1 : -1))
    .forEach((user) => {
      contributors.push({
        name: user.fullName,
        nameIdentifiers: getOrcidNameIdentifier(user.orcidId),
      });
    });
  return contributors;
}

function isUserInCreatorsList(userName, creators) {
  return creators.map((creator) => creator.name).indexOf(userName) > -1;
}

function getRelatedIdentifier(dataCollection) {
  return {
    relatedIdentifier: dataCollection.doi,
    relatedIdentifierType: "DOI",
    relationType: "IsSupplementedBy",
  };
}

function getDataCiteJson(doi, investigation, relatedDataCollections) {
  const datacite = {};
  datacite.doi = doi;
  datacite.url = global.gServerConfig.datacite.landingPage + doi;
  datacite.publicationYear = moment(investigation.releaseDate, "YYYY-MM-DDThh:mm:ss.sTZD").format("YYYY");
  datacite.creators = [];

  if (investigation.investigationUsers) {
    datacite.creators = getDataCiteCreators(investigation.investigationUsers);
    datacite.contributors = getDataCiteContributors(investigation.investigationUsers, datacite.creators);
  }

  datacite.titles = [
    {
      title: investigation.title,
    },
  ];

  datacite.descriptions = [
    {
      description: investigation.summary,
      descriptionType: "Abstract",
    },
  ];

  function getResourceType(doi) {
    if (doi.toUpperCase().indexOf("-DC-") > -1) {
      return "Datacollection";
    }
    return "Experiment Session";
  }

  datacite.types = {
    resourceTypeGeneral: "Dataset",
    resourceType: getResourceType(doi),
  };

  const investigationType = cache.getInvestigationTypes().find((o) => {
    return o.InvestigationType.name === investigation.type.name;
  });

  let investigationDescription = "";
  if (investigationType) {
    if (investigationType.InvestigationType) {
      investigationDescription = investigationType.InvestigationType.description;
    }
  }

  datacite.publisher = global.gServerConfig.datacite.publisher;
  datacite.subjects = getDataCiteSubjects(investigation, investigationDescription);

  datacite.dates = [
    {
      date: moment(investigation.startDate, "YYYY-MM-DDThh:mm:ss.sTZD").format("YYYY-MM-DD"),
      dateType: "Collected",
    },
    {
      date: moment(investigation.releaseDate, "YYYY-MM-DDThh:mm:ss.sTZD").format("YYYY"),
      dateType: "Issued",
    },
  ];

  if (relatedDataCollections && relatedDataCollections.length > 0) {
    datacite.relatedIdentifiers = [];
    relatedDataCollections.forEach((dataCollection) => {
      datacite.relatedIdentifiers.push(getRelatedIdentifier(dataCollection));
    });
  }
  return datacite;
}

exports.getJSONDataciteByDOI = async (req, res) => {
  const { prefix, suffix } = req.params;
  const doi = `${req.params.prefix}/${suffix}`;
  try {
    if (prefix) {
      if (suffix) {
        global.gLogger.debug("getJSONDataciteByDOI", { doi });
        const auth = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
        const investigation = await icat.getInvestigationByDOI(auth.sessionId, doi);
        if (investigation && investigation.length > 0 && investigation[0].Investigation) {
          const inv = investigation[0].Investigation;
          const relatedDataCollections = await icat.getDataCollectionsByInvestigationId(auth.sessionId, inv.id);
          return res.status(200).send(getDataCiteJson(doi, inv, relatedDataCollections));
        }
        return res.status(500).send("No investigation found");
      }
    }
  } catch (e) {
    global.gLogger.error(e);
    return res.status(500).send();
  }
  return res.status(400).send();
};

/**
 * Return true if it investigationId corresponds to a participant of the investigation
 */
function isInvestigationInInvestigationUserList(investigationId, investigationUsers) {
  return _.find(investigationUsers, (i) => {
    return i.investigationId === investigationId;
  });
}

function getInvestigationsIds(investigations) {
  return _.uniq(
    investigations.map((o) => {
      return o.Investigation.id;
    })
  );
}

/**
 * returns the list of investigations and investigationUser for a given user (provided by the sessionId) and for a list of datasetIds
 * throws an error if the user cannot access to one of the datasets (not a participant of the corresponding investigation)
 * @param {sessionId} sessionId
 * @param {datasetIds} datasetIds
 */
exports.isParticipantByDatasetId = async (sessionId, datasetIds) => {
  const [investigations, investigationUsers] = await Promise.all([
    icat.getInvestigationsByDatasetListIdsURL(sessionId, datasetIds),
    icat.asyncGetInvestigationUserBySessionId(sessionId),
  ]);
  if (investigationUsers.length === 0) {
    throw new Error(`investigationUsers is empty ${sessionId}`);
  }

  if (investigations.length === 0) {
    throw new Error(`No investigations found ${sessionId}`);
  }

  const investigationIds = getInvestigationsIds(investigations);

  /** Foreach investigationId we check that user is a participant*/
  for (let i = 0; i < investigationIds.length; i++) {
    if (!isInvestigationInInvestigationUserList(investigationIds[i], investigationUsers)) {
      throw new Error(`investigation is not in investigationUser list ${investigationIds[i]}`);
    }
  }
  return [investigations, investigationUsers];
};

exports.mint = async function (sessionId, investigations, name, fullName, datasetIdList, title, abstract, authors, skip, res) {
  // TODO SKIP until we use the test datacite plateform #242

  const datasetIds = datasetIdList;
  const skipMint = skip || false;
  global.gLogger.debug("Starts mint", { sessionId, datasetIds, title, abstract, authors: JSON.stringify(authors), skipMint });

  try {
    const investigationIds = getInvestigationsIds(investigations);

    const investigationNames = _.uniq(
      investigations.map((o) => {
        return o.Investigation.name;
      })
    );
    const instrumentsNames = _.uniq(
      investigations.map((o) => {
        return o.Investigation.investigationInstruments[0].instrument.name;
      })
    );

    global.gLogger.debug("[Mint] Allowed", { investigationIds, investigationNames, title, instrumentsNames });

    global.gLogger.debug("[Mint] Creating a datacollection");
    let dataCollectionId = await createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, name, fullName);
    global.gLogger.debug("[Mint] Created datacollection", { dataCollectionId });
    dataCollectionId = String(dataCollectionId[0]);
    global.gLogger.debug("[Mint] Minting DOI", { dataCollectionId, title });

    if (skipMint) {
      global.gLogger.debug("[Mint] skip for tests");
      return res.send({ message: `skipMintDOI ${dataCollectionId}` });
    }
    const doi = await mintDOI(dataCollectionId, title, abstract, authors, datasetIds, investigationNames, instrumentsNames);

    await icat.updateDataCollectionDOI(global.gServerConfig.icat.authorizations.minting.user, parseInt(dataCollectionId), doi);
    res.send({ message: doi });
  } catch (e) {
    global.gLogger.error(e.message);
    sendError(500, "An error ocurred during the minting", e, res);
  }
};

/**
 * Create a datacollection in the metadata manager for given dataset
 * @param {*} minterCredential
 * @returns {Promise}
 */
async function createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) {
  const dataCollectionId = await icat.mint(
    global.gServerConfig.icat.authorizations.minting.user,
    datasetIds,
    title,
    abstract,
    investigationNames,
    instrumentsNames,
    mintedByName,
    mintedByFullName
  );
  global.gLogger.debug("DatacollectionId has been created", { dataCollectionId });
  return dataCollectionId;
}

/**
 * Mint the DOI at datacite
 * @param {*} dataCollectionId dataCollection identifier
 * @param {*} title title of teh DOI
 * @param {*} abstract abstract of the DOI
 * @param {*} authors authors of the DOI
 * @param {*} datasetIdList identifiers of the datasets used in this DOI
 * @param {*} investigationNames
 * @param {*} instrumentsNames
 * @return {Promise}
 */
async function mintDOI(dataCollectionId, title, abstract, authors, datasetIdList, investigationNames, instrumentsNames) {
  const { prefix, suffix, landingPage, username, password, proxy, mds } = global.gServerConfig.datacite;

  const doi = `${prefix}/${suffix}${dataCollectionId}`;
  const url = landingPage + doi;

  const xmls = generateXML(doi, dataCollectionId, title, abstract, authors, investigationNames, instrumentsNames);
  global.gLogger.info("Creation of the XML from the DOI to be minted as requested by datacite", {
    xmls,
    doi,
    title,
    abstract,
    datasetIdList,
    investigationNames,
    instrumentsNames,
  });
  // mint metadata
  await axios({
    method: "post",
    url: `${mds}/metadata`,
    headers: { "content-type": "application/xml", charset: "UTF-8" },
    auth: { username, password },
    proxy: { host: proxy.host, port: proxy.port },
    params: {},
    data: xmls,
  });

  global.gLogger.info("[SUCCESS] DOI metadata has been minted on datacite");
  // register doi
  await axios({
    method: "post",
    url: `${mds}/doi`,
    auth: { username, password },
    proxy: { host: proxy.host, port: proxy.port },
    params: { doi, url },
  });
  global.gLogger.info(`[SUCCESS] DOI has been registered on datacite: ${doi}`);
  return doi;
}

function generateXML(doi, dataCollectionId, title, abstract, authors, investigationNames, instrumentsNames) {
  /** This function is a string is blank */
  function isBlank(str) {
    return !str || /^\s*$/.test(str);
  }

  /** validation */
  if (authors) {
    if (authors.length === 0) {
      throw "List of authors is empty";
    }
  } else {
    throw "No authors found";
  }

  if (isBlank(title)) {
    throw "Title should not be empty";
  }

  if (isBlank(abstract)) {
    throw "Abstract should not be empty";
  }

  if (dataCollectionId) {
    if (Number.isInteger(dataCollectionId)) {
      throw "It was not possible to create a data collection on ICAT";
    }
  }

  /** Parsing authors */
  let authorsXML = "";
  for (let i = 0; i < authors.length; i++) {
    if (authors[i].creatorName) {
      authorsXML = `${authorsXML}<creator><creatorName>${authors[i].creatorName}</creatorName></creator>`;
    } else {
      authorsXML = `${authorsXML}<creator><creatorName>${authors[i].surname},${authors[i].name}</creatorName></creator>`;
    }
  }

  const xml = template.xml
    .replace("%DOI%", doi)
    .replace("%PUBLICATION_YEAR%", new Date().getFullYear())
    .replace("%AUTHORS%", authorsXML)
    .replace("%TITLE%", title)
    .replace("%ABSTRACT%", abstract)
    .replace("%INSTRUMENT_NAME%", instrumentsNames)
    .replace("%INVESTIGATION_NAME%", investigationNames);

  return xml;
}
