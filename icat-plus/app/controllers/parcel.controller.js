const Shipment = require("../models/shipment.model.js");
const Parcel = require("../models/parcel.model.js");
const { ERROR } = require("../errors.js");
const STATUS = require("../models/status.js");
const helper = require("./helpers/helper.tracking.controller.js");
const { sendEmail } = require("./helpers/helper.email.js");
const {
  asyncGetInvestigationById,
  getInvestigationUserByInvestigationId,
  getInvestigationsAsParticipantBySessionId,
  getInvestigationByInstrumentScientist,
  getInvestigationUserByInvestigationIdAndRole,
} = require("../api/icat.js");

const labelsHelper = require("./helpers/helper.labels.tracking.controller.js");
const { ROLE_LOCAL_CONTACT } = require("../constants.js");

function isStatusValid(parcel, newStatus) {
  return Object.keys(STATUS).includes(newStatus);
}

/**
 * It will find the parcel and will set the new status of the parcel
 */
async function setStatus(sessionId, investigationId, parcelId, status, body, username) {
  global.gLogger.info("SetStatus", { _id: parcelId, investigationId });
  const newStatus = status.toUpperCase();

  const parcels = await helper.findParcels({
    _id: parcelId,
    investigationId,
  });

  if (parcels.length === 0) {
    throw new Error(`No parcels were found with investigationId: ${investigationId} and parcelId: ${parcelId}`);
  }
  global.gLogger.info("setStatus: parcel found", { _id: parcelId, investigationId });
  const parcel = parcels[0];

  if (!isStatusValid(parcel, newStatus)) {
    throw new Error(`New status ${newStatus} is not valid for parcelId ${parcelId} and status ${status}`);
  }

  const statusObject = {
    status: newStatus,
    createdBy: username,
    comments: body.comments,
  };

  const updateFields = {
    $push: { statuses: statusObject },
    // containsDangerousGoods should only be updated when setting the status to READY
    containsDangerousGoods: newStatus === STATUS.READY ? body.containsDangerousGoods : parcel.containsDangerousGoods,
    localContactNames: await getLocalContactNameListByInvestigationId(sessionId, investigationId),
  };

  const updated = await Parcel.findOneAndUpdate({ _id: parcel._id }, updateFields, {
    new: true,
  }).populate({ path: "items", match: { status: { $ne: "REMOVED" } } });

  try {
    global.gLogger.info("App is going to send the emails", { sessionId });
    const response = await asyncGetInvestigationById(sessionId, updated.investigationId);
    const data = await sendEmail(sessionId, response[0].Investigation, updated);
    global.gLogger.info("Emails have been sent", data);
    return updated.toObject({ virtuals: true });
  } catch (error) {
    global.gLogger.error("Error sending the emails", error);
    throw error;
  }
}

async function setParcelStatus(req, res) {
  const { sessionId, parcelId, investigationId, status } = req.params;
  global.gLogger.debug("setParcelStatus", { sessionId, investigationId, parcelId, status });

  try {
    const parcel = await setStatus(sessionId, investigationId, parcelId, status, req.body, req.session.name);
    res.send(parcel);
  } catch (e) {
    helper.sendError(ERROR.PARCEL_FAILED_TO_SET_STATUS.code, ERROR.PARCEL_FAILED_TO_SET_STATUS.message, e, res);
  }
}

async function getParcelBySessionId(req, res) {
  const { sessionId, name } = req.session;
  /**If it is administrator then it retrieves all the parcels otherwise only my parcels */
  let investigationIds = [];
  if (!req.session.isAdministrator) {
    const [myInvestigations, localContactInvestigations] = await Promise.all([
      getInvestigationsAsParticipantBySessionId(sessionId, name),
      getInvestigationByInstrumentScientist(sessionId, name),
    ]);
    const investigations = myInvestigations.concat(localContactInvestigations);
    investigationIds = investigations.map((inv) => inv.Investigation.id);
  }

  const query = req.session.isAdministrator
    ? {}
    : {
        investigationId: {
          $in: investigationIds,
        },
      };

  const parcels = await helper.findNotRemovedParcels(query);
  res.send(parcels);
}

/**
 * Returns a parcel by id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getParcelById(req, res) {
  const parcels = await helper.findParcels({
    _id: req.params.parcelId,
    investigationId: req.params.investigationId,
  });
  if (!parcels) {
    throw new Error("Parcel not found ");
  }
  res.send(parcels[0]);
}

/**
 * Gets all parcels by status
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getParcelsByStatus(req, res) {
  global.gLogger.info("getParcelsByStatus", {
    sessionId: req.params.sessionId,
    status: req.params.status,
  });

  //TODO: this would be much more efficient if we can use the query instead of filtering all results
  // `status` is a virtual field and cannot be queried directly (https://mongoosejs.com/docs/tutorials/virtuals.html#limitations)
  const parcels = await helper.findParcels({});
  const filteredByStatus = parcels.filter((parcel) => parcel.status === req.params.status.toUpperCase());
  res.send(filteredByStatus);
}

async function getLabels(req, res) {
  const { sessionId, investigationId, parcelId } = req.params;
  global.gLogger.info("getLabels", { sessionId, investigationId, parcelId });

  const parcels = await helper.findNotRemovedParcels({
    investigationId,
    _id: req.params.parcelId,
  });

  global.gLogger.debug("Parcels found", { sessionId, investigationId, parcelId });

  const shipment = await Shipment.findById(parcels[0].shipmentId).populate({ path: "parcels", match: { status: { $ne: "REMOVED" } } });
  global.gLogger.debug("Shipment found", { sessionId, investigationId, parcelId, shipmentId: shipment._id });

  const response = await asyncGetInvestigationById(req.params.sessionId, req.params.investigationId);
  const investigationUsers = await getInvestigationUserByInvestigationId(req.params.sessionId, [req.params.investigationId]);
  labelsHelper.getLabels(res, shipment, parcels[0], response[0].Investigation, investigationUsers);
}

async function getLocalContactNameListByInvestigationId(sessionId, investigationId) {
  const users = await getInvestigationUserByInvestigationIdAndRole(sessionId, investigationId, ROLE_LOCAL_CONTACT);
  return users.map((u) => u.name);
}

/**
 * Gets parcels by shipmentId.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getParcelsByShipmentId(req, res) {
  const { sessionId, investigationId, shipmentId } = req.params;
  global.gLogger.info("getParcelsByShipmentId", { sessionId, shipmentId, investigationId });
  const parcels = await helper.findNotRemovedParcels({ investigationId, shipmentId });
  res.send(parcels);
}

async function createParcel(req, res) {
  const { sessionId, investigationId, shipmentId } = req.params;
  const { username } = req.session;

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    global.gLogger.error(`[createParcel] ${req.params.investigationId} does not match with investigationId of parcel ${req.body.investigationId}`);
    return helper.sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  if (req.params.shipmentId !== shipmentId)
    return helper.sendError(ERROR.SHIPMENTID_DOES_NOT_MATCH.code, ERROR.SHIPMENTID_DOES_NOT_MATCH.message, ERROR.SHIPMENTID_DOES_NOT_MATCH, res);

  /** Because a parcel is created from an investigation then it becomes automatically scheduled */
  const myParcel = req.body;
  myParcel.statuses = [
    {
      status: STATUS.CREATED,
      createdBy: username,
    },
    {
      status: STATUS.SCHEDULED,
      createdBy: username,
    },
  ];

  const investigations = await asyncGetInvestigationById(sessionId, investigationId);
  if (investigations[0] && investigations[0].Investigation) {
    const { name } = investigations[0].Investigation;
    myParcel.investigationName = name;
    myParcel.localContactNames = await getLocalContactNameListByInvestigationId(sessionId, investigationId);
    const parcel = await new Parcel(myParcel).save();
    global.gLogger.debug("Parcel created", { id: parcel._id });
    let shipment = await helper.findShipmentById(req.body.shipmentId);
    if (shipment) {
      shipment.parcels.push(parcel._id);
      /** save shipment */
      shipment = await Shipment.findOneAndUpdate(
        { _id: req.body.shipmentId },
        {
          parcels: shipment.parcels,
          modifiedBy: username,
        },
        { new: true }
      );
      res.send(shipment.toObject({ virtuals: true }));
    }
  } else {
    return helper.sendError(ERROR.NO_INVESTIGATION_FOUND.code, ERROR.NO_INVESTIGATION_FOUND.message, ERROR.NO_INVESTIGATION_FOUND, res);
  }
}

async function updateParcel(req, res) {
  global.gLogger.debug("updateParcel", { sessionId: req.params.sessionId, parcels: JSON.stringify(req.body) });

  if (Number(req.params.investigationId) !== Number(req.body.investigationId))
    return helper.sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);

  try {
    const { name, type, description, comments, storageConditions, shippingAddress, returnAddress, containsDangerousGoods } = req.body;
    const parcel = await Parcel.findOneAndUpdate(
      { _id: req.body._id },
      { name, type, description, comments, storageConditions, shippingAddress, returnAddress, containsDangerousGoods },
      { new: true }
    );
    res.send(parcel.toObject());
  } catch (error) {
    global.gLogger.error(error);
    return helper.sendError(ERROR.PARCEL_FAILED_TO_UPDATE.code, ERROR.PARCEL_FAILED_TO_UPDATE.message, ERROR.PARCEL_FAILED_TO_UPDATE, res);
  }
}

async function deleteParcel(req, res) {
  const { sessionId, investigationId, shipmentId } = req.params;
  global.gLogger.debug("deleteParcel", { sessionId, investigationId, shipmentId, body: req.body });

  try {
    const parcel = await setStatus(sessionId, investigationId, req.body._id, "REMOVED", {}, req.session.name);
    res.send(parcel);
  } catch (error) {
    helper.sendError(ERROR.PARCEL_FAILED_TO_DELETE.code, ERROR.PARCEL_FAILED_TO_DELETE.message, error, res);
  }
}

async function transfer(req, res) {
  const { sessionId, investigationId, toInvestigationId } = req.params;
  global.gLogger.debug("transfer", { sessionId, investigationId, toInvestigationId, parcels: req.body });

  try {
    const shipment = await getOrCreateShipmentByInvestigationId(sessionId, toInvestigationId);
    if (shipment) {
      await transferParcelTo(sessionId, toInvestigationId, req.body);
      res.send();
    } else {
      helper.sendError(ERROR.NO_SHIPMENT_FOUND.code, ERROR.NO_SHIPMENT_FOUND.message, ERROR.NO_SHIPMENT_FOUND, res);
    }
  } catch (e) {
    helper.sendError(ERROR.PARCEL_FAILED_TO_BE_TRANSFERED.code, ERROR.PARCEL_FAILED_TO_BE_TRANSFERED.message, e, res);
  }
}

async function transferParcelTo(sessionId, investigationId, parcels) {
  const investigations = await asyncGetInvestigationById(sessionId, investigationId);
  if (investigations[0] && investigations[0].Investigation) {
    const investigationName = investigations[0].Investigation.name;
    const updateResponse = await Parcel.updateMany(
      {
        _id: {
          $in: parcels,
        },
      },
      { investigationName, investigationId }
    );
    if (updateResponse.ok) {
      return;
    }
  } else {
    throw ERROR.NO_INVESTIGATION_FOUND;
  }
  throw ERROR.PARCEL_FAILED_TO_UPDATE;
}
/**
 * It will return or will create a new shipment if it does not exist
 */
async function getOrCreateShipmentByInvestigationId(sessionId, investigationId) {
  const shipments = await helper.findNotRemovedShipmentsBy({ investigationId });
  if (shipments.length === 0) {
    global.gLogger.debug("No shipment associated to investigation", { investigationId });
    const shipment = await helper.createShipment(
      new Shipment({
        name: "New shipment",
      }),
      investigationId,
      sessionId
    );
    return shipment;
  }
  return shipments[0];
}

module.exports = {
  setParcelStatus,
  getParcelBySessionId,
  getParcelById,
  getParcelsByStatus,
  getLabels,
  getParcelsByShipmentId,
  createParcel,
  updateParcel,
  deleteParcel,
  transfer,
};
