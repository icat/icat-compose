const Parcel = require("../models/parcel.model.js");
const Item = require("../models/item.model.js");
const helper = require("./helpers/helper.tracking.controller.js");
const ICAT = require("../api/icat.js");

async function addItemToParcel(parcelId, investigationId, item, res) {
  try {
    const parcels = await Parcel.find({ _id: parcelId, investigationId }).populate("items");

    if (parcels.length === 0) {
      throw new Error("addItemToParcel: No parcel found");
    }
    const parcel = parcels[0];
    const savedItem = await new Item(item).save();

    parcel.items.push(savedItem);
    const newParcel = await Parcel.findOneAndUpdate({ _id: parcel._id }, { items: parcel.items }, { new: true }).populate({ path: "items", match: { status: { $ne: "REMOVED" } } });
    res.send(newParcel);
  } catch (error) {
    helper.sendError(500, "Failed to addItemToParcel", error, res);
  }
}

async function addItem(req, res) {
  const { sessionId, parcelId, investigationId } = req.params;
  const item = req.body;
  global.gLogger.debug("addItem", { sessionId, item: JSON.stringify(item), parcelId, investigationId });

  try {
    //**Checks on items that are sample sheet */
    if (item.type === "SAMPLESHEET") {
      if (item.sampleId == null) {
        return helper.sendError(403, "addItem: No sampleId has been found on item of type sample sheet", new Error(), res);
      }
      /** Case sample is a sample sheet */
      const response = await ICAT.getSampleById(sessionId, item.sampleId);
      item.sampleId = response.data[0].Sample.id;
      global.gLogger.debug("SampleSheet found", {
        sampleId: item.sampleId,
      });
    }
    return await addItemToParcel(parcelId, investigationId, item, res);
  } catch (e) {
    helper.sendError(500, "Failed to addItem", e, res);
  }
}

async function updateItem(req, res) {
  const { sessionId, investigationId, parcelId, itemId } = req.params;
  global.gLogger.debug("updateItem", { sessionId, investigationId, parcelId, itemId, item: req.body });

  try {
    const item = await Item.findOneAndUpdate(
      { _id: req.params.itemId },
      {
        name: req.body.name,
        description: req.body.description,
        comments: req.body.comments,
        sampleId: req.body.sampleId,
        type: req.body.type,
      },
      { new: true }
    );
    res.send(item);
  } catch (error) {
    helper.sendError(500, "Failed to updateItem", error, res);
  }
}

async function deleteItem(req, res) {
  const { sessionId, investigationId, parcelId, itemId } = req.params;
  global.gLogger.debug("deleteItem", { sessionId, investigationId, parcelId, itemId });

  try {
    const response = await Parcel.updateOne({ investigationId, _id: parcelId }, { $pull: { items: itemId } }, { safe: true });
    if (!response.ok) {
      throw Error("UpdateOne from deleted item returned not ok");
    }
    const parcel = await helper.findParcels({ investigationId, _id: parcelId });
    const responseRemoved = await Item.deleteOne({ _id: req.params.itemId });
    if (!responseRemoved.ok) {
      throw Error("Delete item from collection returned not ok");
    }
    res.send(parcel);
  } catch (error) {
    helper.sendError(500, "Failed to deleteItem", error, res);
  }
}

module.exports = {
  addItem,
  updateItem,
  deleteItem,
};
