const { createToken, getQuestionnaires } = require("../api/dmp");
const { ERROR } = require("../errors");

/**
 * Returns the list of questionnaires uuid from the DMP with the given investigationName
 * The DMP should be enabled in the configuration file
 * @param {*} investigationName
 * @param {*} res
 * @returns [{uuid:''}]
 */
exports.getQuestionnaires = async (investigationName, res) => {
  global.gLogger.debug("DMP getQuestionnaires", { investigationName });
  try {
    const { server, credentials, enabled } = global.gServerConfig.dmp;
    if (enabled) {
      if (!investigationName) {
        global.gLogger.error("InvestigationName is missing!");
        return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
      }
      const auth = await createToken(server, credentials);
      const questionnaires = await getQuestionnaires(server, auth.token, investigationName);
      const uuids = questionnaires._embedded.questionnaires.map((questionnaire) => ({ uuid: questionnaire.uuid }));
      return res.send(uuids);
    }
    global.gLogger.error("DMP is not enabled!");
    return res.status(ERROR.DMP_DISABLED.code).send(ERROR.DMP_DISABLED.message);
  } catch (error) {
    global.gLogger.error(error);
    return res.status(ERROR.FAILED_TO_RETRIEVE_DMP.code).send(ERROR.FAILED_TO_RETRIEVE_DMP.message);
  }
};
