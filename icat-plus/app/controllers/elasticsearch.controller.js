const elasticsearch = require("elasticsearch");
const Moment = require("moment");
const INDEX = "all_datasets";
const PUBLIC_INDEX = "public_datasets";
const axios = require("axios-https-proxy-fix");

/**
 * It allows _msearch on the public index
 * @param {*} req
 * @param {*} res
 */
exports.msearch = async (req, res) => {
  try {
    const url = `${global.gServerConfig.elasticsearch.server}/${PUBLIC_INDEX}/_msearch`;
    const response = await axios.post(url, req.body, {
      headers: {
        "Content-Type": "application/x-ndjson",
      },
    });
    res.send(response.data);
  } catch (error) {
    res.status(400).send(error.message);
  }
};

/**runs a query */
const runESQuey = async (body) => {
  const client = getClient();
  const response = await client.search({
    index: INDEX,
    body,
  });
  return response;
};

function getClient() {
  const client = new elasticsearch.Client({
    host: global.gServerConfig.elasticsearch.server,
    apiVersion: global.gServerConfig.elasticsearch.apiVersion ? "7.2" : global.gServerConfig.elasticsearch.apiVersion,
  });

  if (!global.gServerConfig.elasticsearch.apiVersion) {
    global.gLogger.error("apiVersion should be defined in the elasticsearch section of the configuration file");
  }
  return client;
}

function getRangeFilterByDateField(dateField, ranges) {
  const filters = {};
  for (let i = 0; i < ranges.length; i++) {
    const element = ranges[i];
    const dict = {};
    dict[dateField] = {
      gte: element.start,
      lte: element.end,
    };
    filters[element.name] = {
      range: dict,
    };
  }

  return filters;
}

function getInstrumentByDateField(dateFields, ranges) {
  const dict = {};
  for (let i = 0; i < dateFields.length; i++) {
    const dateField = dateFields[i];
    dict[dateField] = {
      filters: {
        filters: getRangeFilterByDateField(dateField, ranges),
      },
      aggs: {
        volume_stats: {
          extended_stats: {
            field: "volume",
          },
        },
        parametersCount_stats: {
          extended_stats: {
            field: "parametersCount",
          },
        },
        fileCount_stats: {
          extended_stats: {
            field: "fileCount",
          },
        },
        definition: {
          terms: {
            field: "definition",
            size: 10000,
          },
          aggs: {
            volume_stats: {
              extended_stats: {
                field: "volume",
              },
            },
            fileCount_stats: {
              extended_stats: {
                field: "fileCount",
              },
            },
            elapsedTime_stats: {
              extended_stats: {
                field: "fileCount",
              },
            },
          },
        },
        investigationId: {
          terms: {
            field: "investigationId",
            size: 10000,
          },
          aggs: {
            volume_stats: {
              extended_stats: {
                field: "volume",
              },
            },
            fileCount_stats: {
              extended_stats: {
                field: "fileCount",
              },
            },
          },
        },
        instrument: {
          terms: {
            field: "instrumentName",
            size: 10000,
          },
          aggs: {
            fileCount_stats: {
              extended_stats: {
                field: "fileCount",
              },
            },
            volume_stats: {
              extended_stats: {
                field: "volume",
              },
            },
            parametersCount_stats: {
              extended_stats: {
                field: "parametersCount",
              },
            },
          },
        },
      },
    };
  }

  return dict;
}

const parseHistogramToCSV = (response, separator, metric) => {
  const defititionsSet = new Set();

  const definitions = response.aggregations.histogram.buckets.map((bucket) => bucket.definition);
  definitions.forEach((definition) => {
    const defs = definition.buckets.map((bucket) => bucket.key);
    defs.forEach((def) => {
      defititionsSet.add(`${def}_${metric}`);
    });
  });

  const sortedBeamlines = Array.from(defititionsSet).sort();
  const lines = [];
  lines.push(`#Date${separator} volume(bytes)${separator}fileCount${separator}${sortedBeamlines.map((definition) => definition + separator)}`.replace(/,/g, ""));
  response.aggregations.histogram.buckets.forEach((bucket) => {
    let stats = "";
    sortedBeamlines.forEach((def) => {
      const definitionName = def.substring(0, def.lastIndexOf("_"));
      const definitionField = def.substring(def.lastIndexOf("_") + 1);
      const statsByDefinition = bucket.definition.buckets.find((b) => b.key === definitionName);
      if (!statsByDefinition) {
        stats = `${stats + separator}0`;
      } else {
        stats = stats + separator + statsByDefinition[definitionField].value;
      }
    });
    lines.push(`${Moment(bucket.key_as_string).format("DD-MM-YYYY")}${separator}${bucket.volume.value}${separator}${bucket.fileCount.value}${stats}`);
  });
  return lines.join("\n");
};
exports.getHistogram = async (req, res) => {
  const { startDate, endDate, instrumentName } = req.query;
  let { separator, format, interval, metric, aggregation } = req.query;
  global.gLogger.debug("getHistogram", { separator, startDate, endDate, instrumentName, format, metric, aggregation, interval });

  separator = separator || "\t";
  format = format || "json";
  interval = interval || "json";
  metric = metric || "volume";
  aggregation = aggregation || "instrumentName";

  const filter = [
    {
      range: {
        startDate: {
          gte: startDate,
          lte: endDate,
        },
      },
    },
  ];
  if (instrumentName) {
    filter.push({ term: { instrumentName } });
  }
  // TODO: Multiple beamlines
  /*
   filter: [
          {
            bool: {
              should: [{ term: { instrumentName: "id16ani" } }, { term: { instrumentName: "cm01" } }],
            },
          },
          {
            range: {
              startDate: {
                gte: startDate,
                lte: endDate,
              },
            },
          },
        ],
  */
  const query = {
    size: 0,
    query: {
      bool: {
        filter,
      },
    },

    aggs: {
      histogram: {
        date_histogram: {
          field: "startDate",
          fixed_interval: interval,
        },
        aggs: {
          volume: {
            sum: {
              field: "volume",
            },
          },
          fileCount: {
            sum: {
              field: "fileCount",
            },
          },
          definition: {
            terms: {
              field: aggregation,
              size: 10000,
            },
            aggs: {
              volume: {
                sum: {
                  field: "volume",
                },
              },

              fileCount: {
                sum: {
                  field: "fileCount",
                },
              },
            },
          },
        },
      },
    },
  };

  try {
    const response = await runESQuey(query);

    global.gLogger.debug("getHistogram run done", { separator, startDate, endDate, instrumentName, format, metric, aggregation, interval });

    if (format.toLowerCase() === "csv") {
      return res.send(parseHistogramToCSV(response, separator, metric));
    }
    if (format.toLowerCase() === "csv_json") {
      return res.send([
        {
          id: JSON.stringify(req.query),
          data: parseHistogramToCSV(response, separator, metric),
        },
      ]);
    }
    return res.send(response);
  } catch (error) {
    global.gLogger.error("getInstrumentStatsByDate", { error: error.message });
    res.status(500).send({ message: error.message });
  }
};

exports.getInstrumentStatsByDate = async (req, res) => {
  const { ranges } = req.body;
  const { instrumentName, openData } = req.query;
  global.gLogger.debug("getInstrumentStatsByDate", { ranges, instrumentName, openData });

  const body = {
    size: 1,
    aggs: getInstrumentByDateField(["startDate"], ranges),
  };
  const filter = [];

  if (instrumentName) {
    filter.push({ term: { instrumentName } });
  }

  if (openData === "true") {
    filter.push({
      exists: {
        field: "investigationDOI",
      },
    });
    filter.push({
      range: {
        releaseDate: {
          lt: "now/d",
        },
      },
    });
  }

  if (instrumentName || openData === "true") {
    body.query = {
      bool: {
        filter,
      },
    };
  }

  try {
    const response = await runESQuey(body);
    res.send(response.aggregations);
  } catch (error) {
    global.gLogger.error("getInstrumentStatsByDate", { error: error.message });
    res.status(500).send({ message: error.message });
  }
};
