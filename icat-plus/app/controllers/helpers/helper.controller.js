const moment = require("moment");

/**
 * returns the string param as a date, undefined if not valid
 * @param {*} date
 * @returns the formatted date as `YYYY-MM-DD`, undefined if the date is not valid
 */
exports.getParamAsDateString = (date) => {
  const format = "YYYY-MM-DD";
  return moment(date, format, true).isValid() ? moment(date, format).format(format) : undefined;
};
/**
 * returns the string in lower case and remove simple quote
 * @param {*} str
 * @returns the string in lower case, where simple quote is removed.
 */
exports.getParamAsEscapedLowerString = (str) => {
  return str ? str.toLowerCase().replace(/[']/gi, "") : "";
};

/**
 * returns the sortOrder 1 or -1 if sortBy is defined, -1 otherwise
 * @param {*} sortBy
 * @param {*} sortOrder
 * @returns 1 or -1
 */
exports.getSortOrder = (sortBy, sortOrder) => {
  return sortBy && sortOrder && (sortOrder === "1" || sortOrder === "-1") ? Number(sortOrder) : -1;
};
