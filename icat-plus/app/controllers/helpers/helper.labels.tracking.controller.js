"use strict";

// const fs = require('fs');
const wkhtmltopdf = require("wkhtmltopdf");
const QRCode = require("qrcode");
const moment = require("moment");

const serverConfig = global.gServerConfig;
const trackingConfig = global.gTrackingConfig;

const STYLES = `
* {
  box-sizing: border-box;
}

html {
  width: 21cm;
  height: 29.7cm;
  margin: 0;
  font-family: Arial;
}

body {
  height: 100%;
  margin: 0;
}

section {
  min-height: 50%;
  padding: 1.5rem 4rem;
}

section,
section * {
  page-break-inside: avoid;
}

section:first-child {
  border-bottom: 1px dashed #aaa;
}

header {
  margin-bottom: 1rem;
  padding: 0.5rem;
  color: #555;
  font-size: 0.875rem;
  text-align: center;
  background-color: #f5f5f5;
}

header > h4 {
  margin: 0 0 0.125rem;
  text-transform: uppercase;
  font-size: 0.75rem;
}

header > p {
  margin: 0;
  font-style: italic;
  font-size: 0.625rem;
}

table {
  border-collapse: collapse;
  width: 100%;
}

td {
  vertical-align: top;
}

h5 {
  margin: 0 0 0.5rem;
  font-size: 0.75rem;
  text-transform: uppercase;
}

img {
  display: block;
  height: auto;
}

.label-table {
  border: 1px solid #555;
}

.label-table > tbody > tr > td {
  border: 1px solid #555;
}

.label-table__from-cell,
.label-table__info-cell {
  padding: 0.5rem;
}

.label-table__from-cell {
  position: relative;
  width: 40%;
}

.label-table[data-return] .label-table__from-cell {
  width: 45%;
}

.label-table__to-cell,
.label-table__important-cell {
  padding: 1rem 0.5rem;
}

.label-table__qr-cell {
  width: 120px;
  padding-left: 10px;
}

.label-table__to-addr-cell {
  text-align: center;
}

.label-table__from-img {
  position: absolute;
  top: 0.5rem;
  right: 0.5rem;
}

.label-table__to-img {
  margin-left: auto;
  margin-right: auto;
}

.label-table__important-cell {
  font-size: 0.875rem;
  text-align: center;
}

.info-table {
  font-size: 0.75rem;
}

.info-table td:first-child {
  padding-right: 0.75rem;
  white-space: nowrap;
  font-style: italic;
}

.info-table td:nth-child(2) {
  width: 100%;
}

.label-table[data-return] .info-table tr:first-child > td:not(:last-child) {
  padding-top: 1.25rem;
}

.address {
  display: inline-block;
  font-style: normal;
  font-size: 0.75rem;
  text-align: left;
}

.address--to {
  font-size: 1.25rem;
  font-weight: bold;
}

.tel {
  font-size: 0.75rem;
}

.tel--first {
  margin-top: 0.375rem;
}

.destroy {
  margin: 2rem 0;
  color: red;
  font-size: 1.25rem;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
}

.dangerous-goods {
  margin: 0 0 0.5rem;
  font-weight: bold;
}

.dangerous-goods > strong {
  color: red;
}

.storage {
  margin: 0;
  font-size: 0.8125rem;
}
`;

const LOGO = `${serverConfig.server.url}/ESRF-Logo-CMYK.png`;

function getAddress(address) {
  if (!address) return "";
  return `
    ${address.name} ${address.surname}<br>
    ${address.companyName ? `${address.companyName}<br>` : ""}
    ${address.address}<br>
    ${address.postalCode} ${address.city.toUpperCase()}<br>
    ${address.region ? `${address.region}<br>` : ""}
    ${address.country ? `${address.country.toUpperCase()}<br>` : ""}
    ${address.phoneNumber ? `<div class="tel tel--first">Phone: ${address.phoneNumber}</div>` : ""}
    ${address.fax ? `<div class="tel">Fax: ${address.fax}</div>` : ""}
  `;
}

function getParcelInformation(investigation, parcel, localContacts, shipment, fromImage, isReturnLabel) {
  if (!investigation) return "";
  const parcels = shipment.parcels.filter((parcel) => parcel.status !== "REMOVED");

  return `<table class="info-table">
  <tbody>
    <tr>
      <td>Parcel name:</td>
      <td>${parcel.name}</td>
      <td class="label-table__qr-cell" rowspan="${isReturnLabel ? 4 : 6}">
        <img src="${fromImage}" width="110">
      </td>
    </tr>
    ${
      isReturnLabel
        ? ""
        : `<tr>
        <td>Shipment content:</td>
        <td>${parcels.length} parcel${parcels.length > 1 ? "s" : ""}</td>
      </tr>`
    }
    <tr>
      <td>Proposal:</td>
      <td>${investigation.name}</td>
    </tr>
    <tr>
      <td>Beamline:</td>
      <td>${investigation.investigationInstruments[0].instrument.name}</td>
    </tr>
    <tr>
      <td>Session start date:</td>
      <td>${moment(new Date(investigation.startDate)).format("DD-MM-YYYY")}</td>
    </tr>
    ${
      isReturnLabel
        ? ""
        : `<tr>
        <td>Local contact${localContacts.length > 1 ? "s" : ""}:</td>
        <td>${localContacts.map((user) => `<div>${user.fullName}</div>`).join("")}</td>
      </tr>`
    }
  </tbody>
</table>`;
}

function getLabel(fromAddress, toAddress, investigation, parcel, localContacts, shipment, fromImage, toImage) {
  const isReturnLabel = !toImage;
  return `<table class="label-table" ${isReturnLabel ? "data-return" : ""}>
  <tbody>
    <tr>
      <td class="label-table__from-cell">
        <h5>From:</h5>
        <address class="address">${getAddress(fromAddress)}</address>
        ${isReturnLabel ? `<img class="label-table__from-img" src="${LOGO}" width="100">` : ""}
      </td>
      <td class="label-table__info-cell">
        ${getParcelInformation(investigation, parcel, localContacts, shipment, fromImage, isReturnLabel)}
      </td>
    </tr>
    <tr>
      <td class="label-table__to-cell" colspan="2">
        ${
          toAddress
            ? `<table>
            <tbody>
              <tr>
                <td>
                  <h5>To:</h5>
                </td>
                <td class="label-table__to-addr-cell">
                  <address class="address address--to">${getAddress(toAddress)}</address>
                </td>
                <td>
                  ${toImage ? `<img class="label-table__to-img" src="${toImage}" width="110">` : ""}
                </td>
              </tr>
            </tbody>
          </table>`
            : `<p class="destroy">Destroy by ESRF requested</p>`
        }
      </td>
    </tr>
    <tr>
      <td class="label-table__important-cell" colspan="2">
        ${parcel.containsDangerousGoods ? `<p class="dangerous-goods">This parcel contains <strong>dangerous</strong> goods</p>` : ""}
        ${
          parcel.storageConditions
            ? `<p class="storage">Storage conditions: ${
                parcel.storageConditions === "Other" ? parcel.comments : `${parcel.storageConditions}${parcel.comments ? ` - ${parcel.comments}` : ""}`
              }</p>`
            : ""
        }
      </td>
    </tr>
  </tbody>
</table>`;
}

function getHTML(shipment, parcel, investigation, shippingAddress, esrfAddress, investigationUsers) {
  const localContacts = (investigationUsers || []).filter((user) => user.role.toUpperCase() === "LOCAL CONTACT");
  const qrContent = `${trackingConfig.qrCode.server}/investigation/${investigation.id}/parcel/${parcel._id}?fromBarCode=true`;

  return QRCode.toDataURL(qrContent, { width: 110, margin: 0 }).then(
    (qrCode) =>
      `<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <style>
      ${STYLES}
    </style>
  </head>
  <body>
    <section>
      <header>
        <h4>Outward-bound address label</h4>
        <p>Affix to the parcel's container</p>
      </header>
      ${getLabel(shippingAddress, esrfAddress, investigation, parcel, localContacts, shipment, qrCode, LOGO)}
    </section>
    <section>
      <header>
        <h4>Return-bound address label</h4>
        <p>Include on the back of the outbound addresss label or inside the parcel's container</p>
      </header>
      ${getLabel(esrfAddress, parcel.returnAddress, investigation, parcel, localContacts, shipment, qrCode)}
    </section>
  </body>
</html>`
  );
}

function getLabels(response, shipment, parcel, investigation, investigationUsers) {
  const esrfAddress = trackingConfig.facilityAddress;
  const shippingAddress = parcel.shippingAddress;

  getHTML(shipment, parcel, investigation, shippingAddress, esrfAddress, investigationUsers).then(
    (html) => {
      // fs.writeFileSync(`${__dirname}/labels-test.html`, html);
      global.gLogger.debug("PDF generation succeeded.");

      //sendPDF(data, req, req.params.sessionId, req.params.investigationId, res);
      const pdf = wkhtmltopdf(html, {
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        disableSmartShrinking: true, // use CSS width/height
        dpi: 300, // https://github.com/wkhtmltopdf/wkhtmltopdf/issues/3256#issuecomment-653282428
      });

      global.gLogger.debug("PDF has been sent");
      const date = moment(new Date(investigation.startDate)).format("DD-MM-YYYY");
      response.header("Content-Disposition", `attachment; filename="${parcel.name}-${parcel.investigationName}-${date}.pdf"`);
      pdf.pipe(response);
    },
    (error) => {
      global.gLogger.debug("PDF has not been sent", { error });
      response.status(500).send("PDF generation failed");
    }
  );
}

exports.getLabels = getLabels;
