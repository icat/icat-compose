const moment = require("moment");
const _ = require("lodash");
const { createModel } = require("mongoose-gridfs");
const nl2br = require("nl2br");
const fs = require("fs");
const Tag = require("../../models/tag.model.js");
const Event = require("../../models/event.model.js");
const CONSTANTS = require("../../constants");
const { ERROR } = require("../../errors.js");
const ObjectId = require("mongoose").Types.ObjectId;
const { asyncGetInvestigationById } = require("../../api/icat");
const cache = require("../../cache/cache.js");
const { findTagByName } = require("./helper.tag.controller.js");
const { EVENT_TYPE_BROADCAST } = require("../../constants");
/**
 * This is a helper that converts a comma separated list of type-category into an array of objects
 * Example:
 * Input: "annotation-comment, notification-error"
 * Output: [{type:"annotation", category:"comment"}, {type:"notification", category:"error"}]
 * @param {*} types
 */
exports.parseTypesToTypeCategory = (types) => {
  return types
    ? types.split(",").map((categoryRaw) => {
        const type = categoryRaw.split("-")[0];
        const category = categoryRaw.split("-")[1];
        return {
          type,
          category,
        };
      })
    : [];
};

/**
 * This is a helper that converts a comma separated list of type-category into an array of objects
 * Example:
 * Input: "annotation-comment, notification-error"
 * Output: [{type:"annotation", category:"comment"}, {type:"notification", category:"error"}]
 * @param {*} types
 */
exports.parseTypesToTypeCategory = (types) => {
  return types
    ? types.split(",").map((categoryRaw) => {
        const type = categoryRaw.split("-")[0];
        const category = categoryRaw.split("-")[1];
        return {
          type,
          category,
        };
      })
    : [];
};

/** This is the way to fill automatically the fullName without need to use virtuals that will not work with the function lean() */
const fillFullName = (e) => {
  while (e) {
    e.fullName = cache.getUserFullNameByName(e.username);
    e = e.previousVersionEvent;
  }
  return e;
};

/*************************************************
 * DATABASE RELATED
 * ***********************************************/

exports.getEventPage = async (_id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder) => {
  global.gLogger.debug("getEventPage", { _id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder });

  /** Otherwise the buildEventFindQuery will do (investigationId && instrumentName == undefined) */
  if (investigationId) {
    instrumentName = null;
  }

  if (!investigationId && !instrumentName) {
    instrumentName = null;
    investigationId = null;
  }

  const totalEventsQuery = await this.buildEventFindQuery(investigationId, filterInvestigation, instrumentName, types, date, null);

  totalEventsQuery.$and.push({
    _id: Number(sortOrder) === 1 ? { $lte: _id } : { $gte: _id },
  });

  limit = parseFloat(limit) || global.gServerConfig.icat.maxQueryLimit;

  function getCountCriteria() {
    if (investigationId) return { investigationId };
    return { instrumentName };
  }
  /** Retrieve the number of events with the search, the result of the search with the skip/limit, and the whole logbook to calculate the indexes */
  const [total, position, events] = await Promise.all([Event.countDocuments(getCountCriteria()), this.count(totalEventsQuery), Event.find({ _id }).populate("tag").lean()]);

  global.gLogger.debug("Response", { total, position, limit, events, index: Math.ceil(position / limit) });

  if (events && events.length === 1) {
    const e = events[0];
    fillFullName(e);
    e.meta = {
      page: {
        total,
        limit,
        index: Math.ceil(position / limit),
      },
    };
    global.gLogger.debug("Metadata found", { meta: e.meta });
    return e.meta;
  }
};

/**
 * This function will return the list of events which content of tag contains the keyword `search`
 * @param {*} search keyword parameter to be searched
 * @param {*} sort Example: {_id : -1}
 * @param {*} investigationId
 * @param {*} limit
 * @returns List of events
 */
exports.searchEvents = async (search, sort, investigationId, limit, skip, filterInvestigation, instrumentName, types, date) => {
  const searchQuery = await this.buildEventFindQuery(investigationId, filterInvestigation, instrumentName, types, date, search);

  global.gLogger.debug("[searchEvents] init", { search, searchQuery: JSON.stringify(searchQuery) });

  limit = parseFloat(limit) || global.gServerConfig.icat.maxQueryLimit;
  skip = parseFloat(skip) || 0;

  /** Retrieve the number of events with the search, the result of the search with the skip/limit, and the whole logbook to calculate the indexes */
  const occurrences = await Event.find(searchQuery).populate("tag").sort(sort).skip(skip).limit(limit).lean();
  occurrences.forEach((e) => {
    fillFullName(e);
  });

  return occurrences;
};

/**
 *
 * @param {*} find
 * @param {*} sort
 * @param {*} skip
 * @param {*} limit
 * @param {*} paginationMetadata True if the pagination metadata (number of page, indeces, etc...) will be included in the results
 * @param {*} findForTotalEvents
 * @returns
 */
exports.findEvents = async (find, sort, skip, limit) => {
  const hrstart = process.hrtime();
  global.gLogger.debug("[findEvents] Find clause is:", { find: JSON.stringify(find) });
  const events = await Event.find(find).populate("tag").sort(sort).skip(skip).limit(limit).lean();
  const executionTimeMs = process.hrtime(hrstart)[1] / 1000000;
  global.gLogger.debug("[findEvents] Database ops finished time to find: ", { time: executionTimeMs, events: events.length, find: JSON.stringify(find) });

  events.forEach((e) => {
    fillFullName(e);
  });

  global.gLogger.debug("[findEvents] Final time to find: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return events;
};

exports.count = async (find) => {
  const hrstart = process.hrtime();
  global.gLogger.debug("[countEvents] Find clause is:", { find: JSON.stringify(find) });
  const count = await Event.countDocuments(find);
  const executionTimeMs = process.hrtime(hrstart)[1] / 1000000;
  global.gLogger.debug("[Count Events] Database ops finished time to find: ", { time: executionTimeMs, count, find: JSON.stringify(find) });
  return count;
};

/**
 * If tag already exists with same name in the scope (investigation, instrument or global) then tag will no be saved otherwise it will be saved in the database
 */
exports.saveTags = async (tags) => {
  global.gLogger.debug("saveTags", { tags: JSON.stringify(tags) });
  const savedTags = [];

  if (tags) {
    for (const tag of tags) {
      const name = tag.name.toLowerCase();
      const { description, investigationId, instrumentName, color } = tag;
      const tags = await findTagByName(investigationId, instrumentName, name);
      if (tags.length > 0) {
        savedTags.push(tags[0]);
      } else {
        const createdTag = await new Tag({ name, description, color, instrumentName, investigationId }).save();
        savedTags.push(createdTag);
      }
    }
  }
  return savedTags;
};

/**
 * Return the instrumentName attached to the investigation
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.getInstrumentNameByInvestigationId = async (sessionId, investigationId) => {
  try {
    const investigation = await asyncGetInvestigationById(sessionId, investigationId);
    return investigation[0].Investigation.investigationInstruments[0].instrument.name.toLowerCase();
  } catch {
    return null;
  }
};

/**
 * Given a mongoose event it will be saved in the database
 */
exports.saveEvent = (event) => {
  try {
    if (event.previousVersionEvent) {
      if (event.type !== event.previousVersionEvent.type) {
        throw ERROR.TYPE_OF_EVENT_NOT_MODIFICABLE;
      }
      if (event.category !== event.previousVersionEvent.category) {
        throw ERROR.CATEGORY_OF_EVENT_NOT_MODIFICABLE;
      }
    }
    event.category = event.category.toLowerCase();
    event.type = event.type.toLowerCase();
    if (!event.creationDate) {
      event.creationDate = moment().toDate();
    }
  } catch (e) {
    return Promise.reject(e);
  }

  return new Promise((resolve, reject) => {
    event
      .save()
      .then((event) => Event.populate(event, { path: "tag" }))
      .then((populatedEvent) => {
        resolve(populatedEvent);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * This method creates a temporal file and will return the file path
 */
function getTemporalPathforBase64File(base64) {
  let temporalFolder = "/tmp";
  if (global.gServerConfig.server.temporalFolder) {
    temporalFolder = global.gServerConfig.server.temporalFolder;
  } else {
    global.gLogger.debug("Temporal Folder is not defined in the configuration file");
  }
  global.gLogger.debug("Using temporal folder", { path: temporalFolder });
  const filePath = `${temporalFolder}/${moment()}_camera.png`;
  global.gLogger.info("Creating temporal file", { filePath });
  /** Writting temporal file */
  fs.writeFile(filePath, base64.replace(/^data:image\/png;base64,/, ""), "base64", (error) => {
    if (error) {
      return global.gLogger.error(error);
    }
  });
  return filePath;
}

/**
 * Returns the URL of a pre-uploaded file
 * @param {string} req.params.sessionId session identifier
 * @param {string} fileId file identifier stored in mongo gridFS
 **/
function getFileDownloadURL(sessionId, fileId) {
  return `${global.gServerConfig.server.url}/resource/${sessionId}/file/download?resourceId=${fileId}`;
}

exports.createBase64Event = (sessionId, investigationId, base64, datasetId, software, machine, username, fullName, investigationName, instrumentName) => {
  const _this = this;
  return new Promise((resolve, reject) => {
    try {
      const Attachment = createModel({
        modelName: "fs",
      });
      /** Creating stream */
      global.gLogger.debug("Creating temporal file with base64", { base64 });

      const temporalFilePath = getTemporalPathforBase64File(base64);
      global.gLogger.debug("Reading temporal file", { path: temporalFilePath });
      const readStream = fs.createReadStream(temporalFilePath);
      global.gLogger.debug("File is read", { path: temporalFilePath });
      Attachment.write({ filename: temporalFilePath, contentType: "application/octet-stream" }, readStream, (error, file) => {
        global.gLogger.debug("File is written by using gridFs", {
          id: file._id,
        });

        global.gLogger.debug("Creating event", {
          investigationId,
          type: CONSTANTS.EVENT_TYPE_ATTACHMENT,
          category: CONSTANTS.EVENT_CATEGORY_FILE,
        });

        const event = new Event({
          investigationId,
          investigationName,
          datasetId,
          type: CONSTANTS.EVENT_TYPE_ATTACHMENT,
          category: CONSTANTS.EVENT_CATEGORY_FILE,
          text: "",
          filename: temporalFilePath,
          username,
          software,
          machine,
          instrumentName,
          file: file._id,
          contentType: "application/octet-stream",
        });

        _this.saveEvent(event).then(
          (data) => {
            /** File is written */
            global.gLogger.debug("File is created in gridFS and removed temporal file", { id: file._id, temporalFilePath });
            fs.unlinkSync(temporalFilePath);

            const event = new Event({
              investigationId,
              investigationName,
              type: CONSTANTS.EVENT_TYPE_ANNOTATION,
              category: CONSTANTS.EVENT_CATEGORY_COMMENT,
              username,
              previousVersionEvent: null,
              content: _this.translateEventContentToHtml([
                {
                  format: "plainText",
                  text: "",
                },
                {
                  format: "html",
                  text: `<p><img src="${getFileDownloadURL(sessionId, data._id)}" /></p>`,
                },
              ]),
              instrumentName,
            });

            _this.saveEvent(event).then(
              (event) => {
                global.gLogger.debug("Attachement has been created successfully");
                event.username = fullName;
                resolve(event);
              },
              (err) => {
                reject(err);
              }
            );
          },
          (err) => {
            reject(err);
          }
        );
      });
    } catch (e) {
      reject(e);
    }
  });
};

/***************************************
 * Non-DB helpers
 ****************************************/
// Build the HTML text
function getTextColorByCategory(category) {
  if (category === CONSTANTS.EVENT_CATEGORY_INFO) {
    return "blue";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_ERROR) {
    return "red";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_COMMANDLINE) {
    return "gray";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_DEBUG) {
    return "purple";
  }
  return "black";
}

function getFontSizeByType(eventType) {
  if (eventType === CONSTANTS.EVENT_TYPE_ANNOTATION) {
    return "12px";
  }
  if (eventType === CONSTANTS.EVENT_TYPE_NOTIFICATION) {
    return "10px";
  }
}

function layoutEventInPDF(eventFormattedContent, date, isANotificationComment, event) {
  if (eventFormattedContent.text) {
    const dateColor = "#4D4D4D";
    const dateComponent = `<span style='color:${dateColor}'> [${date.format("YYYY-MM-DD")}] </span>`;
    const timeComponent = `<span style='color:${dateColor}'> [${date.format("HH:mm:ss")}] </span>`;

    if (isANotificationComment) {
      return (
        `<div style='text-align:left;margin-left:30px;font-size:${getFontSizeByType(CONSTANTS.EVENT_TYPE_ANNOTATION)}; color:${getTextColorByCategory(
          CONSTANTS.EVENT_CATEGORY_COMMENT
        )};'>` +
        `<div style='float:left; padding-right:10px;'>${timeComponent}${dateComponent} <span style='color:${dateColor}'> (latest comment) </span> </div>${eventFormattedContent.text.replace(
          /"/g,
          '"'
        )}</div>`
      );
    }
    return (
      `<div style='text-align:left;font-size:${getFontSizeByType(event.type)}; color:${getTextColorByCategory(event.category)};'>` +
      `<div style='float:left; padding-right:10px;'>${timeComponent}${dateComponent}</div>${eventFormattedContent.text.replace(/"/g, '"')}</div>`
    );
  }
  return "";
}

/**
 * Get HTML formated text for a given event
 * @param {Event} event event to extract the HTML text from
 * @returns {string} html formated text
 */
exports.getHTMLTextByEvent = (event) => {
  let finalHTMLText = "";
  if (event) {
    const eventsToConvert = [];

    // For notifications, get the original version of the event
    if (event.type === CONSTANTS.EVENT_TYPE_NOTIFICATION) {
      if (event.previousVersionEvent) {
        let originalVersion = event;
        try {
          while (originalVersion.previousVersionEvent) {
            originalVersion = originalVersion.previousVersionEvent;
          }
          eventsToConvert.push(originalVersion);
        } catch (e) {
          global.gLogger.error(e);
        }
      }
    }

    eventsToConvert.push(event);

    for (let i = 0; i < eventsToConvert.length; i++) {
      const event = eventsToConvert[i];
      const isANotificationComment = i === 1 ? true : false;

      if (event.content) {
        const HTMLContent = event.content.find((o) => {
          if (o.format) {
            return o.format === CONSTANTS.EVENT_CONTENT_HTML_FORMAT;
          }
          return null;
        });
        if (HTMLContent) {
          finalHTMLText = finalHTMLText + layoutEventInPDF(HTMLContent, moment(event.creationDate), isANotificationComment, event);
        } else {
          const plainTextContent = event.content.find((o) => {
            if (o.format) {
              return o.format === CONSTANTS.EVENT_CONTENT_PLAINTEXT_FORMAT;
            }
            return null;
          });
          if (plainTextContent) {
            plainTextContent.text = `${plainTextContent.text}<br/>`;
            finalHTMLText = finalHTMLText + layoutEventInPDF(plainTextContent, moment(event.creationDate), isANotificationComment, event);
          }
        }
      }
    }
  }
  return finalHTMLText;
};

exports.replaceImgSrcInText = (text, sessionId, serverURI) => {
  if (!text) {
    return text;
  }
  /** The following regular expression:
   * extracts eventual presence of image style or other img options in p1
   * extracts eventId which needs to be updated into p2
   * input:
   * ....<img src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... style=.... />
   * OR ....<img style= ..... src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... />
   * Note: Extraction occurs everywhere the pattern is found in stringifiedEvents. Even when the serverURI is the same as what we expect, the replacement
   * occurs because the whole API has changed and needs to be replaced completely.
   */
  text = text.replace(/<img([^>]*)src="\S+\/investigations\/\d+\/events\/(\S+)\/file?\S+"/g, (match, p1, p2) => {
    if (match && p2) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download?resourceId=${p2}"`;
    }
  });

  // Replace img src using OLD API surounded by single quotes or \"
  text = text.replace(/<img([^>]*)src='?(?:")?\S+\/file\/id\/(\S+)\/investigation\S+'?(?:")?/g, (match, p1, p2) => {
    if (match && p2) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download?resourceId=${p2}"`;
    }
  });

  // Replace img src using last API without investigation Id "
  text = text.replace(/<img([^>]*)src='?(?:")?\S+\/file\/download/g, (match, p1) => {
    if (match && p1) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download`;
    }
  });

  return text;
};

exports.replaceImageSrcInEvent = (event, sessionId, serverURI) => {
  if (event.content) {
    event.content.forEach((content) => {
      if (content.text) {
        content.text = this.replaceImgSrcInText(content.text, sessionId, serverURI);
      }
    });
  }
  if (event.previousVersionEvent) {
    this.replaceImageSrcInEvent(event.previousVersionEvent, sessionId, serverURI);
  }
};

/**
 * Rebuild the image src value with proper API and a valid sessionId for a set of events
 * @param {Array} events all events to be investigated
 * @param {String} sessionId new session identifier to be used in img src
 * @param {String} investigationId investigation identifier
 * @param {String} serverURI http protocol and servername to use example: https://icatplus.esrf.fr
 * @return {Array} updated events
 */
exports.replaceImageSrc = (events, sessionId, serverURI) => {
  events.forEach((event) => {
    this.replaceImageSrcInEvent(event, sessionId, serverURI);
  });
  return events;
};

/**
 * Concatenate all events into a single string as html format
 * @param {Array} events list of events to be concatenated
 * @param {string} investigationId investigation identifier
 * @param {string} serverURI protocol and server name of icat+
 * @returns {string} all events in HTML format
 */
exports.convertEventsToHTML = (events, sessionId, investigationId, serverURI) => {
  let html = "";
  if (events) {
    if (events.length > 0) {
      // replaces image src value with a new serverURI, new sessionId and using a new API
      events = this.replaceImageSrc(events, sessionId, serverURI);
      events.forEach(function (event) {
        const eventHTML = this.getHTMLTextByEvent(event);
        // if it contains formatting characters then use PRE tag
        eventHTML.indexOf("\n") === -1 ? (html = `${html}${eventHTML}`) : (html = `${html}<PRE>${eventHTML}</PRE>`);
      }, this);
    }
  }
  return html;
};

/**
 * Get the serverURI
 * @param {object} req the request object
 * @returns {string} the server URI such as https://icatplus.esrf.fr:8000
 */
exports.getServerURI = (host, isSecure) => {
  return (isSecure ? "https://" : "http://") + host;
};

/**
 * Translate a plain text formated event content to html formated event content when necessary.
 * This operates only on event content which have a plain text formated content  + no html formated content + plain text syntax which needs to be translated such as \n.
 * This concerns notifications or annotations sent from consoles.
 * @param {array} content original event content [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }]
 * @returns {array} updated event content; null when there is no content.
 * returned example: [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }, {format:'html', text: '<p>this is a comment on one line.</p><p>This is another line</p>' },]
 */
exports.translateEventContentToHtml = (content) => {
  /** If content comes as string then try to parse it */
  try {
    content = JSON.parse(content);
  } catch {
    //global.gLogger.warn("Object could not be converted to JSON");
  }
  if (content && content instanceof Array && content.length > 0) {
    if (!content.some((item) => item.format === "html")) {
      // there is no text in html format
      const plainTextEventContent = _.find(content, (item) => {
        return item.format === "plainText";
      });
      if (plainTextEventContent) {
        if (plainTextEventContent.text.split("\n").length > 1)
          content.push({
            format: "html",
            text: nl2br(plainTextEventContent.text, false),
          });
        return content;
      }
    }
    return content;
  }
  return null;
};

/**
 * Converts the date (moment) into id
 * see converter https://steveridout.github.io/
 */
function parseMomentToId(date) {
  return `${Math.floor(date.toDate().getTime() / 1000).toString(16)}0000000000000000`;
}

/** Converts a date (moment) into an ObjectID see https://docs.mongodb.com/manual/reference/method/ObjectId/ */
function getObjectIdFromMoment(date) {
  return ObjectId(parseMomentToId(date));
}

exports.isSearchingBroadcastEvents = (types) => {
  return !types || types.toLowerCase().indexOf(EVENT_TYPE_BROADCAST) !== -1;
};

/**
 * builds the query to find broadcast events between 2 event ids, depending on different categories
 * @param {*} categories
 * @param {*} minEventId
 * @param {*} maxEventId
 * @returns
 */
exports.buildBroadcastQuery = (categories, minEventId, maxEventId) => {
  const $and = [{ investigationId: null }, { instrumentName: null }];
  if (minEventId) {
    $and.push({ _id: { $gte: minEventId } });
  }
  if (maxEventId) {
    $and.push({ _id: { $lte: maxEventId } });
  }
  const categoryFilter = categories
    .filter((category) => category.type === EVENT_TYPE_BROADCAST)
    .map((category) => {
      if (category.type && category.category) {
        return category;
      }
      return { type: category.type };
    });

  if (categoryFilter.length > 0) {
    $and.push({ $and: categoryFilter });
  } else {
    $and.push({ type: EVENT_TYPE_BROADCAST });
  }
  return { $and };
};

/**
 * build the find query to retrieve events depending on investigation or instrument and type of events and date
 * @param {*} investigationId
 * @param {*} filterInvestigation
 * @param {*} instrumentName
 * @param {*} types
 * @param {*} date
 * @param {*} minEventId
 * @param {*} maxEventId
 * @returns
 */
exports.buildEventFindQuery = async (investigationId, filterInvestigation, instrumentName, types, date, search, minEventId, maxEventId) => {
  let $and = [];

  if (investigationId) {
    $and.push({
      $or: [{ investigationId: parseFloat(investigationId) }],
    });
  }

  if (filterInvestigation === "true" && !investigationId) {
    $and = [{ investigationId: null }];
  }

  if (instrumentName && instrumentName !== undefined && instrumentName !== "undefined") {
    $and.push({ $or: [{ instrumentName: instrumentName.toLowerCase() }, { instrumentName: instrumentName.toUpperCase() }] });
  }

  const categories = this.parseTypesToTypeCategory(types);
  if (types) {
    $and.push({
      $or: categories.map((category) => {
        if (category.type && category.category) {
          return category;
        }
        return { type: category.type };
      }),
    });
  }

  if (date) {
    const startDate = moment(date, "YYYY-MM-DD");
    const endDate = moment(startDate).add(1, "days");
    $and.push({ _id: { $gte: getObjectIdFromMoment(startDate), $lt: getObjectIdFromMoment(endDate) } });
  }

  if (search) {
    /** Regular exp to search case insensitive */
    const regExp = new RegExp(`.*${search}.*`, "i");
    /** Find tags that belongs to the investigations */
    const tags = await Tag.find({ $and: [{ $or: [{ investigationId }, { investigationId: null }] }, { name: regExp }] });

    /** Create query to search in the content.text */
    const searchParameters = ["content.text"].map((param) => {
      const obj = {};
      obj[param] = regExp;
      return obj;
    });

    /* Adding the search by tags **/
    if (tags.length > 0) searchParameters.push({ tag: { $in: tags.map((tag) => new ObjectId(tag._id)) } });
    //find = _.cloneDeep(find);
    $and.push({ $or: searchParameters });

    global.gLogger.debug("Search query", { find: JSON.stringify($and) });
  }

  let find = { $and };

  if (this.isSearchingBroadcastEvents(types) && minEventId && maxEventId) {
    const broadcastQuery = this.buildBroadcastQuery(categories, minEventId, maxEventId);
    find = { $or: [{ $and }, broadcastQuery] };
  }

  return find;
};

/**
 * retrieve all distinct dates of events that match a query
 * @param {*} query
 * @returns an array of {_id: "YYYY-MM-DD", event_count: number}
 */
const getDatesByQuery = async (query) => {
  const group = { $group: { _id: { $substr: ["$creationDate", 0, 10] }, event_count: { $sum: 1 } } };
  const match = { $match: query };
  let dates = await Event.aggregate([match, group, { $sort: { _id: -1 } }]);
  dates = dates.filter((d) => d._id !== "");
  return dates;
};

/**
 * retrieve all distinct dates of events that match a query
 * @param {*} investigationId
 * @param {*} filterInvestigation
 * @param {*} instrumentName
 * @param {*} types
 * @param {*} search
 * @param {*} minEventId
 * @param {*} maxEventId
 * @returns an array of {_id: "YYYY-MM-DD", event_count: number}
 */
exports.getDates = async (investigationId, filterInvestigation, instrumentName, types, search, minEventId, maxEventId) => {
  const query = await this.buildEventFindQuery(investigationId, filterInvestigation, instrumentName, types, null, search, minEventId, maxEventId);
  return getDatesByQuery(query);
};

exports.getMinAndMaxIdForBroadcastEvents = async (investigationId, instrumentName) => {
  const $and = [];

  if (investigationId) {
    $and.push({
      $or: [{ investigationId: parseFloat(investigationId) }],
    });
  }

  if (instrumentName) {
    $and.push({ $or: [{ instrumentName: instrumentName.toLowerCase() }, { instrumentName: instrumentName.toUpperCase() }] });
  }

  if ($and.length > 0) {
    const [minEvent, maxEvent] = await Promise.all([Event.find({ $and }).sort({ _id: 1 }).limit(1).lean(), Event.find({ $and }).sort({ _id: -1 }).limit(1).lean()]);
    return [minEvent.length > 0 ? minEvent[0]._id : undefined, maxEvent.length > 0 ? maxEvent[0]._id : undefined];
  }

  return [0, Number.MAX_SAFE_INTEGER];
};
