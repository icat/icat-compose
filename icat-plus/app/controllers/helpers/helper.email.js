const nodemailer = require("nodemailer");
const config = global.gTrackingConfig;
const { getInvestigationUserByInvestigationId } = require("../../api/icat.js");
const STATUS = require("../../models/status.js");
const moment = require("moment");

function getSubject(investigation, parcel) {
  let subject = `[ESRF Tracking] ${investigation.name} ${investigation.investigationInstruments[0].instrument.name} ${moment(investigation.startDate).format("DD-MM-YYYY")} ${
    parcel.name
  }: `;

  switch (parcel.status) {
    case STATUS.READY:
      subject = `${subject}Safety Validation Pending`;
      break;
    case STATUS.APPROVED:
      subject = `${subject}Parcel Approved by Safety`;
      break;
    case STATUS.REFUSED:
      subject = `${subject}Parcel Refused by Safety`;
      break;
    case STATUS.INPUT:
      subject = `${subject}Information Requested by Safety`;
      break;
    case STATUS.SENT:
      subject = `${subject}Parcel Sent`;
      break;
    case STATUS.STORES:
      subject = `${subject}Parcel Arrived at the Stores`;
      break;
    case STATUS.BEAMLINE:
      subject = `${subject}Parcel Arrived on the Beamline`;
      break;
    case STATUS.BACK_STORES:
      subject = `${subject}Parcel Returned to the Stores`;
      break;
    case STATUS.BACK_USER:
      subject = `${subject}Parcel Sent Back to User`;
      break;
    case STATUS.DESTROYED:
      subject = `${subject}Parcel Destroyed`;
      break;
    default:
      subject = `${subject}Parcel changed status to ${parcel.status}`;
      break;
  }
  return subject;
}

function getText(usersWithRoles, investigation, parcel) {
  const { status, returnAddress } = parcel;
  const localContacts = (usersWithRoles || []).filter((user) => user.role.toUpperCase() === "LOCAL CONTACT");

  const greetings = "<p>Dear User,</p>";
  const parcelPresentation = `<b>${parcel.name}</b> for proposal <b>${investigation.name}</b> and linked to the session starting on <b>${moment(investigation.startDate).format(
    "DD-MM-YYYY"
  )}</b> on <b>${investigation.investigationInstruments[0].instrument.name}</b>`;

  const parcelFields = `<h3>Details of the parcel</h3>
  <p style="margin-left: 40px;"><b>Name</b>: <a href='https://data.esrf.fr/investigation/${investigation.id}/parcel/${parcel._id}'>${parcel.name}</a><br/>
  <b>Proposal</b>: <a href='https://data.esrf.fr/investigation/${investigation.id}/shipping'>${investigation.name}</a><br/>
  <b>Beamline</b>: ${investigation.investigationInstruments[0].instrument.name}<br/>
  <b>Start date</b>: ${moment(investigation.startDate).format("DD-MM-YYYY")}<br/>
  <b>Local contact${localContacts.length > 1 ? "s" : ""}</b>:${localContacts
    .map((user) => ` ${user.fullName} (<a href="mailto:${user.email}">${user.email}</a>)`)
    .toString()}<br/>`;

  const itemsContent = `<ul style="margin-left: 40px;">${parcel.items.reduce((acc, item) => `${acc}<li>${item.name} (${item.type})</li>`, "")}</ul>`;

  const dangerousGoodsDisclaimer = parcel.containsDangerousGoods
    ? '<p style="margin-left: 40px;">This parcel contains <span style="color: red;"><b>dangerous</b></span> goods.</p>'
    : "";

  const parcelDetails = `${parcelFields + itemsContent + dangerousGoodsDisclaimer}<p>Best regards,<br> ESRF Parcel Tracking</p>`;

  if (status === STATUS.READY) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>is awaiting validation from the ESRF Safety group</b>.</p>
      <p>If the Safety group has any questions concerning your shipment, they will contact you via ICAT or e-mail.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.APPROVED) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>has been APPROVED by the ESRF Safety group</b>.</p>
      <p><b>Before</b> sending your parcel to the ESRF, <b>you must</b>:</p>
      <ol>
      <li>Download and print your shipping label</li>
      <li>Affix the "to ESRF" part to your parcel</li>
      <li>Insert the return address label in your parcel</li>
      <li>Complete the paperwork for your courier company - remember to request the <b>courier company return label</b> and put it in your parcel otherwise it will not be returned to you.</li>
      <li><b>Update the status</b> of your parcel to <b>Sent</b>, once you have sent it.
      </ol>
      <p>Please note that any parcel <b><u>without</u></b> a label will <b><u>not be accepted</u></b> by the ESRF Stores.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.REFUSED) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>has been REFUSED by the ESRF Safety group</b>.</p>
      <p>Please contact the Safety group (<a href='mailto:expsaf@esrf.fr'>expsaf@esrf.fr</a>) and your local contact to discuss the required modifications to the parcel content.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.INPUT) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>requires MORE INFORMATION before it can be approved for sending</b>.</p>
      <p>Please:</p>
      <ol>
      <li><a href='https://data.esrf.fr/investigation/${investigation.id}/parcel/${parcel._id}'>Read the Safety request on ICAT</a> for this parcel</li>
      <li>Modify your parcel content accordingly</li>
      <li>In case of questions, contact <a href='mailto:expsaf@esrf.fr'>expsaf@esrf.fr</a></li>
      </ol>
      ${parcelDetails}`;
  }

  if (status === STATUS.SENT) {
    return `${greetings} <p>You have sent the parcel ${parcelPresentation}.</p>
      <p>You will be notified when the parcel arrives at the ESRF Stores.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.STORES) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b> has ARRIVED at the ESRF Stores</b>.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.BEAMLINE) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b> has ARRIVED on the BEAMLINE</b>.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.BACK_STORES) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>has arrived BACK at the ESRF Stores, and is ready to be shipped</b>.</p>
      ${parcelDetails}`;
  }

  if (status === STATUS.BACK_USER) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>has been SENT to the RETURN ADDRESS you have defined</b>:</p>
      <p style="margin-left: 40px;">
      ${returnAddress.name} ${returnAddress.surname}<br>
      ${returnAddress.companyName ? `${returnAddress.companyName}<br>` : ""}
      ${returnAddress.address}<br>
      ${returnAddress.postalCode} ${returnAddress.city.toUpperCase()}<br>
      ${returnAddress.region ? `${returnAddress.region}<br>` : ""}
      ${returnAddress.country ? `${returnAddress.country.toUpperCase()}<br>` : ""}
      </p>
      ${parcelDetails}`;
  }

  if (status === STATUS.DESTROYED) {
    return `${greetings}<p>The parcel ${parcelPresentation} <b>has been DESTROYED</b>.</p>
    ${parcelDetails}`;
  }

  return `${greetings}<p>The parcel ${parcelPresentation} changed status to ${parcel.status}.</p>
    ${parcelDetails}`;
}

/**
 * Promised based method that returns an array with the list of emails which user.role is in roles
 */
async function getUsersByRoles(sessionId, parcel) {
  const status = config.notifications.statuses[parcel.status];
  if (!status.roles) {
    return [];
  }

  const users = await getInvestigationUserByInvestigationId(sessionId, parcel.investigationId);
  return users.filter((user) => user.email !== null && status.roles.toString().toUpperCase().includes(user.role.toUpperCase()));
}

/**
 * Get the emails based in the field containsDangerousGoods

function getRecipientsIfDangerousGoods(status, parcel) {
  return parcel.containsDangerousGoods && status.containsDangerousGoods ? status.containsDangerousGoods : [];
}
**/

/**
 * Get the emails by mailing investigation
 */
function getRecipientsIfMailingInvestigation(status, investigation) {
  config.notifications.mailingProposals.forEach((proposal) => {
    if (investigation.name.toUpperCase().startsWith(proposal.toUpperCase())) {
      return status.isMailingInvestigation;
    }
  });
  return [];
}

/**
 * It gets a comma separated list of emails: broadcast, roles, config and mailingInvestigation
 */
function getRecipients(usersWithRoles, parcel, investigation) {
  global.gLogger.debug("getRecipients", { parcelId: parcel._id, status: parcel.status, roles: config.notifications.statuses[parcel.status].roles });

  const configStatus = config.notifications.statuses[parcel.status];

  const recipients = [
    config.notifications.broadcastEmail,
    ...usersWithRoles.map((user) => user.email),
    ...configStatus.emails,
    ...getRecipientsIfMailingInvestigation(configStatus, investigation),
  ];

  // Remove duplicates
  return [...new Set(recipients)];
}

exports.sendEmail = async (sessionId, investigation, parcel) => {
  const usersWithRoles = await getUsersByRoles(sessionId, parcel);
  const message = {
    from: config.notifications.fromAddress,
    replyTo: config.notifications.fromAddress,
    to: getRecipients(usersWithRoles, parcel, investigation),
    subject: getSubject(investigation, parcel),
    html: getText(usersWithRoles, investigation, parcel),
  };
  const transport = nodemailer.createTransport({
    host: config.notifications.smtp.host,
    port: config.notifications.smtp.port,
  });

  global.gLogger.debug("Email is about to be send", message);

  if (!config.notifications.enabled) {
    global.gLogger.info("Notifications are disabled and no email will be sent");
    return Promise.resolve("Notifications are disabled and no email will be sent");
  }

  return new Promise((resolve, reject) => {
    transport.sendMail(message, (err, info) => {
      if (err) {
        global.gLogger.error("Error", err);
        return reject(err);
      }

      global.gLogger.info("info", info);
      return resolve(info);
    });
  });
};
