const Parcel = require("../../models/parcel.model.js");
const Address = require("../../models/address.model.js");
const Shipment = require("../../models/shipment.model.js");
const STATUS = require("../../models/status.js");
const icat = require("../../api/icat.js");
const { getUserFullNameByName } = require("../../cache/cache.js");

/**
 *
 * @param {*} shipment Shipment Mongoose object. Example: let shipment = new Shipment(req.body);
 * @param {*} investigationId
 * @param {*} sessionId
 */
async function createShipment(shipment, investigationId, sessionId) {
  const response = await icat.getInvestigationById(sessionId, investigationId);
  shipment.investigationName = response.data[0].Investigation.name;
  shipment.investigationId = response.data[0].Investigation.id;
  const newShipment = await new Shipment(shipment).save();
  shipment = await this.findShipmentById(newShipment._id);
  return shipment;
}

async function createAddress(address, investigationId, sessionId, username) {
  return icat.getInvestigationById(sessionId, investigationId).then(async (investigations) => {
    if (investigations.data.length > 0) {
      address.investigationName = investigations.data[0].Investigation.name;
      address.createdBy = username;
      address.investigationId = investigationId;
      return new Address(address).save();
    }
  });
}

function sanitizeShipmentAddresses(shipment) {
  const sanitizedShipment = shipment;

  if (shipment.defaultReturnAddress && shipment.defaultReturnAddress.status === "REMOVED") {
    sanitizedShipment.set("defaultReturnAddress", undefined);
  }

  if (shipment.defaultShippingAddress && shipment.defaultShippingAddress.status === "REMOVED") {
    sanitizedShipment.set("defaultShippingAddress", undefined);
  }

  return sanitizedShipment;
}

async function findShipmentById(id) {
  const shipment = await Shipment.findById(id).populate("defaultReturnAddress").populate("defaultShippingAddress").exec();
  return sanitizeShipmentAddresses(shipment);
}

async function findShipmentsBy(query) {
  const shipments = await Shipment.find(query).populate("defaultReturnAddress").populate("defaultShippingAddress").exec();
  return shipments.map(sanitizeShipmentAddresses);
}

function findNotRemovedShipmentsBy(query) {
  return findShipmentsBy({ ...query, status: { $ne: "REMOVED" } });
}

function sendError(code, message, e, res) {
  if (e) {
    global.gLogger.error(e);
    global.gLogger.error(message, {
      error: e.message,
    });
  }
  res.status(code).send({
    message: e.message || message,
  });
}

async function findNotRemovedParcels(query) {
  // `status` is a virtual field and cannot be queried directly (https://mongoosejs.com/docs/tutorials/virtuals.html#limitations)
  const parcels = await findParcels(query);
  return parcels.filter((parcel) => parcel.status !== STATUS.REMOVED);
}

async function findParcels(query) {
  try {
    const data = await Parcel.find(query).populate({ path: "items", match: { status: { $ne: "REMOVED" } } });
    return data.map((parcel) => parcel.toObject({ virtuals: true }));
  } catch (e) {
    global.gLogger.error(e);
    throw e;
  }
}

module.exports = {
  createShipment,
  createAddress,
  findShipmentById,
  findShipmentsBy,
  findNotRemovedShipmentsBy,
  sendError,
  findNotRemovedParcels,
  findParcels,
};
