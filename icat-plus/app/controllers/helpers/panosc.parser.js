const {
  asyncGetSession,
  getInvestigationUserByInvestigationId,
  getReleasedInvestigationsBySessionId,
  getDatasetsByInvestigationId,
  getSamplesByInvestigationId,
  getDatafilesByDatasetId,
  getInvestigationById,
} = require("../../api/icat.js");
const { transform } = require("node-json-transform");

/** Mapping between Dataset@ICAT to DatasetSample@PANOSC  */
const DatasetSampleMap = {
  item: { id: "id", datasetId: "id", sampleId: "sampleId" },
};

/** Mapping between Dataset@ICAT to Dataset@PANOSC  */
const DatasetMap = {
  item: {
    pid: "id",
    id: "id",
    title: "name",
    creationDate: "startDate",
    documentId: "investigation.doi",
    instrumentId: "investigation.investigationInstruments[0].instrument.id",
  },
  each(item) {
    item.isPublic = true;
    return item;
  },
};

/** Mapping between investigationUsers@ICAT and Member@PANOSC */
const MemberMap = {
  item: { id: "name", role: "role", personId: "name", documentId: "investigationId" },
};

/** Mapping between files@ICAT and File@PANOSC */
const FileMap = {
  item: { id: "Datafile.id", name: "Datafile.name", datasetId: "Datafile.datasetId", path: "Datafile.location", size: "Datafile.fileSize" },
};

/** Mapping between investigationUsers@ICAT and Person@PANOSC */
const PersonMap = {
  item: { id: "name" },
  each(item) {
    item.fullName = "name";
    return item;
  },
};

/** Mapping between sample@ICAT and sample@PANOSC */
const SampleMap = {
  item: {
    pid: "Sample.id",
    id: "Sample.id", //
    name: "Sample.name",
  },
  each(item) {
    return item;
  },
};

/** Mapping between investigation@ICAT and document@PANOSC */
const DocumentMap = {
  item: {
    doi: "Investigation.doi",
    id: "Investigation.id",
    title: "Investigation.title",
    summary: "Investigation.summary",
    startDate: "Investigation.startDate",
    endDate: "Investigation.endDate",
    releaseDate: "Investigation.releaseDate",
  },
  each(item) {
    if (item.doi) {
      item.doi.trim().length > 0 ? (item.pid = item.doi) : (item.pid = item.id);
    }
    item.isPublic = true;
    item.type = "proposal";
    item.license = "CC-BY-4.0";
    return item;
  },
};

/** MApping between Instrument@ICAT and Instrument@PANOSC */
const InstrumentMap = {
  item: { pid: "id", id: "id", name: "name" },
  each(item) {
    item.facility = "ESRF";
    return item;
  },
};

const getDatasetsByInvestigations = async (sessionId, investigations) => {
  let datasetList = [];
  for (let i = 0; i < investigations.length; i++) {
    const datasets = await getDatasetsByInvestigationId(sessionId, investigations[i].Investigation.id);
    datasetList = datasetList.concat(datasets);
  }
  return datasetList;
};

const getUsersByInvestigations = async (sessionId, investigations) => {
  let arr = [];
  for (let i = 0; i < investigations.length; i++) {
    const items = await getInvestigationUserByInvestigationId(sessionId, investigations[i].Investigation.id);
    arr = arr.concat(items);
  }
  return arr;
};

const getSamplesByInvestigations = async (sessionId, investigations) => {
  let sampleList = [];
  for (let i = 0; i < investigations.length; i++) {
    const samples = await getSamplesByInvestigationId(sessionId, investigations[i].Investigation.id);
    sampleList = sampleList.concat(samples);
  }
  return sampleList;
};

/** Returns the array mapped into the mapping passed as parameter and as a dictionary which key is PID*/
const parse = (arr, map, key) => {
  if (!key) key = "pid";
  return Object.assign({}, ...transform(arr, map).map((o) => ({ [o[key]]: o })));
};

const parseInstruments = (investigations) => {
  const instruments = investigations.map((investigation) => {
    if (investigation.Investigation.investigationInstruments) {
      if (investigation.Investigation.investigationInstruments[0]) {
        return investigation.Investigation.investigationInstruments[0].instrument;
      }
    }
    return null;
  });
  const instrumentIds = [
    ...new Set(
      instruments.map((instrument) => {
        return instrument.id;
      })
    ),
  ];
  const uniqueInstruments = instrumentIds.map((id) => instruments.find((i) => i.id === id));
  return parse(uniqueInstruments, InstrumentMap);
};

const getFiles = async (sessionId, datasets) => {
  let fileList = [];
  for (let i = 0; i < datasets.length; i++) {
    const files = await getDatafilesByDatasetId(sessionId, datasets[i].id);
    fileList = fileList.concat(
      files.map((file) => {
        file.Datafile.datasetId = datasets[i].id;
        return file;
      })
    );
  }
  return fileList;
};

const parseParameters = (investigations, datasets) => {
  const investigationParameters = investigations.map((i) => {
    const map = {
      item: { id: "id", value: "stringValue", name: "type.name", units: "type.units" },
      each(item) {
        i.Investigation.doi.trim().length === 0 ? (item.documentId = i.Investigation.id) : (item.documentId = i.Investigation.doi);
        return item;
      },
    };
    return Object.assign({}, ...transform(i.Investigation.parameters, map).map((p) => ({ [p.id]: p })));
  });

  const datasetParameters = datasets.map((dataset) => {
    const map = {
      item: { id: "id", value: "value", name: "name", units: "units" },
      each(item) {
        item.datasetId = dataset.id;
        return item;
      },
    };
    return Object.assign({}, ...transform(dataset.parameters, map).map((p) => ({ [p.id]: p })));
  });

  // CONVERTING ARRAYS OF DICTIONARIES INTO A SINGLE DICTIONARY
  const params = {};
  datasetParameters.concat(investigationParameters).map((dp) => {
    for (const key in dp) {
      params[key] = dp[key];
    }
    return dp;
  });

  return params;
};

exports.getDBByInvestigationId = async (investigationId) => {
  const data = await asyncGetSession(global.gServerConfig.icat.anonymous);
  const { sessionId } = data;

  const investigations = await getInvestigationById(sessionId, investigationId);
  return parseInvestigationsIntoPanoscModel(sessionId, investigations.data);
};

async function parseInvestigationsIntoPanoscModel(sessionId, investigations) {
  const datasets = await getDatasetsByInvestigations(sessionId, investigations);
  const samples = await getSamplesByInvestigations(sessionId, investigations);
  const datasetSample = parse(datasets, DatasetSampleMap, "id");
  const investigationUsers = await getUsersByInvestigations(sessionId, investigations);
  const files = await getFiles(sessionId, datasets);

  return {
    ids: {
      AccessToken: 1,
      ACL: 1,
      Affiliation: 2,
      Dataset: 7,
      DatasetSample: 5,
      DatasetTechnique: 3,
      Document: 2,
      File: 2,
      Instrument: 16,
      Member: 3,
      Parameter: 7,
      Person: 3,
      Role: 1,
      RoleMapping: 1,
      Sample: 2,
      Technique: 3,
      User: 1,
    },
    models: {
      AccessToken: {},
      ACL: {},
      File: parse(files, FileMap, "id"),
      Parameter: parseParameters(investigations, datasets),
      Document: parse(investigations, DocumentMap),
      Member: parse(investigationUsers, MemberMap, "id"),
      Person: parse(investigationUsers, PersonMap, "id"),
      Instrument: parseInstruments(investigations),
      DatasetSample: datasetSample,
      Sample: parse(samples, SampleMap),
      Dataset: parse(datasets, DatasetMap),
      DatasetTechnique: {},
      Technique: {},
      Affiliation: {},
    },
  };
}

/** Converts all ICAT public investigations into a PANOSC-compiant format */
exports.getDB = async () => {
  const data = await asyncGetSession(global.gServerConfig.icat.authorizations.notifier.user);
  const { sessionId } = data;

  const investigations = await getReleasedInvestigationsBySessionId(sessionId);
  return parseInvestigationsIntoPanoscModel(sessionId, investigations);
};
