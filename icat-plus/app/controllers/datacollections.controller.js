const icat = require("../api/icat.js");
const { sendError } = require("../errors.js");

exports.getDataCollectionsBySessionId = async (req, res) => {
  global.gLogger.debug("getDataCollectionsBySessionId", { sessionId: req.params.sessionId });
  try {
    const data = await icat.getDataCollectionsBySessionId(req.params.sessionId);
    return res.send(data);
  } catch (e) {
    sendError(500, "Failed to getDataCollectionsBySessionId", e, res);
  }
};
