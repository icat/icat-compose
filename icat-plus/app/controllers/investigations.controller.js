const {
  asyncGetInvestigationById,
  getInvestigationUserByInvestigationId,
  getEmbargoedInvestigationsBySessionId,
  countEmbargoedInvestigations,
  getReleasedInvestigationsBySessionId,
  countReleasedInvestigations,
  getInvestigationsAsParticipantBySessionIdWithNoParse,
  getInvestigationByInstrumentScientist,
  getInvestigations,
  asyncGetSession,
  asyncGetInvestigationByName,
  addInvestigationUser,
  deleteInvestigationUser,
  getInstrumentsBySessionId,
  getInstrumentsForUser,
  getInstrumentsForInstrumentScientist,
  getInvestigationsByInstrument,
  countInvestigationsAsParticipant,
  countInvestigationsByInstrumentScientist,
  countInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId,
  getInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId,
  countInvestigationsByInstrument,
  countAllInvestigations,
} = require("../api/icat.js");
const { ROLE_COLLABORATOR } = require("../constants.js");
const MARGIN_HOURS = 48;
const { ERROR, sendError } = require("../errors.js");
const investigationHelper = require("../helpers/investigation.js");
const investigationParser = require("../api/parsers/investigationparser.js");
const instrumentParser = require("../api/parsers/instrumentparser.js");
const { getUserByName } = require("../cache/cache.js");
const moment = require("moment");
const { getParamAsDateString, getParamAsEscapedLowerString, getSortOrder } = require("./helpers/helper.controller.js");

function normalize(req, res) {
  const { investigationName } = req.params;
  if (investigationName) {
    return res.send(investigationHelper.normalize(investigationName));
  }
  return res.status(500).send("No investigation name given");
}

const getInvestigationByNameAndInstrumentBySessionId = async (sessionId, investigationName, instrumentName, createdAt) => {
  investigationName = investigationHelper.normalize(investigationName);
  let investigations = await asyncGetInvestigationByName(sessionId, investigationName);
  global.gLogger.debug("Found investigations matching the name", { createdAt, investigationName, count: investigations.length });

  /** Filter investigation of type proposals */
  investigations = investigations.filter((investigation) => {
    return investigation.visitId !== "PROPOSAL";
  });
  global.gLogger.debug("Filtered investigations that are not proposals", { investigationName, count: investigations.length });

  /** if a single experiment then we retrieve it */
  if (investigations.length === 1) {
    global.gLogger.debug("Found a single session that is not a proposal");
    return investigations[0];
  }

  /** Find investigation by time slot and instrument */
  let investigation = investigations.find((investigation) => {
    const { visitId, instrument, startDate, endDate } = investigation;
    if (visitId !== "PROPOSAL" && instrument && instrument.name && instrumentName && instrument.name.toUpperCase() === instrumentName.toUpperCase()) {
      if (startDate && endDate) {
        return moment(createdAt).isBetween(moment(startDate).subtract(MARGIN_HOURS, "hours"), moment(endDate).add(MARGIN_HOURS, "hours"));
      }
    }
    global.gLogger.warn("Investigation does not macth", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return false;
  });

  if (investigation) {
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.info("Investigation found", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return investigation;
  }

  global.gLogger.warn("Search for a test proposal", { investigationName, instrumentName, createdAt });
  /** Find a test investigation, a test investigation has the name of the instrument in the visitId */
  investigation = investigations.find((investigation) => {
    if (investigation.visitId.toUpperCase() === instrumentName.toUpperCase()) {
      return true;
    }
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.warn("Investigation does not macth", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return false;
  });

  if (investigation) {
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.info("Investigation found", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return investigation;
  }

  global.gLogger.error(`No proposal for ${investigationName} and ${instrumentName} could be allocated`);
  return null;
};

/**
 *  Returns a promise with a single investigation or reject if more than one have been found
 *
 * @param {investigationName} name of the investigation (proposal)
 * @param {instrumentName} name of the instrument (beamline)
 * @param {createdAt} time when the event was created
 */
const getInvestigationByNameAndInstrument = async (investigationName, instrumentName, createdAt) => {
  const data = await asyncGetSession(global.gServerConfig.icat.authorizations.notifier.user);
  const investigation = await getInvestigationByNameAndInstrumentBySessionId(data.sessionId, investigationName, instrumentName, createdAt);
  return investigation;
};

async function getUnpackedInvestigationById(req, res) {
  global.gLogger.debug("getUnpackedInvestigationById", req.params);
  try {
    const investigationArray = await asyncGetInvestigationById(req.params.sessionId, req.params.investigationId);
    const investigations = investigationParser.lightParse(investigationArray);
    investigations.length > 0 ? res.send(investigations[0]) : sendError(ERROR.NO_INVESTIGATION_FOUND.code, ERROR.NO_INVESTIGATION_FOUND.message, ERROR.NO_INVESTIGATION_FOUND, res);
  } catch (error) {
    global.gLogger.error(error);
    throw error;
  }
}

async function getInvestigationById(req, res) {
  try {
    const investigations = await asyncGetInvestigationById(req.params.sessionId, req.params.investigationId);
    return res.send(investigationParser.lightParse(investigations));
  } catch (error) {
    sendError(500, "Failed to getInvestigationById", error, res);
  }
}

async function getInvestigationUserByInvestigation(req, res) {
  try {
    const investigatinUsers = await getInvestigationUserByInvestigationId(req.params.sessionId, req.params.investigationId);
    res.send(investigatinUsers);
  } catch (error) {
    sendError(ERROR.FAILED_TO_RETRIEVE_INVESTIGATIONUSERS.code, ERROR.FAILED_TO_RETRIEVE_INVESTIGATIONUSERS.message, error, res);
  }
}

async function getInvestigationsBy(sessionId, filter, instrumentName, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("getInvestigations. ", { username, filter, instrumentName, startDate, endDate, limit, skip, search, sortBy, sortOrder });
  let investigations = [];

  if (filter) {
    switch (filter.toUpperCase()) {
      case "EMBARGOED":
        investigations = await findEmbargoedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder);
        break;
      case "RELEASED":
        investigations = await findReleasedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder);
        break;
      case "PARTICIPANT":
        investigations = await findInvestigationsAsParticipantBySessionIdWithNoParse(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder);
        break;
      case "INSTRUMENTSCIENTIST":
        investigations = await findInvestigationByInstrumentScientist(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder);
        break;
      case "INDUSTRY":
        investigations = await findInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(
          sessionId,
          username,
          startDate,
          endDate,
          limit,
          skip,
          search,
          sortBy,
          sortOrder
        );
        break;
      default:
        throw new Error(`Parameter filter is invalid. filter=${filter}. Valid values:[EMBARGOED|RELEASED|PARTICIPANT|INSTRUMENTSCIENTIST|INDUSTRY]`);
    }
    if (instrumentName) {
      investigations = investigations.filter((investigation) => {
        const { investigationInstruments: instruments } = investigation.Investigation;
        const instrument = instruments && instruments.length > 0 && instruments[0].instrument;
        return instrument && instrument.name.toUpperCase() === instrumentName;
      });
    }
  } else {
    if (instrumentName) {
      investigations = await findInvestigationsByInstrument(sessionId, instrumentName, startDate, endDate, limit, skip, search, sortBy, sortOrder);
    } else {
      /** Get all investigations */
      investigations = await findAllInvestigations(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder);
    }
  }

  global.gLogger.debug("getInvestigations done. ", {
    count: investigations.length,
    filter: `${filter}`,
    instrument: `${instrumentName}`,
    startDate: `${startDate}`,
    endDate: `${endDate}`,
    limit: `${limit}`,
    skip: `${skip}`,
    search: `${search}`,
  });
  return investigations;
}

async function findAllInvestigations(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findAllInvestigations. ", { startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countAllInvestigations(sessionId, startDate, endDate, search),
    getInvestigations(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findAllInvestigations: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findInvestigationsByInstrument(sessionId, instrumentName, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findInvestigationsByInstrument. ", { instrumentName, startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countInvestigationsByInstrument(sessionId, instrumentName, startDate, endDate, search),
    getInvestigationsByInstrument(sessionId, instrumentName, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findInvestigationsByInstrument: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findEmbargoedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findEmbargoedInvestigationsBySessionId. ", { startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countEmbargoedInvestigations(sessionId, startDate, endDate, search),
    getEmbargoedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findEmbargoedInvestigationsBySessionId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findReleasedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findReleasedInvestigationsBySessionId. ", { startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countReleasedInvestigations(sessionId, startDate, endDate, search),
    getReleasedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findReleasedInvestigationsBySessionId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId. ", { username, startDate, endDate, limit, skip, search, sortBy, sortOrder });

  const investigationConfig = global.gInvestigationConfig;
  const investigationNames = investigationConfig.industrialProposals;
  const visitIdName = investigationConfig.participantVisitId;
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(sessionId, username, investigationNames, visitIdName, startDate, endDate, search),
    getInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(
      sessionId,
      username,
      investigationNames,
      visitIdName,
      startDate,
      endDate,
      limit,
      skip,
      search,
      sortBy,
      sortOrder
    ),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findInvestigationsAsParticipantBySessionIdWithNoParse(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findInvestigationsAsParticipantBySessionIdWithNoParse. ", { username, startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const visitIdName = global.gInvestigationConfig.participantVisitId;
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countInvestigationsAsParticipant(sessionId, username, visitIdName, startDate, endDate, search),
    getInvestigationsAsParticipantBySessionIdWithNoParse(sessionId, username, visitIdName, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findInvestigationsAsParticipantBySessionIdWithNoParse: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

async function findInvestigationByInstrumentScientist(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findInvestigationByInstrumentScientist. ", { username, startDate, endDate, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const [total, investigations] = await Promise.all([
    countInvestigationsByInstrumentScientist(sessionId, username, startDate, endDate, search),
    getInvestigationByInstrumentScientist(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  ]);
  setMetaDataToInvestigations(investigations, total, limit, skip);

  global.gLogger.debug("Final time to findInvestigationByInstrumentScientist: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return investigations;
}

function setMetaDataToInvestigations(investigations, total, limit, skip) {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;
  investigations.forEach((i) => {
    i.Investigation.meta = {
      page: {
        total,
        totalPages,
        currentPage,
      },
    };
  });
}

const getAllocatedInvestigation = async (req, res) => {
  const { investigationName, instrument } = req.params;
  const { time } = req.query;

  let date = new Date();
  if (time) {
    try {
      date = new Date(time);
    } catch (e) {
      return sendError(400, `Can not convert string ${time} to date`, e, res);
    }
  }

  global.gLogger.debug("getAllocatedInvestigation", { instrument, investigationName, date });
  try {
    const investigation = await getInvestigationByNameAndInstrument(investigationName, instrument, date);
    if (!investigation) throw ERROR.NO_INVESTIGATION_FOUND;
    res.send(investigation);
  } catch (e) {
    sendError(500, "Failed to getAllocatedInvestigation", e, res);
  }
};

async function getInvestigationsBySessionId(req, res) {
  const { filter, startDate, endDate, limit, skip, search, sortBy, sortOrder, investigationName } = req.query;
  let { instrumentName } = req.query;
  let { ids } = req.query;
  const { sessionId } = req.params;
  const formattedStartDate = getParamAsDateString(startDate);
  const formattedEndDate = getParamAsDateString(endDate);
  const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
  const searchSkip = parseInt(skip) || 0;
  instrumentName = instrumentName ? instrumentName.toUpperCase() : "";
  const searchText = getParamAsEscapedLowerString(search);

  global.gLogger.debug("getInvestigationsBySessionId. ", {
    filter,
    instrumentName,
    ids,
    sessionId,
    formattedStartDate,
    formattedEndDate,
    limit,
    skip,
    searchText,
    sortBy,
    sortOrder,
    investigationName,
  });

  try {
    /** if instrument and investigationName  then we retrieve the ids */
    if (instrumentName && investigationName) {
      const date = startDate ? new Date(startDate) : new Date();
      const investigation = await getInvestigationByNameAndInstrumentBySessionId(sessionId, investigationName, instrumentName, date);
      if (investigation) {
        ids = ids ? ids : [];
        ids.push(investigation.id);
      }
    }
    const formattedSortBy = getInvestigationParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);
    const investigations = ids
      ? await asyncGetInvestigationById(sessionId, ids)
      : await getInvestigationsBy(
          sessionId,
          filter,
          instrumentName,
          req.session.name,
          formattedStartDate,
          formattedEndDate,
          searchLimit,
          searchSkip,
          searchText,
          formattedSortBy,
          formattedSortOrder
        );

    if (investigationName) {
      const investigationMatching = investigationParser.lightParse(investigations).filter((i) => i.name === investigationHelper.normalize(investigationName));
      if (investigationMatching.length === 0) {
        return res.status(404).send("Investigation not found");
      }
      return res.send(investigationMatching);
    }
    return res.send(investigationParser.lightParse(investigations));
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, "Failed to getInvestigationsBySessionId", error, res);
  }
}

function getInvestigationParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "NAME":
        sortFilter = "investigation.name";
        break;
      case "INSTRUMENT":
        sortFilter = "instrument.name";
        break;
      case "STARTDATE":
        sortFilter = "investigation.startDate";
        break;
      case "TITLE":
        sortFilter = "investigation.title";
        break;
      case "SUMMARY":
        sortFilter = "investigation.summary";
        break;
      case "RELEASEDATE":
        sortFilter = "investigation.releaseDate";
        break;
      case "DOI":
        sortFilter = "investigation.doi";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[NAME|INSTRUMENT|STARTDATE|TITLE|SUMMARY|RELEASEDATE|DOI]`);
    }
  }
  return sortFilter;
}

/**
 * Returns all instruments linked to a user
 *    - if administrator: all instruments
 *    - if instrument scientists: all instruments linked to the instrumentScientist and all instruments linked to his/her investigations
 *    - all instruments linked to the user's investigations
 * @param {*} req
 * @param {*} res
 * @returns array of instruments
 */
async function getInstruments(req, res) {
  const { sessionId } = req.params;
  const { filter } = req.query;
  global.gLogger.debug("getInstruments. ", { sessionId, filter });
  try {
    let instruments = [];
    if (req.session.isAdministrator) {
      instruments = await getInstrumentsBySessionId(sessionId);
    } else {
      if (filter) {
        instruments = await getFilteredInstruments(sessionId, req.session.name, filter);
      } else {
        const [instrumentsUser, instrumentsScientists] = await Promise.all([
          getInstrumentsForUser(sessionId, req.session.name),
          getInstrumentsForInstrumentScientist(sessionId, req.session.name),
        ]);
        instruments = [...new Set([...instrumentsUser, ...instrumentsScientists])];
      }
    }
    return res.send(instrumentParser.parse(instruments));
  } catch (error) {
    return sendError(500, "Failed to getInstruments", error, res);
  }
}

async function getFilteredInstruments(sessionId, username, filter) {
  let instruments = [];
  switch (filter.toUpperCase()) {
    case "PARTICIPANT":
      instruments = await getInstrumentsForUser(sessionId, username);
      return instruments;
    case "INSTRUMENTSCIENTIST":
      instruments = await getInstrumentsForInstrumentScientist(sessionId, username);
      return instruments;
    default:
      throw new Error(`Parameter filter is invalid. filter=${filter}. Valid values:[PARTICIPANT|INSTRUMENTSCIENTIST]`);
  }
}

/**
 * Adds an user to an investigation. Only users with role Collaborator can be added
 * @param {*} req
 * @param {*} res
 * @returns
 */
const addInvestigationUserToInvestigation = async (req, res) => {
  try {
    const { sessionId, investigationId } = req.params;
    const { name } = req.body;
    global.gLogger.info("addInvestigationUserToInvestigation", { sessionId, investigationId, name });

    const investigations = await asyncGetInvestigationById(sessionId, investigationId);

    if (investigations.length !== 1) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
    }

    if (!name) {
      return res.status(ERROR.NO_USERS_BAD_PARAMS.code).send(ERROR.NO_USERS_BAD_PARAMS.message);
    }

    const user = getUserByName(name);
    if (!user) {
      return res.status(ERROR.NO_USERS_BAD_PARAMS.code).send(ERROR.NO_USERS_BAD_PARAMS.message);
    }

    global.gLogger.debug("User found", { user: JSON.stringify(user) });
    const investigationUser = {
      role: ROLE_COLLABORATOR,
      user: { id: user.id },
      investigation: { id: investigations[0].Investigation.id },
    };

    try {
      const investigationUsers = await addInvestigationUser(sessionId, {
        InvestigationUser: investigationUser,
      });
      investigationUser.id = investigationUsers[0];
      return res.send(investigationUser);
    } catch (error) {
      return sendError(500, error.response.data.message, error, res);
    }
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, error, error, res);
  }
};

/**
 * Removes an user from an investigation. Only users with role Collaborator can be remove
 * @param {*} req
 * @param {*} res
 * @returns
 */
const deleteInvestigationUserToInvestigation = async (req, res) => {
  try {
    const { sessionId, investigationId } = req.params;
    const { name } = req.body;
    global.gLogger.info("deleteInvestigationUserToInvestigation", { sessionId, investigationId, name });

    let investigationUsers = await getInvestigationUserByInvestigationId(sessionId, [investigationId]);
    global.gLogger.debug(`Found ${investigationUsers.length} within the investigation`);
    /** Note that the id of the User becomes the name for ICAT */
    const collaboratorToBeRemoved = investigationUsers.filter((i) => {
      return i.role.toUpperCase() === ROLE_COLLABORATOR.toUpperCase() && i.name === name;
    });

    global.gLogger.debug(`Found ${collaboratorToBeRemoved.length} investigation users to be removed`);
    if (collaboratorToBeRemoved.length > 0) {
      await deleteInvestigationUser(sessionId, [{ InvestigationUser: { id: collaboratorToBeRemoved[0].id } }]);
      investigationUsers = await getInvestigationUserByInvestigationId(sessionId, [investigationId]);
      return res.status(200).send(investigationUsers);
    }
    return res.status(400).send();
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, "Failed to deleteInvestigationUserToInvestigation", error, res);
  }
};

module.exports = {
  addInvestigationUserToInvestigation,
  deleteInvestigationUserToInvestigation,
  getAllocatedInvestigation,
  normalize,
  getInvestigationByNameAndInstrument,
  getUnpackedInvestigationById,
  getInvestigationById,
  getInvestigationUserByInvestigation,
  getInvestigationsBySessionId,
  getInstruments,
};
