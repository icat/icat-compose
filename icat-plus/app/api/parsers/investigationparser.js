// Next line does not work as getParameterTypeDictionary becomes undefined
//const { getParameterTypeDictionary } = require("../../cache/cache.js");
const cache = require("../../cache/cache.js");

exports.parse = (investigations) => {
  try {
    const parameterTypes = cache.getParameterTypeDictionary();
    const result = [];
    for (const i in investigations) {
      const investigation = investigations[i];
      const parameters = [];
      if (investigation) {
        if (investigation.Investigation) {
          if (investigation.Investigation.parameters) {
            for (let j = 0; j < investigation.Investigation.parameters.length; j++) {
              const parameterTypeId = investigation.Investigation.parameters[j].type.id;
              const parameterTypeName = parameterTypes[parameterTypeId];
              parameters.push({
                name: parameterTypeName,
                value: investigation.Investigation.parameters[j].stringValue,
              });
            }
          }
        }
      }
      investigation.Investigation.parameters = parameters;
      result.push(investigation);
    }
    return result;
  } catch (e) {
    global.gLogger.error(e);
  }
};

exports.lightParse = (investigations) => {
  try {
    const parameterTypes = cache.getParameterTypeDictionary();
    return investigations.map((investigation) => {
      if (investigation.Investigation) {
        if (investigation.Investigation.parameters) {
          const parameters = {};
          investigation.Investigation.parameters.forEach((parameter) => {
            if (parameter.type) {
              parameters[parameterTypes[parameter.type.id]] = parameter.stringValue;
            }
          });

          let instrument = {};
          if (investigation.Investigation.investigationInstruments) {
            if (investigation.Investigation.investigationInstruments.length > 0) {
              if (investigation.Investigation.investigationInstruments[0].instrument) {
                instrument = {
                  name: investigation.Investigation.investigationInstruments[0].instrument.name.toUpperCase(),
                  id: investigation.Investigation.investigationInstruments[0].instrument.id,
                };
              }
            }
          }

          const { name, startDate, endDate, id, doi, title, visitId, releaseDate, summary, meta } = investigation.Investigation;
          return { name, startDate, endDate, id, doi, title, visitId, releaseDate, summary, parameters, instrument, meta };
        }
      }
      return null;
    });
  } catch (e) {
    global.gLogger.error(e);
  }
};
