const cache = require("../../cache/cache.js");

exports.parse = (datacollections) => {
  const parameterTypes = cache.getParameterTypeDictionary();
  const result = [];
  for (let i = 0; i < datacollections.length; i++) {
    if (datacollections[i]) {
      if (datacollections[i].DataCollection) {
        const parameters = [];
        if (datacollections[i].DataCollection.parameters) {
          for (let j = 0; j < datacollections[i].DataCollection.parameters.length; j++) {
            const parameterTypeId = datacollections[i].DataCollection.parameters[j].type.id;
            parameters.push({
              name: parameterTypes[parameterTypeId],
              value: datacollections[i].DataCollection.parameters[j].stringValue,
            });
          }
        }
        datacollections[i].DataCollection.parameters = parameters;
        result.push(datacollections[i].DataCollection);
      }
    }
  }
  return result;
};
