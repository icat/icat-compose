exports.parse = (instruments) => {
  return instruments.map((element) => {
    return {
      name: element.Instrument.name,
      id: element.Instrument.id,
    };
  });
};
