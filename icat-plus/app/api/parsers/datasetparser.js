function getParameters(dataset) {
  const params = [];
  for (let i = 0; i < dataset.Dataset.parameters.length; i++) {
    const parameter = dataset.Dataset.parameters[i];
    params.push({
      name: parameter.type.name,
      value: parameter.stringValue,
      id: parameter.id,
      units: parameter.type.units,
    });
  }
  return params;
}

exports.parse = (datasets) => {
  const parsed = [];

  for (let i = 0; i < datasets.length; i++) {
    const element = datasets[i];

    parsed.push({
      id: element.Dataset.id,
      name: element.Dataset.name,
      startDate: element.Dataset.startDate,
      endDate: element.Dataset.endDate,
      location: element.Dataset.location,
      investigation: element.Dataset.investigation,
      type: element.Dataset.type ? element.Dataset.type.name : null,
      sampleName: element.Dataset.sample ? element.Dataset.sample.name : null,
      sampleId: element.Dataset.sample ? element.Dataset.sample.id : null,
      parameters: element.Dataset.parameters ? getParameters(element) : [],
    });
  }
  return parsed;
};
