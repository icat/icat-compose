exports.parse = (users) => {
  return users.map((element) => {
    return {
      name: element.User.name,
      fullName: element.User.fullName,
      email: element.User.email,
      id: element.User.id,
      orcidId: element.User.orcidId,
    };
  });
};

exports.fullParse = (users) => {
  return users.map((element) => {
    return element.User;
  });
};
