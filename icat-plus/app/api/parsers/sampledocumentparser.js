const { getParameterTypeDictionary } = require("../../cache/cache.js");

exports.parse = (samples) => {
  const result = [];
  try {
    const parameterTypes = getParameterTypeDictionary();
    for (const i in samples) {
      const sample = samples[i];
      const parameters = [];
      if (sample && sample.Sample && sample.Sample.parameters) {
        for (let j = 0; j < sample.Sample.parameters.length; j++) {
          const parameterTypeId = sample.Sample.parameters[j].type.id;
          const parameterTypeName = parameterTypes[parameterTypeId];
          parameters.push({
            name: parameterTypeName,
            value: sample.Sample.parameters[j].stringValue,
          });
        }
      }
      sample.Sample.parameters = parameters;
      result.push(sample.Sample);
    }
  } catch (e) {
    global.gLogger.error(e);
  }
  return result;
};
