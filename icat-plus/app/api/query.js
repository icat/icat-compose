"use strict";

const ICAT = global.gServerConfig.icat;
const IDS = global.gServerConfig.ids;

const GET_SESSION = "sessionId=:sessionId";

const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

const SERVER = `&server=${ICAT.server}`;

const GET_INVESTIGATIONS_ID = "SELECT distinct(Inv.id) from Investigation Inv";

const GET_DATASETS_IDS_BY_INVESTIGATION_ID = "SELECT dataset.id FROM Dataset dataset JOIN dataset.investigation investigation where investigation.id =:investigationId";

const GET_DATASETS_DATASET_IDS =
  "SELECT dataset FROM Dataset dataset JOIN dataset.investigation investigation where dataset.id IN (:datasetIDs) INCLUDE dataset.type type, dataset.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument, dataset.parameters parameters, parameters.type, dataset.sample";

const GET_INVESTIGATION_BY_DATASETS_ID =
  "SELECT Inv from Investigation Inv JOIN Inv.datasets ds where ds.id in (:datasetIds) include Inv.investigationInstruments ii, ii.instrument";

const GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID =
  "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id, user.email, investigationUser.id, user.orcidId FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where inv.id in (:investigationIds) ";

const GET_INVESTIGATIONUSER_BY_USERNAME =
  "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id, user.email, investigationUser.id, user.orcidId FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where user.name = ':username'";

const GET_INVESTIGATION_BY_INSTRUMENTSCIENTIST_AND_INVESTIGATION_ID =
  "select distinct investigation from Investigation investigation, investigation.investigationInstruments as investigationInstrument, investigationInstrument.instrument as instrument, instrument.instrumentScientists as instrumentScientists, instrumentScientists.user as user where user.name = ':username' and investigation.id= :investigationId INCLUDE investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ";

const GET_INSTRUMENTS_FOR_USER =
  "SELECT DISTINCT instrument FROM Instrument instrument JOIN instrument.investigationInstruments investigationInstruments JOIN investigationInstruments.investigation  investigation JOIN investigation.investigationUsers  investigationUsers JOIN investigationUsers.user  user WHERE user.name = ':username' ";

const GET_INSTRUMENTS_FOR_INSTRUMENT_SCIENTIST =
  "SELECT DISTINCT instrument FROM Instrument instrument JOIN instrument.instrumentScientists instrumentScientists JOIN instrumentScientists.user user WHERE user.name = ':username' ";

const EMBARGOED_FILTER = "investigation.releaseDate > CURRENT_TIMESTAMP ";

const RELEASED_FILTER = "( investigation.releaseDate < CURRENT_TIMESTAMP and investigation.doi <> null )";

const PARTICIPANT_FILTER = "investigationUser.name = ':username' ";

const USERNAME_FILTER = "user.name = ':username'";

const INVESTIGATION_ID_FILTER = "investigation.id =:investigationId ";

const DC_DOI_FILTER = "dc.doi = ':doi' ";

const IDS_GET_DATA = "/ids/getData?sessionId=:sessionId&";

exports.getPropertiesQuery = () => {
  return `${ICAT.server}/icat/properties`;
};

exports.getSessionQuery = (sessionId) => {
  return GET_SESSION.replace(":sessionId", sessionId);
};

exports.getSession = () => {
  return `${ICAT.server}/icat/session`;
};

exports.getSessionInformationURL = (sessionId) => {
  return `${this.getSession()}/${sessionId}`;
};

exports.logout = (sessionId) => {
  return `${this.getSession()}/${sessionId}`;
};

exports.getInvestigationUserByInvestigationIdURL = (sessionId, investigationIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID.replace(":investigationIds", investigationIds)}${SERVER}`;
};

exports.getInvestigationUserByUserNameURL = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONUSER_BY_USERNAME.replace(":username", username)}${SERVER}`;
};

exports.getInvestigations = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONS_ID}${SERVER}`;
};

exports.getInvestigationsByDatasetListIdsURL = (sessionId, datasetIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATION_BY_DATASETS_ID.replace(":datasetIds", datasetIds)}${SERVER}`;
};

exports.getInvestigationByInstrumentScientistAndInvestigationId = (sessionId, username, investigationId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=${GET_INVESTIGATION_BY_INSTRUMENTSCIENTIST_AND_INVESTIGATION_ID.replace(":username", username).replace(":investigationId", investigationId)}${SERVER}`
  );
};

exports.getDeleteQuery = (sessionId, entities) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&entities=${entities}`;
};

exports.getCreateQuery = (sessionId) => {
  return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId);
};

exports.getCreateDataCollectionQuery = (sessionId) => {
  return this.getCreateQuery(sessionId);
};

exports.getParameterTypeList = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=Select p from ParameterType p${SERVER}`;
};

exports.getInvestigationTypes = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=Select p from InvestigationType p${SERVER}`;
};

exports.getInvestigationByName = (sessionId, investigationName) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT Inv from Investigation Inv where UPPER(inv.name) = UPPER('${investigationName}') INCLUDE Inv.investigationInstruments i, i.instrument, Inv.parameters${SERVER}`
  );
};

exports.getInvestigationById = (sessionId, investigationId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT Inv from Investigation Inv where inv.id in (${investigationId}) INCLUDE Inv.investigationInstruments i, i.instrument, Inv.parameters p, p.type${SERVER}`
  );
};

exports.getInvestigationByInstrumentScientist = (sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([USERNAME_FILTER.replace(":username", username), filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument JOIN instrument.instrumentScientists as instrumentScientists JOIN instrumentScientists.user as user ${filters} order by ${orderStatement} include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countInvestigationsByInstrumentScientist = (sessionId, username, startDate, endDate, search) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([USERNAME_FILTER.replace(":username", username), filterDates, searchCond]);
  return encodeURI(
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
      `query=select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument JOIN instrument.instrumentScientists as instrumentScientists JOIN instrumentScientists.user as user  ${filters} ${SERVER} `
  );
};

exports.getReleasedInvestigationsBySessionId = (sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([RELEASED_FILTER, filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument ${filters} order by ${orderStatement} include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countReleasedInvestigationsBySessionId = (sessionId, startDate, endDate, search) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([RELEASED_FILTER, filterDates, searchCond]);
  return encodeURI(
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
      `query=select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument ${filters} ${SERVER} `
  );
};

exports.getEmbargoedInvestigationsBySessionId = (sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([EMBARGOED_FILTER, filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument ${filters} order by ${orderStatement} include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countEmbargoedInvestigationsBySessionId = (sessionId, startDate, endDate, search) => {
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([EMBARGOED_FILTER, filterDates, searchCond]);
  return encodeURI(
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
      `query=select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument ${filters} ${SERVER} `
  );
};

/**
 * builds the WHERE clause form an arry of filters, separated by a AND clause
 * Example of returned clause:
 * for filters = ["instrument.name = 'ID00'", "(LOWER(investigation.name) LIKE '%test%' OR LOWER(investigation.title) LIKE '%test%')", "investigation.startDate <= '2020-01-01'", ""]
 * "WHERE instrument.name = 'ID00' AND (LOWER(investigation.name) LIKE '%test%' OR LOWER(investigation.title) LIKE '%test%') AND investigation.startDate <= '2020-01-01'"
 * @param {*} filters  array of filters
 * @returns the clause
 */
exports.buildFiltersConditions = (filters) => {
  const allDefinedFilters = filters.filter((f) => f !== "");
  return allDefinedFilters.length > 0 ? `WHERE ${allDefinedFilters.join("AND ")}` : "";
};

/**
 * Returns all investigations for a given sessionId. If startDate and endDate are defined, it filters investigations based on their experiment startDate.
 * @param {string} sessionId the sessionId - must be defined
 * @param {string} instrument filter to retrieve investigations on s specific instrument name - could be undefined
 * @param {string} startDate filter to retrieve investigations which starts or ends after this date - format YYYY-DD-MM - could be undefined
 * @param {string} endDate filter to retrieve investigations which starts before this date. - format YYYY-DD-MM - could be undefined
 * @param {string} limit limit number of investigations to retrieve
 * @param {string} skip index of investigations to retrieve
 * @param {string} search search
 * @param {string} sortBy sortBy
 * @param {string} sortOrder sortOrder
 *
 */
exports.getInvestigationsByDateRangesOrInstrument = (sessionId, instrument, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const filterInstrument = instrument ? ` instrument.name = '${instrument}' ` : "";
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([filterInstrument, filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation LEFT OUTER JOIN investigation.investigationInstruments investigationInstruments LEFT OUTER JOIN investigationInstruments.instrument instrument ${filters} order by ${orderStatement}  include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countInvestigationsByInstrument = (sessionId, instrument, startDate, endDate, search) => {
  const filterInstrument = instrument ? ` instrument.name = '${instrument}' ` : "";
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([filterInstrument, filterDates, searchCond]);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select count (distinct investigation) from Investigation investigation LEFT OUTER JOIN investigation.investigationInstruments investigationInstruments LEFT OUTER JOIN investigationInstruments.instrument instrument ${filters} ${SERVER} `;
  return encodeURI(url);
};

exports.getInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId = (
  sessionId,
  username,
  investigationName,
  visitIdName,
  startDate,
  endDate,
  limit,
  skip,
  search,
  sortBy,
  sortOrder
) => {
  const investigationFilter = this.buildInvestigationFilter(investigationName, visitIdName);
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([PARTICIPANT_FILTER.replace(":username", username), investigationFilter, filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument  JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser ${filters} order by ${orderStatement} include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId = (sessionId, username, investigationName, visitIdName, startDate, endDate, search) => {
  const investigationFilter = this.buildInvestigationFilter(investigationName, visitIdName);
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([PARTICIPANT_FILTER.replace(":username", username), investigationFilter, filterDates, searchCond]);
  return encodeURI(
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
      `query=select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument  JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser ${filters} ${SERVER} `
  );
};

exports.getInvestigationsBySessionIdAndUsername = (sessionId, username, visitIdName, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const clauseVisitId = visitIdName && visitIdName.length > 0 ? ` investigation.visitId <> '${visitIdName}' ` : "";
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([PARTICIPANT_FILTER.replace(":username", username), clauseVisitId, filterDates, searchCond]);
  const orderStatement = this.buildSortInvestigations(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser ${filters} order by ${orderStatement} include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countInvestigationsAsParticipant = (sessionId, username, visitIdName, startDate, endDate, search) => {
  const clauseVisitId = visitIdName && visitIdName.length > 0 ? ` investigation.visitId <> '${visitIdName}' ` : "";
  const filterDates = this.getInvestigationFilterDate(startDate, endDate);
  const searchCond = this.buildSearchInvestigations(search);
  const filters = this.buildFiltersConditions([PARTICIPANT_FILTER.replace(":username", username), clauseVisitId, filterDates, searchCond]);
  return encodeURI(
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
      `query=select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser  ${filters} ${SERVER} `
  );
};

exports.getEntityDatasetsByInvestigationId = (sessionId, investigationId, limit, skip, search, sortBy, sortOrder, datasetType) => {
  const searchCond = this.buildSearchDataset(search);
  const filters = !datasetType
    ? this.buildFiltersConditions([INVESTIGATION_ID_FILTER.replace(":investigationId", investigationId), searchCond])
    : this.buildFiltersConditions([INVESTIGATION_ID_FILTER.replace(":investigationId", investigationId), `datasetType.name='${datasetType}'`, searchCond]);
  const orderStatement = this.buildSortDatasets(sortBy, sortOrder);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct dataset from Dataset dataset JOIN dataset.investigation investigation JOIN dataset.type datasetType JOIN dataset.sample sample JOIN dataset.parameters parameters JOIN parameters.type parameterType ${filters} order by ${orderStatement} include dataset.type type, dataset.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, dataset.parameters parameters, parameters.type, dataset.sample ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countEntityDatasetsByInvestigationId = (sessionId, investigationId, search, datasetType) => {
  const searchCond = this.buildSearchDataset(search);
  const filters = !datasetType
    ? this.buildFiltersConditions([INVESTIGATION_ID_FILTER.replace(":investigationId", investigationId), searchCond])
    : this.buildFiltersConditions([INVESTIGATION_ID_FILTER.replace(":investigationId", investigationId), `datasetType.name='${datasetType}'`, searchCond]);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select count (distinct dataset) from Dataset dataset JOIN dataset.type datasetType JOIN dataset.investigation investigation JOIN dataset.sample sample JOIN dataset.parameters parameters JOIN parameters.type parameterType ${filters} ${SERVER} `;

  return encodeURI(url);
};

exports.getSamplesByInvestigationId = (sessionId, investigationId) => {
  return `${
    ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)
  }&${"query=SELECT sample FROM Sample sample JOIN sample.investigation investigation where investigation.id =:investigationId order by sample.id DESC INCLUDE  sample.parameters parameters, parameters.type".replace(
    ":investigationId",
    investigationId
  )}${SERVER}`;
};

exports.getSampleById = (sessionId, sampleId) => {
  return `${
    ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)
  }&${"query=SELECT sample FROM Sample sample JOIN sample.investigation investigation where sample.id =:sampleId order by sample.id DESC INCLUDE  sample.parameters parameters, parameters.type".replace(
    ":sampleId",
    sampleId
  )}${SERVER}`;
};

/**
 * @Deprecated it takes too long, we presume because of rules
 */
exports.getDatasetsByDatasetIds = (sessionId, datasetIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_DATASETS_DATASET_IDS.replace(":datasetIDs", datasetIds)}${SERVER}`;
};

exports.getDatasetIdsByInvestigationId = (sessionId, investigationId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_DATASETS_IDS_BY_INVESTIGATION_ID.replace(":investigationId", investigationId)}${SERVER}`;
};

exports.getDataColletionByDatasetId = (sessionId, datasetId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT dc from DataCollection dc JOIN dc.dataCollectionDatasets dcds  JOIN dcds.dataset ds where dc.doi is not null and ds.id=${datasetId} include dc.parameters p, p.type${SERVER}`
  );
};

exports.getDataCollectionsBySessionId = (sessionId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT distinct(dc) from DataCollection dc JOIN dc.dataCollectionDatasets dcds JOIN dcds.dataset ds where dc.doi is not null include dc.parameters p, p.type, dc.dataCollectionDatasets dcds, dcds.dataset${SERVER}`
  );
};

exports.getInvestigationIdByDOI = (sessionId, doi) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT inv.id from Investigation inv where inv.doi='${doi}'${SERVER}`;
};

exports.getInvestigationByDOI = (sessionId, doi) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT inv from Investigation inv where inv.doi='${doi}' INCLUDE inv.investigationUsers invUser, invUser.user, inv.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, inv.type${SERVER}`
  );
};

exports.getDatasetsByDOI = (sessionId, doi, limit, skip, search, sortBy, sortOrder) => {
  const searchCond = this.buildSearchDataset(search);
  const filters = this.buildFiltersConditions([DC_DOI_FILTER.replace(":doi", doi), searchCond]);
  const orderStatement = this.buildSortDatasets(sortBy, sortOrder);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct dataset from Dataset dataset JOIN dataset.dataCollectionDatasets dcds JOIN dcds.dataCollection dc JOIN dataset.investigation investigation JOIN dataset.sample sample JOIN dataset.parameters parameters JOIN parameters.type parameterType ${filters} order by ${orderStatement} include dataset.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, dataset.parameters parameters, parameters.type, dataset.sample ${this.buildlimit(
      limit,
      skip
    )} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.countDataCollectionIdListByDOI = (sessionId, doi, search) => {
  const searchCond = this.buildSearchDataset(search);
  const filters = this.buildFiltersConditions([DC_DOI_FILTER.replace(":doi", doi), searchCond]);

  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select count(distinct dataset) from Dataset dataset JOIN dataset.dataCollectionDatasets dcds JOIN dcds.dataCollection dc JOIN dataset.investigation investigation JOIN dataset.sample sample JOIN dataset.parameters parameters JOIN parameters.type parameterType ${filters}  ${SERVER}`;
  return encodeURI(url);
};

/**
 * Get the url to retrieve datasets from a dataset list
 * @param {string} datasetIds a comma separated string containing different datasetId
 * @param {string} sessionId
 */
exports.getDatasetsByIdList = (sessionId, datasetIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_DATASETS_DATASET_IDS.replace(":datasetIDs", datasetIds)}${SERVER}`;
};

/**
 * Get the url to retrieve datasets by a range date
 * @param {string} startDate Format is YYYY-MM-DD
 * @param {string} endDate Format is YYYY-MM-DD
 * @param {string} sessionId
 */
exports.getDatasetsByDateRange = (sessionId, startDate, endDate) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT dataset FROM Dataset dataset JOIN dataset.investigation investigation where dataset.startDate >= '${startDate}' and dataset.startDate<='${endDate}' order by dataset.id DESC INCLUDE dataset.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument, dataset.parameters parameters, parameters.type, dataset.sample${SERVER}`
  );
};

exports.getUsersByGroupName = (sessionId, groupNames) => {
  const query = "SELECT user from User user JOIN user.userGroups userGroup JOIN userGroup.grouping grouping where  grouping.name in (':groupNames')";
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query.replace(":groupNames", groupNames)}${SERVER}`;
};

exports.getUsers = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT user from User user${SERVER}`;
};

exports.getInstruments = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT instrument from Instrument instrument ORDER BY instrument.name ${SERVER}`;
};

exports.getInstrumentScientistsByUserName = (sessionId, userName) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT instrumentScientist from InstrumentScientist instrumentScientist where instrumentScientist.user.name='${userName}' INCLUDE instrumentScientist.instrument instrument, instrumentScientist.user user${SERVER}`
  );
};

exports.getInstrumentScientists = (sessionId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT instrumentScientist from InstrumentScientist instrumentScientist INCLUDE instrumentScientist.instrument instrument, instrumentScientist.user user${SERVER}`
  );
};

exports.getDatafilesByDatasetId = (sessionId, datasetIds, limit, skip, search) => {
  const searchCond = this.buildSearchDatafile(search);
  const filters = this.buildFiltersConditions([`dataset.id IN (${datasetIds})`, searchCond]);
  // include datafile.dataset might cause performance issues. it is currently needed for the UI
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct datafile from Datafile datafile JOIN datafile.dataset dataset  ${filters} include datafile.dataset dataset ${this.buildlimit(limit, skip)} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.getDatafilesByIds = (sessionId, datafileIds) => {
  const query = "select df from Datafile df where df.id IN (:datafileIds)";
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query.replace(":datafileIds", datafileIds)}${SERVER}`;
};

exports.getStatusByDatasetIds = (sessionId, datasetIds) => {
  const query = IDS.server + "/ids/getStatus?sessionId=:sessionId&datasetIds=:datasetIds".replace(":sessionId", sessionId).replace(":datasetIds", datasetIds);
  return query;
};

exports.getStatusByIvestigationIds = (sessionId, investigationIds) => {
  const query =
    IDS.server + "/ids/getStatus?sessionId=:sessionId&investigationIds=:investigationIds".replace(":sessionId", sessionId).replace(":investigationIds", investigationIds);
  return query;
};

/**
 * Get JPQL query which is used to get instrument scientists for a given instrumentName
 * @param {string} sessionId session identifier
 * @param {string } instrumentName instrument name
 * @returns { string} JPQL query
 */
exports.getInstrumentScientistsByInstrumentName = (sessionId, instrumentName) => {
  const query = "SELECT u FROM InstrumentScientist insts JOIN insts.user u WHERE insts.instrument.name=':InstrumentName'".replace(":InstrumentName", instrumentName);
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

exports.getInstrumentsForUser = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INSTRUMENTS_FOR_USER.replace(":username", username)}${SERVER}`;
};

exports.getInstrumentsForInstrumentScientist = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INSTRUMENTS_FOR_INSTRUMENT_SCIENTIST.replace(":username", username)}${SERVER}`;
};

exports.getInvestigationFilterDate = (startDate, endDate) => {
  const filterStartDate = startDate ? ` (investigation.startDate >= '${startDate}' or investigation.endDate >= '${startDate}') ` : "";
  const filterEndDate = endDate ? `investigation.startDate <= '${endDate}'` : "";
  const allDefinedFilters = [filterStartDate, filterEndDate].filter((f) => f !== "");
  const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
  return filters;
};

exports.buildlimit = (limit, skip) => {
  return limit !== undefined && skip !== undefined ? ` LIMIT ${skip}, ${limit} ` : "";
};

/**
 * builds a sql clause to search a word in different fields. The search is based on lower case.
 * The fields are investigation title, summary, doi, name and instrument's name.
 * Example of returned clause:
 * "(LOWER(investigation.title) LIKE '%search%' OR  LOWER(investigation.summary) LIKE '%search%' OR  LOWER(investigation.doi) LIKE '%search%' OR  LOWER(investigation.name) LIKE '%search%' OR  LOWER(intrument.name) LIKE '%search%' )"
 * @param {*} search the word to search
 * @returns the clause
 */
exports.buildSearchInvestigations = (search) => {
  const likeSearch = search ? `LIKE '%${search}%'` : "";
  const searchFields = ["investigation.title", "investigation.summary", "investigation.doi", "investigation.name", "instrument.name"];
  return search ? `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")})` : "";
};

exports.buildSortInvestigations = (sortBy, sortOrder) => {
  const order = sortOrder === 1 ? "ASC" : "DESC";
  return sortBy ? `${sortBy} ${order}` : "investigation.startDate DESC ";
};

/**
 * builds a sql clause to filter investigations by  name and visitId
 * Example of returned clause:
 * ""investigation.visitId = 'PROPOSAL' AND (investigation.name like 'IX%' OR investigation.name like 'FX%' OR investigation.name like 'IN%' OR investigation.name like 'IM%') "
 * @param {*} investigationName array of investigation name
 * @param {*} visitId filter on the visitId
 * @returns the clause
 */
exports.buildInvestigationFilter = (investigationName, visitId) => {
  const clauseVisitId = visitId && visitId.length > 0 ? `investigation.visitId = '${visitId}'` : "";
  const clauseName = investigationName && investigationName.length > 0 ? `(${investigationName.map((n) => `investigation.name LIKE '${n}%'`).join(" OR ")})` : "";
  const allDefinedFilters = [clauseVisitId, clauseName].filter((f) => f !== "");
  const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
  return filters;
};

/**
 * builds a sql clause to search a word in different fields. The search is based on lower case.
 * The fields are dataset name, sample name, definition, volume, fileCount
 * Example of returned clause:
 * "(LOWER(dataset.name) LIKE '%search%' OR  LOWER(sample.name) LIKE '%search%'
 *  OR (LOWER(parameter.stringValue) LIKE '%search%' AND parameterType.name is 'definition'))"
 * @param {*} search the word to search
 * @returns the clause
 */
exports.buildSearchDataset = (search) => {
  const likeSearch = search ? `LIKE '%${search}%'` : "";
  const searchFields = ["dataset.name", "sample.name"];
  if (search) {
    const sqlSearch = `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")}`;
    const parameterSearch = ` OR (LOWER(parameters.stringValue) ${likeSearch} AND parameterType.name = 'definition' ))`;
    return sqlSearch + parameterSearch;
  }
  return "";
};

exports.buildSortDatasets = (sortBy, sortOrder) => {
  const order = sortOrder === 1 ? "ASC" : "DESC";
  return sortBy ? `${sortBy} ${order}` : "dataset.startDate DESC ";
};

/**
 * Returns a dataset parameter for a specified parameterId
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getDatasetParameterById = (sessionId, parameterId) => {
  const url = `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT parameter from DatasetParameter parameter  WHERE parameter.id = ${parameterId} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.downloadData = (sessionId, datasetIds, datafileIds) => {
  const params = datasetIds ? "datasetIds=:datasetIds".replace(":datasetIds", datasetIds) : "datafileIds=:datafileIds".replace(":datafileIds", datafileIds);
  const query = IDS.server + IDS_GET_DATA.replace(":sessionId", sessionId) + params;
  return query;
};

/**
 * Returns other user's starting with the prefix expect the given usernameWithPrefix (username/smisPk)
 * @param {*} sessionId
 * @param {*} prefix
 * @param {*} userNameWithSmisPk
 * @returns a list of users corresponding to the prefix, could be empty
 */
exports.getUserByUsernamePrefix = (sessionId, prefix, userNameWithPrefix) => {
  const query = `SELECT user FROM User user WHERE user.name like '${prefix}/%' AND user.name <> '${userNameWithPrefix}' ORDER BY user.createTime DESC`;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

/**
 * builds a sql clause to search a word in different fields of a datafile. The search is based on lower case.
 * The fields are name, location, fileSize.
 * Example of returned clause:
 * "(LOWER(datafile.name) LIKE '%search%' OR  LOWER(datafile.location) LIKE '%search%'  )"
 * @param {*} search the word to search
 * @returns the clause
 */
exports.buildSearchDatafile = (search) => {
  const likeSearch = search ? `LIKE '%${search}%'` : "";
  const searchFields = ["datafile.name", "datafile.location"];
  return search ? `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")})` : "";
};

/**
 * count the number of datafiles for a given datasetId. A search condition on datafile location/name could be added
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} search
 * @returns the number of datafiles
 */
exports.countEntityDatafilesByDatasetId = (sessionId, datasetsId, search) => {
  const searchCond = this.buildSearchDatafile(search);
  const filters = this.buildFiltersConditions([`dataset.id IN (${datasetsId}) `, searchCond]);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select count (distinct datafile) from Datafile datafile JOIN datafile.dataset dataset ${filters} ${SERVER} `;
  return encodeURI(url);
};

exports.getDataCollectionsByInvestigationId = (sessionId, investigationId) => {
  const query = `SELECT distinct(dc) from DataCollection dc JOIN dc.dataCollectionDatasets dcds JOIN dcds.dataset ds JOIN ds.investigation inv where dc.doi is not null and inv.id=${investigationId} order by dc.doi`;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};
