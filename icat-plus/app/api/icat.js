const axios = require("axios-https-proxy-fix");
const datasetParser = require("./parsers/datasetparser.js");
const investigationParser = require("./parsers/investigationparser.js");
const datacollectionParser = require("./parsers/datacollectionparser.js");
const instrumentScientistParser = require("./parsers/instrumentscientistparser.js");
const userParser = require("./parsers/userparser.js");
const qs = require("qs");
//const { getParameterTypes } = require("../cache/cache.js");
const icatQuery = require("./query.js");
const proxy = false;

exports.getInvestigationsByDatasetListIdsURL = async (sessionId, datasetIds) => {
  const response = await doGet(icatQuery.getInvestigationsByDatasetListIdsURL(sessionId, datasetIds));
  return response.data;
};

exports.getPropertiesQuery = async () => {
  const response = await axios.get(icatQuery.getPropertiesQuery());
  return response.data;
};

exports.getInvestigationTypes = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const investigationTypes = await axios.get(icatQuery.getInvestigationTypes(data.sessionId));
  return investigationTypes.data;
};

exports.getParameterTypeList = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const parameterTypeList = await axios.get(icatQuery.getParameterTypeList(data.sessionId));
  return parameterTypeList.data;
};

function parseInvestigationUserToObject(investigationUserRecord) {
  return {
    name: investigationUserRecord[0],
    fullName: investigationUserRecord[1],
    role: investigationUserRecord[2],
    investigationName: investigationUserRecord[3],
    investigationId: investigationUserRecord[4],
    email: investigationUserRecord[5],
    id: investigationUserRecord[6],
    orcidId: investigationUserRecord[7],
  };
}
/**
 * This method will retrieve an array of objects with the information concerning the investigation user
 *
 * Example : [{"name":"reader","role":"Principal investigator","investigationName":"IH-CH-1336","investigationId":77570462}]
 *
 */
exports.getInvestigationUserByInvestigationId = async (sessionId, investigationIds) => {
  const response = await doGet(icatQuery.getInvestigationUserByInvestigationIdURL(sessionId, investigationIds));
  return response.data.map((o) => {
    return parseInvestigationUserToObject(o);
  });
};

/**
 * Retrieves all users linked to a specified investigation with a specified role
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} role
 * @returns Array of investigationUser
 */
exports.getInvestigationUserByInvestigationIdAndRole = async (sessionId, investigationId, role) => {
  const users = await this.getInvestigationUserByInvestigationId(sessionId, investigationId);
  return users.filter((u) => u.role === role);
};

/**
 * This method will retrieve an array of objects with the information concerning the investigation user, his role and investigation.
 * @example
 * [{"name":"reader","role":"Principal investigator","investigationName":"IH-CH-1336","investigationId":77570462}]
 * @param {string} sessionId
 * @param {string} username - The second color, in hexadecimal format.
 * @param {function} onSuccess - callback function if succeed
 * @param {function} onError - callback function if error
 * @return {array} Array of objects containing: name, role, investigationName and investigationId
 */
exports.getInvestigationUserByUserName = (sessionId, username, onSuccess, onError) => {
  axios
    .get(icatQuery.getInvestigationUserByUserNameURL(sessionId, username), { proxy })
    .then((response) => {
      onSuccess(
        response.data.map((o) => {
          return parseInvestigationUserToObject(o);
        })
      );
    })
    .catch((error) => {
      onError(error);
    });
};

exports.asyncGetInvestigationUserByUserName = async (sessionId, username) => {
  const response = await doGet(icatQuery.getInvestigationUserByUserNameURL(sessionId, username));
  return response.data.map((o) => {
    return parseInvestigationUserToObject(o);
  });
};

/**
 * Get the debugrmation of the sessionId or return 403 if session is not found. Documentation: https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html#669420320
 * @example
 * {"userName":"db/root","remainingMinutes":117. 87021666666666}
 * @param {string} sessionId
 * @return {json} JSON object with username and remainingMinutes fields
 */
exports.asyncGetSessionInformation = async (sessionId) => {
  const response = await doGet(icatQuery.getSessionInformationURL(sessionId));
  return response.data;
};

/**
 * Removes the session from the icat server. It means a log out: https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html#1024708709
 * @param {*} sessionId
 * @returns
 */
exports.logout = async (sessionId) => {
  const response = await axios.delete(icatQuery.logout(sessionId));
  return response;
};

exports.asyncGetInvestigationUserBySessionId = async (sessionId) => {
  const data = await this.asyncGetSessionInformation(sessionId);
  const responseInvestigationUsers = await this.asyncGetInvestigationUserByUserName(sessionId, data.userName);
  return responseInvestigationUsers;
};

exports.getProxy = () => {
  return false;
};

/**
 * THIS METHOD DUPLICATED FUNCTIONALITYOF GETSESSION SO WE CAN MIGRATE SMOOTLY ALL CLIENTS
 * Create an ICAT session given a credential
 * @param {*} credentials the crendential to create the new session with
 */
exports.asyncGetSession = async (credentials) => {
  const response = await axios.post(
    icatQuery.getSession(),
    qs.stringify({
      json: JSON.stringify(credentials),
    }),
    {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    }
  );
  return response.data;
};

exports.updateDataCollectionDOI = async (authentication, dataCollectionId, doi) => {
  const getSessionResponse = await this.asyncGetSession(authentication);
  const { sessionId } = getSessionResponse;

  const dataCollection = [
    {
      DataCollection: {
        id: dataCollectionId,
        doi,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(dataCollection),
  });

  const response = await doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data);
  return response.data;
};

function doDelete(endPoint, data) {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  };
  return axios.delete(endPoint, data, headers);
}

function createParameterType(parameterType, value) {
  return {
    type: { id: parameterType[0].ParameterType.id },
    stringValue: value.toString(),
  };
}
/**
 * This method will create the data collection composed by the datasetIdList
 */
exports.mint = async (authentication, datasetIdList, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) => {
  if (abstract.length > 4000) {
    global.gLogger.debug("[mint] Abstract is longer than 4000 chars", { abstract });
  }

  /** It gets a new session for the minter user */

  const getSessionResponse = await this.asyncGetSession(authentication);
  const { sessionId } = getSessionResponse;
  global.gLogger.debug("[mint ICAT SUCCESS] New session for the minter user received", { sessionId });
  const parameters = [];
  /** Getting parameters: title, abstract */
  const parameterTypes = global.appCache.get("parameterTypes"); //getParameterTypes(); For unit tests this function gets undefined
  if (parameterTypes) {
    const getParameterByName = (name) => {
      return parameterTypes.filter((p) => {
        return p.ParameterType.name === name;
      });
    };

    const titleParameterType = getParameterByName("title");
    const abstractParameterType = getParameterByName("abstract");
    const mintedByNameParameterType = getParameterByName("mintedByName");
    const mintedByFullNameParameterType = getParameterByName("mintedByFullName");
    const investigationNamesParameterType = getParameterByName("investigationNames");
    const instrumentNamesParameterType = getParameterByName("instrumentNames");
    /** Setting dataCollection Parameters */
    if (titleParameterType) {
      parameters.push(createParameterType(titleParameterType, title));
    } else {
      global.gLogger.warn("ParameterType title has not been found");
    }
    if (abstractParameterType) {
      parameters.push(createParameterType(abstractParameterType, abstract));
    } else {
      global.gLogger.warn("ParameterType abstract has not been found");
    }

    if (investigationNamesParameterType) {
      parameters.push(createParameterType(investigationNamesParameterType, investigationNames));
    } else {
      global.gLogger.warn("ParameterType investigationNames has not been found");
    }

    if (instrumentNamesParameterType) {
      parameters.push(createParameterType(instrumentNamesParameterType, instrumentsNames));
    } else {
      global.gLogger.warn("ParameterType instrumentNames has not been found");
    }

    if (mintedByNameParameterType) {
      parameters.push(createParameterType(mintedByNameParameterType, mintedByName));
    } else {
      global.gLogger.warn("ParameterType mintedByName has not been found");
    }

    if (mintedByFullNameParameterType) {
      parameters.push(createParameterType(mintedByFullNameParameterType, mintedByFullName));
    } else {
      global.gLogger.warn("ParameterType mintedByFullName has not been found");
    }
  } else {
    global.gLogger.warn("There are no parameter types retrieved in cache");
  }

  const dataCollection = [
    {
      DataCollection: {
        dataCollectionDatasets: datasetIdList.map((id) => {
          return { dataset: { id } };
        }),
        parameters,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(dataCollection),
  });

  /** This creates a datacollection */
  const response = await doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data);
  return response.data;
};

exports.addInvestigationUser = async (sessionId, users) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(users),
  });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

exports.getDatasetsByInvestigationId = async (sessionId, investigationId, limit, skip, search, sortBy, sortOrder, datasetType) => {
  const response = await doGet(icatQuery.getEntityDatasetsByInvestigationId(sessionId, investigationId, limit, skip, search, sortBy, sortOrder, datasetType));
  return datasetParser.parse(response.data);
};

exports.countDatasetByInvestigationId = async (sessionId, investigationId, search, datasetType) => {
  const response = await doGet(icatQuery.countEntityDatasetsByInvestigationId(sessionId, investigationId, search, datasetType));
  return response.data;
};

/** Return all datasets by DOI, it means there are in a collection or public investigation */
exports.getDatasetsByDOI = async (sessionId, doi, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getDatasetsByDOI(sessionId, doi, limit, skip, search, sortBy, sortOrder));
  const datasets = response.data;
  if (datasets && datasets.length > 0) {
    return datasetParser.parse(datasets);
  }
  const responseInvestigation = await doGet(icatQuery.getInvestigationIdByDOI(sessionId, doi));
  if (responseInvestigation.data) {
    const datasetsPerInvestigation = await this.getDatasetsByInvestigationId(sessionId, responseInvestigation.data, limit, skip, search, sortBy, sortOrder);
    return datasetsPerInvestigation;
  }
  return [];
};

exports.countDatasetsByDOI = async (sessionId, doi, search) => {
  const response = await doGet(icatQuery.countDataCollectionIdListByDOI(sessionId, doi, search));
  const datasetsCount = response.data;
  if (datasetsCount && datasetsCount > 0) {
    return datasetsCount;
  }
  const responseInvestigation = await doGet(icatQuery.getInvestigationIdByDOI(sessionId, doi));
  const responsePerInvestigation = await icatQuery.countEntityDatasetsByInvestigationId(sessionId, responseInvestigation.data, search);
  return responsePerInvestigation.data;
};

/** Return all datasets by DOI, it means there are in a collection or public investigation */

exports.getInvestigationByDOI = async (sessionId, doi) => {
  const response = await doGet(icatQuery.getInvestigationByDOI(sessionId, doi));
  return response.data;
};

/** Return all datasets by investigationId */
exports.getDatasetsById = async (sessionId, datasetIds) => {
  if (datasetIds) {
    if (datasetIds.length > 0) {
      const response = await doGet(icatQuery.getDatasetsByIdList(sessionId, datasetIds));
      return datasetParser.parse(response.data);
    }
  }
  return [];
};

exports.getDatasetsByDateRange = async (sessionId, startDate, endDate) => {
  const response = await doGet(icatQuery.getDatasetsByDateRange(sessionId, startDate, endDate));
  return datasetParser.parse(response.data);
};

/** Return all DOI's related to a dataset */
exports.getDataColletionByDatasetId = async (sessionId, datasetId) => {
  const response = await doGet(icatQuery.getDataColletionByDatasetId(sessionId, datasetId));
  return response.data;
};

/** Return all DOI's related to a dataset */
exports.getDataCollectionsBySessionId = async (sessionId) => {
  /*function parse(data) {
    onSucess(datacollectionParser.parse(data));
  }

  global.gLogger.debug(icatQuery.getDataCollectionsBySessionId(sessionId), { method: "getDataCollectionsBySessionId" });
  doGet(icatQuery.getDataCollectionsBySessionId(sessionId), parse, onError);*/

  const response = await doGet(icatQuery.getDataCollectionsBySessionId(sessionId));
  if (response) {
    if (response.data) {
      return datacollectionParser.parse(response.data);
    }
  }
  return [];
};

exports.getInvestigations = async (sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getInvestigationsByDateRangesOrInstrument(sessionId, undefined, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

exports.getInvestigationsByInstrument = async (sessionId, instrument, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getInvestigationsByDateRangesOrInstrument(sessionId, instrument, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

/** Counts all investigations by instrumentName */
exports.countInvestigationsByInstrument = async (sessionId, instrument, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countInvestigationsByInstrument(sessionId, instrument, startDate, endDate, search));
  return response.data;
};

/** Counts all investigations  */
exports.countAllInvestigations = async (sessionId, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countInvestigationsByInstrument(sessionId, undefined, startDate, endDate, search));
  return response.data;
};

/**
 * Gets the investigation that belongs to a user, it means a user is a participant in the investigation
 * @param {string} sessionId
 * @return {array} Array with the investigations
 * @example
 *  [{Investigation:
   { id: 123398449,
     createId: 'root',
     createTime: '2018-11-06T14:34:16.386+01:00',
     modId: 'root',
     modTime: '2018-11-06T14:34:16.386+01:00',
     datasets: [],
     investigationGroups: [],
     investigationInstruments: [],
     investigationUsers: [],
     keywords: [],
     name: 'ID212020',
     parameters: [],
     publications: [],
     samples: [],
     shifts: [],
     studyInvestigations: [],
     summary: 'DCM',
     title: 'DCM',
     visitId: 'id21' } }]
 *
 */
exports.getInvestigationsAsParticipantBySessionId = async (sessionId, username) => {
  global.gLogger.debug("getInvestigationsBySessionId", { username, query: icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username) });
  const response = await doGet(icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username));
  return investigationParser.parse(response.data);
};

/**
 * This method is a workaround that should be replaced by getInvestigationsAsParticipantBySessionId once the parsers are unified: investigationParser.parser and investigationParser.lightParse
 * @param {*} sessionId
 * @param {*} username
 */
exports.getInvestigationsAsParticipantBySessionIdWithNoParse = async (sessionId, username, visitIdName, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  global.gLogger.debug("getInvestigationsBySessionId", {
    username,
    query: icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username, visitIdName, startDate, endDate, limit, skip, search, sortBy, sortOrder),
  });
  const response = await doGet(icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username, visitIdName, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

exports.getInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId = async (
  sessionId,
  username,
  investigationName,
  visitIdName,
  startDate,
  endDate,
  limit,
  skip,
  search,
  sortBy,
  sortOrder
) => {
  const response = await doGet(
    icatQuery.getInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(
      sessionId,
      username,
      investigationName,
      visitIdName,
      startDate,
      endDate,
      limit,
      skip,
      search,
      sortBy,
      sortOrder
    )
  );
  return response.data;
};

// TODO: investigation.parse should be changed by parseLight
exports.getInvestigationByInstrumentScientistAndInvestigationId = async (sessionId, username, investigationId) => {
  const response = await doGet(icatQuery.getInvestigationByInstrumentScientistAndInvestigationId(sessionId, username, investigationId));
  return investigationParser.parse(response.data);
};

exports.getInvestigationByInstrumentScientist = async (sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getInvestigationByInstrumentScientist(sessionId, username, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

/** Counts all investigations by instrumentscientist by sessionId */
exports.countInvestigationsByInstrumentScientist = async (sessionId, username, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countInvestigationsByInstrumentScientist(sessionId, username, startDate, endDate, search));
  return response.data;
};

/** Return all investigations  by sessionId */
exports.getInvestigationsBySessionId = (sessionId, onSucess, onError) => {
  const parse = (data) => {
    global.gLogger.debug("getInvestigationsBySessionId", { sessionId, count: data.length });
    onSucess(investigationParser.parse(data));
  };
  const onRequestError = (error) => {
    onError(error);
  };
  global.gLogger.debug("getInvestigationsBySessionId started", {
    sessionId,
    query: icatQuery.getInvestigationsByDateRangesOrInstrument(sessionId),
  });
  doGet(icatQuery.getInvestigationsByDateRangesOrInstrument(sessionId), parse, onRequestError);
};

exports.asyncGetInvestigationsBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getInvestigationsByDateRangesOrInstrument(sessionId));
  return investigationParser.parse(response.data);
};

exports.getReleasedInvestigationsBySessionId = async (sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getReleasedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

/** Counts all open/released investigations by sessionId */
exports.countReleasedInvestigations = async (sessionId, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countReleasedInvestigationsBySessionId(sessionId, startDate, endDate, search));
  return response.data;
};

/** Return all investigations under embargo by sessionId */
exports.getEmbargoedInvestigationsBySessionId = async (sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder) => {
  const response = await doGet(icatQuery.getEmbargoedInvestigationsBySessionId(sessionId, startDate, endDate, limit, skip, search, sortBy, sortOrder));
  return response.data;
};

/** Counts all investigations under embargo by sessionId */
exports.countEmbargoedInvestigations = async (sessionId, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countEmbargoedInvestigationsBySessionId(sessionId, startDate, endDate, search));
  return response.data;
};

/** Counts all investigations as participant by sessionId */
exports.countInvestigationsAsParticipant = async (sessionId, username, visitIdName, startDate, endDate, search) => {
  const response = await doGet(icatQuery.countInvestigationsAsParticipant(sessionId, username, visitIdName, startDate, endDate, search));
  return response.data;
};

/** Counts all investigations as participant by sessionId filtered by investigation name and visitId*/
exports.countInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId = async (sessionId, username, investigationName, visitIdName, startDate, endDate, search) => {
  const response = await doGet(
    icatQuery.countInvestigationsAsParticipantFilteredByInvestigationNameAndVisitId(sessionId, username, investigationName, visitIdName, startDate, endDate, search)
  );
  return response.data;
};

function doPost(endPoint, data, onSuccess, onError) {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  };
  if (onSuccess) {
    axios
      .post(endPoint, data, headers)
      .then((response) => {
        onSuccess(response.data);
      })
      .catch((error) => {
        onError(error);
      });
  } else {
    return axios.post(endPoint, data, headers);
  }
}

function doGet(url, onSuccess, onError, postProcessing) {
  if (!onSuccess) {
    if (postProcessing) {
      return new Promise((resolve, reject) => {
        axios
          .get(url, { proxy })
          .then((response) => {
            resolve(postProcessing(response.data));
          })
          .catch((error) => {
            reject(error);
          });
      });
    }
    return axios.get(url, { proxy });
  }
  axios
    .get(url, { proxy })
    .then((response) => {
      if (postProcessing) {
        onSuccess(postProcessing(response.data));
      } else {
        onSuccess(response.data);
      }
    })
    .catch((error) => {
      try {
        onError(error);
      } catch (error) {
        global.gLogger.error(error);
      }
    });
}

/** Return an investigation by investigationId */
exports.getInvestigationById = (sessionId, investigationId, onSucess, onError) => {
  return doGet(icatQuery.getInvestigationById(sessionId, investigationId), onSucess, onError);
};

exports.asyncGetInvestigationById = async (sessionId, investigationId) => {
  const response = await doGet(icatQuery.getInvestigationById(sessionId, investigationId));
  return response.data;
};

/** Return an investigation by investigation name */
exports.getInvestigationByName = (sessionId, investigationName, onSucess, onError) => {
  const url = icatQuery.getInvestigationByName(sessionId, investigationName);
  global.gLogger.debug("getInvestigationByName.", { query: url });
  if (!onSucess) {
    return axios.get(url, { proxy });
  }
  doGet(url, onSucess, onError);
};

/** Return an investigation by investigation name */
exports.asyncGetInvestigationByName = async (sessionId, investigationName) => {
  const response = await doGet(icatQuery.getInvestigationByName(sessionId, investigationName));
  return investigationParser.lightParse(response.data);
};

/** Return all users by group name */
exports.getUsersByGroupName = async (authentication, groupName) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getUsersByGroupName(data.sessionId, groupName));
  return response.data;
};

/** Returns all users by group name */
exports.getUsers = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getUsers(data.sessionId));
  return response.data;
};

exports.getInstruments = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getInstruments(data.sessionId));
  return response.data;
};

exports.getInstrumentsBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getInstruments(sessionId));
  return response.data;
};

/** Returns all users by sessionId */
exports.getUsersBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getUsers(sessionId));
  global.gLogger.debug(icatQuery.getUsers(sessionId), { users: userParser.parse(response.data).length });
  return userParser.parse(response.data);
};

/** Returns instrumentScientists by user's name' */
exports.getInstrumentScientistsByUserName = async (sessionId, userName) => {
  const response = await doGet(icatQuery.getInstrumentScientistsByUserName(sessionId, userName));
  return instrumentScientistParser.parse(response.data);
};

/** Returns instrumentScientists by user's name' */
exports.getInstrumentScientists = async (sessionId) => {
  const response = await doGet(icatQuery.getInstrumentScientists(sessionId));
  return instrumentScientistParser.parse(response.data);
};

/**
 * Get a list of data files by dataset id
 * @param {string} sessionId
 * @param {string} datasetId
 * @param {string} limit
 * @param {string} skip
 * @param {string} search
 */
exports.getDatafilesByDatasetId = async (sessionId, datasetId, limit, skip, search) => {
  const response = await doGet(icatQuery.getDatafilesByDatasetId(sessionId, datasetId, limit, skip, search));
  return response.data;
};

/**
 * count the number of datafiles of a given datasetId, could be filtered with a search text on datafile location/name
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} search
 * @returns
 */
exports.countDatafileByDatasetId = async (sessionId, datasetsId, search) => {
  const response = await doGet(icatQuery.countEntityDatafilesByDatasetId(sessionId, datasetsId, search));
  return response.data;
};

/**
 * Get a list of data files by ids
 * @param {string} sessionId
 * @param {string} datasetIds A comma separated list of datafileIds ids. Example: "2342,1323,234234"
 */
exports.getDatafilesByIds = async (sessionId, datafileIds) => {
  const response = await doGet(icatQuery.getDatafilesByIds(sessionId, datafileIds));
  return response.data;
};

/**
 * Return the archive status of the data files specified by the  datasetIds  along with a sessionId.
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} datasetIds A comma separated list of dataset ids. Example: "2342,1323,234234"
 */
exports.getStatusByDatasetIds = async (sessionId, datasetIds) => {
  const response = await doGet(icatQuery.getStatusByDatasetIds(sessionId, datasetIds));
  return response.data;
};

/**
 * Return the archive status of the data files specified by the investigationIds along with a sessionId.
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} investigationIds A comma separated list of investigation ids. Example: "2342,1323,234234"
 */
exports.getStatusByIvestigationIds = (sessionId, investigationIds, onSucess, onError) => {
  doGet(icatQuery.getStatusByIvestigationIds(sessionId, investigationIds), onSucess, onError);
};

/**
 * Get InstrumentScientists by instrument name.
 * @param {string} sessionId session identifier identifying the user.
 * @param {string} instrumentName instrument name identifying the instrument (=beamline)
 */
exports.getInstrumentScientistsByInstrumentName = async (sessionId, instrumentName) => {
  global.gLogger.debug("getInstrumentScientistsByInstrumentName()", { sessionId, instrumentName });
  const response = await doGet(icatQuery.getInstrumentScientistsByInstrumentName(sessionId, instrumentName));
  return response.data;
};

exports.createInstrumentScientists = async (sessionId, instrumentScientists) => {
  global.gLogger.debug("createInstrumentScientists", { sessionId });
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(instrumentScientists),
  });
  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

exports.deleteInstrumentScientists = async (sessionId, instrumentScientists) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(instrumentScientists),
  });

  global.gLogger.debug("deleteInstrumentScientists", { data, sessionId, instrumentScientists });
  const response = await doDelete(icatQuery.getDeleteQuery(sessionId, JSON.stringify(instrumentScientists)), data);
  return response.data;
};

exports.deleteInvestigationUser = async (sessionId, investigationUser) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(investigationUser),
  });

  global.gLogger.debug("deleteInvestigationUser", { data, sessionId, investigationUser });
  const response = await doDelete(icatQuery.getDeleteQuery(sessionId, JSON.stringify(investigationUser)), data);
  return response.data;
};

exports.getSamplesByInvestigationId = async (sessionId, investigationId) => {
  const response = await doGet(icatQuery.getSamplesByInvestigationId(sessionId, investigationId));
  return response.data;
};

exports.getSampleById = (sessionId, sampleId) => {
  global.gLogger.debug("getSampleById", { query: icatQuery.getSampleById(sessionId, sampleId) });
  return doGet(icatQuery.getSampleById(sessionId, sampleId));
};

exports.getInstrumentsForUser = async (sessionId, username) => {
  global.gLogger.debug("getInstrumentsForUser", { username, query: icatQuery.getInstrumentsForUser(sessionId, username) });
  const response = await doGet(icatQuery.getInstrumentsForUser(sessionId, username));
  return response.data;
};

exports.getInstrumentsForInstrumentScientist = async (sessionId, username) => {
  global.gLogger.debug("getInstrumentsForInstrumentScientist", { username, query: icatQuery.getInstrumentsForInstrumentScientist(sessionId, username) });
  const response = await doGet(icatQuery.getInstrumentsForInstrumentScientist(sessionId, username));
  return response.data;
};

/**
 * Returns a dataset parameter for a specified id
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getDatasetParameterById = async (sessionId, parameterId) => {
  global.gLogger.debug("getDatasetParameterById", { parameterId });
  const response = await doGet(icatQuery.getDatasetParameterById(sessionId, parameterId));
  return response.data;
};

/**
 * Updates a dataset parameter value
 * @param {*} sessionId
 * @param {*} parameterId
 * @param {*} value
 * @returns empty array
 */
exports.updateDatasetParameter = async (sessionId, parameterId, value) => {
  const datasetParameter = [
    {
      DatasetParameter: {
        id: parameterId,
        stringValue: value,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(datasetParameter),
  });

  global.gLogger.debug("updateDatasetParameter", { data });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

/**
 * Returns other user's starting with the prefix expect the given usernameWithPrefix (username/smisPk)
 * @param {*} sessionId the sessionId
 * @param {*} prefix  the username
 * @param {*} userNameWithPrefix  the userName concatenated with smisPk, separated by a slash
 * @returns a list of users corresponding to the prefix, could be empty
 */
exports.getUserByUsernamePrefix = async (sessionId, prefix, userNameWithPrefix) => {
  const response = await doGet(icatQuery.getUserByUsernamePrefix(sessionId, prefix, userNameWithPrefix));
  return userParser.fullParse(response.data);
};

/**
 * Returns all dataCollections linked to an investigation
 * dataCollections containing datasets linked to the specific investigation
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.getDataCollectionsByInvestigationId = async (sessionId, investigationId) => {
  const response = await doGet(icatQuery.getDataCollectionsByInvestigationId(sessionId, investigationId));
  return response && response.data ? datacollectionParser.parse(response.data) : [];
};
