/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

/** Address schema */
const AddressSchema = mongoose.Schema(
  {
    investigationName: {
      type: String,
      required: true,
    },
    investigationId: {
      type: Number,
      //required: true,
    },
    status: {
      type: String,
      enum: ["ACTIVE", "REMOVED"],
      default: "ACTIVE",
    },
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    companyName: String,
    address: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    region: String,
    postalCode: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
    },
    modifiedBy: String,
    deletedBy: String,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Address", AddressSchema);
