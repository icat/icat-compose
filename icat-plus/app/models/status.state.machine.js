const Status = require("./status.js");

module.exports = Object.freeze({
  CREATED: [],
  SCHEDULED: [Status.CREATED],
  READY: [Status.SCHEDULED],
  APPROVED: [Status.READY],
  REFUSED: [Status.READY],
  INPUT: [Status.READY],
  SENT: [Status.APPROVED],
  STORES: [Status.SENT],
  BEAMLINE: [Status.STORES],
  BACK_STORES: [Status.BEAMLINE],
  BACK_USER: [Status.BACK_STORES],
  DESTROYED: "DESTROYED",
  REMOVED: [],
});
