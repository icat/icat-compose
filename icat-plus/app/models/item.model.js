const mongoose = require("mongoose");

const ItemSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String },
    comments: { type: String },
    sampleId: { type: Number },
    type: {
      type: String,
      required: true,
      enum: ["SAMPLESHEET", "TOOL", "OTHER"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Item", ItemSchema);
