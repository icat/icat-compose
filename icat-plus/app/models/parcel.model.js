/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const _ = require("lodash");
const Address = require("./address.model.js");
const { StatusSchema } = require("./status.schema.js");
const { getUserFullNameByName } = require("../cache/cache.js");
const Schema = require("mongoose").Schema;

const ParcelSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    shipmentId: { type: Schema.Types.ObjectId },
    /** Investigation identifier indicating which investigation this event belongs to.*/
    investigationId: { type: Number, required: true },
    /** Name of the investigation */
    description: { type: String },
    investigationName: { type: String, required: true },
    containsDangerousGoods: { type: Boolean },
    type: {
      type: String,
      enum: ["DEFAULT"],
      default: "DEFAULT",
    },
    statuses: [StatusSchema],
    returnAddress: Address.schema,
    shippingAddress: Address.schema,
    /* List of localContacts stored as User.name */
    localContactNames: [{ type: String }],
    items: [{ type: Schema.Types.ObjectId, ref: "Item" }],
    comments: { type: String },
    storageConditions: { type: String },
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
  }
);

ParcelSchema.virtual("status").get(function () {
  const statusSortedByCreationTime = _.sortBy(this.statuses, [(status) => status._id]);
  if (statusSortedByCreationTime.length > 0) {
    return statusSortedByCreationTime[statusSortedByCreationTime.length - 1].status;
  }
  return "Not available";
});

ParcelSchema.virtual("investigation").get(function () {
  return global.investigationsKeys[this.investigationId];
});

ParcelSchema.virtual("localContactFullnames").get(function () {
  return this.localContactNames ? this.localContactNames.map((l) => getUserFullNameByName(l)) : [];
});

module.exports = mongoose.model("Parcel", ParcelSchema);
