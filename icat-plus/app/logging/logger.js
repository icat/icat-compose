const winston = require("winston");
const WinstonGraylog2 = require("winston-graylog2");
const { format } = require("winston");
const { combine, timestamp, printf } = format;

function parseParameter(info) {
  let msg = "\n  ";
  for (const i in info) {
    if ((i !== "level") & (i !== "message") & (i !== "timestamp")) {
      msg = `${msg}\t\t\t\t ${i} : ${info[i]}\n`;
    }
  }
  return msg;
}

/**
 * Callback which reshapes Winston log format. This format is used for all transports.
 */
const myFormat = printf((log) => {
  if (log.message === undefined) {
    log.message = "undefined";
  }
  if (log instanceof Error) {
    return `${log.timestamp} [${log.level}]: ${log.message} ${log.stack}`;
  }
  return `${log.timestamp} [${log.level}]: ${log.message} ${parseParameter(log)}`;
});

/**
 * Get the transport object required to use Graylog log management software
 */
function getWinstonGrayLogTransport() {
  try {
    const options = {
      name: "Graylog",
      level: "silly",
      silent: false,
      handleExceptions: true,
      graylog: {
        servers: [
          {
            host: global.gServerConfig.logging.graylog.host,
            port: global.gServerConfig.logging.graylog.port,
          },
        ],
        hostname: global.gServerConfig.logging.graylog.hostname,
        facility: global.gServerConfig.logging.graylog.facility,
        bufferSize: 1400,
      },
    };
    return new WinstonGraylog2(options);
  } catch {}
}

/**
 * Adds all transports we want to use into Winston
 * Graylog needs to be enabled to be used
 */
function getTransportProtocols() {
  const transportProtocols = [];
  transportProtocols.push(new winston.transports.Console({ level: global.gServerConfig.logging.console.level }));
  if (global.gServerConfig.logging.file.enabled) {
    transportProtocols.push(new winston.transports.File({ filename: global.gServerConfig.logging.file.filename, level: global.gServerConfig.logging.file.level }));
  }
  if (global.gServerConfig.logging.graylog.enabled) {
    const graylogTransport = getWinstonGrayLogTransport();
    if (graylogTransport) {
      transportProtocols.push(graylogTransport);
    }
  }
  return transportProtocols;
}

module.exports = {
  logger: winston.createLogger({
    format: combine(winston.format.colorize(), timestamp(), myFormat),
    transports: getTransportProtocols(),
  }),
};
