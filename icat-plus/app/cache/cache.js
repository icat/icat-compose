const icat = require("../api/icat.js");
const _ = require("lodash");

/** Given a username and a key in the cache if will return true if the username exists in the list of users */
const isUserInGroup = (username, group) => {
  return global.appCache.get(group).find((u) => {
    return u.User.name === username;
  });
};
/**
 * Returns true if the sessionId belongs to a user that is administrator.
 */
const isAdministrator = (username) => {
  return isUserInGroup(username, "administrators");
};

const isMinter = (username) => {
  return isUserInGroup(username, "minters");
};

/** returns all investigations of the database */
exports.getInvestigations = () => global.investigations;

/**
 * This method intializes the cache of an user
 */
exports.initSession = async (sessionId) => {
  try {
    if (sessionId) {
      if (global.API_KEY !== sessionId) {
        const data = await icat.asyncGetSessionInformation(sessionId);
        const { userName, remainingMinutes } = data;
        const [instrumentScientists, usersWithSamePrefix] = await Promise.all([
          icat.getInstrumentScientistsByUserName(sessionId, userName),
          this.getUserByUsernamePrefix(sessionId, userName),
        ]);
        const isInstrumentScientist = instrumentScientists && instrumentScientists.length > 0;
        const user = {
          name: userName,
          username: userName,
          fullName: global.users[userName].fullName,
          lifeTimeMinutes: remainingMinutes,
          isAdministrator: !!isAdministrator(userName),
          isInstrumentScientist: !!isInstrumentScientist,
          isMinter: !!isMinter(userName),
          sessionId,
          usersByPrefix: usersWithSamePrefix,
        };
        return user;
      }

      return {
        name: "API_KEY",
        username: "API_KEY",
        fullName: "API_KEY",
        lifeTimeMinutes: 0,
        isAdministrator: false,
        isInstrumentScientist: false,
        sessionId,
        usersByPrefix: [],
      };
    }
    throw Error("Session can not be initialize. Session id is empty");
  } catch (e) {
    global.gLogger.error("Unable to initialize the user'session", { sessionId, error: e.message });
  }
};

exports.getUserByUsernamePrefix = async (sessionId, userName) => {
  const users = await icat.getUserByUsernamePrefix(sessionId, this.extractUserNamePrefix(userName), userName);
  return users;
};

exports.extractUserNamePrefix = (userName) => {
  return userName ? userName.split("/")[0] : "";
};

exports.getInvestigationTypes = () => global.appCache.get("investigationTypes");

exports.getParameterTypes = () => {
  return global.appCache.get("parameterTypes");
};

/**
 * It returns a dictionary id: name
 * Example:
 * '127988408': 'InstrumentSource_current_start',
 */
exports.getParameterTypeDictionary = () => {
  return global.appCache.get("parametersDictionary");
};

exports.findOrCreateFullNameByPatternNameFromCache = (searchName) => {
  let user = global.usersByPattern[searchName];
  if (user) {
    return user;
  }
  user = this.getUserByNamePattern(searchName);
  global.usersByPattern[searchName] = user;
  return user;
};

/**
 * Get user fullname by name  or by suffix.
 * @param {string} nameOrSuffix name or suffix of the user.
 * @returns {string} fullname if it exists. Unknown if nameOrSuffix is null; nameOrSuffix otherwise.
 */
exports.getUserFullNameByName = (nameOrSuffix) => {
  if (!nameOrSuffix) {
    return "Unknown";
  }
  const userFromCache = global.users[nameOrSuffix];
  if (userFromCache) {
    return userFromCache.fullName;
  }
  const userPrefixAndSuffix = nameOrSuffix.split("/");
  const searchName = userPrefixAndSuffix[1] ? `.*/${userPrefixAndSuffix[1]}` : userPrefixAndSuffix[0];
  const user = this.findOrCreateFullNameByPatternNameFromCache(searchName);
  return user ? user.fullName : nameOrSuffix;
};

/**
 * returns the first user name corresponding to a specified pattern, undefined if no user found
 * @param {*} namePattern - Example '/123456'
 * @returns the user name,
 */
exports.getUserByNamePattern = (namePattern) => {
  const name = Object.keys(global.users)
    .filter((key) => key.indexOf(namePattern) !== -1)
    .shift();

  return name ? this.getUserByName(name) : undefined;
};

exports.getStats = () => global.appCache.getStats();

exports.getKeys = () => global.appCache.keys();

exports.getDataByKey = (key) => global.appCache.mget([key]);

exports.getLifeTimeMinutes = () => global.appCache.get("lifetimeMinutes");

/**
 * Get user by name
 * @param {*} name name of the user
 * @returns {Object} the user object. Undefined if not found
 */
exports.getUserByName = (name) => global.users[name];

/**
 * Return the object in the cache by sessionId
 * In case it is not available then it will init the cache
 */
exports.getSessionById = async (sessionId) => {
  let session = global.appCache.get(sessionId);
  if (!session) {
    global.gLogger.debug("No session found for sessionId and will be initialized", sessionId);
    session = await this.initSession(sessionId);
    global.gLogger.debug("Session has been initialized", sessionId);
  }
  return session;
};

exports.getParameterTypeById = (id) => {
  const attribute = global.appCache.get("parametersDictionary");
  if (attribute) {
    return attribute[id];
  }

  return null;
};

exports.getInstruments = () => {
  return global.instruments;
};

exports.getInstrumentByName = (name) => {
  return _.find(global.instruments, (i) => {
    return i.name === name;
  });
};

exports.setKey = async (sessionId, key, value) => {
  let session = await this.getSessionById(sessionId);
  if (!session) {
    global.gLogger.info("{}  => cache", { sessionId });
    this.setSession(sessionId);
    session = await this.getSessionById(sessionId);
  }

  session[key] = value;
  global.gLogger.info(`${key} => cache`, { sessionId, key });
  global.appCache.set(sessionId, session);
};

exports.cache = global.appCache;
