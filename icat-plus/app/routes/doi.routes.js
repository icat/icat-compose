module.exports = (app) => {
  const doiController = require("../controllers/doi.controller.js");
  const datasetController = require("../controllers/datasets.controller.js");
  const auth = require("../authentication/icat.js");
  const { ERROR, sendError } = require("../errors.js");
  const isParticipant = async (sessionId, datasetIds) => {
    const [investigations, investigationUsers] = await doiController.isParticipantByDatasetId(sessionId, datasetIds);
    return {
      investigations,
      investigationUsers,
    };
  };
  /**
   * @swagger
   * /doi/{sessionId}/mint:
   *   post:
   *     summary: It mints a DOI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: body
   *         description : Title of the DOI
   *         name: title
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Abstact of the DOI
   *         name: abstract
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Array with the authors with either createName or (name and surname)
   *         name: authors
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Array with the datasetIds
   *         name: datasetIdList
   *         schema :
   *           type : Array
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/responses/doi'
   *       '400':
   *         description: 'No description'
   *       '500':
   *         description: 'No description'
   *     tags:
   *       - DOI
   */
  app.post("/doi/:sessionId/mint", auth.requiresSession, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { skip } = req.query;
      const { title, abstract } = req.body;
      const { datasetIdList, authors } = req.body;

      global.gLogger.info("Mint", { title, abstract, authors, datasetIdList });
      const { investigations, investigationUsers } = await isParticipant(sessionId, req.body.datasetIdList);
      doiController.mint(sessionId, investigations, investigationUsers[0].name, investigationUsers[0].fullName, datasetIdList, title, abstract, authors, skip, res);
    } catch (e) {
      global.gLogger.error(e);
      return sendError(ERROR.NO_PARTICIPANT.code, ERROR.NO_PARTICIPANT.message, new Error(ERROR.NO_PARTICIPANT.message), res);
    }
  });

  /**
   * @swagger
   * /doi/{prefix}/{suffix}/datasets:
   *   get:
   *     description: Returns a set of datasets given a DOI.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : prefix of the doi
   *         name: prefix
   *         schema :
   *           type : string
   *         example : 10.15151
   *         required: true
   *       - in: path
   *         description : suffix of the doi
   *         name: suffix
   *         schema :
   *           type : string
   *         example : ESRF-ES-142846529
   *         required: true
   *       - in: query
   *         name: limit
   *         description : limit the number of datasets retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of datasets retreived
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the datasets
   *         schema:
   *           type : string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/dataset'
   *       '400':
   *         description: 'No description'
   *       '500':
   *         description: 'No description'
   *     tags:
   *       - DOI
   */
  app.get("/doi/:prefix/:suffix/datasets", datasetController.getDatasetsByDOI);

  /**
   * @swagger
   * /doi/{prefix}/{suffix}/json-datacite:
   *   get:
   *     description: Returns a document in a json-datacite format with the description of the DOI
   *     parameters:
   *       - in: path
   *         description : prefix of the doi
   *         name: prefix
   *         schema :
   *           type : string
   *         example : 10.15151
   *         required: true
   *       - in: path
   *         description : suffix of the doi
   *         name: suffix
   *         schema :
   *           type : string
   *         example : ESRF-ES-142846529
   *         required: true
   *     responses:
   *       '200':
   *         description: 'Return a json-datacite compliant format'
   *         schema:
   *           $ref: '#/components/schemas/json-datacite'
   *       '400':
   *         description: 'No description'
   *       '500':
   *         description: 'No description'
   *     tags:
   *       - DOI
   */
  app.get("/doi/:prefix/:suffix/json-datacite", doiController.getJSONDataciteByDOI);
};
