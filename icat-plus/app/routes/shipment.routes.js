module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const shipmentController = require("../controllers/shipment.controller.js");

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment:
   *   get:
   *     summary: Lists shipments associated to an investigation
   *     description : Lists shipments associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *        $ref: '#/components/responses/shipments'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    shipmentController.getShipmentsByInvestigationId
  );

  /**
   * @swagger
   * /tracking/{sessionId}/shipment:
   *   get:
   *     summary: Lists shipments by user
   *     description : Lists shipments associated to a user. All shipments of the investigations he makes part of. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipments'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get("/tracking/:sessionId/shipment", auth.requiresSession, shipmentController.getShipmentsBySessionId);

  /**
   * @swagger
   * /tracking/{sessionId}/shipment/id/{shipmentId}:
   *   get:
   *     summary: Gets a shipment by shipmentId
   *     description : Returns a shipment by shipmentId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/shipmentId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/shipment/id/:shipmentId",
    auth.requiresSession,
    shipmentController.addInvestigationIdToRequest,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    shipmentController.getShipmentById
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment:
   *   post:
   *     summary: Creates a shipment associated to an investigation
   *     description : Creates a shipment associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    shipmentController.createShipment
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment:
   *   put:
   *     summary: Modifies a shipment associated to an investigation
   *     description : Modifies an existing shipment. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    shipmentController.updateShipment
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment:
   *   delete:
   *     summary: Deletes a shipment
   *     description : Deletes an existing shipment. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    shipmentController.deleteShipment
  );
};
