module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const dmpController = require("../controllers/dmp.controller.js");

  /**
   * @swagger
   * /dmp/{sessionId}/questionnaires:
   *   get:
   *     summary: Returns a list of questionnaires for a given investigation
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description : investigationId
   *         name: investigationId
   *         schema :
   *           type : string
   *         required: true
   *       - in: query
   *         description : investigationName
   *         name: investigationName
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                  $ref: '#/components/schemas/dmpquestionnaire'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - DMP
   */
  app.get("/dmp/:sessionId/questionnaires", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, (req, res) => {
    const { investigationName } = req.query;
    return dmpController.getQuestionnaires(investigationName, res);
  });
};
