const investigationController = require("../controllers/investigations.controller.js");
const datasetController = require("../controllers/datasets.controller.js");
const sampleController = require("../controllers/samples.controller.js");
const dataCollectionController = require("../controllers/datacollections.controller.js");
const datasfileController = require("../controllers/datafile.controller.js");
const userController = require("../controllers/user.controller.js");
const instrumentController = require("../controllers/instrument.controller.js");
const parameterController = require("../controllers/parameter.controller.js");
const auth = require("../authentication/icat.js");

module.exports = (app) => {
  /**
   * @swagger
   * /catalogue/parameters:
   *   get:
   *     summary: Returns all parameters
   *     description : Return a list of parameters that are registered in ICAT
   *     parameters:
   *       - in: query
   *         name: parameterTypeId
   *         schema:
   *           type : integer
   *       - in: query
   *         name: name
   *         schema:
   *           type : string
   *     responses:
   *       200
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/parameters", parameterController.getParameters);

  /**
   * @swagger
   * /catalogue/instruments:
   *   get:
   *     summary: Returns all instruments
   *     description : Return a list of instruments that are registered in ICAT
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/instrument'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/instruments", instrumentController.getInstruments);

  /**
   * @swagger
   * /catalogue/{sessionId}/instruments:
   *   get:
   *     summary: Returns all instruments linked to the user
   *     description : |
   *       Returns instruments linked to the user, with instruments linked to the scientist if the user is beamline responsible, all instruments if administrator
   *
   *       if the filter is participant, it returns only the instruments linked to the user
   *       if the filter is instrumentscientist, it returns only the instruments linked to the scientist if the user is beamline responsible
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/instrument'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/instruments", auth.requiresSession, investigationController.getInstruments);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   get:
   *     summary: Gets users involved in a investigation
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation/id/:investigationId/investigationusers", auth.requiresSession, investigationController.getInvestigationUserByInvestigation);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   post:
   *     summary: Add users to an investigation. Only allowed to PI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *       - in: body
   *         name: name
   *         description : user's name in the ICAT database
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.post(
    "/catalogue/:sessionId/investigation/id/:investigationId/investigationusers",
    auth.requiresSession,
    auth.allowsPrincipalInvestigatorOrLocalContactOrAdministrator,
    investigationController.addInvestigationUserToInvestigation
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   delete:
   *     summary: Deletes users to an investigation. Only allowed to PI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *       - in: body
   *         name: name
   *         description : user's name in the ICAT database
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.delete(
    "/catalogue/:sessionId/investigation/id/:investigationId/investigationusers",
    auth.requiresSession,
    auth.allowsPrincipalInvestigatorOrLocalContactOrAdministrator,
    investigationController.deleteInvestigationUserToInvestigation
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation:
   *   get:
   *     summary: Get investigations by sessionId
   *     description : |
   *        If ids, it returns the investigations corresponding to these ids.
   *
   *        If filter is embargoed, it returns all investigations which are under embargoed, ie. with releaseDate is after today.
   *        If filter is released, it returns all investigations which are open, ie. with releaseDate is before today and with a DOI.
   *        If filter is instrumentscientist, it returns all investigations where user is instrumentScientitst.
   *        If filter is partipant, it returns all investigations where user has reading permissions.
   *        If filter is industry, it returns all investigations where user has reading permissions and investigations are from industry (IX, FX, IN, IM).
   *        If no filter, it returns all user's investigations.
   *
   *        Instrument allows to filter investigations by instrument name.
   *
   *        StartDate and EndDate allows to filter investigations which occur during this period.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist, released, embargoed, industry]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *       - in: query
   *         name: instrumentName
   *         description : It filters the investigations by instrument name
   *         schema:
   *           type : string
   *       - in: query
   *         name: ids
   *         schema:
   *           type : string
   *         description : list of investigation ids
   *       - in: query
   *         name: startDate
   *         example : 2019-01-01
   *         description : It filters the investigations for which startDate or endDate is after this startDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: endDate
   *         example : 2019-01-31
   *         description : It filters the investigations for which startDate is lower than this endDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: limit
   *         description : limit the number of investigations retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of investigations retreived from the mongo search
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the investigations
   *         schema:
   *           type : string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [NAME|INSTRUMENT|STARTDATE|TITLE|SUMMARY|RELEASEDATE|DOI]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *       - in: query
   *         description: It is the name of the proposal or investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: Comma separated investigation identifiers list
   *         name: ids
   *         schema:
   *           type: string
   *       - in: query
   *         description: It is the time in between the start and end of the time allocated to the experiment
   *         name: time
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/investigation'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation", auth.requiresSession, investigationController.getInvestigationsBySessionId);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/name/{investigationName}/instrument/name/{instrumentName}:
   *   get:
   *     deprecated: true
   *     summary: Gets the investigation allocated to the proposal and beamline in this right instance
   *     parameters:
   *       - $ref: '#/components/parameters/investigationName'
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: time
   *         schema:
   *           type : string
   *         example : 2019-01-01
   *         description : time that will be used to match an investigation
   *     responses:
   *       '200'
   *     tags:
   *       - Catalogue
   */
  app.get(
    "/catalogue/:sessionId/investigation/name/:investigationName/instrument/name/:instrumentName",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    investigationController.getAllocatedInvestigation
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigation:
   *   get:
   *     summary: Get investigation by identifier
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Investigation's identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/investigation'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation/id/:investigationId/investigation", auth.requiresSession, auth.requiresInvestigation, investigationController.getInvestigationById);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigation:
   *   get:
   *     summary: Get investigation by identifier
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Investigation's identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/investigation'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation/id/:investigationId", auth.requiresSession, auth.requiresInvestigation, investigationController.getUnpackedInvestigationById);

  /**
   * @swagger
   * /catalogue/{sessionId}/datacollection:
   *   get:
   *     description: Gets all datacollection for a given sessionId
   *     parameters:
   *       - in: path
   *         description : string with the ICAT's sessionId
   *         name: sessionId
   *         schema :
   *           type : string
   *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/datacollection'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/datacollection", auth.requiresSession, dataCollectionController.getDataCollectionsBySessionId);

  /**
   * @swagger
   * /catalogue/investigation/name/{investigationName}/normalize:
   *   get:
   *     description: Most of users don't write their proposal names using the same convention so it might happen that a given proposal can be written in many different ways. This method will homogenize such name.  Example IH-LS-0001, ihls0001, ih-LS-0001 are normalized like IH-LS-0001.
   *     parameters:
   *       - in: path
   *         description : name of the investigation to normalize
   *         name: investigationName
   *         schema :
   *           type : string
   *         example : ihls0001
   *         required: true
   *     responses:
   *       '200':
   *         description : It returns a string with the normalized proposal name
   *         content:
   *           application/json:
   *             schema :
   *               type : string
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/investigation/name/:investigationName/normalize", investigationController.normalize);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/dataset:
   *   get:
   *     deprecated : true
   *     description: Gets datasets for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - in: query
   *         name: limit
   *         description : limit the number of datasets retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of datasets retreived
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the dataset
   *         schema:
   *           type : string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/investigation/id/:investigationId/dataset", auth.requiresSession, (req, res) => {
    req.query.investigationId = req.params.investigationId;
    return datasetController.getDatasetsByInvestigationId(req, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset:
   *   get:
   *     description: Gets datasets for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *       - in: query
   *         name: limit
   *         description : limit the number of datasets retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of datasets retreived
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the dataset
   *         schema:
   *           type : string
   *       - in: query
   *         description: type of the dataset
   *         name: datasetType
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/dataset", auth.requiresSession, (req, res) => {
    if (req.query.datasetIds) {
      return datasetController.getDatasetsById(req, res);
    }

    /** if investigationId is present then we call getDatasetsByInvestigationId  */
    if (req.query.investigationId) {
      return datasetController.getDatasetsByInvestigationId(req, res);
    }
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/sample:
   *   get:
   *     description: Gets samples for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/investigation/id/:investigationId/sample", auth.requiresSession, sampleController.getSamplesByInvestigationId);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset:
   *   get:
   *     deprecated : true
   *     description: Get a list of dataset from a datasetId list
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetIds'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetIds/dataset", auth.requiresSession, (req, res) => {
    req.query.datasetIds = req.params.datasetIds;
    return datasetController.getDatasetsById(req, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset_document:
   *   get:
   *     description: Get a list of dataset documents from a datasetId list. A dataset document is used for indexing a dataset in elastic search. It unflatters dataset's parameters.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetIds'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset_document'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetIds/dataset_document", auth.requiresSession, datasetController.getDatasetsDocumentsById);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/startdate/{startDate}/enddate/{enddatate}/dataset_document:
   *   get:
   *     description: Get a list of dataset documents by date range. A dataset document is used for indexing a dataset in elastic search. It unflatters dataset's parameters.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDate'
   *       - $ref: '#/components/parameters/endDate'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset_document'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/startdate/:startDate/enddate/:endDate/dataset_document", auth.requiresSession, datasetController.getDatasetsDocumentsByDateRange);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetId}/datacollection:
   *   get:
   *     description: Returns datacollections related to a given dataset along with a datasetId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datacollection'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetId/datacollection", auth.requiresSession, datasetController.getDataColletionByDatasetId);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetIds}/status:
   *   get:
   *     description: Return the IDS status of the data files specified by the datasetIds along with a sessionId.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata catalogue dataset identifier list (comma separated)
   *         name: datasetIds
   *         schema :
   *           type : string
   *         example : 132123,1523433
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               items:
   *                 $ref: '#/components/schemas/status'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetIds/status", auth.requiresSession, datasetController.getStatusByDatasetIds);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetId}/datafile:
   *   get:
   *     description: Get datacollection for a given dataset id
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetId'
   *       - in: query
   *         name: limit
   *         description : limit the number of datafiles retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of datafiles retreived
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the datafiles location
   *         schema:
   *           type : string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetId/datafile", auth.requiresSession, datasfileController.getDatafilesByDatasetId);

  /**
   * @swagger
   * /catalogue/{sessionId}/user:
   *   get:
   *     description: Get user list of ICAT (Restricted to administrators)
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/user", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.getUsers);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/{:investigationId}/user:
   *   get:
   *     description: Get user list of ICAT if the user if PI of the investigaton
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get(
    "/catalogue/:sessionId/investigation/:investigationId/user",
    auth.requiresSession,
    auth.allowsPrincipalInvestigatorOrLocalContactOrAdministrator,
    userController.getUsers
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/user/name/{name}/instrumentscientist:
   *   get:
   *     description: Get instrumentScientist by user name
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : ICAT's user name
   *         name: name
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.get("/catalogue/:sessionId/user/name/:name/instrumentscientist", auth.requiresSession, auth.allowAdministrators, userController.getInstrumentScientistsByUserName);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   get:
   *     description: Get all InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrumentscientist'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.get("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.getInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   put:
   *     description: It creates InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: body
   *         description : Comma separated user names
   *         name: usernames
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Comma separated instrument names
   *         name: instrumentnames
   *         example : "ID01, ID21, BM01"
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrumentscientist'
   *       '403':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.put("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.createInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   delete:
   *     description: It deletes InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: body
   *         description : Comma separated user names
   *         name: usernames
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Comma separated instrumentScientist ids
   *         name: instrumentscientistsid
   *         schema :
   *           type : integer
   *         required: true
   *     responses:
   *       '200':
   *          description: OK
   *       '403':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.delete("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.deleteInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/datasetParameter:
   *   put:
   *     description: Update a value of a dataset parameter
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the parameter
   *         name: parameterId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateParameter'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: string
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.put("/catalogue/:sessionId/datasetParameter", auth.requiresSession, auth.allowAdministrators, (req, res) => {
    return parameterController.updateParameter(req.params.sessionId, req.query.parameterId, req.body.value, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/data/download:
   *   get:
   *     description: Redirects to IDS server to download datasets or datafiles
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the datasets
   *         name: datasetIds
   *         schema:
   *           type: array
   *           items:
   *              $ref: '#/components/schemas/dataset'
   *       - in: query
   *         description: identifier of the datafiles
   *         name: datafileIds
   *         schema:
   *           type: array
   *           items:
   *              $ref: '#/components/schemas/datafile'
   *     responses:
   *       '200':
   *          description : 'Ok'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/data/download", auth.requiresSession, (req, res) => {
    return datasetController.downloadData(req.params.sessionId, req.query.datasetIds, req.query.datafileIds, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/restore:
   *   post:
   *     description: Records dataset restoration request by a user and redirects to IDS server to download datasets or datafiles
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the dataset
   *         name: datasetId
   *         schema:
   *           $ref: '#/components/schemas/dataset'
   *       - in: body
   *         description: identifier of the user
   *         name: name
   *         schema:
   *           type: string
   *       - in: body
   *         description: email of the user (if anonymous)
   *         name: email
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *          description : 'Ok'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.post("/catalogue/:sessionId/dataset/restore", auth.requiresSession, (req, res) => {
    return datasetController.restoreData(req.params.sessionId, req.query.datasetId, req.body.name, req.body.email, res);
  });
};
