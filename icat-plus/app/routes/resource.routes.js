module.exports = (app) => {
  const resource = require("../controllers/resource.controller.js");
  const auth = require("../authentication/icat.js");

  /**
   * @swagger
   * /resource/{sessionId}/file/upload:
   *   post:
   *     summary: Uploads a file to ICAT+
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument (case of beamline logbook)
   *         name: instrumentName
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         description: 'Ok'
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/event'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Resource
   */

  app.post(
    "/resource/:sessionId/file/upload",
    auth.requiresSession,
    (req, res, next) => {
      if (auth.isValidApiKey(req)) {
        return next();
      }
      return auth.allowAccessToInstrumentScientistOrUsers(req, res, next);
    },
    resource.upload
  );

  /**
   * @swagger
   * /resource/{sessionId}/file/id/{fileId}/investigation/id/{investigationId}/download:
   *   get:
   *     deprecated: true
   *     summary: Downloads a resource
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : File's identifier
   *         name: resourceId
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description : 'Ok'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Resource
   */

  app.get("/resource/:sessionId/file/id/:resourceId/investigation/id/:investigationId/download", auth.requiresSession, (req, res) => {
    return resource.download(req.params.resourceId, res);
  });

  /**
   * @swagger
   * /resource/{sessionId}/file/download:
   *   get:
   *     summary: Downloads a resource
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: resourceId
   *         description : File's identifier
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description : 'Ok'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Resource
   */

  app.get("/resource/:sessionId/file/download", auth.requiresSession, (req, res) => {
    return resource.download(req.query.resourceId, res);
  });

  app.get("/resource/test", resource.test);

  app.get("/resource/test", resource.test);
};
