module.exports = (app) => {
  const controller = require("../controllers/elasticsearch.controller.js");
  const auth = require("../authentication/icat.js");

  /**
   * @swagger
   * /elasticsearch/{sessionId}/datasets/_msearch:
   *   get:
   *     summary: Proxy to elastic search server
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *     tags:
   *       - Elastic Search
   */
  app.post("/elasticsearch/:sessionId/datasets/_msearch", auth.isElasticSearchEnabled, auth.requiresSession, controller.msearch);

  /**
   * @swagger
   * /elasticsearch/{sessionId}/stats/instrument/metrics:
   *   get:
   *     summary: Given an array of dates it returns the statistics for each instrument
   *     description: metrics are avg_volume, sum_volume, avg_fileCount, sum_fileCount, datasetCount, avg_metadata, max_metadata, min_metadata
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: instrumentName
   *         schema:
   *           type : string
   *         example : id16ani
   *         description : Filter by instrumentName or match all if empty
   *       - in: body
   *         name: ranges
   *         type: array
   *         required: true
   *         description: Array of date ranges
   *         items :
   *           type : object
   *           properties :
   *             name :
   *               type : string
   *             start :
   *               type : string
   *             end :
   *               type : string
   *       - in: query
   *         name: openData
   *         schema:
   *           type : boolean
   *         example : true
   *         description : If openData then the statistics will be calculated based on the datasets with a raw DOI which embargo period has expired
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *     tags:
   *       - Elastic Search
   */
  app.post(
    "/elasticsearch/:sessionId/stats/instrument/metrics",
    auth.requiresSession,
    auth.isElasticSearchEnabled,
    auth.allowsInstrumentScientistsOrAdministrators,
    controller.getInstrumentStatsByDate
  );

  /**
   * @swagger
   * /elasticsearch/{sessionId}/stats/histogram:
   *   get:
   *     summary: Gets time series statistics. Only administrator and instrumentScientists are allowed
   *     description:
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: format
   *         schema:
   *           type : string
   *           enum: [csv, json, csv_json]
   *         example : csv
   *         description : Output format. Default is json. csv_json returns a json with the embeded csv
   *       - in: query
   *         name: metric
   *         schema:
   *           type : string
   *           enum: [volume, fileCount]
   *         example : volume
   *         description : Metric that will be returned
   *       - in: query
   *         name: aggregation
   *         schema:
   *           type : string
   *           enum: [instrumentName, definition]
   *         example : instrumentName
   *         description : Parameter which data will be aggregated. Default is instrumentName
   *       - in: query
   *         name: interval
   *         schema:
   *           type : string
   *         example : 3d
   *         description : Fixed interval used to group statistics in the time series.
   *       - in: query
   *         name: startDate
   *         schema:
   *           type : string
   *         example : 2021-04-31
   *         description : Start of the time series
   *       - in: query
   *         name: endDate
   *         schema:
   *           type : string
   *         example : 2021-04-31
   *         description : End of the time series
   *       - in: query
   *         name: instrumentName
   *         schema:
   *           type : string
   *         example : id16ani
   *         description : Filter by instrumentName or match all if empty
   *     responses:
   *         '200':
   *           description: OK
   *     tags:
   *       - Elastic Search
   */
  app.get("/elasticsearch/:sessionId/stats/histogram", auth.requiresSession, auth.isElasticSearchEnabled, auth.allowsInstrumentScientistsOrAdministrators, controller.getHistogram);
};
