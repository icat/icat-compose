const { sendError } = require("../errors.js");

module.exports = (app) => {
  const controller = require("../controllers/panosc.controller.js");
  app.get("/api/Datasets", controller.request);
  app.get("/api/Datasets/:id", controller.request);
  app.get("/api/Datasets/:id/files", controller.request);
  app.get("/api/Datasets/:id/count", controller.request);
  app.get("/api/Datasets/count", controller.request);
  app.get("/api/Documents", controller.request);
  app.get("/api/Documents/:id", controller.request);
  app.get("/api/Documents/count", controller.request);
  app.get("/api/Instruments", controller.request);
  app.get("/api/Instruments/:id", controller.request);
  app.get("/api/Instruments/count", controller.request);

  app.get("/api/db", (req, res) => {
    if (req.query.investigationId) {
      return controller.getByInvestigationId(req.query.investigationId, res);
    }
    return controller.getDB(req, res);
  });

  function redirectToHoa(req, res, next) {
    req.params.server = global.gServerConfig.panosc.humanOrgans.server.url;
    req.params.port = global.gServerConfig.panosc.humanOrgans.server.port;
    next();
  }
  app.get("/api/hoa/Datasets", redirectToHoa, controller.request);
  app.get("/api/hoa/Datasets/:id", redirectToHoa, controller.request);
  app.get("/api/hoa/Datasets/:id/files", redirectToHoa, controller.request);
  app.get("/api/hoa/Datasets/:id/count", redirectToHoa, controller.request);
  app.get("/api/hoa/Datasets/count", redirectToHoa, controller.request);
  app.get("/api/hoa/Documents", redirectToHoa, controller.request);
  app.get("/api/hoa/Documents/:id", redirectToHoa, controller.request);
  app.get("/api/hoa/Documents/count", redirectToHoa, controller.request);
  app.get("/api/hoa/Instruments", redirectToHoa, controller.request);
  app.get("/api/hoa/Instruments/:id", redirectToHoa, controller.request);
  app.get("/api/hoa/Instruments/count", redirectToHoa, controller.request);

  /**
   * @swagger
   * /api/scoring/items:
   *   get:
   *     summary: Gets public datasets by date and then it parses into items for the scoring database
   *     parameters:
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *     responses:
   *       '200':
   *         description: 'Returns a scoring item object'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Panosc
   */
  app.get("/api/scoring/items", async (req, res) => {
    try {
      const { startDate, endDate } = req.query;
      res.send(await controller.getItemsByDate(startDate, endDate));
    } catch (e) {
      sendError(500, "Failed to getItemsByDate", e, res);
    }
  });
};
