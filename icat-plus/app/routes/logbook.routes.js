module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const logbook = require("../controllers/logbook.controller.js");
  const tag = require("../controllers/tag.controller.js");

  const allowAccessToInstrumentScientistOrUsersOrReleased = (req, res, next) => {
    /** This allows to implement allowing the access to beamline scientist to the logbook of the beamline */
    if (!req.query.investigationId && req.query.instrumentName) {
      return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
    }
    return auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser(req, res, next);
  };

  /**
   * @swagger
   * /logbook/{sessionId}/event/createfrombase64:
   *   post:
   *     summary: Create an event given a base64 string
   *     description : this is used mainly when uploading a photo from the mobile device. It will create a new event (in an investigation or in a beamline logbook) that will contain the image.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: body
   *         description: String with the image encoded in base64. It is the full base64 with the data:image/type;<base64>
   *         name: base64
   *         schema:
   *           type: string
   *           example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/event/createfrombase64", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, (req, res) => {
    const { base64, datasetId, software, machine } = req.body;
    const { username, fullName } = req.session;
    return logbook.createfrombase64(req.params.sessionId, req.query.investigationId, req.query.instrumentName, username, fullName, base64, datasetId, software, machine, res);
  });

  /**
   * @swagger
   * /logbook/{apiKey}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event/createfrombase64:
   *   post:
   *     deprecated: true
   *     summary: Create an event given a base64 string. It requires the API token
   *     parameters:
   *       - $ref: '#/components/parameters/investigationName'
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: body
   *         description : Comma separated user names
   *         name: usernames
   *         schema :
   *           type : string
   *         required: true
   *
   *       - in: body
   *         name: base64
   *         description: String with the image encoded in base64
   *         required: true
   *         schema:
   *           type: string
   *           example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post(
    "/logbook/:apiKey/investigation/name/:investigationName/instrument/name/:instrumentName/event/createfrombase64",
    auth.requiresApiKey,
    logbook.createfrombase64ByInvestigationName
  );

  /**
   * @swagger
   * /logbook/{sessionId}/investigation/id/{investigationId}/event/create:
   *   post:
   *     summary: Create an event
   *     deprecated: true
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post(
    "/logbook/:sessionId/investigation/id/:investigationId/event/create",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    logbook.createEvent
  );

  /**
   * @swagger
   * /logbook/{apiKey}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event:
   *   post:
   *     deprecated : true
   *     summary: Use /logbook/sessionId/notification instead
   *     parameters:
   *       - $ref: '#/components/parameters/investigationName'
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/apiKey'
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:apiKey/investigation/name/:investigationName/instrument/name/:instrumentName/event", auth.requiresApiKey, logbook.createInvestigationNotification);

  /**
   * @swagger
   * /logbook/{apiKey}/notification:
   *   post:
   *     deprecated : true
   *     summary: Method used to create a notification (for a investigation or beamline logbook)
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post(
    "/logbook/:apiKey/notification",
    auth.requiresApiKey,
    (req, res, next) => {
      /** This allows to use createInvestigationNotification with the former deprecated methods which use path params */
      req.params.investigationName = req.query.investigationName;
      req.params.instrumentName = req.query.instrumentName;
      next();
    },
    (req, res) => {
      /** This allows to use createInvestigationNotification with the former deprecated methods */
      if (req.params.investigationName) return logbook.createInvestigationNotification(req, res);
      return logbook.createBeamlineNotification(req, res);
    }
  );

  /**
   * @swagger
   * /logbook/{apiKey}/instrument/name/{instrumentName}/event:
   *   post:
   *     deprecated : true
   *     summary: Method used to create a beamline logbook entry with the API token
   *     parameters:
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/apiKey'
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:apiKey/instrument/name/:instrumentName/event", auth.requiresApiKey, logbook.createBeamlineNotification);

  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   get:
   *     summary: Lists events
   *     parameters:
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *       - in: query
   *         description: skips a number of events from the mongo search result
   *         name: skip
   *         schema:
   *           type: string
   *       - in: query
   *         description: limit the number of events retrieved
   *         name: limit
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - in: query
   *         description: word to be searched in the events
   *         name: search
   *         schema:
   *           type: string
   *       - in: query
   *         description: format of the returned events, json by default
   *         name: format
   *         schema:
   *           type: string
   *           enum: [json, pdf]
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/events'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get(
    "/logbook/:sessionId/event",
    auth.requiresSession,
    (req, res, next) => {
      if (!req.query.investigationId && !req.query.instrumentName) return auth.allowAdministrators(req, res, next);
      if (!req.query.investigationId) return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
      /** just in case error in the code downstream  a user can not filter investigation in the investigation logbook*/
      req.query.filterInvestigation = false;
      return allowAccessToInstrumentScientistOrUsersOrReleased(req, res, next);
    },
    logbook.getEvents
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/count:
   *   get:
   *     summary: Count events
   *     parameters:
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - in: query
   *         description: word to be searched in the events
   *         name: search
   *         schema:
   *           type: string
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/totalNumber'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get(
    "/logbook/:sessionId/event/count",
    auth.requiresSession,
    (req, res, next) => {
      if (!req.query.investigationId && !req.query.instrumentName) return auth.allowAdministrators(req, res, next);
      if (!req.query.investigationId) return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
      /** just in case error in the code downstream  a user can not filter investigation in the investigation logbook*/
      req.query.filterInvestigation = false;
      return allowAccessToInstrumentScientistOrUsersOrReleased(req, res, next);
    },
    logbook.countEvents
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/page:
   *   get:
   *     summary: Lists events
   *     parameters:
   *       - in: query
   *         description: event identifier
   *         name: _id
   *         schema:
   *           type: string
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *       - in: query
   *         description: skips a number of events from the mongo search result
   *         name: skip
   *         schema:
   *           type: string
   *       - in: query
   *         description: limit the number of events retrieved
   *         name: limit
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - in: query
   *         description: word to be searched in the events
   *         name: search
   *         schema:
   *           type: string
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/events'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/event/page", auth.requiresSession, allowAccessToInstrumentScientistOrUsersOrReleased, logbook.getPageByEventId);

  /**
   * @swagger
   * /logbook/{sessionId}/event/dates:
   *   get:
   *     summary: Lists events dates
   *     parameters:
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - in: query
   *         description: word to be searched in the events
   *         name: search
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/dates'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/event/dates", auth.requiresSession, allowAccessToInstrumentScientistOrUsersOrReleased, logbook.findAllDatesByEvents);

  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   post:
   *     summary: Creates an event
   *     parameters:
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/event", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, logbook.createEvent);

  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   put:
   *     summary: Updates an event
   *     parameters:
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToUpdate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put("/logbook/:sessionId/event", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, logbook.updateEvent);

  /**
   * @swagger
   * /logbook/{sessionId}/stats/investigation:
   *   get:
   *     summary: Lists statistics on events for each investigation that are allocated in the date range
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDate'
   *       - $ref: '#/components/parameters/endDate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/logbookInvestigationStats'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/stats/investigation", auth.requiresSession, auth.allowAdministrators, logbook.getEventsStatistics);

  /**
   * @swagger
   * /logbook/{sessionId}/stats/count:
   *   get:
   *     summary: Counts number of event for date range
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDate'
   *       - $ref: '#/components/parameters/endDate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/logbookInvestigationStatsCount'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/stats/count", auth.requiresSession, auth.allowAdministrators, logbook.getEventsCount);

  /**
   * @swagger
   * /logbook/{sessionId}/move/event:
   *   post:
   *     summary:
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: investigationId of the source event
   *         name: sourceInvestigationId
   *         schema:
   *           type: number
   *       - in: query
   *         description: investigationId of the destination event
   *         name: destinationInvestigationId
   *         schema:
   *           type: number
   *     responses:
   *       '200':
   *         description: Events moved successfully
   *         $ref: '#/components/responses/modifiedObject'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/event/move", auth.requiresSession, auth.allowAdministrators, logbook.moveEvents);

  /**
   * @swagger
   * /logbook/{sessionId}/tag:
   *   get:
   *     summary: GET all tags available, (ie used in the investigation or not) for a given logbook including global, beamline specific and investigation specific tags. Tags available but not used are also retrieved.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/tags'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/tag", auth.requiresSession, allowAccessToInstrumentScientistOrUsersOrReleased, tag.getTags);

  /**
   * @swagger
   * /logbook/:sessionId/tag:
   *   post:
   *     summary: Create an investigation tag for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/createTag'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/tagCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/tag", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, tag.createTag);

  /**
   * @swagger
   * /logbook/{sessionId}/tag:
   *   put:
   *     summary: Update an investigation tag
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/tagId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateTag'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/tagUpdated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put("/logbook/:sessionId/tag", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, tag.updateTag);
};
