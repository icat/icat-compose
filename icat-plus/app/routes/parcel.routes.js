module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const parcelController = require("../controllers/parcel.controller.js");
  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment/id/{shipmentId}/parcel:
   *   get:
   *     summary: Lists parcels associated to an shipment
   *     description : Lists parcels associated to an shipment. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/parcels'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.getParcelsByShipmentId
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}:
   *   get:
   *     summary: Get parcel by id
   *     description : It returns a parcel by Id.
   *     parameters:
   *       - $ref: '#/components/parameters/parcelId'
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/parcel'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.getParcelById
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/labels:
   *   get:
   *     summary: Returns a file with the labels
   *     description : It triggers the download of a PDF file with the labels. This method is only available when the status is APPROVED.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: 'Return a PDF file with the labels'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/labels",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.getLabels
  );

  /**
   * @swagger
   * /tracking/{sessionId}/parcel/status/{status}:
   *   get:
   *     summary: Returns a list of parcels by status
   *     description : Returns a list of parcels for all investigations find by status. Only for administrators, safety and stores.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/parcels'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get("/tracking/:sessionId/parcel/status/:status", auth.requiresSession, auth.allowAdministrators, parcelController.getParcelsByStatus);

  /**
   * @swagger
   * /tracking/{sessionId}/parcel:
   *   get:
   *     summary: Returns a list of parcels by sessionId.
   *     description : Returns a list of parcels. It returns all parcels for administrators and only the parcels associated to My Data for others
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/parcels'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/parcel",
    (req, res, next) => {
      global.gLogger.info("getParcelBySessionId auth init");
      next();
    },
    auth.requiresSession,
    (req, res, next) => {
      global.gLogger.info("getParcelBySessionId auth finish");
      next();
    },
    parcelController.getParcelBySessionId
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment/id/{shipmentId}/parcel:
   *   post:
   *     summary: Creates a parcel associated to an shipment
   *     description : Creates a parcel associated to an parcel. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.createParcel
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment/id/{shipmentId}/parcel:
   *   put:
   *     summary: Modifies a parcel
   *     description : Modifies an existing parcel. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.updateParcel
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/:investigationId/parcel/id/{parcelId}/parcel/status/{status}:
   *   put:
   *     summary: Modifies a the status of a parcel
   *     description : The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/status/:status",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.setParcelStatus
  );

  /**
   * @swagger
   * /tracking/{sessionId}/shipment/id/{shipmentId}/parcel:
   *   delete:
   *     summary: Deletes a parcel
   *     description : Deletes an existing parcel. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/parcel",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.deleteParcel
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/shipment/id/{shipmentId}/investigation/id/{toInvestigationId}/parcel:
   *   put:
   *     summary: Moves parcels to another investigations.
   *     description : Shipment can be prepared even if a session is still not scheduled. Then shipmnent is associated to an investigation that represents a proposal an once the session is scheduled then the parcels can be transferred to the investigation associated to the session
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *       - $ref: '#/components/parameters/toInvestigationId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcelIdList'
   *     responses:
   *       '200':
   *         description : It returns the new shipment where the parcels have been transferred
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/responses/shipment'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/shipment/id/:shipmentId/investigation/id/:toInvestigationId/parcel",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.transfer
  );
};
