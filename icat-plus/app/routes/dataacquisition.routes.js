module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const logbook = require("../controllers/logbook.controller.js");
  const investigationController = require("../controllers/investigations.controller.js");
  const datasetController = require("../controllers/datasets.controller.js");
  const icat = require("../api/icat.js");
  const { ERROR, sendError } = require("../errors.js");
  const doiController = require("../controllers/doi.controller.js");
  const { getSession } = require("../authentication/helper.authentication.js");

  const datasetAccessController = require("../controllers/datasetaccess.controller.js");

  const connectAsDataAcquisitionUser = async (req, res, next) => {
    try {
      const dataAcquisitionUserSession = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
      req.params.sessionId = dataAcquisitionUserSession.sessionId;
      return next();
    } catch {
      global.gLogger.error(`connectAsDataAcquisitionUser ${global.gServerConfig.icat.authorizations.dataacquisition.dataacquisition.credentials[0].username} failed`);
      return sendError(ERROR.LOGIN_FAILED.code, ERROR.LOGIN_FAILED.message, ERROR.LOGIN_FAILED, res);
    }
  };

  /**
   * This allows to use createInvestigationNotification with the former deprecated methods which use path params
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  const supportDeprecatedMethods = (req, res, next) => {
    req.params.investigationName = req.query.investigationName;
    req.params.instrumentName = req.query.instrumentName;
    req.params.investigationId = req.query.investigationId;
    next();
  };

  /**
   * @swagger
   * /dataacquisition/{apiKey}/mint:
   *   post:
   *     summary: It mints a DOI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: body
   *         description : Title of the DOI
   *         name: title
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Abstact of the DOI
   *         name: abstract
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Comma separated list of authors
   *         name: authors
   *         schema :
   *           type : string
   *         required: true
   *       - in: body
   *         description : Array with the datasetIds
   *         name: datasetIdList
   *         schema :
   *           type : Array
   *         required: true
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/responses/doi'
   *       '400':
   *         description: 'No description'
   *       '500':
   *         description: 'No description'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/mint", auth.requiresApiKey, async (req, res) => {
    try {
      let { sessionId } = req.params;
      const { skip } = req.query;
      const { title, abstract, authors } = req.body;
      let { datasetIdList } = req.body;

      /** it might happens that it arrives as string */
      if (!Array.isArray(datasetIdList)) {
        datasetIdList = JSON.parse(datasetIdList);
      }

      global.gLogger.info("Mint", { title, abstract, authors, datasetIdList });

      const authorsList = authors.split(",").map((a) => {
        return { creatorName: a };
      });

      /** If sessionId is the key then we log as minter */
      const data = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
      req.params.sessionId = data.sessionId;
      sessionId = data.sessionId;
      req.session = await getSession(req);

      if (req.session.isMinter) {
        const investigations = await icat.getInvestigationsByDatasetListIdsURL(sessionId, datasetIdList);
        doiController.mint(sessionId, investigations, req.session.name, req.session.fullName, datasetIdList, title, abstract, authorsList, skip, res);
      }
    } catch (e) {
      global.gLogger.error(e);
      return sendError(ERROR.NO_PARTICIPANT.code, ERROR.NO_PARTICIPANT.message, new Error(ERROR.NO_PARTICIPANT.message), res);
    }
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/base64:
   *   post:
   *     summary: Uploads an image in base64 format to the investigation electronic logbook
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: proposal's name
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: body
   *         description : Comma separated user names
   *         name: usernames
   *         schema :
   *           type : string
   *         required: true
   *
   *       - in: body
   *         name: base64
   *         description: String with the image encoded in base64
   *         required: true
   *         schema:
   *           type: string
   *           example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/base64", auth.requiresApiKey, supportDeprecatedMethods, logbook.createfrombase64ByInvestigationName);

  /**
   * @swagger
   * /dataacquisition/{apiKey}/notification:
   *   post:
   *     summary: Adds an event to the logbook (for an investigation or beamline logbook). It could also be a machine event
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: name of the investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/notification", auth.requiresApiKey, supportDeprecatedMethods, (req, res) => {
    /** This allows to use createInvestigationNotification with the former deprecated methods */
    if (req.params.investigationName) return logbook.createInvestigationNotification(req, res);
    if (req.params.instrumentName) return logbook.createBeamlineNotification(req, res);
    return logbook.createBroadcastNotification(req, res);
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/investigation:
   *   get:
   *     summary: Get investigations
   *     description : |
   *        If ids, it returns the investigations corresponding to these ids.
   *        If filter is embargoed, it returns all investigations which are under embargoed, ie. with releaseDate is after today.
   *        If filter is released, it returns all investigations which are open, ie. with releaseDate is before today and with a DOI.
   *        If filter is instrumentscientist, it returns all investigations where user is instrumentScientitst.
   *        If filter is partipant, it returns all investigations where user has reading permissions.
   *        If filter is industry, it returns all investigations where user has reading permissions and investigations are from industry (IX, FX, IN, IM).
   *        If no filter, it returns all user's investigations.
   *
   *        Instrument allows to filter investigations by instrument name.
   *
   *        StartDate and EndDate allows to filter investigations which occur during this period.
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist, released, embargoed, industry]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *       - in: query
   *         name: instrumentName
   *         description : It filters the investigations by instrument name
   *         schema:
   *           type : string
   *       - in: query
   *         name: ids
   *         schema:
   *           type : string
   *         description : list of investigation ids
   *       - in: query
   *         name: startDate
   *         example : 2019-01-01
   *         description : It filters the investigations for which startDate or endDate is after this startDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: endDate
   *         example : 2019-01-31
   *         description : It filters the investigations for which startDate is lower than this endDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: limit
   *         description : limit the number of investigations retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of investigations retreived from the mongo search
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the investigations
   *         schema:
   *           type : string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [NAME|INSTRUMENT|STARTDATE|TITLE|SUMMARY|RELEASEDATE|DOI]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *       - in: query
   *         description: It is the name of the proposal or investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: Comma separated investigation identifiers list
   *         name: ids
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               $ref: '#/components/schemas/investigation'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *     tags:
   *       - DataAcquisition
   */
  app.get("/dataacquisition/:apiKey/investigation", auth.requiresApiKey, connectAsDataAcquisitionUser, auth.requiresSession, investigationController.getInvestigationsBySessionId);

  /**
   * @swagger
   * /dataacquisition/{apiKey}/dataset:
   *   get:
   *     summary: Gets datasets
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         name: investigationId
   *         description : Identifier of the investigation
   *         schema:
   *           type : string
   *       - in: query
   *         name: limit
   *         description : limit the number of datasets retrieved
   *         schema:
   *           type : string
   *       - in: query
   *         name: skip
   *         description : skip a number of datasets retreived
   *         schema:
   *           type : string
   *       - in: query
   *         name: search
   *         description : word to be searched in the dataset
   *         schema:
   *           type : string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - in: query
   *         description: order applied to the sorting (-1 or 1) (ascending or descending)
   *         name: sortOrder
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */

  app.get(
    "/dataacquisition/:apiKey/dataset",
    auth.requiresApiKey,
    connectAsDataAcquisitionUser,
    auth.requiresSession,
    supportDeprecatedMethods,
    datasetController.getDatasetsByInvestigationId
  );
  /**
   * @swagger
   * /dataacquisition/{apiKey}/dataset/restore:
   *   post:
   *     summary: Update the restore status of a dataset access and send an email to the users who asked for the restoration
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: datasetId
   *         name: datasetId
   *         schema:
   *           type: number
   *       - in: body
   *         description: message
   *         name: message
   *         schema:
   *           type: string
   *       - in: body
   *         schema:
   *           type : string
   *           enum: [info, warning, error]
   *         example : level
   *         description : Level
   *     requestBody:
   *       $ref: '#/components/requestBodies/restoreInfo'
   *     responses:
   *       '200':
   *         $ref: '#/components/responses/datasetaccesses'
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/dataset/restore", auth.requiresApiKey, (req, res) => {
    return datasetAccessController.updateRestoreStatus(req.query.datasetId, req.body.level, req.body.message, res);
  });
};
