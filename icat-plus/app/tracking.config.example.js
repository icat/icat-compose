const STATUS = require("./models/status.js");
/**
 * sample tracking configuration file
 */
module.exports = {
  qrCode: {
    server: "https://data.esrf.fr",
  },
  /** Address of the facility where the experiments will be conducted */
  facilityAddress: {
    name: "ESRF Stores",
    surname: "",
    address: "71 avenue des Martyrs",
    city: "Grenoble",
    postalCode: "38000",
    email: "",
    phoneNumber: "+33 (0)4 76 88 2733",
    fax: "+33 (0)4 76 88 2347",
    country: "France",
  },

  notifications: {
    enabled: true,
    fromAddress: "sampletrackingrequests@esrf.fr",
    smtp: {
      host: "smtp.esrf.fr",
      port: 25,
    },
    /** All emails will be sent to this address */
    broadcastEmail: "sampletracking@esrf.fr",
    mailingProposals: ["IN", "IM"],
    /**
     * statuses describe to whom the emails should be sent for each different status of a parcel
     * isMailingInvestigation : when IM/IN
     */
    statuses: {
      [STATUS.CREATED]: {
        roles: [],
        emails: [],
        isMailingInvestigation: [],
        containsDangerousGoods: [],
      },
      [STATUS.SCHEDULED]: {
        roles: [],
        emails: [],
        isMailingInvestigation: [],
        containsDangerousGoods: [],
      },
      [STATUS.READY]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: ["expsaf@esrf.fr"],
        isMailingInvestigation: [],
        containsDangerousGoods: [],
      },
      [STATUS.APPROVED]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: ["esrftransportdouanes@esrf.fr"],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: ["expsaf@esrf.fr"],
      },
      [STATUS.REFUSED]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: [],
      },
      [STATUS.INPUT]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: [],
      },
      [STATUS.SENT]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: ["esrftransportdouanes@esrf.fr"],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: ["expsaf@esrf.fr"],
      },
      [STATUS.STORES]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: ["expsaf@esrf.fr"],
      },
      [STATUS.BEAMLINE]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: [],
      },
      [STATUS.BACK_STORES]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: ["expsaf@esrf.fr"],
      },
      [STATUS.BACK_USER]: {
        roles: ["Participant", "Local contact", "Scientist", "Proposal scientist"],
        emails: [],
        isMailingInvestigation: ["industry@esrf.eu"],
        containsDangerousGoods: [],
      },
      [STATUS.DESTROYED]: {
        roles: [],
        emails: [],
        isMailingInvestigation: [],
        containsDangerousGoods: [],
      },
      [STATUS.REMOVED]: {
        roles: [],
        emails: [],
        isMailingInvestigation: [],
        containsDangerousGoods: [],
      },
    },
  },
};
