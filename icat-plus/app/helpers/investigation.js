const { getInvestigationTypes } = require("../cache/cache.js");

exports.normalize = (investigationName) => {
  /** Get the proposal type */
  const investigationTypes = getInvestigationTypes();
  let normalizedInvestigationType = null;
  let normalizedInvestigationNumber = null;

  for (let i = 0; i < investigationTypes.length; i++) {
    const investigationTypeName = investigationTypes[i].InvestigationType.name;
    /** replacing - */

    const investigationNameClear = investigationTypeName.replace(new RegExp("-", "g"), "").toUpperCase();
    investigationName = investigationName.replace(new RegExp("-", "g"), "").toUpperCase();

    if (investigationName.toUpperCase().startsWith(investigationNameClear)) {
      normalizedInvestigationType = investigationTypeName;
      normalizedInvestigationNumber = investigationName.toUpperCase().replace(investigationNameClear, "").replace(new RegExp("-", "g"), "");
    }
  }
  if (normalizedInvestigationType == null || normalizedInvestigationNumber == null) {
    global.gLogger.error("Investigation name can not be normalized. ", {
      investigationName,
      normalizedInvestigationType,
      normalizedInvestigationNumber,
    });
    throw `Investigation ${investigationName} name can not be normalized`;
  }

  /** ID are testing investigations and does not follow same format **/
  if (normalizedInvestigationType.toUpperCase().startsWith("ID")) {
    return investigationName;
  }

  /** ID are testing investigations and does not follow same format **/
  if (normalizedInvestigationType.toUpperCase().startsWith("BM")) {
    return investigationName;
  }

  return `${normalizedInvestigationType}-${normalizedInvestigationNumber}`;
};
