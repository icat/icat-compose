/**
 * restore email configuration file
 */
module.exports = {
  enabled: true,
  fromAddress: "restorerequests@esrf.fr",
  subject: "[ESRF Data Restoration] Dataset restoration request",
  smtp: {
    host: "smtp.esrf.fr",
    port: 25,
  },
};
