/**
 * ICAT+ configuration file
 */
/** @module confguration */
module.exports = {
  /** ICAT+ server configuration section */
  server: {
    /** HTTP port used by ICAT+ */
    port: 8000,
    /** ICAT+ URL. Used by the PDF generator and SWAGGER */
    url: "http://icatplus.server",
    /** API_KEY to short circuit default authentication mechanism. Distribute this key to trusted clients. */
    API_KEY: "[API ALLOWS TO STORE NOTIFICATIONS IN THE LOGBOOK WITH NO AUTH]",
    /** Temporal folder used by the application in case of need for writting files. Example: create base64 images*/
    temporalFolder: "/tmp"
  },

  /** Database configuration section */
  database: {
    /** URI to connect to mongoDB */
    uri: "mongodb://MONGO_USER:MONGO_PASSWORD@mongo:27017/icatplus?authSource=admin"
  },

  panosc: {
    /** If not enabled then operation will not be run **/
    search: {
      enabled: true,
      server: {
        /** Server that actually implements search API and requests will be transfered to */
        url: "http://111.111.11.11",
        port: 3000
      }
    },
    humanOrgans: {
      enabled: true,
      server: {
        /** Server that actually implements search API and requests will be transfered to */
        url: "http://111.11.11.11",
        port: 3000
      }
    }
  },

  /** OAI-PMH Section */
  oaipmh: {
    /** If not enabled then operation will not be run **/
    enabled: true,
    server: {
      /** Server that actually implements OAI-PMH and requests will be transfered to */
      url: "http://localhost",
      port: 8080
    }
  },
  /** H5Grove (HDF5 reader) */
  h5grove: {
    /** If not enabled then operation will not be run **/
    enabled: true,
    server: {
      /** Server that actually implements OAI-PMH and requests will be transfered to */
      url: "http://111.11.11.11:8890"
    }
  },

  /** Elastic search configuration section */
  elasticsearch: {
    /** If enabled elastic search will send queries to the server */
    enabled: true,
    /** Server and pord where elasticsearch is running */
    server: "http://[SERVER]:[PORT]"
  },
  /** Logging configuration section */
  logging: {
    /** Logging to graylog */
    graylog: {
      /** If TRUE enables the sending of logs to graylog server */
      enabled: false,
      /** Graylog server URL */
      host: "graylog-dau.esrf.fr",
      /** Source of the logs */
      hostname: "ICAT+",
      /** HTTP port to use on the host side where graylog server is listening */
      port: 12205,
      /** Facility */
      facility: "ESRF"
    },
    /** Logging to the terminal */
    console: {
      /** Level of verbosity. Use '0' for no logs ; Use 'silly' to see all logs */
      level: "silly"
    },
    /** Logging to file */
    file: {
      /** If TRUE, logs are written to a file */
      enabled: true,
      /** Absolute filepath where the logs will be stored */
      filename: "/tmp/server.log",
      /** Verbosity level. See logging/console/level for details*/
      level: "silly"
    }
  },

  /** Datacite configuration section. Datacite is an organization which creates DOIs (Digital Object Identifiers).
   *  We use this third-party services to create DOI for our data. More info [here](https://www.datacite.org/)*/
  datacite: {
    /** DOI prefix for your institution */
    prefix: "*****",
    /** Fixed part in the DOI suffix. */
    suffix: "*****",
    /** Username used for the authentication on datacite services */
    username: "*****",
    /** Password used for authentication on datacite services */
    password: "*****",
    /** Datacite metadata store (MDS) URL */
    mds: "https://mds.datacite.org",
    /** Common part of the URL shared by all DOI landing pages */
    landingPage: "https://doi.esrf.fr/",
    /** Proxi settings */
    proxy: {
      host: "proxy.esrf.fr",
      port: "3128"
    }
  },
  /** ICAT data service configuration section */
  ids: {
    /** IDS server URL */
    server: "http://[SERVER]:[PORT]"
  },
  /** ICAT metadata catalogue configuration section */
  icat: {
    /** ICAT server URL */
    server: "http://icat:8080",
    /** Maximum number of events that can be returned from the elogbook by a single HTTP request */
    maxQueryLimit: 10000,
    /** Authorization management section*/
    authorizations: {
      adminUsersReader: {
        /** Name of the group for users with special privileges (usually the admin group) */
        administrationGroups: ["admin"],
        /**
         * User is capable to get the list of users that are administrators
         * User needs access to tables user and userGroup in order to know who are administrators
         */
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "root" }, { password: "root" }]
        }
      },
      /**
       * This user is allowed to mint DOI's and needs some permission on ICAT tables
       * User has the permission to create and read datacollections in the ICAT database.
       */
      minting: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "root" }, { password: "root" }]
        }
      },
      /**
       * This user can read the list of parameter types from ICAT
       */
      parameterListReader: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "root" }, { password: "root" }]
        }
      },
      /**
       * This user can read the list of investigations from ICAT
       */
      investigationReader: {
        user: {
          plugin: "db",
          credentials: [{ username: "root" }, { password: "root" }]
        }
      },
      /**
       * notifier needs to get access to all investigations as it will convert investigationName and beamline into investigationId
       */
      notifier: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "root" }, { password: "root" }]
        }
      }
    },
    anonymous: {
      /** Authentication mechanism used by ICAT server */
      plugin: "db",
      credentials: [{ username: "root" }, { password: "root" }]
    }
  },
  dmp: {
    enabled: true,
    server: "https://[SERVER]",
    credentials: { email: "root", password: "root" }
  }
};
