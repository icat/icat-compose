/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     parcelIdList:
 *       description: List of [_id]
 *     shipment:
 *       description: shipment
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/shipment'
 *     parcels:
 *       description: parcel
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parcels'
 *     parcel:
 *       description: parcel
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parcel'
 *     item:
 *       description: item
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/item'
 */
