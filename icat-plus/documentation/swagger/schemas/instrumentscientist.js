/**
 * @swagger
 *
 * components:
 *   schemas:
 *     instrumentscientist:
 *       required:
 *         - "name"
 *       properties:
 *         user:
 *           type : "object"
 *           required:
 *             - "name"
 *           properties:
 *             name:
 *               type: "string"
 *             fullName:
 *               type: "string"
 *         id:
 *           type: "string"
 *         modId:
 *           type: "string"
 *         createId:
 *           type: "string"
 *         mod:
 *           type : "object"
 *           properties:
 *             time:
 *               type: "string"
 *               format : "date-time"
 *             user:
 *               type : "object"
 *               required:
 *                 - "name"
 *               properties:
 *                 name:
 *                   type: "string"
 *                 fullName:
 *                   type: "string"
 *         instrument:
 *           required:
 *             - "name"
 *           properties:
 *             name:
 *               type: "string"
 *             description:
 *               type: "string"
 */

/**
name: element.InstrumentScientist.instrument.name,
modId: element.InstrumentScientist.modId,
mod: {
  time: element.InstrumentScientist.modTime,
  modId: element.InstrumentScientist.modId,
  user: modUser ? {
    fullName: modUser ? modUser.fullName : "",
    name: element.InstrumentScientist.modId
  } : null
},
user: {
  fullName: element.InstrumentScientist.user.fullName,
  name: element.InstrumentScientist.user.name
},
instrument: {
  name: element.InstrumentScientist.instrument.name,
  description: element.InstrumentScientist.instrument.description
}
**/
