/**
 * @swagger
 *
 * components:
 *   schemas:
 *     parameter:
 *       description: a dataset parameter
 *       type: object
 *       required:
 *         - value
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         value:
 *           type: string
 */
