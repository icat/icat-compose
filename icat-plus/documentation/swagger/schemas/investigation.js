/**
 * @swagger
 *
 * components:
 *   schemas:
 *     investigation:
 *         required:
 *           - "id"
 *         properties:
 *           id:
 *             type: "number"
 *           createId:
 *             type: "string"
 *           createTime:
 *             type: "string"
 *           modId:
 *             type: "string"
 *           modTime:
 *             type: "string"
 *           datasets:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           doi:
 *             type: "string"
 *           endDate:
 *             type: "string"
 *           investigationGroups:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           investigationInstruments:
 *             type: "array"
 *             items:
 *               type: "object"
 *               properties:
 *                 id:
 *                   type: "number"
 *                 createId:
 *                   type: "string"
 *                 createTime:
 *                   type: "string"
 *                 modId:
 *                   type: "string"
 *                 modTime:
 *                   type: "string"
 *                 instrument:
 *                   required:
 *                     - "id"
 *                     - "createId"
 *                     - "createTime"
 *                     - "modId"
 *                     - "modTime"
 *                     - "description"
 *                     - "fullName"
 *                     - "instrumentScientists"
 *                     - "investigationInstruments"
 *                     - "name"
 *                     - "type"
 *                   properties:
 *                     id:
 *                       type: "number"
 *                     createId:
 *                       type: "string"
 *                     createTime:
 *                       type: "string"
 *                     modId:
 *                       type: "string"
 *                     modTime:
 *                       type: "string"
 *                     description:
 *                       type: "string"
 *                     fullName:
 *                       type: "string"
 *                     instrumentScientists:
 *                       type: "array"
 *                       items:
 *                         type: "undefined"
 *                     investigationInstruments:
 *                       type: "array"
 *                       items:
 *                         type: "undefined"
 *                     name:
 *                       type: "string"
 *                     type:
 *                       type: "string"
 *                   type: "object"
 *           investigationUsers:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           keywords:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           name:
 *             type: "string"
 *           parameters:
 *             type: "array"
 *             items:
 *               type: "object"
 *               properties:
 *                 id:
 *                   type: "number"
 *                 createId:
 *                   type: "string"
 *                 createTime:
 *                   type: "string"
 *                 modId:
 *                   type: "string"
 *                 modTime:
 *                   type: "string"
 *                 stringValue:
 *                   type: "string"
 *           publications:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           releaseDate:
 *             type: "string"
 *           samples:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           shifts:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           startDate:
 *             type: "string"
 *           studyInvestigations:
 *             type: "array"
 *             items:
 *               type: "undefined"
 *           summary:
 *             type: "string"
 *           title:
 *             type: "string"
 *           visitId:
 *             type: "string"
 */
