/**
 * @swagger
 *
 * components:
 *   schemas:
 *     page:
 *       properties:
 *         limit:
 *           type: "number"
 *         total:
 *           type: "number"
 *         index:
 *           type: "number"
 */
