/**
 * @swagger
 *
 * components:
 *   schemas:
 *     investigationlight:
 *         required:
 *           - "id"
 *         properties:
 *           id:
 *             type: "number"
 *           members:
 *             type : "array"
 *             items :
 *               schema:
 *                  $ref: '#/components/schemas/member'
 *           doi:
 *             type: "string"
 *           startDate:
 *             type: "string"
 *           endDate:
 *             type: "string"
 *           releaseDate:
 *             type: "string"
 *           name:
 *             type: "string"
 *           parameters:
 *             type: "array"
 *             items:
 *               type: "object"
 *               properties:
 *                 name:
 *                   type: "string"
 *                 value:
 *                   type: "string"
 *           summary:
 *             type: "string"
 *           title:
 *             type: "string"
 */
