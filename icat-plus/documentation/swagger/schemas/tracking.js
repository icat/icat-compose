/**
 * @swagger
 *
 * components:
 *   schemas:
 *     shipment:
 *       description: a shipment
 *       type: object
 *       required:
 *         - name
 *         - investigationName
 *       properties:
 *         _id:
 *           type: string
 *         investigationId:
 *           type: integer
 *         investigationName:
 *           type: string
 *         name:
 *           type: string
 *         defaultReturnAddress:
 *           description: a postal address that will be used by default as returning address of the parcels
 *           $ref: '#/components/schemas/address'
 *         defaultShippingAddress:
 *           $ref: '#/components/schemas/address'
 *         parcels:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/parcel'
 *     parcel:
 *       description: A box that will be sent by courier to the faciity with items inside and will be tracked independently.
 *       type: object
 *       required:
 *         - name
 *         - investigationName
 *         - investigationId
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         investigationId:
 *           type: number
 *         investigationName:
 *           type: string
 *         storageConditions:
 *           type: string
 *         comments:
 *           type: string
 *         containsDangerousGoods:
 *           type: boolean
 *         status:
 *           type: string
 *           description: Status of the parcel
 *         shippingAddress:
 *           $ref: '#/components/schemas/address'
 *         returnAddress:
 *           $ref: '#/components/schemas/address'
 *         description:
 *           type: string
 *         items:
 *           type: array
 *           $ref: '#/components/schemas/item'
 *     parcels:
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/parcel'
 *     address:
 *       description: a postal address of a person for shipments
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         surname:
 *           type: string
 *         companyName:
 *           type: string
 *         address:
 *           type: string
 *         city:
 *           type: string
 *         region:
 *           type: string
 *         postalCode:
 *           type: string
 *         country:
 *           type: string
 *         email:
 *           type: string
 *         phoneNumber:
 *           type: string
 *     tracking:
 *       description: a tracking object is composed by a postal address and a tracking number
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         trackingId:
 *           type: string
 *         address:
 *           $ref: '#/components/schemas/address'
 *     item:
 *       description: an item that can be contained in a parcel
 *       type: object
 *       required:
 *         - name
 *         - type
 *         - id
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         type:
 *           type: string
 *         description:
 *           type: string
 *         comments:
 *           type: string
 *
 *
 */
