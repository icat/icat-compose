/**
 * @swagger
 *
 * components:
 *   schemas:
 *     address:
 *       description: a postal address of a person for shipments
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - address
 *         - city
 *         - postalCode
 *         - country
 *         - email
 *         - phoneNumber
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         surname:
 *           type: string
 *         companyName:
 *           type: string
 *         address:
 *           type: string
 *         city:
 *           type: string
 *         region:
 *           type: string
 *         postalCode:
 *           type: string
 *         country:
 *           type: string
 *         email:
 *           type: string
 *         phoneNumber:
 *           type: string
 *     tracking:
 *       description: a tracking object is composed by a postal address and a tracking number
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         trackingId:
 *           type: string
 *         address:
 *           $ref: '#/components/schemas/address'
 *
 *
 */
