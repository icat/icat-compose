/**
 * @swagger
 *
 * components:
 *   responses:
 *     datasetaccesses:
 *       description: List of dataset access
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/datasetaccess'
 *     datasetaccess:
 *       description: A dataset access
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/datasetaccess'
 */
