/**
 * @swagger
 *
 * components:
 *   responses:
 *     doiMinted:
 *        description: DOI was minted succesfully
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                  description: the newly created DOI.
 *            example:
 *              message: 10.15151/ESRF-ES-90632078
 *
 */
