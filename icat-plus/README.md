# ICAT+

ICAT+ is a NodeJS package that exposes a RestFul API and uses a MongoDB database. It allows:

- Linking a logbook to an existing investigation.
- Managing external resources such as images or any file.
- Controlled access to the resources by using a Metadata Catalogue (currently [ICAT](https://icatproject.org/))
- Minting of DOIs (by list of datasets)

ICAT+ is a collaborative project and licensed under the MIT license.

# Menu

1. [Installation](#installation)
2. [Configuration](#configuration)
3. [API](#api)
4. [Database schema](#database-schema)
5. [Elastic search](#elastic-search)
6. [Elogbook](#elogbook)
7. [Debugging](#debugging)
8. [Tests](#tests)
9. [Code quality](#code-quality)
10. [Development](#development)

# Installation

1. Download the source code from github repository
2. Install dependencies

```bash
npm install
```

3. A MongoDB instance is required. The following command will download and install a docker image containing mongoDB database. Then it will start a mongoDB instance listening at port 27017.

```
docker run  -p 27017:27017 mongo
```

4. Install the PDF generator (**optional and only needed for elogbook reporting**)

For Debian it is necessary to install wkhtmltopdf as described [here](https://wkhtmltopdf.org/downloads.html). It seems that the version they provided has more features than the one provided by apt-get in the central Debian repositories.

```
wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb
dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb
```

5. Run

```bash
npm start
```

Or Run with nodemon. This will update the server automatically

```bash
 nodemon npm start
```

# Configuration

## ICAT+ configuration

The configuration settings are in [server.config.js](config/server.config.example.js). An [example file](config/server.config.example.js) is provided and needs to be copied into configuration file:

```bash
cp config/server.config.example.js config/server.config.js
```

Edit the file 'server.config.js' with your favorite text editor

<!--START configuration -->

```js
/**
 * ICAT+ configuration file
 */
/** @module confguration */
module.exports = {
  /** ICAT+ server configuration section */
  server: {
    /** HTTP port used by ICAT+ */
    port: 8000,
    /** ICAT+ URL. Used by the PDF generator */
    url: "https://icatplus.esrf.fr",
    /** API_KEY to short circuit default authentication mechanism. Distribute this key to trusted clients. */
    API_KEY: "[API ALLOWS TO STORE NOTIFICATIONS IN THE LOGBOOK WITH NO AUTH]",
  },

  /** Database configuration section */
  database: {
    /** URI to connect to mongoDB */
    uri: "mongodb://[USER]:[PASSWORD]@[HOST]:[PORT]/[DATABASE]?authSource=admin",
  },

  /** Elastic search configuration section */
  elasticsearch: {
    /** If enabled elastic search will send queries to the server */
    enabled: true,
    /** Server and pord where elasticsearch is running */
    server: "http://[SERVER]:[PORT]",
  },
  /** Logging configuration section */
  logging: {
    /** Logging to graylog */
    graylog: {
      /** If TRUE enables the sending of logs to graylog server */
      enabled: true,
      /** Graylog server URL */
      host: "graylog-dau.esrf.fr",
      /** Source of the logs */
      hostname: "ICAT+",
      /** HTTP port to use on the host side where graylog server is listening */
      port: 12205,
      /** Facility */
      facility: "ESRF",
    },
    /** Logging to the terminal */
    console: {
      /** Level of verbosity. Use '0' for no logs ; Use 'silly' to see all logs */
      level: "silly",
    },
    /** Logging to file */
    file: {
      /** If TRUE, logs are written to a file */
      enabled: true,
      /** Absolute filepath where the logs will be stored */
      filename: "/tmp/server.log",
      /** Verbosity level. See logging/console/level for details*/
      level: "silly",
    },
  },

  /** Datacite configuration section. Datacite is an organization which creates DOIs (Digital Object Identifiers).
   *  We use this third-party services to create DOI for our data. More info [here](https://www.datacite.org/)*/
  datacite: {
    /** DOI prefix for your institution */
    prefix: "*****",
    /** Fixed part in the DOI suffix. */
    suffix: "*****",
    /** Username used for the authentication on datacite services */
    username: "*****",
    /** Password used for authentication on datacite services */
    password: "*****",
    /** Datacite metadata store (MDS) URL */
    mds: "https://mds.datacite.org",
    /** Common part of the URL shared by all DOI landing pages */
    landingPage: "https://doi.esrf.fr/",
    /** Proxi settings */
    proxy: {
      host: "proxy.esrf.fr",
      port: "3128",
    },
  },
  /** ICAT data service configuration section */
  ids: {
    /** IDS server URL */
    server: "http://[SERVER]:[PORT]",
  },
  /** ICAT metadata catalogue configuration section */
  icat: {
    /** ICAT server URL */
    server: "http://[SERVER]:[PORT]",
    /** Maximum number of events that can be returned from the elogbook by a single HTTP request */
    maxQueryLimit: 10000,
    /** Authorization management section*/
    authorizations: {
      adminUsersReader: {
        /** Name of the group for users with special privileges (usually the admin group) */
        administrationGroups: ["admin"],
        /**
         * User is capable to get the list of users that are administrators
         * User needs access to tables user and userGroup in order to know who are administrators
         */
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "**********" }, { password: "**********" }],
        },
      },
      /**
       * This user is allowed to mint DOI's and needs some permission on ICAT tables
       * User has the permission to create and read datacollections in the ICAT database.
       */
      minting: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "**********" }, { password: "**********" }],
        },
      },
      /**
       * This user can read the list of parameter types from ICAT
       */
      parameterListReader: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "**********" }, { password: "**********" }],
        },
      },
      /**
       * notifier needs to get access to all investigations as it will convert investigationName and beamline into investigationId
       */
      notifier: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: "**********" }, { password: "**********" }],
        },
      },
    },
    anonymous: {
      /** Authentication mechanism used by ICAT server */
      plugin: "db",
      credentials: [{ username: "**********" }, { password: "**********" }],
    },
  },
};
```

<!--END configuration -->

## ICAT configuration

ICAT+ currently depends on [ICAT](https://icatproject.org/).

The ICAT's functionalities which are used :

- User authentication
- User's permission to access and/or store metadata related to investigations, datasets and datafiles.

For this reason, ICAT metadata catalogue also needs to be configured to support ICAT+. This includes the following:
ICAT+ needs to make some operations on ICAT then it needs to have some right to be configured on it:

1. [Minting](#minting)
2. [Elogbook Permissions](#elogbook-permissions)
3. [Notifiers](#notifiers)

## Addtional configuration

The sample tracking configuration settings are in [tracking.config.js](app/tracking.config.example.js). An [example file](app/tracking.config.example.js) is provided and needs to be copied into `app` file:

```bash
cp app/tracking.config.example.js app/tracking.config.js
```

The investigation configuration settings are in [investigation.config.js](config/investigation.config.example.js). An [example file](config/investigation.config.example.js) is provided and needs to be copied into `config` file:

```bash
cp config/investigation.config.example.js config/investigation.config.js
```

The restore email configuration settings are in [restore.config.js](app/restore.config.example.js). An [example file](app/restore.config.example.js) is provided and needs to be copied into `app` folder:

```bash
cp app/restore.config.example.js app/restore.config.js
```

The restoration email template is located in [email.restore.handlbars](app/views/email.restore.handlebars).

Edit these files with your favorite text editor.

### Minting

ICAT+ allows to mint DOIs given a **list of datasets, authors, title and abstract**.

ICAT+ will create in the Metadata Catalogue a new DataCollection entry with the selected datasets so DOI will be stored in the field 'doi' of the DataCollection table.

Then ICAT+ will fill up the tables **DataCollection, DataCollectionDataset and DataCollectionParameter** of the Metadata Catalogue. To make this as safe as possible, an user's account with the proper rights needs to be configured.

Example of valid configuration rules:

**minter** is a user that belongs to the group **minters**

```
User minter
UserGroup minters
```

Rules to be included in ICAT:

```
CRU minters DataCollection
CR minters DataCollectionParameter
CR minters DataCollectionDataset
R  minters Dataset
R  minters ParameterType
R  DataCollection

```

Also a set of parameterTypes are needed (ApplicableToDataCollection):

```text
title
abstract
mintedByFullName
mintedByName
investigationNames
```

### Elogbook Permissions

#### Administrator

Elogbook's permissions are not directly managed by ICAT since there is not events (finest logbook granularity level) as entity in ICAT. So, ICAT+ will allow read and write access to the participants of the investigation and the administrators during the embargo period. A participant is formally described as a investigationUser linked to a investigation.

After the embargo period write access is removed and read access is enabled for everybody.

Then ICAT+ needs to know who are the administrators so it needs to use an user's account properly configured in order to get the list of users that belong to the group administrators.

Example of valid configuration rules:

**userGroupReader** is a user that belongs to the group **userGroupReaders**

```
User userGroupReader
UserGroup userGroupReaders
```

Rules to be included in ICAT:

```
R userGroupReaders User
R userGroupReaders UserGroup

```

Take into account that the name of the group of administrator is a parameter in the configuration file

#### Notifiers

During the data acquisition any software can send notifications (a event of the logbook) to ICAT+. Problem is that such software not always have the possibility to log into ICAT, it means that they have not got access to username and password.
So, implementation has been done with a **magic** API token that will allow store notification on any investigation by given the investigation name and the instrument name. As ICAT+ is linking that notification with the investigationId then it needs to use an user's account properly configured in order to get the investigation given the investigation name and instrument name.

Example of valid configuration rules:

**notifier** is a user that belongs to the group **notifiers**

```
User notifier
UserGroup notifiers
```

Rules to be included in ICAT:

```
R Investigation
R Instrument
R InvestigationInstrument

```

# API

ICAT+ exposes a set of restful webservices. Below is a summary of the API methods. The detailed documentation of these API methods is browsable at <http://server:8000/api-docs> once you have started the application.

<!--START api-docs -->
<table><tr><td>Path</td><td>Method</td><td>Summary</td></tr><tr><td>/cache</td><td>GET</td><td>Gets the cache of a user given a specific sessionId</td></tr><tr><td>/cache/keys</td><td>GET</td><td></td></tr><tr><td>/cache/keys/:key</td><td>GET</td><td></td></tr><tr><td>/cache/stats</td><td>GET</td><td></td></tr><tr><td>/catalogue/investigation/name/{investigationName}/normalize</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/datacollection</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/dataset/id/{datasetIds}/dataset</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/dataset/id/{datasetIds}/dataset_document</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/dataset/id/{datasetIds}/status</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/dataset/id/{datasetId}/datacollection</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/dataset/id/{datasetId}/datafile</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/investigation</td><td>GET</td><td>Gets all my investigations. Investigations that I am participant</td></tr><tr><td>/catalogue/{sessionId}/investigation/id/{investigationId}/dataset</td><td>GET</td><td></td></tr><tr><td>/catalogue/{sessionId}/investigation/id/{investigationId}/investigation</td><td>GET</td><td>Get investigation by identifier</td></tr><tr><td>/catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers</td><td>GET</td><td>Gets users involved in a investigation</td></tr><tr><td>/catalogue/{sessionId}/investigation/status/embargoed/investigation</td><td>GET</td><td>Gets all investigations that are under embargo. releaseDate &gt; NOW</td></tr><tr><td>/catalogue/{sessionId}/investigation/status/released/investigation</td><td>GET</td><td>Gets all investigations which content is public</td></tr><tr><td>/doi/{prefix}/{suffix}/datasets</td><td>GET</td><td></td></tr><tr><td>/doi/{prefix}/{suffix}/json-datacite</td><td>GET</td><td>Returns a document in a json-datacite format with the description of the DOI</td></tr><tr><td>/doi/{sessionId}/mint</td><td>POST</td><td>Mint a DOI</td></tr><tr><td>/elasticsearch/{sessionId}/datasets/_msearch</td><td>GET</td><td>Proxy to elastic search server</td></tr><tr><td>/logbook/{sessionId}/investigation/id/{investigationId}/event/count</td><td>POST</td><td>Method used to retreive the number of logbook events for given search parameters</td></tr><tr><td>/logbook/{sessionId}/investigation/id/{investigationId}/event/create</td><td>POST</td><td>Create an event</td></tr><tr><td>/logbook/{sessionId}/investigation/id/{investigationId}/event/pdf</td><td>GET</td><td>Method used to retreive logbook events for given search parameters under the form of a PDF.</td></tr><tr><td>/logbook/{sessionId}/investigation/id/{investigationId}/event/query</td><td>POST</td><td>Method used to retreive logbook events for given search parameters</td></tr><tr><td>/logbook/{sessionId}/investigation/id/{investigationId}/event/update</td><td>PUT</td><td>Updates an event</td></tr><tr><td>/logbook/{sessionId}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event</td><td>POST</td><td>Method used to create a notification type of event when investigationId is not accessible (from metadata manager). The investigation is identified by investigationName + instrumentName</td></tr><tr><td>/resource/{sessionId}/file/id/{fileId}/investigation/id/{investigationId}/download</td><td>GET</td><td>Downloads a resource</td></tr><tr><td>/resource/{sessionId}/file/investigation/id/{investigationId}/upload</td><td>POST</td><td>Uploads a file to ICAT+</td></tr><tr><td>/session</td><td>POST</td><td>This is used to log into the app</td></tr><tr><td>/session/{sessionId}</td><td>GET</td><td>Gets information about the session in ICAT</td></tr></table>

<!--END api-docs -->

# Database Schema

Below is the current schema for the 'event' collection in the mongoDB which stores logbook's events.

```
<!--START mongoDoc -->
/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require('mongoose');

var Schema = require('mongoose').Schema;

/** Event.content schema */
var Content = new Schema({
    /** Format of the content. Possible values: [plainText, html] */
    format     : String,
    /** Text of the content. */
    text       : String
});

/** Event schema */
const EventSchema = mongoose.Schema({
    /** Investigation identifier indicating which investigation this event belongs to.*/
    investigationId: Number,
    /** Name of the investigation */
    investigationName : String,
    /** Dataset identifier this event belongs to. Only relevant for logbook associated to a dataset  */
    datasetId: Number,
    /** Name of the dataset */
    datasetName: String,
    /** Type of the event. Possible values: ['annotation', notification', 'attachment', 'broadcast'] */
    type: String,
    /** Tag list associated to this event */
    tag: [String],
    /** Title of this event */
    title: String,
    /** Category of this event. Possible values: ['commandLine', 'comment', 'debug', 'error', 'info'] */
    category: String,
    /** NOT USED */
    filepath: String,
    /** Name of the file when the event contains a file */
    filename: String,
    /** Size of the file when the event contains a file */
    fileSize:Number,
    /** User name who created or updated this event */
    username: String,
    /** Textual content of this event */
    content : [Content],
    /** Beamline where this event was generated */
    instrumentName : String,
    /** Program from where this event comes from */
    software: String,
    /** Machine running the program from where this event comes from */
    machine: String,
    /** Previous event version. This entry constitutes the event history. */
    previousVersionEvent :  Object,
    /** TimeStamp when the event was submitted to ICAT+ */
    creationDate : Date,
    /** Updated file when the event contains a file */
    file : [{ type: Schema.Types.ObjectId }],
    /** File content type when the event contains a file */
    contentType : String

}, {
    timestamps: true
});

module.exports = mongoose.model('Event', EventSchema);

<!--END mongoDoc -->
```

# Elastic Search

Currenty, Elastic Search is used by the UI to search for datasets based on top-level metadata (sample, dataset and investigation name). More precisely the user interface uses ReactiveSearch (https://opensource.appbase.io/reactivesearch/) that is a library for components done in REACT that allows display results from elastic search in a REACT application.
In order to allow ReactiveSearch to get the results ICAT+ implemements a endpoint that allows multiple searchs:

```
/elasticsearch/{sessionId}/datasets/_msearch
```

Note: this endpoint is supposed to be used only by ReactiveSearch.

## Installation

Easiest way is to use a docker container:

```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.6.2
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2
```

For development purpose someone can also install Kibana in the same way:

```
docker pull docker.elastic.co/kibana/kibana:7.6.2
docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 {docker-repo}:{version}
```

## Populating Index

Our approach is to create a single index called /dataset. It means that we will index only the datasets of ICAT and not the datafiles or investigation but the dataset_document might have parameters from the investigation i.e: investigationName

### Dataset Document

The next endpoint will convert a dataset from ICAT into a dataset_document

```
/catalogue/{sessionId}/dataset/id/{datasetIds}/dataset_document
```

This creates an unflattern data structure that makes the search simpler. A dataset documents looks like:

```
[
  {
    "id": 142795543,
    "name": "mesh-AFAMIN-revi-B5-1_1_1719731",
    "startDate": "2016-04-08T19:28:20.000+02:00",
    "endDate": "2018-11-06T16:48:51.432+01:00",
    "location": "/data/id30a1/inhouse/opid30a1/20160408/RAW_DATA/AFAMIN/AFAMIN-revi-B5-1/MXPressA_01",
    "sampleName": "AFAMIN-revi-B5-1",
    "MX_template": "mesh-AFAMIN-revi-B5-1_1_####.cbf",
    "MX_numberOfImages": "24",
    "InstrumentSource_mode": "16 bunch",
    "MX_oscillationOverlap": "0",
    "MX_scanType": "OSC",
    "MX_aperture": "50 um",
    "MX_detectorDistance": "234.925",
    "MX_beamSizeAtSampleY": "0.05",
    "InstrumentMonochromator_wavelength": 0.966,
    "MX_beamShape": "ellipse",
    "MX_motors_name": "y z sampx sampy phi kappa chi kappa_phi zoom focus phiz phiy",
    "MX_transmission": "100",
    "MX_oscillationStart": "149.5",
    "MX_resolution": "2",
    "MX_oscillationRange": "0.0416667",
    "MX_startImageNumber": "49",
    "MX_exposureTime": "0.1",
    "MX_dataCollectionId": "1719731",
    "MX_fluxEnd": "143000000000",
    "MX_flux": "142000000000",
    "MX_yBeam": "146.858",
    "MX_directory": "/data/id30a1/inhouse/opid30a1/20160408/RAW_DATA/AFAMIN/AFAMIN-revi-B5-1/MXPressA_01",
    "MX_xBeam": "129.056",
    "MX_beamSizeAtSampleX": "0.05",
    "MX_motors_value": "2.041 -0.596 -0.011 0.157 149.5 0.0 0.0 0.0 2.0 0.0 0.0 -0.605",
    "fileCount": "24",
    "volume": "59492064",
    "elapsedTime": "81382831",
    "ResourcesGallery": "5be1b7d89885c253675ceebc 5be1b7d89885c253675ceec0",
    "ResourcesGalleryFilePaths": "/data/pyarch/2016/id30a1/opid30a1/20160408/RAW_DATA/AFAMIN/AFAMIN-revi-B5-1/MXPressA_01/mesh-AFAMIN-revi-B5-1_1_0049.jpeg,/data/pyarch/2016/id30a1/opid30a1/20160408/RAW_DATA/AFAMIN/AFAMIN-revi-B5-1/MXPressA_01/AFAMIN-revi-B5-1_1_snapshot_before_mesh.png",
    "startTime": "2016-04-08 19:28:20",
    "datasetName": "mesh-AFAMIN-revi-B5-1_1_1719731",
    "dataArchived": "True",
    "parametersCount": 33,
    "MX_motors": [
      {
        "name": "y",
        "numericValue": 2.041,
        "stringValue": "2.041"
      },
      {
        "name": "z",
        "numericValue": -0.596,
        "stringValue": "-0.596"
      },
      {
        "name": "sampx",
        "numericValue": -0.011,
        "stringValue": "-0.011"
      },
      {
        "name": "sampy",
        "numericValue": 0.157,
        "stringValue": "0.157"
      },
      {
        "name": "phi",
        "numericValue": 149.5,
        "stringValue": "149.5"
      },
      {
        "name": "kappa",
        "numericValue": 0,
        "stringValue": "0.0"
      },
      {
        "name": "chi",
        "numericValue": 0,
        "stringValue": "0.0"
      },
      {
        "name": "kappa_phi",
        "numericValue": 0,
        "stringValue": "0.0"
      },
      {
        "name": "zoom",
        "numericValue": 2,
        "stringValue": "2.0"
      },
      {
        "name": "focus",
        "numericValue": 0,
        "stringValue": "0.0"
      },
      {
        "name": "phiz",
        "numericValue": 0,
        "stringValue": "0.0"
      },
      {
        "name": "phiy",
        "numericValue": -0.605,
        "stringValue": "-0.605"
      }
    ],
    "investigationId": 13175377,
    "investigationName": "OPID-1",
    "investigationTitle": "opid-1",
    "investigationVisitId": "id30a1",
    "escompactsearch": "id30a1  OPID-1     ",
    "estype": "dataset"
  }
]
```

### Ingestion

In order to populate we need to store each dataset_document into elastic search. We are doing so by using this scripts. The index should be updated automatically when the data on ICAT changes i.e: when a new dataset has been stored into ICAT.

This script will connect to ICAT to get the investigation and the will use the ICAT+ end point to convert the datasets into documents that will be stored in the elastic search index by using the function BULK.

#### Requirements

```
pip install elasticsearch
```

#### Script

The configuration properties should be changed and adapted to your environment:

```python
import requests
import json
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError
from elasticsearch.helpers import bulk, streaming_bulk
import datetime, sys
import getpass

#################################################
# CONFIGURATION PROPERTIES
#################################################
icatplusServer = "http://lindemaria.esrf.fr:8000"
icatServer = "http://ovm-icat2.esrf.fr:8080"
es = Elasticsearch([{'host': 'kolla-vm029.esrf.fr', 'port': 9200}])
index = "datasets"

#################################################
# AUTHENTICATION INPUT
#################################################
print 'ICAT AUTHENTICATION '

print 'What is the authentication plugin (esrf/db)?'
plugin = sys.stdin.readline().rstrip()
print 'What is ICAT Username?'
username = sys.stdin.readline().rstrip()
password = getpass.getpass(stream=sys.stderr)

#################################################
# AUTHENTICATION
#################################################
request = requests.post(icatplusServer + "/session", {'plugin' : plugin, 'username' : username,  'password' : password})
if request.status_code != 200:
    # AUTHENTICATION FAILED
    print 'User ' + plugin + '/' + username + ' is invalid'
    sys.exit()

#################################################
# AUTHENTICATION SUCCEED
#################################################
session = json.loads(request.text);
token = session["sessionId"]
print ''
print 'User ' + plugin + '/' + username + ' has been successfully authenticated'
print '\tToken:', token
print '\tLife Time (Minutes):', session["lifeTimeMinutes"]


#################################################
# GET INVESTIGATIONS
#################################################
print 'Set a maximum number of investigations to ingest(they will be treated by order DESC)'
n = int(sys.stdin.readline().rstrip())

investigations = []
def getValue(inv, column):
    if inv["Investigation"] != None:
        if column in inv["Investigation"] :
	    return inv["Investigation"][column]
    return ""

investigationsList = json.loads(requests.get(icatServer + "/icat/entityManager?sessionId=" + token + "&query=SELECT inv from Investigation inv order by inv.id DESC").text);
for inv in investigationsList[:n]:
    investigations.append(getValue(inv, "id"))
    print getValue(inv, "name") + "\t" + getValue(inv, "startDate") + "\t" + getValue(inv,  "visitId") + "\t" + getValue(inv, "summary")


#################################################
# START SCRIPT BULK
#################################################
print("# Script started " +  str(datetime.datetime.now()) )
# ignore 400 cause by IndexAlreadyExistsException when creating an index
#print(es.indices.create(index='test-index', ignore=400))

def gendata(dataset):
    dataset["_index"] = index
    dataset["_type"] = "document"
    dataset["_id"] = dataset["id"]
    yield dataset

datasetCount = 0

# Loop over investigations
for i in range(len(investigations)):
    investigationId = str(investigations[i]) #str(investigations[i]["Investigation"]["id"])
    print( str(i) + " of " + str(len(investigations)) + " " + " (" + investigationId + ")")

    # Get datasets
    print(icatplusServer + "/catalogue/" + token + "/investigation/id/" + investigationId + "/dataset_document")
    datasets = json.loads(requests.get(icatplusServer + "/catalogue/" + token + "/investigation/id/" + investigationId + "/dataset_document").text)
    for j in range(len(datasets)):
        dataset = datasets[j]
        dataset["_index"] = index
        dataset["_type"] = "document"
        dataset["_id"] = dataset["id"]

        bulk(es, gendata(dataset))
        print( "\t" + str(j) + " of " + str(len(datasets)) + " Indexed successfully. Total " + str(datasetCount)) + " documents"
        datasetCount = datasetCount + 1

print("# Script finished " +  str(datetime.datetime.now()) )
```

## Search by using Kibana

Kibana has a development console that allows to test its search capabilities (https://www.elastic.co/guide/en/kibana/current/console-kibana.html).

Queries can be performed by using /datasets/search. For example:

```
GET /datasets/_search
{
  "query": {
    "bool": {
      "must": {
        "bool" : {
          "should": [
            { "match": { "definition": "SXM" }},
            { "match": { "InstrumentMonochromatorCrystal_type": "Si" }}
          ],
          "must": { "match": { "Sample_name": "fe2streptor2" }}
        }
      }
    }
  }
}
```

# Elogbook

An API allows to send notifications to the elogbook. The signature of this method can be found [here](https://icatplus.esrf.fr/api-docs/#/Logbook/post_logbook__sessionId__investigation_name__investigationName__instrument_name__instrumentName__event)

Signature of the method is:

> POST http://{server}:{port}/logbook/be70ac55-fd08-4840-9b29/investigation/name/{investigation}/instrument/name/{instrument}/event

Form data is composed by a json with the type, category and content of the notification. Example:

```
{
"type":"notification",
"category":"info",
"content":[
    {
	"text":"This is a info message",
        "format":"plainText"
}],
"creationDate":"2019-10-07T14:56:42.417368"
}
```

Example:

```
curl -d '{"type":"notification", "category":"info","content":[{"text":"This is a info message","format":"plainText"}],"creationDate":"2019-10-07T14:56:42.417368"}' -H "Content-Type: application/json" -X POST http://lindemaria:8000/logbook/be70ac55-fd08-4840-9b29/investigation/name/ELOG-0001/instrument/name/ID00/event

```

## Categories

There are several type of categories:

1. info
2. error
3. debug
4. commandLine
5. comment

## Format

### Plain Text

```
{
"type":"notification",
"category":"info",
"content":[
    {
	"text":"This is a info message",
        "format":"plainText"
}],
"creationDate":"2019-10-07T14:56:42.417368"
}
```

### HTML

HTML is also supported when format=html

```
{
	"type":"notification",
	"category":"info",
	"content":[
		{
			"text":"This is a more sophisticated HTML info message containing 1 table that was generated by the CURL command below
				<table class=\ "table table-striped table-condensed table-hover\">
				    <tbody>
					<tr>
					    <td>m2angle</td>
					    <td class=\ "value\">26</td>
					</tr>
					<tr>
					    <td>m0tz</td>
					    <td class=\ "value\">-22</td>
					</tr>
				       [...............]
					<tr>
					    <td>rotz</td>
					    <td class=\ "value\">NaN</td>
					</tr>
					<tr>
					    <td>tilt</td>
					    <td class=\ "value\">0</td>
					</tr>
				    </tbody>
				</table>",
			"format":"html"
		}
	],
	"creationDate":"2019-10-07T14:56:42.417368"
}
```

Example

```
curl -d '{"category":"info","content":[{"text":"This is a more sophisticated HTML info message containing 1 table that was generated by the CURL command below <table class=\"table table-striped table-condensed table-hover\"><tbody><tr><td>m2angle</td><td class=\"value\">26</td></tr><tr><td>m0tz</td><td class=\"value\">-22</td></tr><tr><td>m0tyrot</td><td class=\"value\">6.7</td></tr><tr><td>mono</td><td class=\"value\">15.9385</td></tr><tr><td>enmono</td><td class=\"value\">7.20004</td></tr><tr><td>r2theta</td><td class=\"value\">-0.0829958</td></tr><tr><td>r2gamma</td><td class=\"value\">-0.456489</td></tr><tr><td>qg2</td><td class=\"value\">-0.660105</td></tr><tr><td>dcmy</td><td class=\"value\">5.5</td></tr><tr><td>dcmz</td><td class=\"value\">0.85</td></tr><tr><td>zpx</td><td class=\"value\">-18.5746</td></tr><tr><td>zpy</td><td class=\"value\">-3.49</td></tr><tr><td>zpz</td><td class=\"value\">7.16492</td></tr><tr><td>zpz1</td><td class=\"value\">7.06708</td></tr><tr><td>zpz2</td><td class=\"value\">7.26277</td></tr><tr><td>zpz3</td><td class=\"value\">7.06708</td></tr><tr><td>zppy</td><td class=\"value\">7.5</td></tr><tr><td>zppz</td><td class=\"value\">100</td></tr><tr><td>kbhf</td><td class=\"value\">0.141575</td></tr><tr><td>kbvf</td><td class=\"value\">-0.511</td></tr><tr><td>hexx</td><td class=\"value\">NaN</td></tr><tr><td>hexy</td><td class=\"value\">NaN</td></tr><tr><td>hexz</td><td class=\"value\">NaN</td></tr><tr><td>rotx</td><td class=\"value\">NaN</td></tr><tr><td>roty</td><td class=\"value\">NaN</td></tr><tr><td>rotz</td><td class=\"value\">NaN</td></tr><tr><td>tilt</td><td class=\"value\">0</td></tr></tbody></table>","format":"html"}],"dataset":null,"creationDate":"2019-10-07T14:56:42.417368","type":"notification"}' -H "Content-Type: application/json" -X POST http://localhost:8000/logbook/be70ac55-fd08-4840-9b29/investigation/name/ELOG-0001/instrument/name/ID00/event

```

![](images/4.png)

## Images

Images can be also uploaded via API either uploading as a file or taken from a mobile device (tablet or smartphone).

![](images/5.png)

# Debugging

- Install Node.js inspector Manager on Chrome/Chromium
- Open Inspector Manager and Developer Tools of the browser
- Run

```
node --inspect-brk server.js
```

# Tests

## Configuration

ICAT+ can run the tests by using mongoUnit (mongoDB in memory) or against a MongoDB instance. By default MongoUnit will be used except if in the configuration file test/config/server.config.js is set to false in the section of databases:

```
    database: {
        uri: 'mongodb://localhost:27017/icatplus?authSource=admin',
        isMongoUnitDisabled : true
    },

```

If isMongoUnitDisabled = true then tests will use the **uri** defined in the database section.

An instance of Mongo can be run with docker:

```
docker run  -p 27017:27017 mongo
```

## Run

```
npm test
```

For running a single test then:

```
npm test -- --grep "logbook/:sessionId/investigation/id/:investigationId/tag"
```

## Coverage

NYC (successor of Instambul https://github.com/istanbuljs/nyc) is used for coverage support and a report is generated when it is run.

Report is generated on public/coverage then accessible from:

```
http(s)://{server}/coverage
```

Example:

```
https://icatplus.esrf.fr/coverage
```

In order to run the coverage tool and update the report run:

```
npm run-script test:coverage
```

# Code quality 🔎

- `npm run lint` - run linting and code formatting commands
- `npm run lint:eslint` - lint all JS files with ESLint (this command will report warnings as errors)
- `npm run lint:prettier` - check that all files have been formatted with Prettier

## Automatic fixing and formatting

- `npm run lint:eslint -- --fix` - auto-fix linting issues
- `npm run lint:prettier -- --write` - format all files with Prettier

## Editor integration

Most editors support fixing and formatting files automatically on save. The configuration for VSCode is provided out of
the box, so all you need to do is install the recommended extensions.
