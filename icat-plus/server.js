/** Remove this in production */
"use strict";

process.env.NODE_ENV = (process.env.NODE_ENV || "development").toUpperCase();
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const express = require("express");
const bodyParser = require("body-parser");

const swaggerUi = require("swagger-ui-express");
const app = express();
const cors = require("cors");

global.gServerConfig = process.env.NODE_ENV === "TEST" ? require("./test/config/server.test.config.js") : require("./config/server.config.js");
global.gTrackingConfig = process.env.NODE_ENV === "TEST" ? require("./test/config/tracking.test.config.js") : require("./app/tracking.config.js");
global.gInvestigationConfig = process.env.NODE_ENV === "TEST" ? require("./test/config/investigation.test.config.js") : require("./config/investigation.config.js");
global.gRestoreConfig = process.env.NODE_ENV === "TEST" ? require("./test/config/restore.test.config.js") : require("./app/restore.config.js");

global.appCache = null;
global.API_KEY = global.gServerConfig.server.API_KEY;
global.gLogger = require("./app/logging/logger.js").logger;
const mongoose = require("mongoose");
const cache = require("./app/cache/appCache.js");

global.gLogger.info(`ICAT server is: ${global.gServerConfig.icat.server}`);

app.use(bodyParser.urlencoded({ limit: "500mb", extended: true }));
app.use(bodyParser.text({ type: "application/x-ndjson" })); // Needed for elastic search msearch
app.use(bodyParser.json({ limit: "500mb", extended: true }));

/** For static content */
app.use(express.static("public"));
app.use("/static", express.static("public"));

/**mongoose */
mongoose.Promise = global.Promise;

// Connecting to the database
const uri = global.gServerConfig.database.uri;

if (process.env.NODE_ENV === "TEST") {
  if (global.gServerConfig.database.isMongoUnitDisabled) {
    global.gLogger.debug("Mongounit disabled. Using MonDB instance: ", { uri });
  } else {
    global.gLogger.debug("Mongounit enabled: ", { uri });
  }
}

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);

mongoose.connection.on("connecting", () => {
  global.gLogger.debug("Connecting to MongoDB...", { uri });
});
mongoose.connection.on("reconnected", () => {
  global.gLogger.debug(" Reconnected to MongoDB");
});

mongoose.connection.on("disconnected", () => {
  global.gLogger.debug("Disconnected from MongoDB", { uri });
  connectDB();
});

function connectDB() {
  mongoose
    .connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
      global.gLogger.debug("Successfully connected to the database", { uri });
    })
    .catch((err) => {
      global.gLogger.error("Could not connect to the database. Exiting now...", { error: err, uri });
      process.exit(1);
    });
}
connectDB();

// use it before all route definitions
app.use(cors({ origin: "*" }));

/** Routes */
app.get("/", (req, res) => {
  res.json({ message: "Welcome to ICAT+. Take notes quickly. Organize and keep track of all your events and resources." });
});

global.gLogger.debug("Loading routes...");

const routes = [
  "h5grove",
  "dataacquisition",
  "actions",
  "resource",
  "session",
  "cache",
  "logbook",
  "catalogue",
  "elasticsearch",
  "oaipmh",
  "panosc",
  "doi",
  "shipment",
  "parcel",
  "address",
  "item",
  "dmp",
];

routes.forEach((route) => {
  const routePath = `./app/routes/${route}.routes.js`;
  try {
    require(routePath)(app);
    global.gLogger.debug(`${route} is loaded`);
  } catch (e) {
    global.gLogger.error(`Error loading ${route}`);
    global.gLogger.error(e);
  }
});

// listen for requests
const server = app.listen(global.gServerConfig.server.port, () => {
  global.gLogger.info(`ICAT+ is listening on port ${global.gServerConfig.server.port}`);
});

server.onCacheInitialized = function () {};

global.gLogger.debug("Init cache...");
/** Init cache */
cache
  .init()
  .then(() => {
    server.onCacheInitialized();
  })
  .catch((error) => {
    global.gLogger.error("[cache] Cache could not be initialized", { error: error.message });
    process.exit();
  });

const swaggerJSDoc = require("swagger-jsdoc");

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "ICAT+ API",

    description: "ICAT+ RESTful API with Swagger",
    version: "1.0.0",
    contact: {
      name: " Alejandro De Maria Antolinos",
      email: "demariaa@esrf.fr",
    },
    license: {
      name: "MIT",
      url: "https://opensource.org/licenses/MIT",
    },
  },
  servers: [
    {
      url: global.gServerConfig.server.url,
    },
  ],
  tags: [
    {
      name: "Session",
      description: "all API methods related to session",
    },
  ],
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["./documentation/swagger/**/*.js", "./**/routes/*.js"], // pass all in array
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);

app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

/** Swagger */
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec, { exporer: true }));

module.exports = server;
